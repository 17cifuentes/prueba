-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-01-2019 a las 16:50:20
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sft_core`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_cliente`
--

CREATE TABLE `tn_sft_cliente` (
  `id` int(11) NOT NULL,
  `cliente_nombre` varchar(250) NOT NULL,
  `cliente_descripcion` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `correo` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_cliente`
--

INSERT INTO `tn_sft_cliente` (`id`, `cliente_nombre`, `cliente_descripcion`, `direccion`, `telefono`, `correo`, `created_at`, `updated_at`) VALUES
(1, 'Softheory', 'Main', '', '', '', '2018-11-07 17:02:45', '0000-00-00 00:00:00'),
(7, 'Prueba Core', 'Prueba Core', '####', '10000', 'prueba@softheory.com', '2019-01-09 15:40:50', '2019-01-09 15:40:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_conf`
--

CREATE TABLE `tn_sft_conf` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `navbar_title` varchar(250) NOT NULL,
  `ico` varchar(250) DEFAULT NULL,
  `search` int(2) NOT NULL,
  `navbar_notification` int(2) NOT NULL,
  `slide_rigth` int(2) NOT NULL,
  `navbar_color` varchar(250) DEFAULT NULL,
  `bradCumb_color` varchar(250) DEFAULT NULL,
  `img_slide` varchar(250) NOT NULL,
  `img_login` varchar(200) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_conf`
--

INSERT INTO `tn_sft_conf` (`id`, `title`, `navbar_title`, `ico`, `search`, `navbar_notification`, `slide_rigth`, `navbar_color`, `bradCumb_color`, `img_slide`, `img_login`, `cliente_id`, `created_at`, `update_at`) VALUES
(1, 'Softheory - Plataforma', 'Softheory - Plataforma', 'conf/images/slide/ composicion_tipografica_inquietante_2.jpg', 1, 1, 1, '#0000ff', '#004080', 'conf/images/slide/composicion_tipografica_inquietante_2.jpg', 'images/logos/login.png', 1, '2019-01-09 00:04:36', '0000-00-00 00:00:00'),
(5, 'Prueba - Plataforma', 'Prueba- Plataforma', 'conf/images/slide/ composicion_tipografica_inquietante_2.jpg', 0, 0, 0, '#0000ff', '#004080', 'conf/images/slide/composicion_tipografica_inquietante_2.jpg', 'images/logos/login.png', 7, '2019-01-09 00:04:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_detalle_modulo_rol`
--

CREATE TABLE `tn_sft_detalle_modulo_rol` (
  `id` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_detalle_modulo_rol`
--

INSERT INTO `tn_sft_detalle_modulo_rol` (`id`, `id_modulo`, `id_rol`, `created_at`, `updated_at`) VALUES
(4, 3, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(6, 12, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(7, 1, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(9, 2, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(12, 4, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(13, 5, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(16, 5, 7, '2019-01-09 15:46:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_detalle_rol_funciones`
--

CREATE TABLE `tn_sft_detalle_rol_funciones` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `funcion_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_detalle_rol_funciones`
--

INSERT INTO `tn_sft_detalle_rol_funciones` (`id`, `rol_id`, `funcion_id`, `created_at`, `updated_at`) VALUES
(6, 1, 22, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(7, 1, 3, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(8, 1, 6, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(9, 1, 9, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(13, 1, 5, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(14, 1, 7, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(18, 1, 1, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(19, 1, 4, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(20, 1, 2, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(21, 1, 8, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(22, 1, 11, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(25, 7, 9, '2019-01-09 15:47:29', '0000-00-00 00:00:00'),
(26, 7, 11, '2019-01-09 15:47:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_det_mod_clientes`
--

CREATE TABLE `tn_sft_det_mod_clientes` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_det_mod_clientes`
--

INSERT INTO `tn_sft_det_mod_clientes` (`id`, `id_cliente`, `id_modulo`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-11-07 05:00:00', '2018-11-07 17:35:16'),
(2, 1, 1, '2018-11-07 05:00:00', '2018-11-07 17:35:16'),
(3, 1, 2, '2018-11-07 05:00:00', '2018-11-07 17:35:32'),
(5, 1, 4, '2018-11-07 17:47:39', '2018-11-07 17:47:39'),
(15, 1, 12, '2019-01-07 20:56:32', '2019-01-07 20:56:32'),
(16, 1, 5, '2019-01-07 21:07:05', '2019-01-07 21:07:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_funciones`
--

CREATE TABLE `tn_sft_funciones` (
  `id` int(11) NOT NULL,
  `fun_nombre` varchar(250) NOT NULL,
  `fun_menu_nombre` varchar(250) NOT NULL,
  `fun_constante` varchar(250) DEFAULT NULL,
  `fun_descripcion` varchar(250) DEFAULT NULL,
  `fun_icono` varchar(250) DEFAULT NULL,
  `fun_ruta` varchar(250) NOT NULL,
  `fun_color` varchar(250) DEFAULT NULL,
  `mod_id` int(11) NOT NULL,
  `fun_sft` varchar(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_funciones`
--

INSERT INTO `tn_sft_funciones` (`id`, `fun_nombre`, `fun_menu_nombre`, `fun_constante`, `fun_descripcion`, `fun_icono`, `fun_ruta`, `fun_color`, `mod_id`, `fun_sft`, `created_at`, `create_at`) VALUES
(1, 'lstModulos', 'Listar módulos', 'SFT_FUN_MOD_LISTAR', 'Funcion para lista rlos modulos del sistema', 'list', 'Modulos/lstModulos', 'indigo', 1, 'S', '2018-11-15 23:40:26', '0000-00-00 00:00:00'),
(2, 'lstModulos', 'Crear módulo', 'SFT_FUN_MOD_CREAR', 'Funcion para crear modulos del sistema', 'dashboard', 'Modulos/createModulo', 'indigo', 1, 'S', '2018-11-22 02:47:20', '0000-00-00 00:00:00'),
(3, 'createPermiso', 'Crear Permiso', 'SFT_FUN_PERMISO_CREAR', 'Función para crear permisos en el sistema', 'done_all', 'Permisos/creatrPermisos', 'indigo', 2, 'S', '2018-11-15 23:40:30', '0000-00-00 00:00:00'),
(4, 'lstPermisos', 'Listar Permisos', 'SFT_FUN_PERMISO_LISTAR', 'Funcion para lista los permisos del sistema', 'list', 'Permisos/lstPermisos', 'indigo', 2, 'S', '2018-11-15 23:40:32', '0000-00-00 00:00:00'),
(5, 'lstClientes', 'Crear Cliente', 'SFT_FUN_CLIENTE_LST', 'Funcion para listar clientes del sistema', 'thumb_up', 'Clientes/create', 'indigo', 3, 'S', '2018-11-22 02:17:27', '0000-00-00 00:00:00'),
(6, 'createRol', 'Crear Rol', 'SFT_FUN_ROL_CREAR', 'Función para crear roles en el sistema', 'done_all', 'Roles/crearRol', 'indigo', 4, 'S', '2018-11-15 23:40:36', '0000-00-00 00:00:00'),
(7, 'lstClientes', 'Listar Clientes', 'SFT_FUN_CLIENTES_LISTAR', 'Funcion para lista los clientes del sistema', 'list', 'Clientes/lstClientes', 'indigo', 3, 'S', '2018-11-15 23:40:37', '0000-00-00 00:00:00'),
(8, 'lstRoles', 'Listar roles', 'SFT_FUN_ROLES_LST', 'Funcion para listar roles del sistema', 'list', 'Roles/lstClientes', 'indigo', 4, 'S', '2018-11-15 23:40:39', '0000-00-00 00:00:00'),
(9, 'createUsuario', 'Crear usuario', 'SFT_FUN_USUARIO_CREAR', 'Función para crear usuarios en el sistema', 'done_all', '/Usuarios/crearUsuario', 'indigo', 5, 'S', '2019-01-07 23:15:30', '0000-00-00 00:00:00'),
(11, 'lstUsuarios', 'Listar Usuarios', 'SFT_FUN_USUARIOS_LST', 'Funcion para listar usuario del sistema', 'list', '/Usuarios/lstUsuarios', 'indigo', 5, 'S', '2019-01-07 22:41:06', '0000-00-00 00:00:00'),
(22, 'ClientesConfiguracion', 'Configurar cliente ', 'SFT_FUN_MOD_LISTAR', 'Funcion para configurar los clientes', 'settings_input_component', '/Clientes/configuracion', 'indigo', 3, 'S', '2019-01-07 23:51:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_historico`
--

CREATE TABLE `tn_sft_historico` (
  `id` int(11) NOT NULL,
  `histo_entidad` int(11) NOT NULL,
  `histo_id` int(11) NOT NULL,
  `histo_observacion` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_modulos`
--

CREATE TABLE `tn_sft_modulos` (
  `id` int(11) NOT NULL,
  `mod_nombre` varchar(250) NOT NULL,
  `mod_constante` varchar(250) DEFAULT NULL,
  `mod_descripcion` varchar(250) DEFAULT NULL,
  `mod_icono` varchar(250) DEFAULT NULL,
  `mod_sft` varchar(2) DEFAULT NULL,
  `creeated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_modulos`
--

INSERT INTO `tn_sft_modulos` (`id`, `mod_nombre`, `mod_constante`, `mod_descripcion`, `mod_icono`, `mod_sft`, `creeated_at`, `updated_at`) VALUES
(1, 'Modulos', 'SFT_MOD_MODULOS', 'Modulo que controla los módulos del sistema', 'widgets', 'S', '2018-11-16 01:24:04', '0000-00-00 00:00:00'),
(2, 'Permisos', 'SFT_MOD_PERMISOS', 'Modulo que controla los permisos de los roles del sistema', 'spellcheck', 'S', '2018-11-16 01:24:07', '0000-00-00 00:00:00'),
(3, 'Clientes', 'SFT_MOD_CLIENTE', 'Modulo para gestionar configuraciones de lsoclientes', 'person_pin', 'S', '2018-11-16 01:24:09', '0000-00-00 00:00:00'),
(4, 'Roles', 'SFT_MOD_ROLES', 'Modulo para gestioanr roles del sistema', 'device_hub', 'S', '2018-11-16 01:24:11', '0000-00-00 00:00:00'),
(5, 'Usuarios', 'SFT_MOD_USUARIOS', 'Modulo para gestioanr usaurios del sistema', 'person', 'S', '2018-11-16 01:24:12', '0000-00-00 00:00:00'),
(12, 'Funcionalidades', 'MOD_FUNCIONALIDADES', 'Modulo para gestioanr funcionalidades de un modulos del sistema', 'touch_app', 'S', '2019-01-07 20:55:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_persona`
--

CREATE TABLE `tn_sft_persona` (
  `id` int(11) NOT NULL,
  `persona_nombre` varchar(200) NOT NULL,
  `persona_fecha_naci` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_roles`
--

CREATE TABLE `tn_sft_roles` (
  `id` int(11) NOT NULL,
  `rol_nombre` varchar(250) NOT NULL,
  `rol_descripcion` varchar(250) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_roles`
--

INSERT INTO `tn_sft_roles` (`id`, `rol_nombre`, `rol_descripcion`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrador', 'Rol que gestiona todo el sistema', 1, '2019-01-07 20:59:55', '0000-00-00 00:00:00'),
(7, 'Administrador', 'Rol que administra el sistema', 7, '2019-01-09 15:42:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `rol_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `img`, `password`, `remember_token`, `cliente_id`, `rol_id`, `created_at`, `updated_at`) VALUES
(1, 'Softheory', 'info@softheory.com', 'images/users/descarga.jpg', '$2y$10$1mGPzKh3oJXJJrK0OWHDiOmWGI9dU0OLr4dcMkcej8.Vj6Vs9powy', 'jwBDAncOy2ZBQlwCGqvJHVhf73yLTjLvhIslBa84tQhTfS0MFATZN8vKy3We', 1, 1, '2018-11-07 21:28:07', '2018-11-07 21:28:07'),
(9, 'Prueba', 'prueba@softheory.com', 'images/users/user.jpg', '$2y$10$E0wkz6WXlXLjTaFOFITyIOHQaCeG.QRUzWi1C8w9k4xR/h1cUzhyu', '5nIgBCNQpZiGyvPVBJbBBlFQPJ4I1slq1xsq9uNXB6VSbwiirDwUAxTqHgJG', 7, 7, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_cliente`
--
ALTER TABLE `tn_sft_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_conf`
--
ALTER TABLE `tn_sft_conf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_detalle_modulo_rol`
--
ALTER TABLE `tn_sft_detalle_modulo_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_modulo` (`id_modulo`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `tn_sft_detalle_rol_funciones`
--
ALTER TABLE `tn_sft_detalle_rol_funciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `funcion_id` (`funcion_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- Indices de la tabla `tn_sft_det_mod_clientes`
--
ALTER TABLE `tn_sft_det_mod_clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_modulo` (`id_modulo`);

--
-- Indices de la tabla `tn_sft_funciones`
--
ALTER TABLE `tn_sft_funciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mod_id` (`mod_id`);

--
-- Indices de la tabla `tn_sft_historico`
--
ALTER TABLE `tn_sft_historico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_modulos`
--
ALTER TABLE `tn_sft_modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_persona`
--
ALTER TABLE `tn_sft_persona`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_roles`
--
ALTER TABLE `tn_sft_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tn_sft_cliente`
--
ALTER TABLE `tn_sft_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tn_sft_conf`
--
ALTER TABLE `tn_sft_conf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tn_sft_detalle_modulo_rol`
--
ALTER TABLE `tn_sft_detalle_modulo_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `tn_sft_detalle_rol_funciones`
--
ALTER TABLE `tn_sft_detalle_rol_funciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `tn_sft_det_mod_clientes`
--
ALTER TABLE `tn_sft_det_mod_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `tn_sft_funciones`
--
ALTER TABLE `tn_sft_funciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `tn_sft_historico`
--
ALTER TABLE `tn_sft_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tn_sft_modulos`
--
ALTER TABLE `tn_sft_modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `tn_sft_persona`
--
ALTER TABLE `tn_sft_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tn_sft_roles`
--
ALTER TABLE `tn_sft_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tn_sft_conf`
--
ALTER TABLE `tn_sft_conf`
  ADD CONSTRAINT `tn_sft_conf_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_detalle_modulo_rol`
--
ALTER TABLE `tn_sft_detalle_modulo_rol`
  ADD CONSTRAINT `tn_sft_detalle_modulo_rol_ibfk_1` FOREIGN KEY (`id_modulo`) REFERENCES `tn_sft_modulos` (`id`),
  ADD CONSTRAINT `tn_sft_detalle_modulo_rol_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `tn_sft_roles` (`id`);

--
-- Filtros para la tabla `tn_sft_detalle_rol_funciones`
--
ALTER TABLE `tn_sft_detalle_rol_funciones`
  ADD CONSTRAINT `tn_sft_detalle_rol_funciones_ibfk_1` FOREIGN KEY (`funcion_id`) REFERENCES `tn_sft_funciones` (`id`),
  ADD CONSTRAINT `tn_sft_detalle_rol_funciones_ibfk_2` FOREIGN KEY (`rol_id`) REFERENCES `tn_sft_roles` (`id`);

--
-- Filtros para la tabla `tn_sft_det_mod_clientes`
--
ALTER TABLE `tn_sft_det_mod_clientes`
  ADD CONSTRAINT `tn_sft_det_mod_clientes_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tn_sft_cliente` (`id`),
  ADD CONSTRAINT `tn_sft_det_mod_clientes_ibfk_2` FOREIGN KEY (`id_modulo`) REFERENCES `tn_sft_modulos` (`id`);

--
-- Filtros para la tabla `tn_sft_funciones`
--
ALTER TABLE `tn_sft_funciones`
  ADD CONSTRAINT `tn_sft_funciones_ibfk_1` FOREIGN KEY (`mod_id`) REFERENCES `tn_sft_modulos` (`id`);

--
-- Filtros para la tabla `tn_sft_roles`
--
ALTER TABLE `tn_sft_roles`
  ADD CONSTRAINT `tn_sft_roles_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`rol_id`) REFERENCES `tn_sft_roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
