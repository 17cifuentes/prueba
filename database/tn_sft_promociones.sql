CREATE TABLE `tn_sft_promociones` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `mensaje` varchar(200) NOT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_promociones`
--

INSERT INTO `tn_sft_promociones` (`id`, `id_producto`, `mensaje`, `inicio`, `fin`, `created_at`, `updated_at`) VALUES
(6, 17, '1', '2019-03-15', '2019-03-15', '2019-03-15 14:22:50', '2019-03-15 14:22:50'),
(7, 17, 'jass', '2019-03-15', '2019-03-15', '2019-03-15 14:25:40', '2019-03-15 14:25:40');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tn_sft_promociones`
--
ALTER TABLE `tn_sft_promociones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tn_sft_promociones_ibfk_1` (`id_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tn_sft_promociones`
--
ALTER TABLE `tn_sft_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tn_sft_promociones`
--
ALTER TABLE `tn_sft_promociones`
  ADD CONSTRAINT `tn_sft_promociones_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tn_sft_productos_servicios` (`id`);