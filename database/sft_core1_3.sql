-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-03-2019 a las 20:14:39
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sft_core1.3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sft_mod_shopcarrt_menu`
--

CREATE TABLE `sft_mod_shopcarrt_menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sft_mod_shopcarrt_menu`
--

INSERT INTO `sft_mod_shopcarrt_menu` (`id`, `nombre`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Ropa', 1, '2019-01-26 05:03:45', '0000-00-00 00:00:00'),
(2, 'Calzado', 1, '2019-01-26 05:03:49', '0000-00-00 00:00:00'),
(3, 'Accesorios', 1, '2019-01-26 05:26:12', '0000-00-00 00:00:00'),
(4, 'Contacto', 1, '2019-01-26 05:26:23', '0000-00-00 00:00:00'),
(5, 'Ayuda', 1, '2019-01-26 05:27:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sft_mod_shopcarrt_sub_menu`
--

CREATE TABLE `sft_mod_shopcarrt_sub_menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sft_mod_shopcarrt_sub_menu`
--

INSERT INTO `sft_mod_shopcarrt_sub_menu` (`id`, `nombre`, `menu_id`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Camisas', 1, 1, '2019-01-26 05:15:12', '0000-00-00 00:00:00'),
(2, 'Pantalones', 1, 1, '2019-01-26 05:15:18', '0000-00-00 00:00:00'),
(3, 'Zapatillas', 2, 1, '2019-01-26 05:15:15', '0000-00-00 00:00:00'),
(4, 'Manillas', 3, 1, '2019-01-26 05:27:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_cambiar_estado`
--

CREATE TABLE `tn_sft_cambiar_estado` (
  `id` int(11) NOT NULL,
  `columna_id` int(11) NOT NULL,
  `namespace` varchar(200) NOT NULL,
  `entidad` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_cambiar_estado`
--

INSERT INTO `tn_sft_cambiar_estado` (`id`, `columna_id`, `namespace`, `entidad`, `created_at`, `updated_at`) VALUES
(1, 6, 'App\\Cliente', '', '2019-03-09 22:43:50', '2019-03-09 22:43:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_campos`
--

CREATE TABLE `tn_sft_campos` (
  `id` int(11) NOT NULL,
  `campo_nombre` varchar(200) NOT NULL,
  `campo_tipo` varchar(200) NOT NULL,
  `funcion_validacion` varchar(200) NOT NULL,
  `mensaje_validacion` varchar(200) NOT NULL,
  `data` varchar(2000) DEFAULT NULL,
  `dataUrl` varchar(300) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_campos`
--

INSERT INTO `tn_sft_campos` (`id`, `campo_nombre`, `campo_tipo`, `funcion_validacion`, `mensaje_validacion`, `data`, `dataUrl`, `created_at`, `updated_at`) VALUES
(1, 'Nombre', 'text', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Este campo es requerido', '', '', '2019-02-27 20:03:02', '2019-02-27 20:03:02'),
(2, 'Descripción', 'text', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Campo requerido, Valor minio 5', '', '', '2019-03-08 00:39:40', '2019-03-08 00:39:40'),
(3, 'Dirección', 'text', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Este campo es requerido', '', '', '2019-02-27 20:03:02', '2019-02-27 20:03:02'),
(4, 'Telefono', 'number', '[{\"required\":\"true\",\"min\":\"7\"}]', 'Campo requerido, Valor minio 7', '', '', '2019-03-08 00:39:40', '2019-03-08 00:39:40'),
(5, 'Correo', 'text', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Este campo es requerido', '', '', '2019-02-27 20:03:02', '2019-02-27 20:03:02'),
(6, 'Estado', 'select', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Campo requerido, Valor minio 5', '[{\"opc1\":[\"A\",\"Activo\"],\"opc2\":[\"I\",\"Inactivo\"],\"opc3\":[\"P\",\"Pendiente\"]}]', '', '2019-03-08 00:39:40', '2019-03-08 00:39:40'),
(7, 'Cedula', 'text', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Este campo es requerido', '', '', '2019-02-27 20:03:02', '2019-02-27 20:03:02'),
(8, 'Nombre', 'text', '[{\"required\":\"true\",\"min\":\"5\"}]', 'Campo requerido, Valor minio 5', '', '', '2019-03-08 00:39:40', '2019-03-08 00:39:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_caracteristicas_productos`
--

CREATE TABLE `tn_sft_caracteristicas_productos` (
  `id` int(11) NOT NULL,
  `id_pro` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `valor` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_caracteristicas_productos`
--

INSERT INTO `tn_sft_caracteristicas_productos` (`id`, `id_pro`, `nombre`, `valor`, `created_at`, `update_at`) VALUES
(1, 1, 'Soporte', 'Call center', '2018-11-29 08:24:54', '2018-11-29 08:24:54'),
(2, 1, 'Soporte', 'Call center2', '2018-11-29 08:24:54', '2018-11-29 08:24:54'),
(5, 1, '', '', '2018-11-29 08:26:31', '2018-11-29 08:26:31'),
(6, 1, '', '', '2018-11-29 08:26:42', '2018-11-29 08:26:42'),
(7, 1, '', '', '2018-11-29 08:29:20', '2018-11-29 08:29:20'),
(8, 1, '', '', '2018-11-29 09:04:52', '2018-11-29 09:04:52'),
(9, 1, '', '', '2018-11-30 02:46:28', '2018-11-30 02:46:28'),
(10, 1, '12', '12', '2019-01-23 21:13:16', '2019-01-23 21:13:16'),
(11, 1, 'Frasco', '500ml', '2019-01-26 18:35:49', '2019-01-26 18:35:49'),
(12, 1, '12', '12', '2019-01-28 19:48:04', '2019-01-28 19:48:04'),
(13, 1, '', '', '2019-02-13 07:12:42', '2019-02-13 07:12:42'),
(14, 1, '', '', '2019-02-14 01:51:40', '2019-02-14 01:51:40'),
(15, 1, '', '', '2019-02-14 02:09:50', '2019-02-14 02:09:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_categoria_productos`
--

CREATE TABLE `tn_sft_categoria_productos` (
  `id` int(11) NOT NULL,
  `cat_nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `cat_tipo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `cat_descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tn_sft_categoria_productos`
--

INSERT INTO `tn_sft_categoria_productos` (`id`, `cat_nombre`, `cat_tipo`, `cat_descripcion`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Soporte Y Analisis', 'Servicio', 'Dar soporte o analisis a informacion', 1, '2019-01-26 04:29:21', '2017-12-25 05:00:00'),
(2, 'Servicio Web', 'Servicio', 'Prestación de servicios informáticos', 1, '2019-01-26 04:29:24', '2018-01-18 05:00:00'),
(3, 'Diseño Creativo Gráfico', 'Servicio', 'Servicio de diseño gráfico.', 1, '2019-01-26 04:29:25', '2018-01-20 05:00:00'),
(4, 'Publicidad', 'Producto', 'Imprecòn de material publicitario', 1, '2019-01-26 04:29:28', '2018-01-20 05:00:00'),
(5, 'Mercadeo', 'Servicio', 'Análisis de mercados', 1, '2019-01-26 04:29:30', '2018-01-23 05:00:00'),
(6, 'Servicios Administractivos', 'Administractivo', 'servicio', 1, '2019-01-26 04:29:31', '2018-09-05 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_cliente`
--

CREATE TABLE `tn_sft_cliente` (
  `id` int(11) NOT NULL,
  `cliente_nombre` varchar(250) NOT NULL,
  `cliente_descripcion` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `correo` varchar(250) NOT NULL,
  `estado_cliente` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_cliente`
--

INSERT INTO `tn_sft_cliente` (`id`, `cliente_nombre`, `cliente_descripcion`, `direccion`, `telefono`, `correo`, `estado_cliente`, `created_at`, `updated_at`) VALUES
(1, 'Softheory', 'Main', '', '', '', 'Activo', '2019-03-10 01:09:34', '2019-03-10 06:09:34'),
(7, 'Prueba Core', 'Prueba Core', '####', '10000', 'prueba@softheory.com', 'Inactivo', '2019-03-10 01:07:15', '2019-03-10 06:07:15'),
(8, 'Impro Nova', 'Impronova', '####', '10000', 'prueba@mipro.com', '', '2019-02-13 20:55:16', '2019-01-09 15:40:50'),
(9, 'Impro Nova', 'Impronova', '####', '10000', 'prueba@mipro.com', 'Activo', '2019-03-10 01:01:08', '2019-03-10 06:01:08'),
(10, '12', '12', '12', '12', '12', '12', '2019-03-08 01:37:16', '2019-03-08 01:37:16'),
(11, '12', '12', '12', '12', '12', '12', '2019-03-08 01:40:46', '2019-03-08 01:40:46'),
(12, '12', '12', '12', '12', '12', '12', '2019-03-08 01:41:08', '2019-03-08 01:41:08'),
(13, '12', '12', '12', '12', '12', '12', '2019-03-08 01:41:51', '2019-03-08 01:41:51'),
(14, '78', '12', '12', '12', '12', '12', '2019-03-08 01:58:05', '2019-03-08 01:58:05'),
(15, 'Prueba compenente', '12', '12', '231', '213', 'A', '2019-03-08 12:50:56', '2019-03-08 12:50:56'),
(16, 'Softheory', 'Main', 'hh', '7', 'hjh', 'A', '2019-03-10 00:46:05', '2019-03-10 00:46:05'),
(17, '12rfdrtf', '12', '12', '12', '12', 'A', '2019-03-11 19:09:21', '2019-03-11 19:09:21'),
(18, 'PruebaBD', 'FD', '12', '12', '12', 'A', '2019-03-11 19:09:52', '2019-03-11 19:09:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_columnas_tabla`
--

CREATE TABLE `tn_sft_columnas_tabla` (
  `id` int(11) NOT NULL,
  `tabla_id` int(11) NOT NULL,
  `columnas_tabla` varchar(200) NOT NULL,
  `columna_usuario` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_columnas_tabla`
--

INSERT INTO `tn_sft_columnas_tabla` (`id`, `tabla_id`, `columnas_tabla`, `columna_usuario`, `created_at`, `updated_at`) VALUES
(1, 1, 'cliente_nombre', 'Nombre cliente', '2019-03-09 21:22:44', '2019-03-09 21:22:44'),
(2, 1, 'cliente_descripcion', 'Cliente descripción', '2019-03-09 21:22:44', '2019-03-09 21:22:44'),
(3, 1, 'direccion', 'Dirección', '2019-03-09 21:22:44', '2019-03-09 21:22:44'),
(4, 1, 'telefono', 'Teléfono', '2019-03-09 21:22:44', '2019-03-09 21:22:44'),
(5, 1, 'correo', 'Correo', '2019-03-09 21:22:44', '2019-03-09 21:22:44'),
(6, 1, 'estado_cliente', 'Estado cliente', '2019-03-09 21:22:44', '2019-03-09 21:22:44'),
(7, 1, 'id', 'id', '2019-03-09 22:04:36', '2019-03-09 22:04:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_column_lista`
--

CREATE TABLE `tn_sft_column_lista` (
  `id` int(11) NOT NULL,
  `id_lista` int(11) NOT NULL,
  `id_column` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_column_lista`
--

INSERT INTO `tn_sft_column_lista` (`id`, `id_lista`, `id_column`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-03-09 21:23:45', '2019-03-09 21:23:45'),
(2, 1, 2, '2019-03-09 21:23:45', '2019-03-09 21:23:45'),
(3, 1, 5, '2019-03-09 21:23:45', '2019-03-09 21:23:45'),
(4, 1, 3, '2019-03-09 21:23:45', '2019-03-09 21:23:45'),
(5, 1, 4, '2019-03-09 21:23:45', '2019-03-09 21:23:45'),
(6, 1, 6, '2019-03-09 21:23:45', '2019-03-09 21:23:45'),
(7, 1, 7, '2019-03-09 22:04:48', '2019-03-09 22:04:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_column_opc_listas`
--

CREATE TABLE `tn_sft_column_opc_listas` (
  `id` int(11) NOT NULL,
  `id_lista` int(11) NOT NULL,
  `accion` varchar(200) NOT NULL,
  `validacion` varchar(200) NOT NULL,
  `evento` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_column_opc_listas`
--

INSERT INTO `tn_sft_column_opc_listas` (`id`, `id_lista`, `accion`, `validacion`, `evento`, `created_at`, `updated_at`) VALUES
(1, 1, 'Editar', 'S', 'Form/editar/id/Cliente/1', '2019-03-09 21:24:50', '2019-03-09 21:24:50'),
(2, 1, 'Cambiar estado', 'N', 'Gen/cambiarEstado?id_estado=1', '2019-03-09 21:24:50', '2019-03-09 21:24:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_conf`
--

CREATE TABLE `tn_sft_conf` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `navbar_title` varchar(250) NOT NULL,
  `ico` varchar(250) DEFAULT NULL,
  `search` int(2) NOT NULL,
  `chat` int(11) DEFAULT NULL,
  `navbar_notification` int(2) NOT NULL,
  `slide_rigth` int(2) NOT NULL,
  `navbar_color` varchar(250) DEFAULT NULL,
  `bradCumb_color` varchar(250) DEFAULT NULL,
  `img_slide` varchar(250) NOT NULL,
  `img_login` varchar(200) NOT NULL,
  `img_logo` varchar(250) NOT NULL,
  `modal_shop` char(1) DEFAULT NULL,
  `estructura_usuario` int(11) NOT NULL,
  `estructura_admin` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_conf`
--

INSERT INTO `tn_sft_conf` (`id`, `title`, `navbar_title`, `ico`, `search`, `chat`, `navbar_notification`, `slide_rigth`, `navbar_color`, `bradCumb_color`, `img_slide`, `img_login`, `img_logo`, `modal_shop`, `estructura_usuario`, `estructura_admin`, `cliente_id`, `created_at`, `update_at`) VALUES
(1, 'Softheory', 'Softheory', 'core/img/conf/ico/imagen softheory.jpg', 0, 0, 0, 0, '#214e92', '#000000', 'core/img/conf/slide/descarga (1).jpg', 'core/img/conf/ico/descarga (1).jpg', 'images/logos/login.png', 'D', 1, 4, 1, '2019-03-10 00:36:05', '0000-00-00 00:00:00'),
(5, 'Prueba - Plataforma', 'Prueba- Plataforma', 'conf/images/slide/ composicion_tipografica_inquietante_2.jpg', 0, 0, 0, 0, '#0000ff', '#004080', 'conf/images/slide/composicion_tipografica_inquietante_2.jpg', 'images/logos/login.png', 'images/logos/login.png', NULL, 1, 4, 7, '2019-02-13 22:01:47', '0000-00-00 00:00:00'),
(6, 'GoodLicor', 'GoodLicor', 'conf/images/ico/Botella-nueva-sin-tapa-330-cerveza-colombiana.png', 0, 0, 0, 0, '#0000ff', '#c0c0c0', 'conf/images/slide/desengrasante-industrial.jpg', 'conf/images/ico/Botella-nueva-sin-tapa-330-cerveza-colombiana.png', 'conf/images/ico/7086741.png', NULL, 3, 2, 9, '2019-02-14 14:05:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_conf_pagos`
--

CREATE TABLE `tn_sft_conf_pagos` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `merchantId` varchar(200) NOT NULL,
  `accountId` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `referenceCode` varchar(200) NOT NULL,
  `tax` varchar(200) NOT NULL,
  `taxReturnBase` varchar(200) NOT NULL,
  `currency` varchar(200) NOT NULL,
  `buyerEmail` varchar(200) NOT NULL,
  `responseUrl` varchar(200) NOT NULL,
  `confirmationUrl` varchar(200) NOT NULL,
  `api_key` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_conf_pagos`
--

INSERT INTO `tn_sft_conf_pagos` (`id`, `cliente_id`, `merchantId`, `accountId`, `description`, `referenceCode`, `tax`, `taxReturnBase`, `currency`, `buyerEmail`, `responseUrl`, `confirmationUrl`, `api_key`, `created_at`, `updated_at`) VALUES
(1, 1, '782061', '788874', 'Tienda Softheory', '', '', '', 'COP', 'info@softheory.com', 'https://tou.com.co/domicilio/217/171', '', 'D000DlByK7yl01GLN6WW0FdoPa', '2019-02-18 18:36:43', '2019-02-21 02:07:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_detalle_form_campos`
--

CREATE TABLE `tn_sft_detalle_form_campos` (
  `id` int(11) NOT NULL,
  `id_form` int(11) NOT NULL,
  `id_campo` int(11) NOT NULL,
  `columna` varchar(200) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_detalle_form_campos`
--

INSERT INTO `tn_sft_detalle_form_campos` (`id`, `id_form`, `id_campo`, `columna`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'cliente_nombre', '2019-02-27 20:04:01', '2019-02-27 20:04:01'),
(2, 1, 2, 'cliente_descripcion', '2019-02-27 20:04:01', '2019-02-27 20:04:01'),
(3, 1, 3, 'direccion', '2019-02-27 20:04:01', '2019-02-27 20:04:01'),
(4, 1, 4, 'telefono', '2019-02-27 20:04:01', '2019-02-27 20:04:01'),
(5, 1, 5, 'correo', '2019-02-27 20:04:01', '2019-02-27 20:04:01'),
(6, 1, 6, 'estado_cliente', '2019-02-27 20:04:01', '2019-02-27 20:04:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_detalle_modulo_rol`
--

CREATE TABLE `tn_sft_detalle_modulo_rol` (
  `id` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_detalle_modulo_rol`
--

INSERT INTO `tn_sft_detalle_modulo_rol` (`id`, `id_modulo`, `id_rol`, `created_at`, `updated_at`) VALUES
(4, 3, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(6, 12, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(7, 1, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(9, 2, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(12, 4, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(13, 5, 1, '2019-01-08 00:06:08', '0000-00-00 00:00:00'),
(15, 1, 1, '2019-03-01 20:38:54', '2019-03-01 20:38:54'),
(16, 5, 7, '2019-01-09 15:46:48', '0000-00-00 00:00:00'),
(19, 5, 8, '2019-02-14 00:51:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_detalle_rol_funciones`
--

CREATE TABLE `tn_sft_detalle_rol_funciones` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `funcion_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_detalle_rol_funciones`
--

INSERT INTO `tn_sft_detalle_rol_funciones` (`id`, `rol_id`, `funcion_id`, `created_at`, `updated_at`) VALUES
(6, 1, 22, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(7, 1, 3, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(8, 1, 6, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(9, 1, 9, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(13, 1, 5, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(14, 1, 7, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(18, 1, 1, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(19, 1, 4, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(20, 1, 2, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(21, 1, 8, '2019-01-07 23:22:33', '0000-00-00 00:00:00'),
(25, 7, 9, '2019-01-09 15:47:29', '0000-00-00 00:00:00'),
(26, 7, 11, '2019-01-09 15:47:59', '0000-00-00 00:00:00'),
(27, 7, 22, '2019-01-26 20:50:28', '0000-00-00 00:00:00'),
(28, 8, 11, '2019-02-14 01:09:34', '0000-00-00 00:00:00'),
(29, 8, 1, '2019-02-14 14:07:32', '0000-00-00 00:00:00'),
(30, 8, 9, '2019-02-14 01:09:34', '0000-00-00 00:00:00'),
(31, 7, 11, '2019-02-20 20:55:39', '0000-00-00 00:00:00'),
(32, 1, 11, '2019-02-20 20:56:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_det_mod_clientes`
--

CREATE TABLE `tn_sft_det_mod_clientes` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_det_mod_clientes`
--

INSERT INTO `tn_sft_det_mod_clientes` (`id`, `id_cliente`, `id_modulo`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-11-07 05:00:00', '2018-11-07 17:35:16'),
(2, 1, 1, '2018-11-07 05:00:00', '2018-11-07 17:35:16'),
(3, 1, 2, '2018-11-07 05:00:00', '2018-11-07 17:35:32'),
(5, 1, 4, '2018-11-07 17:47:39', '2018-11-07 17:47:39'),
(15, 1, 12, '2019-01-07 20:56:32', '2019-01-07 20:56:32'),
(16, 1, 5, '2019-01-07 21:07:05', '2019-01-07 21:07:05'),
(19, 9, 5, '2019-02-14 00:52:43', '2019-02-14 00:52:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_estructuras`
--

CREATE TABLE `tn_sft_estructuras` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_estructuras`
--

INSERT INTO `tn_sft_estructuras` (`id`, `nombre`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'E&E', 'U', '2019-02-13 20:12:01', '0000-00-00 00:00:00'),
(2, 'FUSION', 'A', '2019-02-13 20:12:03', '0000-00-00 00:00:00'),
(3, 'JUNIOR', 'U', '2019-02-13 20:12:08', '0000-00-00 00:00:00'),
(4, 'ADMIN BSB', 'A', '2019-02-13 20:12:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_form`
--

CREATE TABLE `tn_sft_form` (
  `id` int(11) NOT NULL,
  `id_form` varchar(200) NOT NULL,
  `nombre_form` varchar(200) NOT NULL,
  `action` varchar(200) NOT NULL,
  `method` varchar(200) NOT NULL,
  `entidad` varchar(200) NOT NULL,
  `path_entidad` varchar(300) NOT NULL,
  `funcion_js` varchar(200) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `rastro_miga` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_form`
--

INSERT INTO `tn_sft_form` (`id`, `id_form`, `nombre_form`, `action`, `method`, `entidad`, `path_entidad`, `funcion_js`, `cliente_id`, `rastro_miga`, `created_at`, `updated_at`) VALUES
(1, 'formRegistroCliente', 'Registrar Cliente', '', 'GET', 'Cliente', '', 'user.js', 1, '[{\"Inicio\":\"/home\",\"Menú Clientes\":\"/Menu/3\",\"Registrar Cliente\":\"#\"}]', '2019-02-27 19:58:58', '2019-02-27 19:58:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_funciones`
--

CREATE TABLE `tn_sft_funciones` (
  `id` int(11) NOT NULL,
  `fun_nombre` varchar(250) NOT NULL,
  `fun_menu_nombre` varchar(250) NOT NULL,
  `fun_constante` varchar(250) DEFAULT NULL,
  `fun_descripcion` varchar(250) DEFAULT NULL,
  `fun_icono` varchar(250) DEFAULT NULL,
  `fun_ruta` varchar(250) NOT NULL,
  `fun_color` varchar(250) DEFAULT NULL,
  `mod_id` int(11) NOT NULL,
  `fun_sft` varchar(2) NOT NULL,
  `estado_funcion` varchar(20) NOT NULL DEFAULT 'Activo',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_funciones`
--

INSERT INTO `tn_sft_funciones` (`id`, `fun_nombre`, `fun_menu_nombre`, `fun_constante`, `fun_descripcion`, `fun_icono`, `fun_ruta`, `fun_color`, `mod_id`, `fun_sft`, `estado_funcion`, `created_at`, `updated_at`) VALUES
(1, 'lstModulos', 'Listar módulos', 'SFT_FUN_MOD_LISTAR', 'Funcion para lista rlos modulos del sistema', 'list', 'Modulos/lstModulos', 'indigo', 1, 'S', 'Inactivo', '2019-03-10 02:13:21', '2019-03-10 07:13:21'),
(2, 'lstModulos', 'Crear módulo', 'SFT_FUN_MOD_CREAR', 'Funcion para crear modulos del sistema', 'dashboard', 'Modulos/createModulo', 'indigo', 1, 'S', 'Activo', '2019-03-10 01:40:22', '2019-03-10 06:40:22'),
(3, 'createPermiso', 'Crear P. Modulo', 'SFT_FUN_PERMISO_CREAR', 'Función para crear permisos en el sistema', 'done_all', 'Permisos/createPermisosMod', 'indigo', 2, 'S', 'Activo', '2019-03-10 01:42:23', '2019-03-10 06:42:23'),
(4, 'moduloPermisos', 'Gestionar P. Modulos', 'SFT_FUN_PERMISO_LISTAR', 'Funcion para lista los permisos del sistema', 'list', 'Permisos/lstPermisosModulos', 'indigo', 2, 'S', 'Activo', '2019-03-07 23:04:09', '0000-00-00 00:00:00'),
(5, 'lstClientes', 'Crear Cliente', 'SFT_FUN_CLIENTE_LST', 'Funcion para listar clientes del sistema', 'thumb_up', 'Form/1', 'indigo', 3, 'S', 'Activo', '2019-03-08 12:58:19', '0000-00-00 00:00:00'),
(6, 'createRol', 'Crear Rol', 'SFT_FUN_ROL_CREAR', 'Función para crear roles en el sistema', 'done_all', 'Roles/crearRol', 'indigo', 4, 'S', 'Activo', '2018-11-15 23:40:36', '0000-00-00 00:00:00'),
(7, 'lstClientes', 'Listar Clientes', 'SFT_FUN_CLIENTES_LISTAR', 'Funcion para lista los clientes del sistema', 'list', 'Table/1', 'indigo', 3, 'S', 'Activo', '2019-03-09 22:09:56', '0000-00-00 00:00:00'),
(8, 'lstRoles', 'Listar roles', 'SFT_FUN_ROLES_LST', 'Funcion para listar roles del sistema', 'list', 'Roles/lstRoles', 'indigo', 4, 'S', 'Activo', '2019-03-07 23:04:30', '0000-00-00 00:00:00'),
(9, 'createUsuario', 'Crear usuario', 'SFT_FUN_USUARIO_CREAR', 'Función para crear usuarios en el sistema', 'done_all', '/Usuarios/crearUsuario', 'indigo', 5, 'S', 'Activo', '2019-01-07 23:15:30', '0000-00-00 00:00:00'),
(11, 'lstUsuarios', 'Listar Usuarios', 'SFT_FUN_USUARIOS_LST', 'Funcion para listar usuario del sistema', 'list', '/Usuarios/lstUsuarios', 'indigo', 5, 'S', 'Activo', '2019-01-07 22:41:06', '0000-00-00 00:00:00'),
(22, 'ClientesConfiguracion', 'Configurar cliente ', 'SFT_FUN_MOD_LISTAR', 'Funcion para configurar los clientes', 'settings_input_component', '/Clientes/configuracion', 'indigo', 3, 'S', 'Activo', '2019-01-07 23:51:45', '0000-00-00 00:00:00'),
(28, 'lstModulos', 'coco', 'SFT_FUN_MOD_LISTAR', 'Funcion para lista rlos modulos del sistema', 'list', 'Modulos/lstModulos', 'indigo', 5, 'S', 'Activo', '2018-11-15 23:40:26', '0000-00-00 00:00:00'),
(29, 'lstFunciones', 'Listar funciones', 'SFT', 'Funcion para listar funciones del sistema', 'list', 'Funciones/listarFunciones', 'indigo', 12, 'S', 'Activo', '2019-02-26 03:23:22', '2019-02-26 08:23:22'),
(30, 'crearFuncione', 'Crear función', 'SFT', 'Funcion para crear funciones del sistema', 'play_circle_filled', 'Funciones/crearFunciones', 'indigo', 12, 'S', 'Activo', '2019-02-23 21:07:47', '0000-00-00 00:00:00'),
(33, 'FuncionesPermisos', 'Gestionar P. Funciones', 'SFT_FUN_PERMISO_LISTAR', 'Funcion para lista los permisos del sistema', 'list', 'Permisos/listPermisosFunciones', 'indigo', 2, 'S', 'Activo', '2019-03-02 18:22:57', '0000-00-00 00:00:00'),
(35, 'FuncionesPermisos', 'Crear P. Funciones', 'SFT_FUN_PERMISO_LISTAR', 'Funcion para lista los permisos del sistema', 'list', 'Permisos/createPermisosFun', 'indigo', 2, 'S', 'Activo', '2019-03-02 18:21:43', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_historico`
--

CREATE TABLE `tn_sft_historico` (
  `id` int(11) NOT NULL,
  `histo_entidad` int(11) NOT NULL,
  `histo_id` int(11) NOT NULL,
  `histo_observacion` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_listas`
--

CREATE TABLE `tn_sft_listas` (
  `id` int(11) NOT NULL,
  `lista_nombre` varchar(200) NOT NULL,
  `controllador` varchar(200) NOT NULL,
  `namespace` varchar(200) NOT NULL,
  `method` varchar(200) NOT NULL,
  `rastro_miga` varchar(200) NOT NULL,
  `opc_fun` varchar(200) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_listas`
--

INSERT INTO `tn_sft_listas` (`id`, `lista_nombre`, `controllador`, `namespace`, `method`, `rastro_miga`, `opc_fun`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Lista clientes', 'GstCliente', 'App\\Http\\Gst\\', 'getAllClientes', '[{\"Inicio\":\"/home\",\"Menú Clientes\":\"/Menu/3\",\"Lista clientes\":\"#\"}]', '{}', 1, '2019-03-09 21:19:33', '2019-03-09 21:19:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_modulos`
--

CREATE TABLE `tn_sft_modulos` (
  `id` int(11) NOT NULL,
  `mod_nombre` varchar(250) NOT NULL,
  `mod_constante` varchar(250) DEFAULT NULL,
  `mod_descripcion` varchar(250) DEFAULT NULL,
  `mod_icono` varchar(250) DEFAULT NULL,
  `mod_sft` varchar(2) DEFAULT NULL,
  `estado_modulo` varchar(20) NOT NULL,
  `creeated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_modulos`
--

INSERT INTO `tn_sft_modulos` (`id`, `mod_nombre`, `mod_constante`, `mod_descripcion`, `mod_icono`, `mod_sft`, `estado_modulo`, `creeated_at`, `updated_at`) VALUES
(1, 'Modulos', 'SFT_MOD_MODULOS', 'Modulo que controla los módulos del sistema', 'widgets', 'S', '', '2018-11-16 01:24:04', '0000-00-00 00:00:00'),
(2, 'Permisos', 'SFT_MOD_PERMISOS', 'Modulo que controla los permisos de los roles del sistema', 'spellcheck', 'S', '', '2018-11-16 01:24:07', '0000-00-00 00:00:00'),
(3, 'Clientes', 'SFT_MOD_CLIENTE', 'Modulo para gestionar configuraciones de lsoclientes', 'person_pin', 'S', '', '2018-11-16 01:24:09', '0000-00-00 00:00:00'),
(4, 'Roles', 'SFT_MOD_ROLES', 'Modulo para gestioanr roles del sistema', 'device_hub', 'S', '', '2018-11-16 01:24:11', '0000-00-00 00:00:00'),
(5, 'Usuarios', 'SFT_MOD_USUARIOS', 'Modulo para gestioanr usaurios del sistema', 'person', 'S', '', '2018-11-16 01:24:12', '0000-00-00 00:00:00'),
(12, 'Funcionalidades', 'MOD_FUNCIONALIDADES', 'Modulo para gestioanr funcionalidades de un modulos del sistema', 'touch_app', 'S', '', '2019-01-07 20:55:53', '0000-00-00 00:00:00'),
(13, 'Productos', 'SFT_MOD_PRODUCTOS', 'Modulo para gestioanr prodcutos del sistema', 'switch_camera', 'N', 'Activo', '2019-02-25 18:29:52', '0000-00-00 00:00:00'),
(14, 'Tienda', 'SFT_MOD_TIENDA', 'Modulo que gestiona todos los movimientos de la tienda del sistema', 'shopping_cart', 'N', 'Inactivo', '2019-03-05 18:58:06', '2019-03-06 02:58:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_persona`
--

CREATE TABLE `tn_sft_persona` (
  `id` int(11) NOT NULL,
  `persona_nombre` varchar(200) NOT NULL,
  `persona_fecha_naci` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_productos_servicios`
--

CREATE TABLE `tn_sft_productos_servicios` (
  `id` int(11) NOT NULL,
  `pro_nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `pro_descripcion` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `pro_precio_venta` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `pro_precio_interno` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `pro_cantidad` int(11) NOT NULL,
  `img_producto` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `usuario_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tn_sft_productos_servicios`
--

INSERT INTO `tn_sft_productos_servicios` (`id`, `pro_nombre`, `cat_id`, `pro_descripcion`, `pro_precio_venta`, `pro_precio_interno`, `pro_cantidad`, `img_producto`, `cliente_id`, `usuario_id`, `created_at`, `updated_at`) VALUES
(17, 'Aguardiente tapa roja', 1, 'Aguardiente tapa roja', '60000', '40000', 12, 'images/productos/Aguardiente_Tapa_Roja_Garrafa.png', 1, 1, '2019-01-26 18:27:08', '2019-01-23 21:13:16'),
(18, 'Ron viejo de caldas', 4, 'Ron 8 años para pasar una fiestas unicas y copartir con lso que màs quieres', '45000', '30000', 1, 'images/productos/ron-png-2.png', 1, 1, '2019-01-26 18:35:49', '2019-01-26 18:35:49'),
(19, 'prueba', 1, '12121212', '1212', '1212', 1, 'images/productos/Sin título.png', 1, 1, '2019-01-28 19:48:04', '2019-01-28 19:48:04'),
(20, 'Prueba2', 1, 'sssss', '140000', '120000', 1, 'images/productos/14562.png', 1, 1, '2019-02-13 07:12:42', '2019-02-13 07:12:42'),
(21, 'Jabón liquido', 2, 'Ron 8 años para pasar una fiestas unicas y copartir con lso que màs quieres', '30000', '120000', 1, 'images/productos/desengrasante-industrial.jpg', 9, 10, '2019-02-14 01:51:40', '2019-02-14 01:51:40'),
(22, 'Jarra de jaja', 1, 'Ron 8 años para pasar una fiestas unicas y copartir con lso que màs quieres', '5000', '1200', 1, 'images/productos/desengrasante-industrial.jpg', 9, 10, '2019-02-14 02:09:50', '2019-02-14 02:09:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_roles`
--

CREATE TABLE `tn_sft_roles` (
  `id` int(11) NOT NULL,
  `rol_nombre` varchar(250) NOT NULL,
  `rol_descripcion` varchar(250) NOT NULL,
  `estado_roles` varchar(20) NOT NULL DEFAULT 'Activo',
  `cliente_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_roles`
--

INSERT INTO `tn_sft_roles` (`id`, `rol_nombre`, `rol_descripcion`, `estado_roles`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrador', 'Rol que gestiona todo el sistema', 'Activo', 1, '2019-01-07 20:59:55', '0000-00-00 00:00:00'),
(7, 'Administrador', 'Rol que administra el sistema', 'Activo', 7, '2019-01-09 15:42:05', '0000-00-00 00:00:00'),
(8, 'Administrador', 'Rol que administra el sistema', 'Activo', 9, '2019-02-13 21:00:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_sistema`
--

CREATE TABLE `tn_sft_sistema` (
  `id` int(11) NOT NULL,
  `version` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_sistema`
--

INSERT INTO `tn_sft_sistema` (`id`, `version`, `created_at`, `updated_at`) VALUES
(1, '1.1.0', '2019-02-14 01:46:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_tablas`
--

CREATE TABLE `tn_sft_tablas` (
  `id` int(11) NOT NULL,
  `declaracion` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_tablas`
--

INSERT INTO `tn_sft_tablas` (`id`, `declaracion`, `created_at`, `updated_at`) VALUES
(1, 'tn_sft_listas', '2019-03-09 21:20:25', '2019-03-09 21:20:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tn_sft_values_form`
--

CREATE TABLE `tn_sft_values_form` (
  `id` int(11) NOT NULL,
  `id_form` int(11) NOT NULL,
  `id_campo` int(11) NOT NULL,
  `usuario_id` int(10) NOT NULL,
  `indice` varchar(2000) NOT NULL,
  `valor` varchar(2000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tn_sft_values_form`
--

INSERT INTO `tn_sft_values_form` (`id`, `id_form`, `id_campo`, `usuario_id`, `indice`, `valor`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'c', 'c', '2019-02-27 15:15:56', '2019-02-27 15:15:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `rol_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `img`, `password`, `remember_token`, `cliente_id`, `rol_id`, `created_at`, `updated_at`) VALUES
(1, 'Softheory', 'info@softheory.com', 'images/users/imagen softheory.jpg', '$2y$10$QfwbU9CCIn.SYmODWvWg...wb3u5lDMcvO1SatwrE775zjRvi/tGq', '8gqvNsfKcO7wgUwhMXgbPCk89AiXFGal5VYHbj03Yj3oJD3JWPgxzsn6uEMq', 1, 1, '2018-11-07 21:28:07', '2019-03-10 05:26:06'),
(9, 'Good Licor', 'prueba@softheory.com', 'images/users/logoSoftheory.jpg', '$2y$10$e8eB11IH93NwsKRfqTcRlOECsSaF.bE3FvYBxDBFtCQQCBjPUkoNe', '5nIgBCNQpZiGyvPVBJbBBlFQPJ4I1slq1xsq9uNXB6VSbwiirDwUAxTqHgJG', 7, 7, NULL, '2019-01-27 01:49:41'),
(10, 'ImproNova2', 'info@impro.com', 'images/users/logoSoftheory.jpg', '$2y$10$9b6j.3MS72GALQhOwzU9j.jHCaKUGJI57V0FIIzAKhOojZwfEAE4.', 'mr4TqS2WiqHI0lB1qP5WCpATH3udu8W5TAefpuSeRPgd7BLxpBDNiSLou5P5', 9, 8, NULL, '2019-02-14 06:59:24'),
(11, 'Amparo prueba', 'amparo@impro.com', 'images/users/user.jpg', '$2y$10$NiW81Xi//mJg1CsjoqQAI.XFh.2Va7jF2kXJcKZzz7TpTGeiBedtq', 'fsm8ZN3vHNerEzKHT4leTYWQmB8WIRnXEg5saYB0sKva257fUmA9Y3DOWm9Q', 9, 8, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sft_mod_shopcarrt_menu`
--
ALTER TABLE `sft_mod_shopcarrt_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `sft_mod_shopcarrt_sub_menu`
--
ALTER TABLE `sft_mod_shopcarrt_sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_cambiar_estado`
--
ALTER TABLE `tn_sft_cambiar_estado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `columna_id` (`columna_id`);

--
-- Indices de la tabla `tn_sft_campos`
--
ALTER TABLE `tn_sft_campos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_caracteristicas_productos`
--
ALTER TABLE `tn_sft_caracteristicas_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_categoria_productos`
--
ALTER TABLE `tn_sft_categoria_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_cliente`
--
ALTER TABLE `tn_sft_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_columnas_tabla`
--
ALTER TABLE `tn_sft_columnas_tabla`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tabla_id` (`tabla_id`);

--
-- Indices de la tabla `tn_sft_column_lista`
--
ALTER TABLE `tn_sft_column_lista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lista` (`id_lista`),
  ADD KEY `id_column` (`id_column`);

--
-- Indices de la tabla `tn_sft_column_opc_listas`
--
ALTER TABLE `tn_sft_column_opc_listas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lista` (`id_lista`);

--
-- Indices de la tabla `tn_sft_conf`
--
ALTER TABLE `tn_sft_conf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `estructura_admin` (`estructura_admin`),
  ADD KEY `estructura_usuario` (`estructura_usuario`);

--
-- Indices de la tabla `tn_sft_conf_pagos`
--
ALTER TABLE `tn_sft_conf_pagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_detalle_form_campos`
--
ALTER TABLE `tn_sft_detalle_form_campos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_campo` (`id_campo`),
  ADD KEY `id_form` (`id_form`);

--
-- Indices de la tabla `tn_sft_detalle_modulo_rol`
--
ALTER TABLE `tn_sft_detalle_modulo_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_modulo` (`id_modulo`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `tn_sft_detalle_rol_funciones`
--
ALTER TABLE `tn_sft_detalle_rol_funciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `funcion_id` (`funcion_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- Indices de la tabla `tn_sft_det_mod_clientes`
--
ALTER TABLE `tn_sft_det_mod_clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_modulo` (`id_modulo`);

--
-- Indices de la tabla `tn_sft_estructuras`
--
ALTER TABLE `tn_sft_estructuras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_form`
--
ALTER TABLE `tn_sft_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_funciones`
--
ALTER TABLE `tn_sft_funciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mod_id` (`mod_id`);

--
-- Indices de la tabla `tn_sft_historico`
--
ALTER TABLE `tn_sft_historico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_listas`
--
ALTER TABLE `tn_sft_listas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_modulos`
--
ALTER TABLE `tn_sft_modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_persona`
--
ALTER TABLE `tn_sft_persona`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_productos_servicios`
--
ALTER TABLE `tn_sft_productos_servicios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Indices de la tabla `tn_sft_roles`
--
ALTER TABLE `tn_sft_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tn_sft_sistema`
--
ALTER TABLE `tn_sft_sistema`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_tablas`
--
ALTER TABLE `tn_sft_tablas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tn_sft_values_form`
--
ALTER TABLE `tn_sft_values_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_campo` (`id_campo`),
  ADD KEY `id_form` (`id_form`),
  ADD KEY `indice` (`indice`(767)),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sft_mod_shopcarrt_menu`
--
ALTER TABLE `sft_mod_shopcarrt_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sft_mod_shopcarrt_sub_menu`
--
ALTER TABLE `sft_mod_shopcarrt_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tn_sft_cambiar_estado`
--
ALTER TABLE `tn_sft_cambiar_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tn_sft_campos`
--
ALTER TABLE `tn_sft_campos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tn_sft_caracteristicas_productos`
--
ALTER TABLE `tn_sft_caracteristicas_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tn_sft_categoria_productos`
--
ALTER TABLE `tn_sft_categoria_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tn_sft_cliente`
--
ALTER TABLE `tn_sft_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `tn_sft_columnas_tabla`
--
ALTER TABLE `tn_sft_columnas_tabla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tn_sft_column_lista`
--
ALTER TABLE `tn_sft_column_lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tn_sft_column_opc_listas`
--
ALTER TABLE `tn_sft_column_opc_listas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tn_sft_conf`
--
ALTER TABLE `tn_sft_conf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tn_sft_conf_pagos`
--
ALTER TABLE `tn_sft_conf_pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tn_sft_detalle_form_campos`
--
ALTER TABLE `tn_sft_detalle_form_campos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tn_sft_detalle_modulo_rol`
--
ALTER TABLE `tn_sft_detalle_modulo_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `tn_sft_detalle_rol_funciones`
--
ALTER TABLE `tn_sft_detalle_rol_funciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `tn_sft_det_mod_clientes`
--
ALTER TABLE `tn_sft_det_mod_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `tn_sft_estructuras`
--
ALTER TABLE `tn_sft_estructuras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tn_sft_form`
--
ALTER TABLE `tn_sft_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tn_sft_funciones`
--
ALTER TABLE `tn_sft_funciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `tn_sft_historico`
--
ALTER TABLE `tn_sft_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tn_sft_listas`
--
ALTER TABLE `tn_sft_listas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tn_sft_modulos`
--
ALTER TABLE `tn_sft_modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tn_sft_persona`
--
ALTER TABLE `tn_sft_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tn_sft_productos_servicios`
--
ALTER TABLE `tn_sft_productos_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `tn_sft_roles`
--
ALTER TABLE `tn_sft_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tn_sft_sistema`
--
ALTER TABLE `tn_sft_sistema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tn_sft_tablas`
--
ALTER TABLE `tn_sft_tablas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tn_sft_values_form`
--
ALTER TABLE `tn_sft_values_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sft_mod_shopcarrt_menu`
--
ALTER TABLE `sft_mod_shopcarrt_menu`
  ADD CONSTRAINT `sft_mod_shopcarrt_menu_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `sft_mod_shopcarrt_sub_menu`
--
ALTER TABLE `sft_mod_shopcarrt_sub_menu`
  ADD CONSTRAINT `sft_mod_shopcarrt_sub_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `sft_mod_shopcarrt_menu` (`id`),
  ADD CONSTRAINT `sft_mod_shopcarrt_sub_menu_ibfk_2` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_cambiar_estado`
--
ALTER TABLE `tn_sft_cambiar_estado`
  ADD CONSTRAINT `tn_sft_cambiar_estado_ibfk_1` FOREIGN KEY (`columna_id`) REFERENCES `tn_sft_columnas_tabla` (`id`);

--
-- Filtros para la tabla `tn_sft_categoria_productos`
--
ALTER TABLE `tn_sft_categoria_productos`
  ADD CONSTRAINT `tn_sft_categoria_productos_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_columnas_tabla`
--
ALTER TABLE `tn_sft_columnas_tabla`
  ADD CONSTRAINT `tn_sft_columnas_tabla_ibfk_1` FOREIGN KEY (`tabla_id`) REFERENCES `tn_sft_tablas` (`id`);

--
-- Filtros para la tabla `tn_sft_column_lista`
--
ALTER TABLE `tn_sft_column_lista`
  ADD CONSTRAINT `tn_sft_column_lista_ibfk_1` FOREIGN KEY (`id_lista`) REFERENCES `tn_sft_listas` (`id`),
  ADD CONSTRAINT `tn_sft_column_lista_ibfk_2` FOREIGN KEY (`id_column`) REFERENCES `tn_sft_columnas_tabla` (`id`);

--
-- Filtros para la tabla `tn_sft_column_opc_listas`
--
ALTER TABLE `tn_sft_column_opc_listas`
  ADD CONSTRAINT `tn_sft_column_opc_listas_ibfk_1` FOREIGN KEY (`id_lista`) REFERENCES `tn_sft_listas` (`id`);

--
-- Filtros para la tabla `tn_sft_conf`
--
ALTER TABLE `tn_sft_conf`
  ADD CONSTRAINT `tn_sft_conf_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`),
  ADD CONSTRAINT `tn_sft_conf_ibfk_2` FOREIGN KEY (`estructura_admin`) REFERENCES `tn_sft_estructuras` (`id`),
  ADD CONSTRAINT `tn_sft_conf_ibfk_3` FOREIGN KEY (`estructura_usuario`) REFERENCES `tn_sft_estructuras` (`id`);

--
-- Filtros para la tabla `tn_sft_conf_pagos`
--
ALTER TABLE `tn_sft_conf_pagos`
  ADD CONSTRAINT `tn_sft_conf_pagos_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_detalle_form_campos`
--
ALTER TABLE `tn_sft_detalle_form_campos`
  ADD CONSTRAINT `tn_sft_detalle_form_campos_ibfk_1` FOREIGN KEY (`id_campo`) REFERENCES `tn_sft_campos` (`id`),
  ADD CONSTRAINT `tn_sft_detalle_form_campos_ibfk_2` FOREIGN KEY (`id_form`) REFERENCES `tn_sft_form` (`id`);

--
-- Filtros para la tabla `tn_sft_detalle_modulo_rol`
--
ALTER TABLE `tn_sft_detalle_modulo_rol`
  ADD CONSTRAINT `tn_sft_detalle_modulo_rol_ibfk_1` FOREIGN KEY (`id_modulo`) REFERENCES `tn_sft_modulos` (`id`),
  ADD CONSTRAINT `tn_sft_detalle_modulo_rol_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `tn_sft_roles` (`id`);

--
-- Filtros para la tabla `tn_sft_detalle_rol_funciones`
--
ALTER TABLE `tn_sft_detalle_rol_funciones`
  ADD CONSTRAINT `tn_sft_detalle_rol_funciones_ibfk_1` FOREIGN KEY (`funcion_id`) REFERENCES `tn_sft_funciones` (`id`),
  ADD CONSTRAINT `tn_sft_detalle_rol_funciones_ibfk_2` FOREIGN KEY (`rol_id`) REFERENCES `tn_sft_roles` (`id`);

--
-- Filtros para la tabla `tn_sft_det_mod_clientes`
--
ALTER TABLE `tn_sft_det_mod_clientes`
  ADD CONSTRAINT `tn_sft_det_mod_clientes_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tn_sft_cliente` (`id`),
  ADD CONSTRAINT `tn_sft_det_mod_clientes_ibfk_2` FOREIGN KEY (`id_modulo`) REFERENCES `tn_sft_modulos` (`id`);

--
-- Filtros para la tabla `tn_sft_form`
--
ALTER TABLE `tn_sft_form`
  ADD CONSTRAINT `tn_sft_form_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_funciones`
--
ALTER TABLE `tn_sft_funciones`
  ADD CONSTRAINT `tn_sft_funciones_ibfk_1` FOREIGN KEY (`mod_id`) REFERENCES `tn_sft_modulos` (`id`);

--
-- Filtros para la tabla `tn_sft_listas`
--
ALTER TABLE `tn_sft_listas`
  ADD CONSTRAINT `tn_sft_listas_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_productos_servicios`
--
ALTER TABLE `tn_sft_productos_servicios`
  ADD CONSTRAINT `tn_sft_productos_servicios_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`),
  ADD CONSTRAINT `tn_sft_productos_servicios_ibfk_2` FOREIGN KEY (`cat_id`) REFERENCES `tn_sft_categoria_productos` (`id`);

--
-- Filtros para la tabla `tn_sft_roles`
--
ALTER TABLE `tn_sft_roles`
  ADD CONSTRAINT `tn_sft_roles_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`);

--
-- Filtros para la tabla `tn_sft_values_form`
--
ALTER TABLE `tn_sft_values_form`
  ADD CONSTRAINT `tn_sft_values_form_ibfk_1` FOREIGN KEY (`id_campo`) REFERENCES `tn_sft_campos` (`id`),
  ADD CONSTRAINT `tn_sft_values_form_ibfk_2` FOREIGN KEY (`id_form`) REFERENCES `tn_sft_form` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `tn_sft_cliente` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`rol_id`) REFERENCES `tn_sft_roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
