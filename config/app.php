<?php

return [

    /*INGLES
    |--------------------------------------------------------------------------
    | Application Name

    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    /*
    | ------------------------------------------------- -------------------------
    | Nombre de la aplicación
    
    | ------------------------------------------------- -------------------------
    |
    | Este valor es el nombre de su aplicación. Este valor se utiliza cuando el
    | marco debe colocar el nombre de la aplicación en una notificación o
    | Cualquier otro lugar requerido por la aplicación o sus paquetes.
    |
    */
    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*INGLES
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    /*
    | ------------------------------------------------- -------------------------
    | Modo de depuración de la aplicación
    | ------------------------------------------------- -------------------------
    |
    | Cuando su aplicación está en modo de depuración, se muestran mensajes de error detallados con
    | Se mostrarán rastros de pila en cada error que ocurra dentro de su
    | solicitud. Si está deshabilitado, se muestra una página de error genérico simple.
    |
    */
    'debug' => env('APP_DEBUG', false),

    /*INGLES
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    /*ESPAÑOL
    | ------------------------------------------------- -------------------------
    | URL de la aplicación
    | ------------------------------------------------- -------------------------
    |
    | La URL utiliza esta URL para generar correctamente las URL al usar
    | La herramienta de línea de comando artesanal. Debe establecer esto en la raíz de
    | su aplicación para que se utilice al ejecutar tareas de Artisan.
    |
    */
    'url' => env('APP_URL', 'http://localhost'),

    /*INGLES
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */
    /*ESPAÑOL
    | ------------------------------------------------- -------------------------
    | Zona horaria de la aplicación
    | ------------------------------------------------- -------------------------
    |
    | Aquí puede especificar la zona horaria predeterminada para su aplicación, que
    | será utilizado por las funciones de fecha y hora de PHP. Nos hemos ido
    | adelante y configúrelo en un valor predeterminado razonable para que usted salga de la caja.
    |
    */
    'timezone' => 'America/Bogota',

    /*INGLES
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    /*ESPAÑOL
    | ------------------------------------------------- -------------------------
    | Configuración regional de la aplicación
    | ------------------------------------------------- -------------------------
    |
    | La configuración regional de la aplicación determina la configuración regional predeterminada que se utilizará
    | por el proveedor de servicios de traducción. Usted es libre de establecer este valor
    | a cualquiera de las configuraciones regionales que serán compatibles con la aplicación.
    |
    */
    'locale' => 'es',

    /*INGLES
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    /*ESPAÑOL
    | ------------------------------------------------- -------------------------
    | Aplicación Local de respaldo
    | ------------------------------------------------- -------------------------
    |
    | La configuración regional alternativa determina la configuración regional que se utilizará cuando la actual
    | no está disponible. Puede cambiar el valor para corresponder a cualquiera de
    | Las carpetas de idioma que se proporcionan a través de su aplicación.
    |
    */
    'fallback_locale' => 'en',

    /*INGLES
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    /*
    | ------------------------------------------------- -------------------------
    | Clave de encriptación
    | ------------------------------------------------- -------------------------
    |
    | Esta clave es utilizada por el servicio de cifrado Illuminate y debe configurarse
    | a una cadena aleatoria de 32 caracteres, de lo contrario estas cadenas cifradas
    | no será seguro Por favor, haga esto antes de implementar una aplicación!
    |
    */
    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*INGLES
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */
    /*ESPAÑOL
    | ------------------------------------------------- -------------------------
    | Proveedores de servicios con carga automática
    | ------------------------------------------------- -------------------------
    |
    | Los proveedores de servicios enumerados aquí se cargarán automáticamente en el
    | Solicitud a su solicitud. Siéntase libre de agregar sus propios servicios a
    | esta matriz para otorgar funcionalidad expandida a sus aplicaciones.
    |
    */
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */
        
        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

    ],

];
