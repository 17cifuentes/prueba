<?php
//TITTLE
define("TITLE_CREAR_PERMISO_MODULO",	"Crear Permisos/Modulo");
define("TITLE_LISTA_PERMISO_MODULO",	"Lista de Permisos/Modulos");
define('TITLE_LISTA_P_ROLES',			'Lista de Permisos/Roles');
define("TITLE_MENU_PERMISOS",			"Menú Permisos");
define('VISUALIZAR',					'Visualizar');
define('TITLE_PERMISOS',				'Permisos');


//
define('TN_SFT_MODULOS',				'tn_sft_modulos');
define('TN_SFT_DETALLE_MODULO_ROL',		'tn_sft_detalle_modulo_rol');
define('TN_SFT_ROLES',					'tn_sft_roles');
define('TN_SFT_FUNCIONES',				'tn_sft_funciones');
define('TN_SFT_DET_ROL_FUN',			'tn_sft_detalle_rol_funciones');
define("VER_PERMISOS",					"Ver Permisos");
define("VER_PERMISOS_ROLES",     		"Ver Permisos Roles");
define("VER_PERMISOS_MODULOS",     		"Ver Permisos Modulos");
define("PERMISO_NO_PUDO_CREAR",     	"El permiso no se pudo crear");
define("PERMISO_NO_CREADO",     		"Permiso no creado");
define("TITLE_LISTA_P_FUN_ROLES",     	"Lista de Funciones/Roles");
define('PATH_VISUALIZAR_ROLES',			'core.Permisos.visualizarRoles');
define('PATH_PERMISOS_VISUALIZAR_ROLES','/Permisos/VisualizarRoles');
define('PATH_PERMISOS_LISTA_FUNCIONESROLES','/Permisos/listFuncionesRoles');
define("TITLE_VISUALIZAR_ROLES_FUNCIONES",	"Visualizar Funciones/Roles");
define('MSG_FUN_NO_ENCONTRADA',				'Función no encontrada');
define('MSG_NO_ACTUALIZA',					'No se pudo actualizar esta función');
define('MSG_FUNCION_ACTUALIZADA',			'La función se actualizó exitosamente');
define('MSG_PERMISOS_NO_SELECCIONADO',		'La función debe tener como mínimo un permiso seleccionado');
define('IDFUNCION', 						'idFuncion');


?>