<?php
define("SFT_LIB_MOD_REPORTES","Reportes");
define("SFT_LIB_MOD_TIENDA","Tienda");
define("SFT_LIB_MOD_CHAT","Chat");
define("SFT_LIB_MOD_RESERVAS","Reservas");
define("SFT_LIB_MOD_CRM","CRM");
define("SFT_LIB_MOD_PERMISOS","Permisos");
define("SFT_LIB_MOD_PMI","PMI");
define("SFT_LIB_MOD_PUNTO","Punto");
define("SFT_LIB_MOD_CONTABILIDAD","Contabilidad");
define("SFT_LIB_MOD_CURSOS","Cursos");

define("SFT_LIBRERIAS_MOD",
	[
		'Reportes' => 
		[
			'css' => '',
			'js'=>'<script src="/core/js/Reportes/reportes.js"></script>'.'"></script>',
			'funcion'=>''
		],
		'Tienda' => 
		[
			'css' => '',
			'js'=>'<script src="/core/js/CarritoCompras/global.js"></script>
				   <script src="/Modulos/ShopCart/js/shopCart.js"></script>',
			'funcion'=>'',
			'modals'=>
			[
				0=>'Modulos.ShopCart.modals.modalcancelarProducto',
				1=>'Modulos.ShopCart.modals.modalAgregarProductoDomicilio',
				2=>'Modulos.ShopCart.modals.modalExitoContraentrega',
				3=>'Modulos.ShopCart.modals.modalCancelarDomicilio',
				4=>'Modulos.ShopCart.modals.modalsInfoUsuario'
			]
				
		],
		
		'Permisos' => 
		[
			'css' => '',
			'js'=>'<script src="/core/js/Permisos/permisos.js"></script>',
			'funcion'=>''
		],
		'Productos' => 
		[
			'css' => '',
			'js'=>'<script type="text/javascript" src="/Modulos/Productos/js/productos.js"></script>',
			'funcion'=>'',
			'modals'=>
			[
				0=>'Modulos.Productos.modals.padreModal'
			]
		],
		'Chat' => 
		[
			'css' => '<link rel="stylesheet" type="text/css" href="/Modulos/Chat/css/chat.css"/>',
			'js'=>'<script type="text/javascript" src="/Modulos/Chat/js/chat.js"></script>',
			'funcion'=>'starChat'
		],
		'Reservas' => 
		[
			'css' => '',
			'js'=>'<script type="text/javascript" src="/Modulos/Reservas/js/reservas.js"></script>',
			'funcion'=>''
		],
		'CRM' => 
		[
			'css' => '',
			'js'=>'<script src="/Modulos/Crm/js/crm.js"></script>',
			'funcion'=>''
		],
		'PMI' => 
		[
			'css' => '',
			'js'=>'<script src="/Modulos/PMI/js/pmi.js"></script>',
			'funcion'=>'',
			'modals'=>
			[
				0=>'Modulos.PMI.modals.modalagregarTareaProyecto'
			]
		],
		'Contabilidad' =>
		[
			'css' => '',
			'js' => '<script src="/Modulos/Contabilidad/js/contabilidad.js"></script>',
			'funcion'=>'',
			'modals' =>
			[
				0=>'Modulos.Contabilidad.modals.editarNomina',
				1=>'Modulos.Contabilidad.modals.liquidacion',
				2=>'Modulos.Contabilidad.modals.desprendibles'
			]
		],
		'Punto' => 
		[
			'css' => '',
			'js' => '<script src="/Modulos/Punto/js/punto.js"></script>',
			'funcion'=>'',
			'modals'=>
			[
				0=>'Modulos.Punto.modals.modalPuntosVenta'
			]
		],
		'Cursos' => 
		[
			'css' => '',
			'js' => '<script src="/Modulos/Cursos/js/cursos.js"></script>',
			'funcion'=>'',
			'modals'=>
			[
				0=>'Modulos.Cursos.modals.modalCategoriasCurso'
			]
		]
	]
);
