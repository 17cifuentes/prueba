<?php 

define('MENU_ROLES',	'Menú de Roles');
define('LST_ROL',		'Listado de Roles');
define('EDI_ROL',		'Editar Rol');
define('ROLES', 		'roles');
define('CRE_ROL',		'Crear Rol');
define('MSN_LIM_CHAR',	'Ha excedido el limite de caracteres. Limite 250');
define('MSG_UNF_ROL',	'Rol no encontrado');
define('MSG_NUM_ROL',	'Rol no es númerico');

define('SUCC_ROL',		'Rol creado exitosamente');
define('SUCC_EST_ROL',	'Cambio de estado exitoso');
define('ERR_EST_ROL',	'Cambio de estado falló');

define('ERR_ROL',		'Ya hay un rol con ese nombre');
define('ERR_CRE_ROL',	'No se pudo crear el Rol');
define('ERR_EDI_ROL',	'No se pudo editar el Rol');
define('FUN_ROL_CRE_ROL', 'Roles/crearRol');
define('FUN_ROL_LST_ROL', '/Roles/lstRoles');
define('FUN_ROL_EDI_ROL', 'Roles/editarRoles');
define('FUN_ROL_CHA_EST', 'Roles/cambiarEstado');
//TITTLE
define("TITTLE_ROL_NOMBRE",	"Rol Nombre");
define('TITLE_ROL', 		'Rol');


define("TN_ROL_NOMBRE",     "rol_nombre");
define("TN_ROLES",     		"tn_sft_roles");
define("TN_EST_ROL",     	"estado_roles");
define("TN_ROLES_ROL_ID",   "rol_id");
define("TN_ROL_DESCRIPCION",'rol_descripcion');
