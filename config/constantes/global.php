<?php
//RUTAS DEL SISTEMA DEFINIDAS
define("MIDDLEWARE_EXCEPT","except");
define("PATH_FOTO_USU","images/users/");
define("TITLE_INICIO_DASH",     "Inicio");
define("PATH_DASH",     "dashboard");
define("PATH_DASH_2",     "dashboard2");
define("PATH_VIEW_IMG_DATA",     "core.pageImgData");
define("PATH_LIST",     "core.list");
define("PATH_INFOCARD",     "core.infoCard");
define("PATH_VIEW_CLASS_TABLE",     "core/tables/table");
define("PATH_LOGIN","auth.login2");
define("URL_INGRESO","/ingresar/");
define("URL_CONF_CLIENTE","/Clientes/configuracion");
define("URL_EDIT_CLIENTE","/Clientes/lstClientes");
define("URL_EDITAR_CLIENTE","/core/Clientes/editarCliente");
define("URL_TABLE_1","/Table/1");
define("URL_HOME","/home");
define("URL_PAG_NO_FOUND","/ingresar/0");
define("PATH_HOME","/home");
define("PATH_MENU","/Menu/");
define("PATH_MENU_DOS","/Menu/2");
define("PATH_PERFIL","core.Usuarios.perfil");
define("PATH_BREADCUMB","layouts.SFTbreadCumbs");
define("PATH_CORE_AYUDA","core.Sistema.ayuda");
define('TN_FECHA', 'fecha');
define('TN_OPC','opc');
define('TN_CODIGO', 'codigo');
define('TN_NOMBRE_ESTADO', 'nombre_estado');
define('OPCION', 'Opción');
define('TN_TITULO', 'titulo');

define("TITLE_CLIENTE_CONF",     "Cliente a configurar");
define("TITLE_AYUDA",     "Ayuda");
//VARIABLES
define('FEC_INI', 'fec_inicio');
define('FEC_FIN', 'fec_fin');

//DEFAULT SFT
define("SFT_DEFAULT_ICO",     "open_in_new");
define("SFT_COLOR",     "#273679");

//
define("OPC_FUNCIONES",     "opcFuncion");
define("TOKEN",     "_token");
define("DESCRIPCION",     "Descripción");
define("MENU_PERMISOS",     "Menú Permisos");
define("GESTIONAR",     "Gestionar");
define("MODULO",     "Modulo");
define('CODIGO', 'Código');
define("TITLE_ELIMINAR",     "Eliminar");
define("EDITAR",     "Editar");
define("CAMBIAR_ESTADO",     "Cambiar Estado");
define("FOTO",     "foto");
define("DATA",     "data");
define("ADD",     "add");
define("FECHA",     "Fecha");
define("CREATED_AT",     "created_at");
define("TITLE",     "title");
define("SUBTITLE",     "subtitle");
define("ICO",     "ico");
define("COLOR",     "color");
define("URL",     "url");
define("NO",     "No");
define("SI",     "Si");
define("SELECCIONE",     "seleccione");
define("DATAITEM",     "dataItem");
define("BREAD",     "bread");
define("GET",     "GET");
define("COL",     "col");
define("POST",     "POST");
define("FORM",     "form");
define("MIN_AUTH",     "auth");
define("ATRIBUTES",     "atributes");
define("TIPO",     "tipo");
define("TABLE",     "table");
define("ID",     "id");
define("MSG",     "msg");
define("JS_VOID",     "javascript::void()");
//MEDIDAS DE DISEÑO BOOTSTRAP
define("COL_LG_12",     "col-lg-12 ");
define("COL_LG_8",     "col-lg-8 ");
define("COL_LG_6",     "col-lg-6 ");
define("COL_LG_4",     "col-lg-4 ");
define("COL_MD_4",     "col-md-4 ");
define("COL_LG_3",     "col-lg-3 ");
define("COL_LG_2",     "col-lg-2 ");
define("COL_SM_12",     "col-sm-12 ");
define("COL_SM_8",     "col-sm-8 ");
define("COL_SM_6",     "col-sm-6 ");
define("COL_SM_4",     "col-sm-4 ");
define("COL_XS_6",     "col-xs-6 ");
define("COL_SM_3",     "col-sm-3 ");
define("COL_SM_2",     "col-sm-2 ");
//VISTAS 
define("INICIO",     "Inicio");
define("MENU_CLIENTE",     "Menú clientes");
define("MENU",     "Menú ");
define("TITLE_INGRESO",     "Ingreso a la plataforma");
//NOTIFICACIONES TIPOS
define("NOTIFICACION_SUCCESS","success");
define("NOTIFICACION_ERROR","error");
define("NOTIFICACION_WARNING","warn");
define("NOTIFICACION_INFO","info");
define("NOTIFICACION_INCORRECTO","incorrecto");
define("NOTIFICACION_CORRECTO","correcto");
define("NOTIFICACION_PROCESO_CORRECTO","Procesos Exitoso ");
define("NOTIFICACION_PROCESO_YA_REALIZADO","proceso ya realizado ");
define("NOTIFICACION_PROCESO_INCORRECTO","Ups Algo paso intentalo de nuevo");
define("NOTIFICACION_PROCESO_YA_EXISTE_REGISTRO","El registro ya existe en nuestra base de datos");
define("CAMBIO_ESTADO_SUCCESS","Cambio de estado exitoso");

//MSG
define("MSG_CAMPOS_INCORRECTOS","Campos incorrectos");
define("MSG_TIENE_PERMISOS","Ya tiene permisos");
define("MSG_PERMISO_CREADO_EXITOSO","Permiso creado exitosamente");
define("MSG_CRE_EXI","Creado correctamente");
define("MSG_EDI_COR","Editado correctamente");
define("MSG_NO_EDI_COR","No se pudo editar");





