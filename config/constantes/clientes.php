<?php

define("CLIENTE_ID",			"id");
define("CLIENTE_CORREO",     	"Correo");
define("CLIENTE_DIRECCION",     "Dirección");
define("CLIENTE_DESCRIPCION",   "Descripción");
define("CLIENTE_TELEFONO",     	"Télefono");
define("CLIENTE_NOMBRE",     	"Nombre");
define("CLIENTE_APELLIDO",     	"Apellido");
define("CLIENTE_TITLE",     	"title");
define("CLIENTE_TITLE_LISTAR",  "Lista clientes");
define("CLIENTE_CONF",     		"conf");
define("CLIENTE_MENU",     		"menu");
define("CLIENTE_BIENVENIDO",	"Bienvenido a ");
define("CLIENTE_NO_EXISTE",     "El cliente al que intenta ingresar no existe");
define("CLIENTE_FUNCIONES",     "funciones");

define("CLIENTE_TITLE_EDITAR_CLIENTE",	"Editar cliente");
define("CLIENTE_TITLE_LISTAR_CLIENTE",	"Lista de clientes");
define("RUTAS_LISTAR_MODULOS", 			"/Modulos/lstModulos");

//TITLE
define("CLIENTE_CONFIGURAR",	"Cliente a configurar");
define("CONFIGURACIONES",		"Configuraciones");
define("CONF_TITULO_PESTANA",   "Titulo pestaña");
define("CONF_NAVBAR_TITULO",    "NavBar Titulo");
define("CONF_ICONO_PESTANA",    "Icono pestaña");
define("CONF_NAVBAR_COLOR",     "Color NavBar");
define("CONF_BREADCUMD_COLOR",  "Rastro de página color");
define("CONF_MENU_DERECHO",     "Imagén barra derecha");
define("CONF_LOGO",     		"Cargar logo");
define("CONF_IMG_LOGIN",     	"Imagén login");
define("CONF_BUSCADOR",     	"Buscador");
define("CONF_NOTIFICACIONES",   "Notificaicones");
define("CONF_MENU_DERECHO_OPC", "Menú derecho");
define("MENU_CLIENTES",     	"Menú clientes");
define("CREAR_CLIENTES",     	"Crear cliente");
define("FIELD1_CREAR_CLIENTES", "Datos del cliente");
define("CLIENTE_TITLE_ESTADO",  "Estado");
//TN CONF
define("TN_CONF_ID_CLIENTE",		"cliente_id");
define("TN_ID_CLIENTE",				"id");
define("TN_CONF_MENU_DERECHO_OPC",	"slide_rigth");
define("TN_CONF_NOTIFICACIONES",	"navbar_notification");
define("TN_CONF_BUSCADOR",			"search");
define("TN_CONF_MENU_DERECHO",		"img_slide");
define("TN_CONF_IMG_LOGIN",			"img_login");
define("TN_CONF_LOGO",				"img_logo");
define("TN_CONF_BREADCUMD_COLOR",	"bradCumb_color");
define("TN_CONF_NAVBAR_COLOR",		"navbar_color");
define("TN_CONF_ICONO_PESTANA",		"ico");
define("TN_CONF_NAVBAR_TITULO",		"navbar_title");
define("TN_CONF_TITULO_PESTANA",	"title");
define("TN_CLIENTE",				"tn_sft_cliente");
define("TN_NOMBRE_CLIENTE",			"cliente_nombre");
define("TN_CLIENTE_ID",				"id_cliente");

define("TN_CLIENTE_DESCRIPCION","cliente_descripcion");
define("TN_CLIENTE_DIRECCION",	"direccion");
define("TN_CLIENTE_TELEFONO",	"telefono");
define("TN_CLIENTE_CORREO",		"correo");
define("TN_ESTADO_CLIENTE",		"estado_cliente");
define('TN_DESCRIPCION',		'descripcion');
//URL
define("CONF_URL_PROCESS",		"/Clientes/confiProcess");
define("CLIENTE_URL_PROCESS",	"/Clientes/createProcess");
define("CLIENTE_URL_CREATE",	"Clientes/create");
define("CLIENTE_URL_EDITAR",	"Clientes/editarCliente");
define("CLIENTE_URL_CAM_EST",	"Clientes/cambiarEstado");
define("CLIENTE_URL_DELETE",	"Clientes/borrarCliente");
//MENSAJES
define("MSG_CLIENTE_CREATE_EXITOSAMENTE",	"Cliente registrado exitosamente");
define("MSG_CLIENTE_INACTIVO",				"El cliente esta inactivo, comuniquese con el administrador");
define("MSG_FORM_SIN_NUMERAR", 				"¡ERROR! El formulario no se encuentra con un número");
define("MSG_VAL_CHAR",						"¡ERROR! Revise la cantidad de caracteres enviados");
define("MSG_CLIENTE_NO_ACTUALIZADO_ESTADO", "No se pudo cambiar");
define("MSG_UNLOG",							"Ingrese a su cuenta para acceder a la página");
define("MSG_NO_ANSWER",						"Petición no encontrada, intentelo nuevamente");
define("MSG_CLIENTE_CAMBIAR_ESTADO",		"Se cambio el estado");
define("MSG_TIENDA_SIN_CONFIGURAR",			"La tienda no cuenta con la configuración");
define("MSG_FORM_UNFOUND",					"¡ERROR! Formulario no encontrado");









