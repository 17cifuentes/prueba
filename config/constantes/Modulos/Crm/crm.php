<?php 

define('CREAR_CONTACTO',		'Crear contacto');
define('FIELD1_CREAR_CONTACTO', 'Datos del contacto');
define("TN_CONTACTO_NOMBRE", 	"nombre");
define("TN_CONTACTO_APELLIDO",  "apellido");
define("TN_CONTACTO_RESUMEN",   "resumen");
define("CONTACTO_RESUMEN",		"Resumen");
define("TN_CONTACTO_ESTADO",    "estado_contacto");
define("TN_TIEMPO_DE_CONTACTO", "tiempo_de_contacto");
define("TN_ID_CONTACTO", "id_contacto");
define("TN_HISTORIAL_CONTACTO", "sft_mod_historial_contacto");


//define("TITLE_LIST_CONTACTOS","Lista de Contactos");
define("MSG_CONTACTO_CREATE_EXITOSAMENTE", "Contacto creado exitosamente");

define("EDITAR_CONTACTO", 					"Editar contacto");
define("UPDATED_SUCCESS",					"El contacto ha sido editado exitosamente");
define("DELETED_SUCCESS",					"El contacto ha sido borrado exitosamente");
define("MSG_CONTACTO_NO_ENCONTRADO",		"Error, contacto no encontrado");
define("UPDATED_ERROR",						"Error, el contacto no puede ser editado");
define('TITLE_MENUCMR',						'Menú CMR');



define('MSG_NO_CREADO_CONTACTO',		'No se pudo crear el contacto!');
define('TITLE_CONTACTAR',				'Contactar');
define('TITLE_HISTORIAL_CONTACTO',		'Historial Contacto');
define('TITLE_ESTADISTICAS',			'Estadisticas');
define('TITLE_CONTACTAR_CONTACTO',		'Contactar Contacto');
define('TITLE_LIST_CONTACTOS',			'Lista de Contactos');
define('TITLE_TIEMPO_CONTACTO_ACTUAL',	'tiempo_contacto_actual');

define('TN_RESPONSABLE_CONTACTO',		'responsable');
define('TITLE_RESPONSABLE_CONTACTO',	'Responsable');
define('TITLE_RESUMEN_LLAMADA',			'Resumen Llamada');
define('TITLE_FECHA_LLAMADA',			'Fecha Llamada');
define('TITLE_DURACION_CONTACTO',		'Duración Contacto');
define('TIEMPO_DE_CONTACTO', 'Tiempo de contacto');

//URL
define("CONTACTO_URL_PROCESS",		"/Callcenter/createContactProcess");
define('PATH_LIST_CONTACTOS',		'Crm/listContactos');
define('CRM_URL_LISTAR_HISTORIAL',	'Crm/listarHistorialContactos');
define('CRM_URL_EDITAR_CONTACTO',	'Crm/editarContacto');
define('CRM_URL_CONTACTAR_CONTACTO','Crm/contactarContacto');
define('CRM_URL_HISTORIAL_CONTACTO','Crm/historialContacto');
define('CRM_URL_DELETE_CONTACTO',	'Crm/borrarContacto');
define('PATH_CREATE_CONTACTO',		'Modulos.Cmr.createContacto');
define('PATH_EDITAR_CONTACTO',		'Modulos.Cmr.editarContacto');
define('PATH_ESTADISTICAS_CONTACTO','Modulos.Cmr.estadisticas');
define('PATH_CONTACTAR_CONTACTO',	'Modulos.Cmr.contactarContacto');
define('PATH_CRM',					'\App\Http\Modulos\Crm\Controller');















 ?>