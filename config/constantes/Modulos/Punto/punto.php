<?php

define('PATH_PUNTO', '\App\Http\Modulos\Punto\Controllers\PuntoController');
define('PATH_VIEW_CREAR_PUNTO', 'Modulos.Punto.crearPunto');
define('PATH_VIEW_EDITAR_PUNTO', 'Modulos.Punto.editarPunto');
define('PATH_VIEW_PUNTOS_VENTA', 'Modulos.Punto.puntosVenta');
define('PATH_VIEW_LISTAR_PUNTO', 'Punto/lstPuntos');
define('PATH_EDITAR_PUNTO','Punto/editarPunto');
define('PATH_ELIMINAR_PUNTO', 'Punto/eliminarPunto');
define('PATH_CAMBIAR_ESTADO', 'Punto/cambiarEstado');
define('PATH_VIEW_LISTAR', 'lstPuntos');

define('MENU_PUNTO', 'Menú punto');
define('MSG_LISTA_PUNTOS', 'Lista de puntos');
define('CREAR_PUNTO', 'Crear punto');
define('EDITAR_PUNTO', 'Editar punto');

define('TN_ID', 'id');
define('TN_PUNTO_NOMBRE', 'pun_nombre');
define('TN_PUNTO_TIPO', 'pun_tipo');
define('TN_PUNTO_DESCRIPCION', 'pun_descripcion');
define('TN_NAME_ESTADO', 'nombre_estado');
