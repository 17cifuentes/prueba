<?php
//RUTAS 
define("PATH_CTRL_PROVEEDORES","\App\Http\Modulos\Proveedores\Controllers\ProveedoresController");
define("RUTA_CREAR_PROVEE","/Proveedores/crearProveedores");
define("RUTA_CAMBIAR_EST","'Proveedores/cambiarEstadoProveedor'");
define("RUTA_EDITAR_PROVEE","Proveedores/editarProveedor");
define("RUTA_DELETE_PROVEE","Proveedores/eliminarProveedores");
define("RUTA_LST_PROVEE","/Proveedores/listarProveedores");
define("RETURN_VIEW_PROVEE_EDITAR",'Modulos.Proveedores.editarProveedores');
define("RETURN_VIEW_PROVEE_CREAR",'Modulos.Proveedores.crearProveedores');

//TITLES
define("TN_TITLE_PROVEE","Registrar Proveedores");
define("TN_TITLE_EDIT","Editar Proveedor");
//MSG
define("MSG_ESTADO_ACTUALIZADO","Estado Actualizado correctamente");
define("MSG_REGISTER_PROVEE","Proveedor registrado correctamente");
define("ERROR_NO_ENCONTRADO_PROVEEDOR","ERROR! Proveedor no encontrado");
define("ERROR_PROVEE_NO_NUMERICO","ERROR! el Proveedor no se encuentra con un número");
define("ERROR_NO_DB","ERROR! El Proveedor no se encuentra en la base de datos");
define("MSG_ACTUALIZADO","Su proveedor se actualizo correctamente");
define("MSG_DELETE_CORRECTAMENTE_PROVEEDOR","Se elimino correctamente el proveedor");
define("ERROR_CANT_CARACTERES_PROVEEDORES","Error Revise la cantidad de caracteres enviados");
//NAMES
define("NAME_PROVEE","nombre_proveedor");
define("CC_RESPONS","cedula_responsable");
define("NIT","nit");
define("NAME_RESPONS","nombre_responsable");
define("CAMERA_COMERCIO","camara_comercio");
define("RUT","rut");
define("ESTADO_PROVEE","estado_proveedor");
define("MENU_PROVEE",'Menu Proveedor');
define("LST_PROVEE",'Listar Proveedor');
define("EDIT_PROVEE",'Editar Proveedor');

//NAMES TABLE
define("NOMBRE_PROVEE",'Nombre Proveedor');
define("CEDULA_RESPONS",'cedula responsable');
define("NAME_NIT",'NIT');
define("NOMBRE_RESPONSE",'Nombre responsable');
define("CAMERA_COMMERCE",'camara y comercio');
define("NAME_RUT",'RUT');
define("EST",'Estado');
//VALIDACIONES
define("VAL_ESTADO",'val-Cambiar estado');
define("VAL_EDIT",'val-Editar');
define("VAL_DELETE_PROVEE",'val-Eliminar');



