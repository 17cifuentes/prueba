<?php 
define("PATH_CTRL_RESERVAS","\App\Http\Modulos\Reservas\Controllers\ReservasController");
define("RESERVA_MSG_EVENT_GOOD","El evento se registro correctamente");
define("RESERVA_MSG_EVENT_BAD","El evento no se registro");
define("RESERVA_MSG_EVENT_UPDATE_GOOD","El evento se actualizo correctamente");
define("RESERVA_MSG_EVENT_UPDATE_BAD","El evento no se actualizo");
define("RESERVA_MSG_DELETE_GOOD","El evento se elimino");
define("RESERVA_MSG_DELETE_BAD","El evento no se elimino");
