<?php
//RUTAS

define('PATH_PMI',					'\App\Http\Modulos\PMI\Controller\PmiController');
define('PATH_PMI_CREAR_PROYECTO', 	'Modulos.PMI.crearProyecto');
define('PATH_PMI_HISTORIAL_PROYECTO', 	'Modulos.PMI.FormHistorialProyecto');
define('PATH_PMI_EDITAR_PROYECTO','Modulos.PMI.editarProyecto');
define('FUN_LISTAR_PROYECTO',	'PMI/listarProyecto');
define('FUN_LIS_TAR_PRO',		'PMI/listTareasProyecto');	
define('FUN_CRE_PRO',			'PMI/crearProyecto');
define('FUN_EDI_PRO',			'PMI/editarProyecto');
define('FUN_DEL_PRO',			'PMI/eliminarProyecto');
define('FUN_LST_TASK',			'PMI/listTareasMisProyectos');
define('FUN_LST_MIS_PROYECTOS',	'/PMI/misProyectos');
define('FUN_HIS_PRO', 			'PMI/historial');

define('FUN_LST_MIS_TAREAS',	'PMI/listTareasMisProyectos');
define('FUN_LST_FINALIZAR',	    'PMI/finalizarTarea');
define('FUN_LST_DESASIGNAR',	'PMI/desasignarTareaMisProyectos');





//MENSAJES ENCABEZADOS
define('FUNCTION_MYS_PROYECT',  'misProyectos');
define('CREAR_PROYECTO',  'Crear proyecto');
define('LISTAR_PROYECTO', 'Listado de proyectos');
define('LISTAR_TAREAS_PROYECTO','Listar Tareas Proyecto');
define('MIS_PROYECTOS',     'Mis proyectos');
define('LST_MIS_TAREAS',      'Listado mis tareas');
define('DESASIGNAR_TAREAS',     'Desasignar tarea proyecto');
define('EDITAR_PROYECTO', 'Editar proyecto');
define('MENU_PMI',      'Menú Pmi');
define('MSG_CRE_PRO_FAIL',  'No se pudo crear el proyecto');
define('MSG_DESASIGNAR_TASK','Se Desasigno correctamente la tarea');
define('MSG_TAREA_FINALIZADA','Tarea finalizada correctamente');
define('MSG_TAREA_DESASIGNADA','Tarea desasignada correctamente');
define('MSG_TAREA_NO_FINALIZADA','Tarea NO finalizada con exito');
define('MSG_TAREA_NO_DESASIGNADA','Tarea NO desasignada con exito');
define('MSG_CRE_PRO_SUC',   'Creado correctamente');
define('MSG_EDICION_CORRECTA',  'Edición correcta');
define('MSG_EDICION_ERROR',   'No se pudo editar'); 
define('MSG_BORRADO_EXITOSO', 'Borrado exitoso');
define('MSG_BORRADO_ERROR',   'No se pudo borrar'); 
//MODELO
define('TABLA_PMI', 'tn_sft_mod_pmi_proyectos');
define('PMI_NAME', 'pmi_nombre');
define('PMI_DESC','pmi_descripcion');
define('PMI_ENCARGADO','name');
define('PMI_INICIO','fec_inicio');
define('PMI_FIN','fec_fin');
define('PMI_CLIENTE','cliente');
define('PMI_N_TAREA','nombre_tarea');
define('PMI_N_ESTADO','nombre_estado');
define('PMI_FECHA','fecha_tarea');
define('PMI_FINALIZAR','finalizar');
define('PMI_DESASIGNAR','desasignar');
//ERROR
define('ERROR_PMI_PROCCESS','Ocurrio un error, Revises los datos. Reintente o contacte al Administrador');






?>