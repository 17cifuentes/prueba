<?php 

define('TITLE_CREAR_CUENTA',		'Crear Cuenta');
define('MENU_CONTABILIDAD',			'Menú Contabilidad');
define('DELETED_CUENTA_SUCCESS',	'Esta cuenta ha sido eliminada exitosamente');
define('TN_TIPO_ESTADOS',			'tipoEstados');
define('MSG_CUENTA_UPDATE_SUCCESS', 'La cuenta se ha editado exitosamente');
define('MSG_CUENTA_UPDATE_ERROR',	'Error, la cuenta no se pudo editar');
define('MSG_CUENTA_SUCCESS',		'La cuenta se ha creado exitosamente');
define('MSG_CUENTA_ERROR',			'Error,la cuenta no se pudo crear');
define('MSG_CUENTA_NO_ENCONTRADA',	'Error, no hay cuentas relacionadas a éste número de identificación');
define('PATH_LISTA_CUENTA',			'/Contabilidad/listCuenta');
define('PATH_URL_EDITAR_CUENTA',	'Contabilidad/editCuenta');
define('PATH_URL_DELETE_CUENTA',	'Contabilidad/eliminarCuenta');
define('PATH_CONTABILIDAD',			'\App\Http\Modulos\Contabilidad\Controllers');
define('PATH_LIST_CUENTAS',			'Lista de Cuentas');
define('PATH_EDITAR_CUENTA',		'Editar Cuenta');
define('TN_COMISION', 'comision');

 ?>