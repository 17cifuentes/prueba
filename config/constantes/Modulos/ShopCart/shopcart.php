<?php
define("PATH_CTRL_SHOPCART", "\App\Http\Modulos\ShopCart\Controllers\ShopCartController");
define("PATH_VIEW_TIENDA_1", "Modulos.ShopCart.layouts.1.index");
define("PATH_VIEW_TIENDA_3", "Modulos.ShopCart.layouts.3.index");
define("PATH_MOD_SHOPCART_HEADER_1", "Modulos.ShopCart.layouts.1.header");
define("PATH_MOD_SHOPCART_NAVBAR_1", "Modulos.ShopCart.layouts.1.navbar");
define("PATH_MOD_SHOPCART_NAVBARMENU_1", "Modulos.ShopCart.layouts.1.navBarMenu");
define("PATH_MOD_SHOPCART_NAVBARCATEGORIAS_1", "Modulos.ShopCart.layouts.1.navCategorias");
define("PATH_MOD_SHOPCART_NAVCART_1", "Modulos.ShopCart.layouts.1.navCart");
define("PATH_MOD_SHOPCART_OFERTASESPECIALES_1", "Modulos.ShopCart.layouts.1.ofertasEspeciales");
define("PATH_MOD_SHOPCART_PRODUCTOS_1", "Modulos.ShopCart.layouts.1.productos");
define("PATH_MOD_SHOPCART_FOOTER_1", "Modulos.ShopCart.layouts.1.footer");
define("PATH_MOD_SHOPCART_BENEFICIOS_1", "Modulos.ShopCart.layouts.1.beneficios");
define("PATH_MOD_SHOPCART_MODAL_1", "Modulos.ShopCart.layouts.1.modal");
define("SHOPCART_TOTAL", "total");
define("SHOPCART_CART", "cart");
define("SHOPCART_PRODUCTOS", "productos");
define("SHOPCART_PRODUCTOS_NAV", "productos_cart");
define("SHOPCART_ID_PRODUCTO", "idProducto");
define("CATEGORIES", "categorias");
define("MENU_SHOPCART", "menu");
define("SUBMENU_SHOPCART", "subMenu");
define("SHOPCART_CONFIGPAGOS", "configPagos");
define("TN_MENU", "sft_mod_shopcarrt_menu");
define("TN_SUBMENU", "sft_mod_shopcarrt_sub_menu");
define("SFT_APARTADOS_TIENDA", "tn_sft_conf_tienda");
define("SHOPCART_PROMOCION_DEL_MES", "promocion_del_mes");





