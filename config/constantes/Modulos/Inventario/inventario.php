<?php
//RUTAS
define("PATH_CTRL_INVENTARIO","\App\Http\Modulos\Inventario\Controllers\InventarioController");
define("PATH_CTRL_BASTECER","Inventario/abastecerCant");
define("PATH_CTRL_BAJA","Inventario/darDeBajaCant");
define("PATH_CTRL_HISTORIAL","Inventario/listarHistorial");
define("PATH_CTRL_CREAR","/Inventario/crearInventario");
define("PATH_CTRL_LST","Inventario/listarInventario");

//MSG
define('TITLE_BAJO_STOCK', 'Nivel bajo de stock');
define('CUERPO_MSG', 'Apreciado usuario, queremos informarle que la cantidad del producto ha llegado a su nivel mínimo de stock, le aconsejamos por favor abastecer el prducto.');
define("ERROR_CANT_CARACTERES", "Error Revise la cantidad de caracteres enviados");
define("ERROR_YA_EXISTE", "El producto YA EXISTE, abastescalo");
define("REGISTRO_CORRECTAMENTE", "Se registro correctamente en el inventario");
define("ERROR_NO_ENCONTRADO", "ERROR! Producto en el Invetario NO ENCONTRADO");
define("ERROR_NO_NUMERICO", "ERROR! el Producto no se encuentra con un número");
define("ERROR_NO_BD", "ERROR! El Producto NO se encuentra en la base de datos");
define("ABASTECIDO_CORRECTAMENTE", "Su producto se abastecio correctamente");
define("BAJA_CORRECTAMENTE", "Su producto se dio de baja correctamente'");

//NAMES
define('PRODUCTO_NOMBRE','Producto: ');
define('CUERPO_CANTIDAD_ANTERIOR', 'Cantidad anterior');
define('CUERPO_NIVEL_STOCK', 'Nivel de stock establecido');
define('CUERPO_CANTIDAD_ACTUAl', 'Cantidad actual');
define('TN_NIVEL_STOCK', 'nivel_stock');
define('TN_CUERPO','cuerpo');
define('TN_CANT_ANTERIOR', 'cant_anterior');
define('TN_CANTIDAD','cantidad');
define('TN_CAT_FINAL', 'cant_final');
define('TN_INV_CONSTANTE','const');
define("CANT_ACTUAL","cantidad_actual");
define("MOVIMIENTO","movimiento");
define("CANT_NUEVA","cantidad_nueva");
define("FECHA_INV","fecha");
define("NOMBRE_PROD",'Nombre producto');
define("CANT_DISPONIBLE",'Cantidad disponible');
define("NIVEL_STOCK",'Nivel de stock');
define("MENU_INV",'Menu Inventario');
define("LST_INV",'Lista Inventario');

//NOMBRES
define('VAL_ABASTECER','val-Abastecer');
define('VAL_DAR_BAJA','val-Dar de baja');
define('HISTORIAL','Historial');
define("VALOR","Valor");
define('CANTI_ACTUAL','Cantidad nueva');








