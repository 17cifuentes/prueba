<?php

define("PATH_TEMAS","\App\Http\Modulos\Cursos\Controllers\TemasController");
define("MENU_TEMAS","Menú temas");
define("CREAR_TEMAS","Crear temas");
define("PATH_TEMAS_CREAR_TEMAS","Modulos.Cursos.crearTema");
define('TABLA_TEMAS', 'tn_sft_temas');
//MSG
define("MSG_CRE_TEM","Tema registrado correctamente");
define("MSG_UPDATE_TEM","Tema actualizado correctamente");
define("MSG_ERROR_UPDATE_TEM","Tema NO actualizado correctamente");
define("MSG_CRE_TEM_FAIL","Su tema no se registro correctamente");
define("FUN_CRAER_TEMA","Cursos/crearTemas");
define("FUN_LST_TEMA","Cursos/ListarTemas");
define("LISTAR_TEMAS","listado de temas");
define("BTN_EDITAR","val-Editar");
define("ELIMINAR","val-Eliminar");
define("EDITAR_TEMA","Editar tema");
define("CAMBIAR_ESTADO_TEMA","val-Cambiar estado");
define("PATH_TEMAS_EDITAR_TEMA","Modulos.Cursos.editarTema");
define("MSG_DELETE_CORRECTAMENTE","Tema eliminado correctamente");
define("ERROR_CANT_CARACTERES_TEMAS","Error! revise la cantidad de caracteres enviados e intente nuevamente");
define("NOMBRE_DEL_TEMA","Nombre");
define("DESCRIPCIÓN","Descripción");
define("CLIENTE","Cliente");
define("ESTADO","Estado");
define("RUTA_EDITAR","Cursos/editarTema");
define("RUTA_ELIMINAR","Cursos/eliminarTema");
define("RUTA_ESTADO","Cursos/cambiarEstadoTema");
define("BD_NOMBRE","nombre_tema");
define("BD_DESCRIPCION","descripcion_tema");
define("BD_CLIENTE","cliente_id");
define("BD_ESTADO","estado_tema");














?>