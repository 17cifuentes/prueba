<?php
//rutas
define("PATH_CTRL_PROMOCIONES","\App\Http\Modulos\Promociones\Controllers\PromocionesController");
define('LST_PRODUCTOS', 'Productos/lst');
define('LST_PROMO', 'Promociones/listarPromociones');
define('RUTA_DELETE_PROMO', 'Promociones/eliminarPromocion');
define("RUTA_CREAR_PROMO", "/Promociones/crearPromociones");
define('RUTA_LST_PAQUETES', '/Promociones/listPaquetes');
define('RUTA_CREATE_PAQUETES', '/Promociones/createPaquete');
//msg
define('MSG_NO_PROMO', 'ERROR! Promoción no encontrada');
define('NO_NUMERICO', 'ERROR! la promoción no se encuentra con un número');
define('NO_PROMO_BD', 'ERROR! La promoción no se encuentra en la base de datos');
define('CANT_CARACTERES', 'Error Revise la cantidad de caracteres enviados');
define('REGISTER_CORRECTLY', 'Su Promocion se registro correctamente');
define('REGISTER_INCORRECTO', 'Su Promocion no se registro correctamente');
define('ELIMINADO_CORRECTO', 'Se elimino correctamente la promocion');
//NAMES
define('NAME_PRODUCT', 'pro_nombre');
define('INICIO_PROMO', 'inicio');
define('FIN', 'fin');
define('MENSAJE_PROMO', 'mensaje');
//
define('BREAD_MENU_PROMOCIONES', 'Menú Promociones');
define('BREAD_CREAR_PAQUETE', 'Crear Paquete');
define('NOTIFICATION_PAQUETE_CREADO', 'Paquete creado exitosamente');
define('NOTIFICATION_PAQUETE_NO_CREADO', 'Error,el paquete no se pudo crear');
define('TITLE_LISTAR_PROMOCIONES', 'Listar Promociones');
define('TITLE_LISTAR_PAQUETES', 'Listar Paquetes');
define('OPERACION', 'Operación');
define('VALOR_OPERACION', 'Valor de la operacion');
define('TIPO_PROMOCION', 'Tipo de promoción');
define('VALOR_PROMOCION', 'Valor de la promoción');
define('NOTIFICATION_PAQUETE_ELIMINADO', 'El paquete ha sido eliminado correctamente');