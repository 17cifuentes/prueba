<?php 
//RUTAS
define("PRODUCTO_INFO","Tamaño de imagen: 370px por 310px");
define("POPOVER_INFO","Informativo");
define("PRODUCTO_INFO_MAS","Con esta opción puedes agregar más imágenes para el producto ");
define("PRODUCTO_INFO_OPC_VENTA","Con esta opción puedes decidir si el producto aparece o no en la tienda.");
define("PRODUCTO_INFO_OPC_CARACTERISTICAS","Agrega las caracter&iacute;sticas del producto. Selecciona la caracter&iacute;stica digita un valor y clic en agregar");

define("PATH_CTRL_PRODUCTOS","\App\Http\Modulos\Productos\Controllers\ProductosController");
define("PATH_PRO_CREATEPROCCESS","/Productos/createProccess");
define("PATH_PRO_CREATE","/Productos/create");
define("PATH_PRO_IMG","Modulos/Productos/img/");

//ENTIDAD
define("TN_PRODUCTOS_SERVICIOS","tn_sft_productos_servicios");
define("TN_PRO_ID",     "id_pro");
define("TN_PRO_IMG",     "img");
define("TN_PRODUCTO_IMG",     "img_producto");
define("TN_PRO_PRECIO_VENTA",     "pro_precio_venta");
define("TN_PRO_CANTIDAD",     "pro_cantidad");
define("TN_PRO_NOMBRE",     "pro_nombre");
define("TN_PRO_DESCRIPCION",     "pro_descripcion");
define("TN_PRO_PRECIO_INTERNO",     "pro_precio_interno");
//MENSAJES Y TITULOS
define("MSG_ELI_CARAC",     "Proceso exitoso");
define("MSG_PRO_NOMBRE",     "Nombre producto");
define("MSG_PRO_REGISTRAR",     "Registrar producto");
define("MSG_PRO_GESTION",     "Gestión Productos");
define("MSG_VALOR_INTERNO",     "Valor interno");
define("MSG_VALOR_VENTA",     "Valor venta");
define("MSG_CANTIDAD",     "Cantidad");
define("MENU_PRODUCTOS",     "Menú productos");
define("CREAR_PRODUCTOS",     "Crear producto");
define("SFT_MOD_PRODUCTOS",     "Productos");
define("VAR_FORM_PRO",     "formProducto");
define("IMG_PRO_DEFAULT",     "defaulProdcuto.jpg");
define("PRO_VALOR",     "valor");
define("MSG_PRO_CORRECTO",     "Se registro el producto correctamente");
define("MSG_ERROR_PRO_CARACTERS",     "Error, revise la cantidad de caracteres enviados");
define("MSG_ERROR_PRO_INT_NULL",     "Error, el identificador no es un número o esta vació");
define("MSG_ERROR_PRO_ID_NUll",     "Error, No Llegó el identificador");
define("MSG_ERROR_PRO_ID_NUll_DB",     "Error, el identificador no existe en la base de datos");
define("ERROR_DATA", "Error datos incorrectos");
//define("MSG_ERROR", "Error al registrar");






