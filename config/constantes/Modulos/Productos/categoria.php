<?php
//ENTIDADES
define("TN_CAT_PRO",			"tn_sft_categoria_productos");
define("TN_CAT_PRO_NOMBRE",     "cat_nombre");
define("TN_CAT_PRO_DESCRIPCION","cat_descripcion");
define("TN_CAT_PRO_TIPO",     	"cat_tipo");
define("TN_CAT_PRO_ID",     	"cat_id");


//MENSAJES Y TITULOS

define("MSG_CAT_PRO",				"Gestión categorias");
define("MSG_ERROR_ORG_CAT",     	'¡Error! No se pudo organizar');
define("MSG_SUCC_ORG_CAT",     		'Procesado, orden actualizado');
define("MSG_ORG_CAT",     			'Organizar Categorías');
define("MSG_CARACTERISTICAS_PRO",	"Características del producto");
define("MSG_CREAR_PRO",     		"Crear producto");
define("MSG_CAT_PRO_DATOS",     	"Datos del producto");
define("MSG_CAT_PRO_IMG",     		"Imagen del producto");
define("MSG_CATEGORIA",     		"Categoria");
define("MSG_CATEGORIA_REGISTRAR",   "Registrar categoria");



//RUTAS
define("PATH_URL_CAT_PRO_CREATE",		"/Productos/createCategoria");
define("PATH_URL_ORG_CAT",     			'Productos/organizarCategorias');
define("PATH_CAT_PRO_BLOQUE",     		"Modulos.Productos.bloquesProducto");
define("PATH_MOD_ORG_CAT",     			'Modulos.Productos.organizarCategorias');

//VARIABLES
define("VAR_PRODUCTOS",			"productos");
define("CAT_PRO_OPCFUNCION",    "opcFuncion");
define("VAR_CARACTERISTICAS",   "características");
define("CAT_PRO_FILE_UPLOAD",   "file-upload");
define("VAR_ID_CAT",     		"id_categoria");
define("VAR_ID_ORD",     		"orden");
