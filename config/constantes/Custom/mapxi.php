<?php
define("MAPXI_LST_USUS_APP",     "Lista usuarios app");
define("MAPXI_MENU_USUS_APP",     "Menú usuarios app");
//TABLA PERSONA MAPXI
define("MAPXI_TN_PERSONA_ID",     "f001_id_persona");
define("MAPXI_TN_PERSONA_NOMBRES",     "f001_nombres");
define("MAPXI_TN_PERSONA_CORREO",     "f001_correo");
define("MAPXI_TN_PERSONA_PASS",     "f001_password");
define("MAPXI_TN_PERSONA_CEL",     "f001_celular");
define("MAPXI_PERSONA_NOMBRES",     "Nombres");
define("MAPXI_PERSONA_CORREO",     "Correo");
define("MAPXI_PERSONA_CEL",     "Celular");
//TABLA USUARIO MAPXI
define("MAPXI_TN_USU_CODPROMOCIONAL",     "f002_cod_promocional");
define("MAPXI_USU_CODPROMOCIONAL",     "Codigo promocional");
define("MAPXI_TN_USU_CREATE",     "f002_created_at");
define("MAPXI_USU_CREATE",     "Fecha registro");
define("MAPXI_TN_USU_ID",     "f002_id_usuario");
//TABLA ESTADOS MAPXI
define("MAPXI_TN_ESTADOS_NOMBRE",     "f009_descripcion");
define("MAPXI_ESTADOS_NOMBRE",     "Estado");
//PATH VIEWS
define("PATH_MAPXI_VIEW_USERAPP",     "Custom.Mapxi.Usuarios.lstUsuarios");
