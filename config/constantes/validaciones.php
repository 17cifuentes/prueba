<?php
define("CAMPO_REQUERIDO","Este campo es requerido");
define("CAMPO_NUM_MAYOR_1","Este campo debe ser igual o mayor a 1");
define("CAMPO_NUM_MAX_10","Número maximo 10");
define("CAMPO_NUM_MIN_5","Número minimo 5");
define("CAMPO_SELECT","Seleccione este campo");
define("CAMPO_NUM_MIN_10"," Número mínimo de caracteres, 6");
define("CAMPO_EMAIL","Utiliza un formato que coincida con el solicitado, mail@example.com");
define("CAMPO_DATA_MAYOR_HOY","La fecha debe ser mayor o igual a la actual");
define("CAMPO_MAX", "Número maximo de caracteres, 11");
define("SELECT_PRODUC","Selección de producto requerida");
define("CAMPO_MIN","numero minimo de caracteres, 1");

define("CAMPO_NUM_MIN_8","Número minimo de caracteres 8, Número maximo 13");
define("CAMPO_TEXT_MAX_200","Número maximo de caracteres 200");
define("ARCHIVO_REQUERIDO","Este archivo es requerido");


define("FECHA_INICIO_REQUERIDA","Fecha inicio de Promoción Requerida");
define("FECHA_FIN_REQUERIDA","Fecha fin de Promoción Requerida");
define("NUM_MAX_CARACTERES","Numero maximo de caracteres, 200");
define("CAMPO_NUM_MAX_12","Número maximo 12");
define("CAMPO_TEXT_MAX_250","Número maximo de caracteres 250");
