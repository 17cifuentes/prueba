<?php 
define("VALIDATE_DATA",
	[
		'name' => 'required|max:255',
		'nombre' => 'required|max:255',
		'horaInicio' => 'required',
		'horaFin' => 'required',
		'fechaFin' => 'required|date',
		'fechaInicio' => 'required|date',
		'tipoEvento' => 'required',
		'email' => 'required|max:255|email',
		'password' => 'required|min:6',
		'cliente_id' => 'required|integer',
		'cedula' => 'required|integer|min:8',
		'correo' => 'required|max:255|email',
		'direccion'=>'required|max:255|min:5',
		'telefono'=>'required|max:250|min:7',
		//Cursos Crear Curso
		'nombre_curso'=>'required|max:250|min:1',
		'descripcion_curso'=>'required|max:250|min:1',
		'encargado'=>'required|integer',
		'id_categoria'=>'required',
		'file-upload'=>'required',
		//Columnas Pmi @cristian campo
		'pmi_nombre'		=> 'required|max:50',
		'pmi_descripcion'	=> 'required|max:250',
		'fec_fin'	=> 'required|after:fec_inicio',
		//fin pmi
		//promociones
		'fin'	=> 'required|after or equal:inicio',
		//fin promociones

	]
);

?>