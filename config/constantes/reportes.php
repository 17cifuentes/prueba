<?php
//Rutas reportes
define("ROUTE_GENERATE_REPORTE","Reportes/crearReporte");
define("ROUTE_CREATE_REPORTE","core.Reportes.generarReporte");
define("ROUTE_TABLE_REPORTE","core.Reportes.tableReporte");

//Mensajes reportes 
define("MSG_SELECCION_REPORTE","Tipo de reporte no seleccionado, intentelo nuevamente'");
define("MSG_NO_GENERAR_REPORTE","No se puede generar este tipo de reporte, genére otra combinación o contacte al administrador");

//A
define("TITULO_GRAFICO","tituloGrafico");
define("CONTENEDOR_GRAFICO","contenedorGrafico");
define("DATA_GRAFICO","dataGrafico");
define("VALOR_GRAFICO","'valorGrafico'");
define('MENU_REPORTES', 'Menú de Reportes');
define('CREAR_REPORTE', 'Crear Reporte');