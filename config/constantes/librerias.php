<?php 
define("SFT_DATATABLE",'sftDataTable');
define("SFT_SELECT2",'sftSelect2');
define("SFT_LIB",'librerias');
define("SFT_LIB_MOD",'libMod');
define("SFT_GRAFICOS",'graficos');
define("SFT_FULLCALENDAR",'sftFullCalendar');
define("SFT_DATEPICKER","sftDatePicker");

define("SFT_LIBRERIAS_CORE",
	[
		SFT_DATATABLE => 
		[

			'js'=>'<script type="text/javascript" src="/core/plugins/dataTable/datatables.min.js"></script>',
			'css' => '<link rel="stylesheet" type="text/css" href="/core/plugins/dataTable/datatables.min.css"/>',
			'funcion'=>''
		],
		SFT_SELECT2 =>
		[
			'js' => '<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>',
			'funcion' => 'startSelect2',
			'css' => '<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />'
		],
		SFT_GRAFICOS =>
		[
			'js' => '
					<script src="https://code.highcharts.com/highcharts.js"></script>
					<script src="http://code.highcharts.com/modules/exporting.js"></script>
					<script src="http://code.highcharts.com/modules/export-data.js"></script>
					<script src="/core/js/Graficos/graficos.js"></script>',
			'css' =>'',
			'funcion'=>''			
		],
		SFT_FULLCALENDAR =>
		[
			'js' => '
					<script src="/core/plugins/fullcalendar/js/sftFullCalendar.js"></script>
					<script src="/core/plugins/fullcalendar/js/fullcalendar.js" type="text/javascript"></script>
					<script src="/core/plugins/fullcalendar/js/jquery-ui.custom.min.js" type="text/javascript"></script>
					',
					//
					
			'css' =>'
				<link href="/core/plugins/fullcalendar/css/fullcalendar.css" rel="stylesheet"/>
				<link href="/core/plugins/fullcalendar/css/fullcalendar.print.css" rel="stylesheet" media="print" />',
			'funcion'=>'initCalendar',
			'modals'=>
				[
					0=>'Modulos.Reservas.eventoReserva'
				]	
		],
		SFT_DATEPICKER =>
		[
			'js' => '<script src="/core/plugins/datepicker/js/sftDatePicker.js" rel="stylesheet"></script>
					<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
					<script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>',
			'css' => '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
			<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
     href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">',
			'funcion'=>'startDatapicker'

		]
	]
);

?>