<?php

define("MENU_USUARIOS",    "Menú usuarios");
define("EDIT_USER",		   "Editar Usuario");
define("LST_USUARIOS",     "Listar usuarios");
define("CREAR_USUARIOS",   "Crear usuario");
define("PERFIL_USUARIOS",  "Perfil de usuario");
define("EDITAR_USU",  "Editar");
define("VAL_DELETE",  "val-Eliminar");



//EXCEPT
define("EXC_REG_EXT",     	"registerExterno");
define("EXC_REC_PASS",     	"recuperarContrasena");
define("EXC_REC_PASS_PRO",  "recuperarContrasenaProccess");
define("EXC_CHA_PASS",     	"cambioPassword");
define("EXC_DATA",     		"data");
define("EXC_CRE_USU_EXT",   "crearUsuarioExterno");
define("EXC_GET_USU_ROL",   "getUsuRoles");
define("EXC_CRE_PRO",     	"createProccess");
define("EXC_GET_CAMP",     	"getCampos");


define("TN_USU_ID",     		"id");
define("TN_USU_IMG",     		"img");
define("TN_USU_NOMBRE",     	"name");
define("TN_USU_CORRE",     		"email");
define("TN_USU_PASSWORD",     	"password");
define("TN_USU_REMENBER_TOKEN", "remember_token");
define("TN_USU_CLIENTE_ID",     "cliente_id");
define("TN_USU_ROL_ID",     	"rol_id");
define("TN_USUARIO_ID",     	"usuario_id");
define("TN_USUARIO_ROL_NAME",     	"rol_nombre");


//rutas funciones
define(	"FUN_EDIT_USER",     	"Usuarios/editarUsuarios");
define("FUN_LST_USUARIOS",     	"Usuarios/lstUsuarios");
define("FUN_DELETE_USER",     	"Usuarios/eliminarUsuario");

define("PATH_USU_LST",     			"lstUsuarios");
define("PATH_ROUTE_USER",     		"Usuarios\UsuarioController");
define("PATH_USUARIO",     			"core.Usuarios");
define("PATH_URL_USU_CREAR",		"Usuarios/crearUsuario");
define("PATH_URL_USU_PERFIL",     	"Usuarios/perfil");

define("VAR_FORM_USU",     			"formUsu");
define("PATH_IMG_DEFAULT_USER",     "user.jpg");
define("TN_USERS",     				"users");
define("PATH_USU_CR",     			"core.Usuarios.crearUsuario");
define("PATH_USU_CR_PROCESS",    	"/Usuarios/createProccess");
define("USU_NOMBRE",     			"Nombre");
define("USU_CLIENTE",    			"Cliente");
define("USU_ROL",     				"Rol");
define("USU_HTML_FOTO",     		"html-Foto");

define("USU_CONTRASENA",     		"Contraseña");
define("USU_IMG",     				"Imagen");
define("USU_CORREO",     			"Correo");
define("USU_VIEW_LST_TITLE",     	"Lista de usuarios");
define("USU_VIEW_CR_SUBTITLE_1",    "Datos personales");
define("USU_VIEW_CR_SUBTITLE_2",    "Datos de sistema");


define("MSG_ERROR",     				"Error, revise los datos o contacte al Administrador");
define("MSG_ERROR_CHA_PASS",     		"Error al cambiar contraseña vuelva a realizar el proceso");
define("MSG_USU_EXITOSO",     			"Usuario registrado exitosamente");
define("MSG_USU_EXITOSO_ACTUALIZADO",	"Usuario actualizado exitosamente");
define("MSG_USU_ELIMINO_CODIGO",	"Error al eliminar codigo revise");
define("MSG_USU_CAMBIO_CONTRASEÑA",	"Cambio de contraseña Exitoso");












