<?php
define("TITLE_MODULOS_CREAR",     "Crear módulo");
define("TITLE_MODULOS_PERMISO_CREAR",     "Crear P. Módulos");
define("TN_MODULOS_CONSTANTE",     "mod_constante");
define("MODULOS_NOMBRE",     "Nombre");
define("LST_MODULOS",     "Lista de módulos");
define("MENU_MODULOS",     "Menú módulos");
define("MODULOS_DESCRICION",     "Descripción");
define("MODULOS_ICONO",     "Icono");
define("TN_MODULOS_NOMBRE",     "mod_nombre");
define("TN_MODULOS_ICONO",     "mod_icono");
define("TN_MODULOS_DESCRIPCION",     "mod_descripcion");
define("PATH_MODULO",     "Core.Modulos");
define("PATH_MODULO_LST",     "lstModulos");
define("TN_MODULOS",     "tn_sft_modulos");
define("TN_MODULOS_CLIENTE",     "tn_sft_det_mod_clientes");
define("TN_MODULOS_ID",     "id_modulo");
define("MODULOS_SFT",     "CORE SFT");
define("TN_MODULOS_SFT",     "mod_sft");
define("TITLE_MODULOS_SISTEMA",     "Modulo del sistema");
define("TN_TITLE_MODULOS_SISTEMA",     "mod_sft");
define("TN_DET_MOD_ROL",     "tn_sft_detalle_modulo_rol");
define("TN_MOD_ID_MOD",     "id_modulo");
define("TN_DET_MOD_ROL_ID",     "id_rol");
define("MSG_MODULO_NO_NUMERO",     "El módulo ingresado no es un número");
define("TITTLE_MODULOS", 'Modulo');


