<?php
return [ 
	'conf' => 
		[ 
			'PATH_CONS' => 'constants',
			'PACTH_CONS_MODULOS' => 'modulos'
		],
	'modulos' => 
		[ 
			'CONS_MODULOS_NOMBRE' => 'Nombre',
			'CONS_MODULOS_DESCRICION' => 'Descripción',
			'CONS_MODULOS_ICONO' => 'Icono',
			'CONS_TN_MODULOS_NOMBRE' => 'mod_nombre',
			'CONS_TN_MODULOS_ICONO' => 'mod_icono',
			'CONS_TN_MODULOS_DESCRIPCION' => 'mod_descripcion'
		]
	]; 