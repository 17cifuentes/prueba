@if(isset($librerias))
    @foreach($librerias as $lib)
        {!! SFT_LIBRERIAS_CORE[$lib]['css'] !!}
    @endforeach
@endif     
@if(isset($libMod))
    @foreach($libMod as $lib)
        {!! SFT_LIBRERIAS_MOD[$lib]['css'] !!}
    @endforeach
@endif 
</head>