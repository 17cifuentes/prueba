@if(isset($_SESSION[DATA]['layouts']))
@if(isset($_SESSION['data']["conf"][0]->title))
<?php
    $titulo = $_SESSION[DATA]["conf"][0]->title;
    $icono = $_SESSION[DATA]["conf"][0]->ico;
?>
@else
<?php 
    $titulo = $_SESSION[DATA][CLIENTE_TITLE];
    $icono = "";
?>
@endif
<title>{{ $titulo }}</title>
<link href="{{ asset($icono) }}" rel="shortcut icon" type="image/png">
@endif
