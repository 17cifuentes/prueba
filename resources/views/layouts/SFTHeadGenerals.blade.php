<!-- INICIO GENERALS CSS-->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> 
<!-- BOOTSTRAP CSS -->
<link href="{{ asset('core/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">    <!-- ANIMATE CSS CSS -->
<link href="{{ asset('core/plugins/animate-css/animate.css')}}" rel="stylesheet" />
<!-- NOTIFIACIONES CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- INICIO GENERALS CSS-->

