<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	@if(isset($_SESSION['data']["conf"][0]->title))
		<?php
    		$titulo = $_SESSION[DATA]["conf"][0]->title;
    		$icono = $_SESSION[DATA]["conf"][0]->ico;
		?>
	@else
		<?php 
    		$titulo = $_SESSION[DATA][CLIENTE_TITLE];
    		$icono = "";
		?>
	@endif
	<meta charset="UTF-8">
	<title>{{ $titulo }}</title>
	<link href="{{ asset($icono) }}" rel="shortcut icon" type="image/png">