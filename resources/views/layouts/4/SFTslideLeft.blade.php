
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background: url('{{ asset($_SESSION['data']['conf'][0]->img_slide)  }}') no-repeat no-repeat;      background-size: 100%;    
-moz-background-size: 100%;  
  -o-background-size: 100%;   
 -webkit-background-size: 100%;  
   -khtml-background-size: 100%;">
                <div class="image">
                    <img src="{{ asset(Auth::user()->img)}}" width="78" style="margin-top: -0.2em;margin-bottom: -0.5em" height="78" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} / {{ Auth::user()->rol->rol_nombre }}</div>
                    <div class="email">{{ Auth::user()->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ url('Usuarios/perfil') }}"><i class="material-icons">person</i>Perfil</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('Clientes/configuracion') }}"><i class="material-icons">settings</i>Configuracion</a></li>
                            <li><a href="{{ url('carritoCompras/tienda?id='.Auth::user()->cliente_id) }}"><i class="material-icons">shopping_cart</i>Tienda</a></li>
                            <li><a href="{{ url('Usuarios/ayuda') }}"><i class="material-icons">help_outline</i>Ayuda</a></li>
                            <li role="seperator" class="divider"></li>
                            <li>
                                <a class="dropdown-item" href="{{ url('Usuarios/logOut') }}">
                                        <i class="material-icons">input</i>Salir
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENÙ DE NAVEGACIÒN</li>
                    @foreach($_SESSION['data']["menu"] as $menu )

                        <li>
                            <a href="{{ url('Menu/'.$menu->id)}}" >
                                <i class="material-icons">{{ $menu->mod_icono }}</i>
                                <span>{{$menu->mod_nombre}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; Softheory 2019 <a href="javascript:void(0);">SFT - PlatForm</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        