    <nav style="background: {{ $_SESSION['data']['conf'][0]->navbar_color }}" class="navbar">

        <div class="container-fluid">
            <div class="navbar-header">
                @if($_SESSION['data']["conf"][0]->slide_rigth == 1)
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                @endif
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand "  href="{{url('home')}}">{{ $_SESSION['data']["conf"][0]->navbar_title }}</a>
<div class="collapse navbar-collapse" id="navbar-collapse">                                
                                <ul class="nav navbar-nav navbar-right"">

                    <!-- Call Search -->
                    @if($_SESSION['data']["conf"][0]->search == 1)
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    @endif
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    @if($_SESSION['data']["conf"][0]->navbar_notification == 1)
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                    </li>
                    @endif
                    <!-- #END# Notifications -->
                    @if($_SESSION['data']["conf"][0]->slide_rigth == 1)
                    <!-- #END# Tasks -->
                        <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                    @endif
                </ul>
            </div>
            
            <div class="collapse navbar-collapse" id="navbar-collapse">

            </div>
        </div>
    </nav>
    <!-- #Top Bar -->