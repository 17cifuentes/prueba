@include('layouts.4.SFTheader')
@include('layouts.4.SFTpreload')
@include('layouts.SFTConfTitle')
@include('layouts.4.SFTsearching')
@include('layouts.4.SFTnavbar')
@include('layouts.4.SFTslideLeft')
@include('layouts.4.SFTslideRigth')
<section class="content">
	@include('layouts.4.SFTbreadCumbs')
                <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                    <div class="card">
                        <div class="header">
                            <h3 id="coreTitlePage">
                                {{ $_SESSION['data']["title"] }}
                            </h3>
                            @if(isset($_SESSION[DATA]['opcFuncion']) and $_SESSION[DATA]['opcFuncion'] != "")
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                            @foreach($_SESSION[DATA]['opcFuncion'] as $key => $value)
                                                <li><a href="{{ url($value) }}">{{$key}}</a></li>
                                            @endforeach
                                    </ul>
                                </li>
                            </ul>
                            @endif
                        </div>
                        <div class="body">
                            <div class="" style="padding: 1em">
                            @include('layouts.validateData')
                            @yield('contenido')
                            </div>
                            <label for="email_address"></label>
                        </div>
                    </div>
                </div>
            </div>
</section>
@include('layouts.4.SFTfooter')
