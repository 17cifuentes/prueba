@include('layouts.SFTConfHead')
@include('layouts.SFTHeadGenerals')
<!-- INICIO PLATILLA 2-->

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link href="{{ asset('core/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('core/css/all-themes.css')}}" rel="stylesheet" />

<!-- FIRN PLATILLA 2-->

@include('layouts.SFTConfCss')
