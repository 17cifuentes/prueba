<div class="body" style="margin-top: -1em" >
    <ol class="breadcrumb" style="background: {{ $_SESSION['data']['conf'][0]->bradCumb_color }}">
        @if(isset($_SESSION['data']["bread"]))
            <?php $x=0;$con = count($_SESSION['data']["bread"]) ?>
            @foreach($_SESSION['data']["bread"] as $key => $value)
        	   <li>
        		  @if($x+1 == $con)
        		  	<a href="javascript::void()">{{$key}}</a>
        		  @else
        			<a href="{{ $value}}">{{ $key }}</a>
        		  @endif
        	   </li>
               <?php $x++ ?>
            @endforeach
        @endif
    </ol>
</div>