<!-- Right Sidebar -->
<style type="text/css">
    #rightsidebar{
        overflow-y: scroll;        
    }
</style>
        <aside id="rightsidebar" class="right-sidebar">
            <input type="hidden" id="cart" value="@if(isset($_SESSION['cart'])){{ $_SESSION['cart'] }} @endif">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">Carrito de compras</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content" >
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                   <div class="col-lg-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Producto</td>
                                    <td>Nombre</td>
                                    <td>Valor</td>
                                </tr>
                            </thead>
                            <tbody id="cartBody">
                                @if(isset($_SESSION['cart']))
                                    @foreach($_SESSION['cart']['productos'] as $producto)
                                        <tr>
                                            <td>
                                            <img src="{{ asset($producto[0]['img_producto']) }}" class="img img-responsive img-circle" style="width: 5em" />
                                        </td>
                                        <td>{{ $producto[0]['pro_nombre'] }}</td>
                                        <td>{{ $producto[0]['pro_precio_venta'] }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                @if(isset($_SESSION['cart']))
                                   <tr>
                                        <td>Cantidad: {{ count($_SESSION['cart']['productos']) }}</td>
                                        <td>Total: ${{number_format( $_SESSION['cart']['total']) }}</td>
                                    </tr> 
                                    <tr>
                                        <td colspan="3"><a href="" class="btn btn-success btn-block">Comprar ya!</a></td>
                                    </tr>
                                @endif
                            </tfoot>
                        </table>
                   </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>