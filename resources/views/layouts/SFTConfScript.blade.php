@if(isset($librerias))
    @foreach($librerias as $lib)
        @if(isset(SFT_LIBRERIAS_CORE[$lib]['modals']))
            @foreach(SFT_LIBRERIAS_CORE[$lib]['modals'] as $modal)
                @include($modal)
            @endforeach
        @endif
        {!! SFT_LIBRERIAS_CORE[$lib]['js'] !!}
        <script type="text/javascript">
            $(document).ready(function() {
                $("#funcionLib").val({!! SFT_LIBRERIAS_CORE[$lib]['funcion'] !!});
                fun = $("#funcionLib").val();
                if(fun){
                   getLib(fun); 
                }
            });                
        </script>
    @endforeach
@endif 
@if(isset($libMod))
    @foreach($libMod as $lib)
        @if(isset(SFT_LIBRERIAS_MOD[$lib]['modals']))
            @foreach(SFT_LIBRERIAS_MOD[$lib]['modals'] as $modal)
                @include($modal)
            @endforeach
        @endif
        {!! SFT_LIBRERIAS_MOD[$lib]['js'] !!}
        <script type="text/javascript">
            $(document).ready(function() {
                $("#funcionLibMod").val({!! SFT_LIBRERIAS_MOD[$lib]['funcion'] !!});
                fun = $("#funcionLibMod").val();
                if(fun){
                   getLib(fun); 
                }
            });                
        </script>
    @endforeach
@endif
@if(isset($_SESSION['data']["conf"][0]->personalizar) and $_SESSION['data']["conf"][0]->personalizar != null)
    <?php $pers = json_decode($_SESSION['data']["conf"][0]->personalizar,true); 
    ?>
    
    @if(isset($pers[0]))
        @foreach($pers[0] as $key => $per)
            @if($key == "fonts")
                @if(isset(CONF_PERSONALIZAR[$key][$per]))
                    {!! CONF_PERSONALIZAR[$key][$per]['css'] !!}
                    {!! CONF_PERSONALIZAR[$key][$per]['js'] !!}
                    {!! CONF_PERSONALIZAR[$key][$per]['html'] !!}
                    <input type="hidden" id="{{'id'.$key}}" value="{!!CONF_PERSONALIZAR[$key][$per]['nombre']!!}">
                @endif
            @endif
            @if($key == "hides")
                <input type="hidden" id="hides" value="{{json_encode($per)}}">
                <script type="text/javascript">
                        $(document).ready(function() {
                            hideItems($("#hides").val());
                        }); 

                </script>
            @endif
        @endforeach
    @endif
@endif
<input type="hidden" value="{{ asset('')}}" id="baseUrl">
<input type="hidden" id="funcionLibMod" value="">
<input type="hidden" id="funcionLib" value="">
</body>
@if(isset($_SESSION['data']["msg"]))
    <input type="hidden" id="msg" value="{{ $_SESSION['data']['msg'] }}">
    <input type="hidden" id="typenoti" value="{{ $_SESSION['data']['type'] }}">
    <script type="text/javascript">
        showNotification($("#msg").val(),$("#typenoti").val());
    </script>
    <?php unset($_SESSION['data']["msg"]) ?>
@endif 

@if (Session::has('error'))
    <input type="hidden" id="msg" value="{{ Session::get('error') }}">
    <input type="hidden" id="typenoti" value="error">
    @if(isset($_SESSION['data']['shorUrl']))
        <input type="hidden" id="shorUrlnoti" value="{{ $_SESSION['data']['shorUrl'] }}">
    <?php unset($_SESSION['data']['shorUrl']) ?>
    @endif
    <script type="text/javascript">
        showNotification($("#msg").val(),$("#typenoti").val(),$("#shorUrlnoti").val());
    </script>
@endif
@if (Session::has('success'))
    <input type="hidden" id="msg" value="{{ Session::get('success') }}">
    <input type="hidden" id="typenoti" value="success">
    @if(isset($_SESSION['data']['shorUrl']))
        <input type="hidden" id="shorUrlnoti" value="{{ $_SESSION['data']['shorUrl'] }}">
    <?php unset($_SESSION['data']['shorUrl']) ?>
    @endif
    <script type="text/javascript">
        showNotification($("#msg").val(),$("#typenoti").val(),$("#shorUrlnoti").val());
    </script>
@endif 
@if(isset(Auth::user()->id))
    <input type="hidden" id="usuId" value="{{ Auth::user()->id }}">
@else
    <input type="hidden" id="usuId" value="">
@endif
<input type="hidden" id="idCliente" value="{{$_SESSION['data'][CLIENTE_CONF][0]->id}}">
<input type="hidden" id="idHides" value="">
<script type="text/javascript">
    function getLib(funcion){funcion();}
    $(function(){
        urlBase = $("#baseUrl").val();
    })
    function hideItems(ids){
        $("#idHides").val(ids);
        per = JSON.parse(ids);
        $( per ).each(function(indice, elemento) { 
            $("#"+elemento).addClass("hide");  
        });
        ordenarPlandilla();
    }
</script>
</html>