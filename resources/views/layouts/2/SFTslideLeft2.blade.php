        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo" style="background: {{ $_SESSION[DATA]["conf"][0]->navbar_color  }}">
                <a href="{{url('home')}}">
                    <center><img style="height: 5.4em;" src='{{ asset($_SESSION[DATA]["conf"][0]->img_logo)}}'></center>
                </a>
            </div>
            
                <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        @foreach($_SESSION['data']["menu"] as $menu )
                        <li>
                            <a href="{{ url('Menu/'.$menu->id)}}">
                                <i class="material-icons">{{ $menu->mod_icono }}</i>{{$menu->mod_nombre}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="legal">
                <div class="copyright">
                    <hr>&copy; Softheory 2019 <a href="javascript:void(0);">SFT-PlatForm</a>.<br><b>Versón:</b> 1.0.0<hr>
                </div>
            
            </div>

            </div>
            
        </aside>