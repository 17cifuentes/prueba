
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        @foreach($_SESSION['data']["menu"] as $menu )
                        <li>
                            <a href="{{ url('Menu/'.$menu->id)}}">
                                <i class="material-icons">{{ $menu->mod_icono }}</i>{{$menu->mod_nombre}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </nav>
            </header>