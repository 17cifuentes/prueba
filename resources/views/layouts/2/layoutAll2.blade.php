@include('layouts.SFTHeadGenerals')
@include('layouts.SFTConfHead')
@include('layouts.SFTConfCss')
@include('layouts.SFTConfTitle')
@include('layouts.2.SFTheader2')
@include('layouts.2.SFTslideMobile2')
@include('layouts.2.SFTslideLeft2')
@include('layouts.2.SFTnavBar2')
<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            @include('layouts.SFTbreadCumbs')
            <div class="container-fluid">
                <div class="row">
                    <div class="au-card recent-report col-lg-12">
                  		@include('layouts.validateData')
                        @yield('contenido')          
                  	</div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.SFTGeneralScript')
@include('layouts.SFTCoreScript')
@include('layouts.SFTConfScript')
@include('layouts.2.SFTfooter2')

