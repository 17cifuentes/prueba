@if(isset($_SESSION['data']))
    <!-- Fontfaces CSS-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Main CSS-->
    <link href="{{ asset('core/Estructuras/2/css/theme.css')}}" rel="stylesheet" media="all">
</head>
<body class="">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="{{url('home')}}">
                            <center><img width="100" style="border-radius: 15px" src="{{asset($_SESSION[DATA]["conf"][0]->img_logo)}}" alt="Cool Admin" /> {{ $_SESSION[DATA]["conf"][0]->navbar_title}}</center>
                        </a>
                        <!-- <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button> -->
                        <span style="margin-right: 32px" class="hamburger hamburger--slider">Menú
                                <span class="hamburger-inner"></span>
                        </span>
                    </div>
                </div>
            </div>
@endif