@if(isset($_SESSION[DATA]))
@include('layouts.4.SFTheader')
@if(count($_SESSION['data'][CLIENTE_CONF]) > 0)
<body style="background: {{ $_SESSION['data']['conf'][0]->navbar_color }}" id="bodylogin">
	<style type="text/css">#contenidoLogin{margin-top: 8%;}</style>
	<div class="col-lg-6" ><br><br>
		<center>
			<section id="contenidoLogin">
				<div class="container-fluid" style="margin-top: -1.5em">
    				<div class="block-header">
        				<h3 style="color: white">{{ $_SESSION['data']["msg"] }}</h3>
    				</div>
				</div>
				@yield('contenido')
			</section>
		</center>
    </div>
    <div class="col-lg-6 image-blurred-edge visible-lg"  >
		<!--<img src="{{ asset($_SESSION['data']['conf'][0]->img_login) }}" class="img-fluid img-responsiv ">--><br>
    </div>
</body>
<style>
    .image-blurred-edge { 
    background-image: url('{{ asset($_SESSION['data']['conf'][0]->img_login) }}'); 
    height:48em;
    /* you need to match the shadow color to your background or image border for the desired effect*/ 
    box-shadow-left: 0 0 18px 18px  {{ $_SESSION['data']['conf'][0]->navbar_color }} inset; 
      background-size: 100%;    
-moz-background-size: 100%;  
  -o-background-size: 100%;   
 -webkit-background-size: 100%;  
   -khtml-background-size: 100%; 

} 
</style>
@else
<body class="bg-indigo" id="bodylogin">
	<style type="text/css">#contenidoLogin{margin-top: 8%;}</style>
	<div class="col-lg-6" ><br><br>
		<center>
			<div class="card" style="padding:2em;margin-top: 7em">
			<section id="contenidoLogin" style="">
				<div class="container-fluid" style="margin-top: -1.5em">
    				<div class="block-header">
        				<h3 style="color: black">No se encontro el cliente, contacta al administrador</h3>
    				</div>
				</div>
			</section>
		</div>
		</center>
    </div>
    
</body>
@endif
@include('layouts.4.SFTfooter')
@else
<input type="hidden" id="url" value="dd">
<script type="text/javascript">
	window.location='/'; 
</script>
@endif