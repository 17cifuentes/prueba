<!--CORE JS -->
<!--NOTIFICACIONES -->
<script src="{{ asset('core/js/notifications.js')}}"></script>
<script src="{{ asset('core/plugins/notify/notify.js')}}"></script>
<script type="text/javascript" src="{{ asset('core/js/generals/alert.js')}}"></script>
<!--ADMIN -->
<script src="{{ asset('core/js/admin.js')}}"></script>
<!-- SCRIPT COMPONENTE FORMULARIO -->
<script src="{{ asset('core/js/Formularios/formularios.js')}}"></script>
<script src="{{ asset('core/js/Formulario.js')}}"></script>
<!-- SCRIPT COMPONENTE TABLE -->
<script type="text/javascript" src="{{ asset('core/js/Table/table.js') }}"></script>
<!-- SCRIPT DE VALIDACIONES FORMULARIOS -->
<script type="text/javascript" src="{{ asset('core/js/generals/validation.js')}}"></script>
<!-- SCRIPT DE COMPONENTE CAMPOS -->
 <script src="{{ asset('core/js/RolCampos/rolCampos.js')}}"></script>
 <!-- SCRIPT INICIO DE LIBRERIAS -->
 <script src="{{ asset('core/js/startLib.js')}}"></script>
 <!--SCRIPT DE USUARIO-->
  <script src="{{ asset('core/js/usuario/usuario.js')}}"></script>	
<!-- FIN CORE JS -->
