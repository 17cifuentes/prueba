@extends('layouts.SFTlayoutsLogin')
@section('contenido')

<div class="col-lg-12">
   <div class="card" style="  border-radius: 35px;    box-shadow: 5px 5px 15px 5px #3333338f;">
       
       <div class="body">
	<h3 > Señor(a) {{ $usuario->name }}, ah solicitado cambio de contraseña</h3> <br><br>
	<h5 >Digite su nueva contraseña: </h5><br>

<form method="POST" action="{{url('Usuarios/cambioPassword')}}" id="cambioPassword" enctype="multipart/form-data" data-accion="Cambiar Contraseña" class="needs-validation" novalidate>
   
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <input type="hidden" name="id" value="{{$usuario->id}}">
   <input type="hidden" name="cliente_id" value="{{$usuario->cliente_id}}">
   <input type="hidden" name="codigo" value="{{$codigo}}">

   <div class="col-md-6 mb-3 form-line">
        <i class="material-icons" style="color:green">security</i>
        <label for="validationCustom01">Nueva Contraseña:</label>
        <input type="password" value="" id="password" class="form-control" name="password" maxlength="40" required >
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-6 mb-3 form-line">
        <i class="material-icons" style="color:red">lock</i>
        <label for="validationCustom01" >Confirmar Contraseña:</label>
        <input type="password" value="" id="confPasword" class="form-control" name="confPasword" maxlength="40" required >
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
        <br>	
    </div>

    <div class="col-xs-12">
        <button class="btn bg-green waves-effect" type="submit">Recuperar</button>
    </div>
</form>    
@stop