@extends('layouts.SFTlayoutsLogin')
@section('contenido')
    <div class="col-lg-12">
        <div class="card" style="  border-radius: 35px;    box-shadow: 5px 5px 15px 5px #3333338f;">
            <div class="body">
                <form method="POST" action="{{url('Usuarios/recuperarContrasenaProccess')}}" id="recuperarContraseña" enctype="multipart/form-data" data-accion="Recuperar Contraseña" class="needs-validation" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                    
                    <h1><legend>Recuperar Contraseña</legend></h1>
                    <div>Escriba su dirección de correo electrónico registrada.</div>
                    <br>

                    <div class="col-md-12 mb-3 form-line">
                        <i class="material-icons">mail</i>
                            <label for="validationCustom01">Email:</label>
                            <input type="email" value="" class="form-control" name="email" maxlength="255" required >
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                    </div>
                <div class="col-xs-12">
                    <button class="btn bg-green waves-effect" type="submit">Recuperar</button>
                    <a href="{{ url('ingresar/7/0')}}" class="btn bg-red waves-effect">Regresar</a>
                </div>
                <div class="col-xs-12">
                    
                </div>
            </div>

                    
                </form>
            </div>
        </div>
    </div>

@stop

