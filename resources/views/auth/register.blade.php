@extends('layouts.SFTlayoutsLogin')
@section('contenido')
    <div class="col-lg-10">
        <div class="card" style="  border-radius: 25px;    box-shadow: 5px 5px 15px 5px #3333338f;">
            <div class="body">
                <form method="POST" action="{{url('Usuarios/registerExternoProcess')}}" id="registrar">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="remember_token" value="{{ csrf_token() }}">
                        @csrf
                    <div class="msg">Registrate:</div><br>

                     <div class="col-md-12 mb-3 form-line">
                        <i class="material-icons">person</i>
                            <label for="validationCustom01">Nombre:</label>
                            <input type="text" value="" class="form-control" name="name"  maxlength="255" required>
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                     <div class="col-md-12 mb-3 form-line">
                        <i class="material-icons">mail</i>
                            <label for="validationCustom01">Email:</label>
                            <input type="email" value="" class="form-control" name="email" maxlength="255" required >
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                    <div class="col-md-12 mb-3 form-line">
                        <i class="material-icons">lock</i>
                        <label for="validationCustom01">Contraseña:</label>
                        <input type="password"  id="password" value="" class="form-control" maxlength="255" name="password" minlength="5" maxlength="255" required>
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>
                    </div>
                    
                   

                    <div class="col-md-12 mb-3 form-line">
                        <i class="material-icons">folder_shared</i>
                        <label for="validationCustom01">Rol:</label>
                        <select value="" name="rol_id" class="form-control" required>
                        <option value="">seleccione</option>

                        @foreach ($Roles as $rol)   
                          <option value="{{$rol->id}}">{{$rol->rol_nombre}}</option>
                        @endforeach

                    <div class="valid-feedback">Campo seleccionado</div>
                    <div class="invalid-feedback">
                        {{CAMPO_REQUERIDO}}
                    </div>
                </div>
                     </select>
                </div> 

                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn bg-green waves-effect" type="submit">
                                Registrar   
                            </button>
                            <a href="{{url('/ingresar/'.$id.'/0')}}" class="btn bg-red waves-effect" type="submit">
                                Volver   
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@stop

