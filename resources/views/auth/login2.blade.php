@if(isset($_SESSION[DATA]))
@extends('layouts.SFTlayoutsLogin')
@section('contenido')
@include('core.Usuarios.modalCrearUsuarioExterno')
    <div class="col-lg-10">
        <div class="card" style="  border-radius: 25px;    box-shadow: 5px 5px 15px 5px #3333338f;">
            <div class="body">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                    <div class="msg">Ingresa con tus credenciales:</div><br>
                    
                    <label for="email" class="col-sm-3 col-md-3 col-lg-3">Correo: </label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <label for="password" class="col-sm-3 col-md-3 col-lg-3">Contraseña: </label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink ocultar">
                            <label for="rememberme">Recordar</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-green waves-effect" type="submit">
                                Ingresar
                            </button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            
                        </div>
                        @if(isset($id))
                        <div class="container">
                            <div class="row" style="margin-bottom:9%">
                               
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12 md-mt-60 sm-mt-60">
                                <a  href="{{url('/carritoCompras/tienda?id='.$id)}}" class="btn btn-default"><i class="material-icons" >skip_previous</i>Ir a tienda </a>    
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12 md-mt-60 sm-mt-60">
                                <a class="btn btn-default" onclick="registroShopCartAjax()">
                                <i class="material-icons">assignment</i> Registrarse </a>    
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12 md-mt-60 sm-mt-60">
                                <a href="{{url('Usuarios/recuperarContrasena')}}" class="btn btn-default"><i class="material-icons">history</i>Olvido contraseña</a>    
                            </div>
                            </div>
                        </div>

                        
                        @endif

                    </div>
                </form>
                
            </div>
        </div>
    </div>

@stop
@endif
