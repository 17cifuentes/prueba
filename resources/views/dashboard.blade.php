@extends($_SESSION[DATA]['layouts'])
@section('contenido')

	<div  class="coll-lg-12 col-md-12 col-xs-12">
		<center><h6>Inicio2 - Dashboard</h6></center>
		<input type="hidden" id="numCharts" value="{{ count($reportes ) }}">
		@foreach($reportes as $key => $r)
			<div class="col-lg-{{$r['tamano']}} col-xs-{{$r['tamano']}} col-md-{{$r['tamano']}}">
				<div class="" id="{{$key}}">
	  				<div class="panel panel-default active">
	    				<div class="panel-heading">
	      					<h4 class="panel-title">
	        					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{$key}}">
	          						<i class="fa fa-caret-right"></i> {{$r['titulo']}}
	        					</a>
	      					</h4>
	    				</div>
	    				<div id="collapseOne{{$key}}" class="panel-collapse collapse in">
	      					<div class="panel-body">
	        					<div id="contenedor{{$key}}" class="col-lg-12"></div>	
	      					</div>
	    				</div>
	  				</div>
	  			</div>
			</div>
		<input type="hidden" id="tituloGrafico{{$key}}" value="{{ $r['titulo'] }}">
		<input type="hidden" id="tipoGrafico{{$key}}" value="{{ $r['tipo'] }}">
		<input type="hidden" id="contenedorGrafico{{$key}}" value="contenedor{{$key}}">
		<input type="hidden" id="valorGrafico{{$key}}" value="{{ json_encode($r['data']['valores']) }}">
		<input type="hidden" id="dataGrafico{{$key}}" value="{{ json_encode($r['data']['titulos']) }}">
		@endforeach
		<script type="text/javascript">
			window.addEventListener('load', getData, false);
			function getData(){
				getChartDataDash($("#numCharts").val());
			}
		</script>	
@stop