@if(isset($_SESSION[DATA]['layouts']))
@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="row clearfix">
    @foreach($_SESSION['data']["funciones"] as $funcion)
    <?php $roles = json_decode($funcion->roles); $rolId = Auth::user()->rol_id; ?>

    @if($_SESSION['data']["conf"][0]->estructura_admin == 2)
    @foreach($roles as $rol)
    @if( $rol == $rolId )
    <div class="col-lg-6">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="au-card-title" style="background-image:url('{{ asset($_SESSION['data']['conf'][0]->img_slide)  }}');">
                <div class="bg-overlay bg-overlay--blue" style='background-color:{{ $_SESSION[DATA]["conf"][0]->bradCumb_color  }}'></div>
                <h3><i class="material-icons">{{ $funcion->fun_icono }}</i>{{ $funcion->fun_menu_nombre }}</h3>
            </div>
            <a href="{{ url($funcion->fun_ruta)}}" class="btn btn-block au-btn au-btn-load js-load-btn">Ingresar</a>
        </div>
    </div>
    @endif
    @endforeach 
    @else
         
         @foreach($roles as $rol)
                @if( $rol == $rolId )
                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
                    <div class="info-box-2 bg-{{ $funcion->fun_color }} hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">{{ $funcion->fun_icono }}</i>
                        </div>
                        <a href="{{ url($funcion->fun_ruta)}}">
                            <div class="content">
                                <div class="text">Ingresar a:</div>
                                    <div class="number" style="font-size: 20px">{{ $funcion->fun_menu_nombre }}</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endif
             @endforeach 
    @endif
    @endforeach
</div>
@stop
@endif