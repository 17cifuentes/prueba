@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
    <!-- Creación de formulario con datos de la bd-->
    <form action="@if($form[0]->action != ''){{ url($form[0]->action)}}@endif" enctype="multipart/form-data" method="{{$form[0]->method}}" data-accion="{{$form[0]->nombre_form}}" id="formModel" data-url="{{ $form[0]->action }}" data-idform="{{ $form[0]->id_form }}" class="needs-validation" novalidate data-id="{{ $form[0]->id }}">
    	<!-- Json de los campos del formulario-->
    	@if($form[0]->method == 'post' or $form[0]->method == 'POST')
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	@endif
    	<input type="hidden" id="camposForm" value="{{ $campos }}">
    	<input type="hidden" id="idForm" name="idForm" value="{{ $form[0]->id }}">
    	@if(isset($dataForm))<input type="hidden" id="dataForm" value="{{ json_encode($dataForm) }}">@endif
    	<input type="hidden" id="baseUrl" value="{{ url('') }}">
		<!-- Adicciòn de token si es post -->
		@if($form[0]->method == 'POST') <input type="hidden" name="_token" value="
		{{ csrf_token() }}"> @endif
		<!-- Adicciòn de campos desde bd por funcion js -->
		<div id="contenCampos">
			<div class="col-lg-6 col-md-6">
	    		<label for="FORM-NOMBRE">Nombre</label>
	        	<input type="text" value="" class="form-control" name="fun_nombre" required="" id="fun_nombre">
	        	<div class="valid-feedback">Ok!</div>
	        	<div class="invalid-feedback">
	           		{{CAMPO_REQUERIDO}}
	        	</div>
	   		</div>
	   	</div>
	   	<!-- Opcion de boton sibmit -->
		<div id="btnFormSubmit" class="col-md-12 col-lg-12">
			<center>
				<br><input id="btnSubmitModel" type="submit" class="btn btn-success" value="{{$form[0]->nombre_form}}">
	        </center>	
		</div>
	</form>
</div>
<script type="text/javascript" src="{{ $form[0]->funcion_js }}"></script>
<script type="text/javascript">
	window.addEventListener('load',init, false);
	function init() {	
		addCamposForm();
		main();
	}
</script>
@stop