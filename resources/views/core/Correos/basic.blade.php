

<!DOCTYPE html>
<html>
<head>

	<title>{{ $data['titulo'] }}</title>
</head>
<body>
	
	<div class="entireBody">
		
		<h1 id="titulo">{{ $data['titulo'] }}</h1>
		
		<tbody>
			<tr>
				<td>
					{{ $data['cuerpo'][0] }}
				</td>
			</tr><br>
			<tr>
				<td>
					{{ $data['cuerpo'][1] }}
				</td>
			</tr><br>
			<tr>
				<td>
					{{ $data['cuerpo'][2] }}
				</td>
			</tr><br>
			<tr>
				<td>
					{{ $data['cuerpo'][3] }}
				</td>
			</tr>
			<tr>
				<td>
					{{ $data['cuerpo'][4] }}
				</td>
			</tr>
		</tbody>

	</div>
	
</body>
</html>
