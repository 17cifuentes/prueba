@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<form enctype="multipart/form-data" method="POST" action="editarModuloProcess" id="formEditarFuncion" class="needs-validation" data-accion="editar el modulo" novalidate>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" class="form-control" id="id" name="id" value="{{$lsModulo[0]->id}}" required=""> 

    <legend>Datos del modulo</legend>

    <div class="col-md-6 mb-3 form-line">      	
    	<label for="validationCustom01">Nombre* </label>
        <input maxlength="250" minlength="1" type="text" class="form-control" id="mod_nombre" name="mod_nombre" value="{{$lsModulo[0]->mod_nombre}}" required="">           	
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>	
	
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Descripción* </label>
        <textarea type="text" class="form-control" id="mod_descripcion" name="mod_descripcion" value="{{$lsModulo[0]->mod_descripcion}}" required="" maxlength="250" minlength="1">{{$lsModulo[0]->mod_descripcion}}</textarea>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}	
        </div>	
    </div>
	
    <div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Icono*</label>
        <input maxlength="250" minlength="1" type="text" class="form-control" id="mod_icono" name="mod_icono" value="{{$lsModulo[0]->mod_icono}}" required="">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>             	
    </div>	
	
    <div class="col-md-6 mb-3 form-line">
    	<label for="validationCustom01">Modulo del sistema*</label>
        <select name="mod_sft" class="form-control show-tick"  required="">
            <option id="seleccione"  value="">Seleccione</option>
            <option  value="S" @if($lsModulo[0]->mod_sft == 'S' ) selected @endif>Si</option>
            <option value="N" @if($lsModulo[0]->mod_sft == 'N' ) selected @endif>No</option>
        </select> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

<div class="col-lg-12">
    <center>
        <button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar módulos
        </button>
    </center>
</div>
                            
</div>
</form>
@stop
