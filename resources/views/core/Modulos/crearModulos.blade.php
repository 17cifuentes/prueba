@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<form id="idFormRoles" enctype="multipart/form-data" data-accion="crear el modulo" class="needs-validation" novalidate method="post" action="{{url('Modulos/createModuloProcess')}}">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
    
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Nombre*</label>
        <input maxlength="250" minlength="1" required="" class="form-control show-tick" type="text" name="mod_nombre">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>  

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Descripción*</label>
        <textarea required="" class="form-control show-tick" type="text" name="mod_descripcion" maxlength="250" minlength="1"></textarea>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Icono*</label>
        <input maxlength="250" minlength="1" required="" class="form-control show-tick" type="text" name="mod_icono">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>  
    </div>

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Modulo del Sistema* </label>
        <select required="" name="mod_sft" class="form-control show-tick"  id="selectFunciones" >
            <option id=""  value="">Seleccione</option>
            <option id=""  value="S">Si</option>
            <option id=""  value="N">No</option>
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>
    </div>

    
    <div class="col-lg-12"><center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear módulo</button></center></div>
    
</form>
    <label for="email_address"></label>
@stop