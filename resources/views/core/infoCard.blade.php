<div class="{{ $col }} " >
        <a href="{{ $url }}" style="text-decoration: none">
        <div class="info-box bg- hover-zoom-effect" style="background: {{ $color }}">
            <div class="icon">
                <i class="material-icons">{{ $ico }}</i>
            </div>
            <div class="content">
                <div class="text" style="color: white">{{$subtitle}}</div>
                <div><h4 style="color: white">{{$title}}</h4></div>
            </div>
        </div>
    </a>
</div>


