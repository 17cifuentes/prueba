@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form id="idPFunciones" enctype="multipart/form-data" class="needs-validation" data-accion="Crear P. funciones" method="POST" action="{{url('Permisos/createPermisosFunProcess')}}" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Cliente*</label>
            <select name="id_cliente" class="form-control show-tick basic-single"  required="" onchange="cargarRolModulo(this.value)">
                <option id="seleccione"  value="">Seleccione</option>
                @foreach($clientes as $cliente)
                    <option  value="{{ $cliente->id }}">{{ $cliente->cliente_nombre }}</option>
                @endforeach
            </select>
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>
        </div> 

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Rol* </label>
            <select name="rol_id" id="selectRole" class="form-control show-tick"  required="">
                <option id="seleccione"  value="">Seleccione</option>
            </select> 
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Modulo* </label>
            <select name="id_modulo" class="form-control show-tick"  id="selectModulo" onchange="cargarRolModuloFun(this.value)" required="">
                <option id="seleccione"  value="">Seleccione</option>
            </select> 
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Funciones*</label>
                <select  name="funcion_id" class="form-control show-tick"  id="selectFunciones" required="">
                    <option id="seleccione"  value="">Seleccione</option>
                </select> 
                <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_SELECT}}
                </div>
        </div>
        <div class="col-lg-12"><center><input type="submit" class="btn btn-success">


</form>
<label for="email_address"></label>
</div>

@stop
