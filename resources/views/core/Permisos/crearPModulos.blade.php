@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form id="idFormPModulos" class="needs-validation" enctype="multipart/form-data"  method="POST" data-accion="Crear P. Modulo" action="{{url('Permisos/createPModulosProcess')}}" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Cliente*</label>
        <select name="id_cliente" class="form-control show-tick sftSelect2"  required="" >
            <option id="seleccione"  value="">Seleccione</option>
            @foreach($clientes as $cliente)
                <option  value="{{ $cliente->id }}">{{ $cliente->cliente_nombre }}</option>
            @endforeach
        </select> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
        {{CAMPO_SELECT}}
    </div> 
    </div>  
        

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Modulo*</label>
             <select name="id_modulo" class="form-control show-tick"  id="selectModulo" required="">
                <option id="seleccione"  value="">Seleccione</option>
               @foreach($modulos as $mod)
                    <option  value="{{ $mod->id }}">{{ $mod->mod_nombre }}</option>
                @endforeach
            </select> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>
    </div>
    <div class="col-lg-12"><center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear P. modulo</button></center></div>
</form>
<label for="email_address"></label>

@stop