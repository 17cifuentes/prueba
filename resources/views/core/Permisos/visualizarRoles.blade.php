@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	 <form method="post" action="{{url('Permisos/updateFunRoles')}}">
	 	<input type="hidden" name="idFuncion" value="{{$idFuncion}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	 	<input type="submit" class="btn btn-success" value="Actualizar rol">
	 	{!! $table !!}
	 </form>
	@if(isset($opc) || isset($check))
		<input type="hidden" id="ruta">
		<input type="hidden" value="{{ $opc }}" id="opcTable">
		<input type="hidden" name="" value="{{ $check }}" id="visualizar">
		<script type="text/javascript">
			window.addEventListener('load', calculos, false);
			function calculos() {	
				putChecksRoles();
				pintarOpc();
				
			}
		</script>
	@endif
@stop