@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<form id="idFormRoles" enctype="multipart/form-data" data-accion="crear rol" class="needs-validation" novalidate method="post" action="{{url('Roles/crearRolProcess')}}">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
    
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Rol nombre*</label>
        <input maxlength="250" minlength="1" required="" class="form-control show-tick" type="text" name="rol_nombre">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>  

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Descripción*</label>
        <textarea required="" class="form-control show-tick" type="text" name="rol_descripcion" maxlength="250" minlength="1"></textarea>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Cliente*</label>
        <select required="" name="cliente_id" class="form-control show-tick"  id="selectModulo" onchange="">
            <option id="seleccione"  value="">Seleccione</option>
            @foreach($dataClientes as $cliente)
            <option id="seleccione"  value="{{$cliente->id}}">{{$cliente->cliente_nombre}}</option>
            @endforeach
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>  
    </div>

    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Estado*</label>
        <select required="" name="estado_roles" class="form-control show-tick"  id="selectFunciones" >
            <option id=""  value="">Seleccione</option>
            <option id=""  value="Activo">Activo</option>
            <option id=""  value="Inactivo">Inactivo</option>
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>
    </div>

    
    <div class="col-lg-12"><center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear rol</button></center></div>
    
</form>
    <label for="email_address"></label>
@stop