@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form id="forEditarRoles" enctype="multipart/form-data" class="needs-validation" method="POST" data-accion="Editar Roles" action="editarRolesProcess" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">


	<legend>Editar Roles</legend>
    <input  type="hidden" class="form-control" id="id" name="id" value="{{$rol->id}}" required=""> 

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Rol*</label>
        <input maxlength="250" minlength="1" type="text" class="form-control" id="id" name="rol_nombre" value="{{$rol->rol_nombre}}" required="">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>  
    </div>	
	
    <div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Descripción*</label>
        <textarea type="text" class="form-control" id="rol_descripcion" maxlength="250" minlength="1" name="rol_descripcion" value="{{$rol->rol_descripcion}}" required="">{{$rol->rol_descripcion}}</textarea>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>  
    </div>
    		
	
    <div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Cliente*</label>
        <select name="cliente_id" class="form-control show-tick"  required="">
        <option id="seleccione"  value="">Seleccione</option>
        @foreach($clientes as $cliente)
            <option  value="{{$cliente->id}}"  @if($rol->cliente_id == $cliente->id) selected @endif>{{$cliente->cliente_nombre}}</option>
        @endforeach 
      </select> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-6 mb-3 form-line">
        <label>Estado</label>
        <select name="estado_roles" class="form-control show-tick"  required="">
            <option id="seleccione"  value="">Seleccione</option>
            <option value="Activo" @if($rol->estado_roles == 'Activo') selected @endif>Activo</option>
            <option value="Inactivo" @if($rol->estado_roles == 'Inactivo') selected @endif>Inactivo</option>
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div> 
    </div>

<div class="col-lg-12">
    <center>
        <button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar rol
        </button>
    </center>
</div>
</form>
@stop