@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                    <form action="{{ url('Funciones/editarFuncionProcess') }}" enctype="multipart/form-data" method="post" data-accion="Editar funcionalidad" id="formEditarFuncion" class="needs-validation" novalidate>
                      
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="id" value="{{ $funcion->id }}">
                      
                      <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre de la funcion en la base de datos*</label>
                            <input type="text" value="{{ $funcion->fun_nombre }}" class="form-control" name="fun_nombre" required="" id="fun_nombre" min="2" maxlength="250"> 
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre de la funcion en el menú*</label>
                            <input type="text" value="{{ $funcion->fun_menu_nombre}}" class="form-control" name="fun_menu_nombre" required="" min="2" maxlength="250">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre de la constante*</label>
                            <input type="text" class="form-control" name="fun_constante" value="{{ $funcion->fun_constante}}" required="" min="2" maxlength="250">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Descripcion de la funcion*</label>
                            <input type="text" class="form-control" name="fun_descripcion" value="{{ $funcion->fun_descripcion }}" required="" min="2" maxlength="250">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>
                        
                       <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Icono de la funcion*</label>
                            <input type="text" class="form-control" name="fun_icono" value="{{$funcion->fun_icono}}" required="">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Ruta*</label>
                            <input type="text" class="form-control" name="fun_ruta" value="{{$funcion->fun_ruta}}" required="">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Color*</label>
                                <select name="fun_color" class="form-control" required="">
                                    <option value="">Seleccione</option>
                                    <option <value="{{$funcion->fun_color}}" @if($funcion->fun_color == 'indigo') Selected @endif> {{$funcion->fun_color}} </option>
                                </select>
                                <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}
                            </div>
                        </div>

                         
                            <div class="col-md-6 mb-3 form-line">
                                <label for="validationCustom01"> Modulo*</label>
                            	<select name="mod_id" class="form-control" required="">
                            			<option value="">Seleccione</option>
                            			@foreach($modulos as $mod)
                            			<option value="{{ $mod->id }}" @if($mod->id == $funcion->mod_id) selected @endif> {{ $mod->mod_nombre }}</option>
                        				@endforeach
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                            </div>
                              

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Pertenece al Core ?*</label>
                            	<select name="fun_sft" class="form-control" required="">
                            			<option value="">Seleccione</option>
                                		<option value="S" @if($funcion->fun_sft == "S" || $funcion->fun_sft == "Si"  )selected @endif> Si </option>
                                		<option value="N" @if($funcion->fun_sft == "N" || $funcion->fun_sft == "No" )selected @endif> No </option>
                                </select>
                                  <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Estado Funcion*</label>
                            	<select name="estado_funcion" class="form-control" required="">
                            			<option value="">Seleccione</option>
                                		<option <value="Activo" @if($funcion->estado_funcion == 'Activo' )selected @endif > Activo </option>
                                		<option <value="Inactivo"@if($funcion->estado_funcion == 'Inactivo' )selected @endif > Inactivo</option>
                                </select>
                               <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                        </div>    

                    <div class="col-lg-12"><center><button id=""
                    type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar funcion</button></center></div>

                        </form>
            </div>
</div>

@stop