@extends($_SESSION[DATA]['layouts'])

@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                    <form action="{{ url('Funciones/crearFuncion') }}" enctype="multipart/form-data" method="post" data-accion="Crear funcionalidad" id="formEditarFuncion" class="needs-validation" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre de la funcion en la base de datos*</label>
                            <input type="text" value="" class="form-control" name="fun_nombre" required="" id="fun_nombre" min="1" maxlength="250">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre de la funcion en el menú*</label>
                            <input type="text" value="" class="form-control" name="fun_menu_nombre" required="" id="fun_menu_nombre" min="2" maxlength="250">  
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre de la constante*</label>
                            <input type="text" value="" class="form-control" name="fun_constante" required="" id="fun_constante" min="2" maxlength="250">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>                        

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Descripcion de la funcion*</label>
                            <input type="text" value="" class="form-control" name="fun_descripcion" required="" id="fun_descripcion" min="2" maxlength="250">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>                        

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Icono de la funcion*</label>
                            <input type="text" value="" class="form-control" name="fun_icono" required="" id="fun_icono">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div> 

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Ruta*</label>
                            <input type="text" value="" class="form-control" name="fun_ruta" required="" id="fun_ruta">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                                <div class="col-md-6 mb-3 form-line">
                                    <label for="validationCustom01"> Color*</label>
                            	       <select name="fun_color" class="form-control" required="">
                                    		<option value="">Seleccione</option>
                                        	<option <value="indigo"> indigo </option>
                                            </select>
                                    <div class="valid-feedback">Ok!</div>
                                    <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                    </div>
                                </div>    
                            
                           <div class="col-md-6 mb-3 form-line">
                                <label for="validationCustom01"> Modulo*</label>
                            	<select name="mod_id" class="form-control" required="">
                            		<option value="">Seleccione</option>
                            		@foreach($modulos as $mod)
                            		<option value="{{ $mod->id }}"> {{ $mod->mod_nombre }}</option>
                        			@endforeach
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                            </div>
                                
                           

                           
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Pertenece al Core ?*</label>
                            	<select name="fun_sft" class="form-control" required="">
                            			<option value="">Seleccione</option>
                                		<option <value="S"> Si </option>
                                		<option <value="N"> No </option>
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                        </div>    
                        
                        

                         
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Estado Funcion*</label>
                                <select name="estado_funcion" class="form-control" required="">
                            			<option value="">Seleccione</option>
                                		<option <value="Activo"> Activo </option>
                                		<option <value="Inactivo"> Inactivo</option>
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                        </div>    

                        @foreach($clientes as $cli)

                        <div class="col-md-6 mb-3 form-line">
                            <input type="hidden" name="" value="{{$cli['id']}}">
                            <label> {{$cli['cliente_nombre']}}*</label>

                            @foreach($roles as $rol)

                                @if($cli['id']==$rol['cliente_id'])

                                <p><input type="checkbox" name="roles[]" value="{{$rol['id']}}">{{$rol['rol_nombre']}}</p>
                                

                                @endif

                            @endforeach    


                        </div>

                        @endforeach    
                        
                        
                        
                    <div class="col-lg-12"><center><button id=""
                     type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear funcion</button></center></div>

                        </form>
            </div>
</div>

@stop