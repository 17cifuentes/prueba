
<form enctype="multipart/form-data" class="needs-validation" method="GET" action="{{url('Usuarios/createProccessInt')}}" data-accion="Crear Usuario" id="crearFormUsu" novalidate>

	<legend>Datos personales</legend>
		
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Nombre *</label>
	    <input type="text" class="form-control input-modal" id="name" name="name" min="1" maxlength="250" value="{{ old('name') }}" required="">   
	    <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
	</div>
	
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Correo *</label>
         <input type="email" class="form-control input-modal" id="email" name="email" min="1" maxlength="250" value="{{ old('email') }}" pattern="^[\w._%-]+@[\w.-]+\.[a-zA-Z]{2,4}$" required="">
      	<div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_EMAIL}}
        </div>                    	
	</div>	
	
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Contraseña *</label>
        <input type="password" class="form-control input-modal" id="password" name="password" maxlength="250" value="{{old('password')}}" required="" pattern=".{6,}">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_NUM_MIN_10}}
        </div> 
	</div>	
	
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Departamento *</label>
         <select name="departamento_id" id="departamento_id" class="form-control input-modal show-tick {{SFT_SELECT2}}" required="" onchange="cargarMunicipios(this.value,'id','#seleccionCiudad')">
         	<option value="">Seleccione</option>
         	@if(isset($departamentoQuery))
         		@foreach($departamentoQuery as $departamento)
         			<option value="{{$departamento['id']}}">{!! $departamento['nombre'] !!}</option>
         		@endforeach
			@endif         		
         </select>
      	<div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>                    	
	</div>	
	<div class="col-lg-12">
			<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Municipio *</label>
         <select name="municipio_id" id="seleccionMunicipio" class="form-control input-modal show-tick {{SFT_SELECT2}}" required="" onchange="cargarCorregimiento(this.value,'id','#seleccionCorregimiento')">
         	<option value="">Seleccione</option>
         </select>
      	<div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>                              	
	</div>
<!-- 		<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Corregimiento *</label>
         <select id="seleccionCorregimiento" class="form-control input-modal show-tick {{SFT_SELECT2}}">
         	<option>Seleccione</option>
         </select>
      	<div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>                              	
	</div> -->			
	</div>
	<div class="col-md-12 mb-3 form-line">
		@if(isset(Auth::user()->id))
	
		<legend align="center">Datos de sistema </legend>

		<input type="hidden" name="cliente_id">
		<div class="col-md-6 mb-3 form-line">
			<label for="validationCustom01">Cliente *</label>
			<select class="form-control input-modal show-tick" name="cliente_id" id="cliente_id" required="" onchange="cargarDatosRol(this.value)">
				<option value="">Seleccione</option>
				@if(isset($clientes))
					@foreach($clientes as $cli)
					<option value="{{$cli['id']}}">{{$cli['cliente_nombre']}}</option>
					@endforeach
				@endif					
			</select>
			 <div class="valid-feedback">Ok!</div>
		    <div class="invalid-feedback">
		        {{CAMPO_SELECT}}
		    </div>
		</div>

		<div class="col-md-6 mb-3 form-line">
			<label for="validationCustom01">Rol *</label>
			<select id="selectRol" class="form-control input-modal show-tick" name="rol_id"  onchange="cargarDatosRolCampos(this.value)" required="">
				<option value="">Seleccione</option>
			</select>
			<div class="valid-feedback">Ok!</div>
		    <div class="invalid-feedback">
		        {{CAMPO_SELECT}}
		    </div>   
		</div>
		@else
			<div class="col-md-6 mb-3 form-line">
				<label for="validationCustom01">Cedula *</label>
			    <input type="number" class="form-control input-modal" id="cedula" name="cedula" min="1" maxlength="250" value="{{ old('cedula') }}" required=""><div class="valid-feedback">Ok!</div>
			    <div class="invalid-feedback">
			        {{CAMPO_REQUERIDO}}
			    </div>
			</div>			
			<div class="col-md-6 mb-3 form-line">
				<label for="validationCustom01">Telefono *</label>
			    <input type="number" class="form-control input-modal" id="telefono" name="telefono" min="7" maxlength="250" value="{{ old('telefono') }}" required="">   
			    <div class="valid-feedback">Ok!</div>
			    <div class="invalid-feedback">
			        {{CAMPO_REQUERIDO}}
			    </div>
			</div>
			<div class="col-md-6 mb-3 form-line">
				<label for="validationCustom01">Celular </label>
			    <input type="number" class="form-control input-modal" id="celular" name="celular" min="7" maxlength="250" value="{{ old('telefono') }}">   
			    <div class="valid-feedback">Ok!</div>
			    <div class="invalid-feedback">
			        {{CAMPO_REQUERIDO}}
			    </div>
			</div>			
			<div class="col-md-6 mb-3 form-line">
				<label for="validationCustom01">Direccion *</label>
			    <input type="text" class="form-control input-modal" id="direccion" name="direccion" min="5" maxlength="250" value="{{ old('direccion') }}" required="">   
			    <div class="valid-feedback">Ok!</div>
			    <div class="invalid-feedback">
			        {{CAMPO_REQUERIDO}}
			    </div>
			</div>			
		@endif
	</div>
	
	<span id="contenidoCampos"></span>	

	<div class="col-lg-12"><center><button id="btnCrearUsuario"
	type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear usuario</button></center></div>

</form>    
