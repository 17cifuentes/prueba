@extends($_SESSION[DATA]['layouts'])

@section('contenido')
        <div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
<form action="{{ url('Usuarios/actulizaUsuarioProcess') }}" enctype="multipart/form-data" method="post" id="formEditarUsuario" data-accion="Editar Usuario" class="needs-validation" novalidate>

    <input type="hidden" name="id" id="id" value="{{ $usuario->id }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend>Datos personales</legend>
        
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Nombre *</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $usuario->name}}" minlength="1" maxlength="250"  required="">   
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-6 mb-3 form-line">
        <div class="col-md-3">
        <label>Foto</label>
        <img src="{{ asset($usuario->img) }}" class="img img-reponsive img-circle" width="68" height="68">   
        </div>
        <div class="col-md-9"><br><br>
        <input type="file" class="form-control" name="foto" accept=".jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai"> 
        </div>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
    
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Correo *</label>
        <input type="email" value="{{ $usuario->email }}" class="form-control" name="email" minlength="1" maxlength="250" pattern="^[\w._%-]+@[\w.-]+\.[a-zA-Z]{2,4}$" required="">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>  
    
    <div class="col-md-6 mb-3 form-line">
        <label>Cambiar Contraseña </label>
        <input type="password" class="form-control" name="password" value="" minlength="6" maxlength="250" pattern=".{6,}">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_NUM_MIN_10}}
        </div>
</div>

<div id="contenDataRol"></div>

<input type="hidden" id="datosRol" value=" {{$datosRol}}">
    
    <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Departamento *</label>
         <select name="departamento_id" id="departamento_id" class="form-control input-modal show-tick {{SFT_SELECT2}}" required="" onchange="cargarMunicipios(this.value,'id','#seleccionCiudad')">
            <option value="">Seleccione</option>
            @if(isset($departamentoQuery))
                @foreach($departamentoQuery as $departamento)
                    <option value="{{$departamento['id']}}" @if($departamento['id']== $usuario->departamento_id) selected @endif>{!! $departamento['nombre'] !!}</option>
                @endforeach
            @endif              
         </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>                      
    </div>  
    
            <div class="col-md-6 mb-3 form-line">
        <label for="validationCustom01">Municipio *</label>
         <select name="municipio_id" id="seleccionMunicipio" class="form-control input-modal show-tick {{SFT_SELECT2}}" required="" onchange="cargarCorregimiento(this.value,'id','#seleccionCorregimiento')">
            @foreach($municipioUsu as $municipio)
                    <option value="{{$municipio['id']}}" @if($municipio['id']== $usuario->municipio_id) selected @endif>{!! $municipio['nombre'] !!}</option>
                @endforeach
         </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>                                  
    </div>
        
        <div class="col-md-12 mb-3 form-line">
        @if(isset(Auth::user()->id))
    
        <legend align="center">Datos de sistema </legend>

        <input type="hidden" name="cliente_id">
        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Cliente *</label>
            <select class="form-control input-modal show-tick" name="cliente_id" id="cliente_id" required="" onchange="cargarDatosRol(this.value)">
                <option value="">Seleccione</option>
                @if(isset($clientes))
                    @foreach($clientes as $cli)
                    <option value="{{$cli['id']}}" @if($cli['id']== $usuario->cliente_id) selected @endif >{{$cli['cliente_nombre']}}</option>
                    @endforeach
                @endif                  
            </select>
             <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Rol *</label>
            <select id="selectRol" class="form-control input-modal show-tick" name="rol_id"  onchange="cargarDatosRolCampos(this.value)" required="">
                <option value="">Seleccione</option>
            </select>
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>   
        </div>
        @else
            <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Cedula *</label>
                <input type="number" class="form-control input-modal" id="cedula" name="cedula" min="1" maxlength="250" value="{{ old('cedula') }}" required=""><div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
            </div>          
            <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Telefono *</label>
                <input type="number" class="form-control input-modal" id="telefono" name="telefono" min="7" maxlength="250" value="{{ old('telefono') }}" required="">   
                <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
            </div>          
            <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Direccion *</label>
                <input type="text" class="form-control input-modal" id="direccion" name="direccion" min="5" maxlength="250" value="{{ old('direccion') }}" required="">   
                <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
            </div>          
        @endif
    </div>
    <input type="hidden" id="rolId" value="{{$usuario->rol_id}}">
    <span id="contenidoCampos"></span>  

    <div class="col-lg-12"><center><button id="btnCrearUsuario"
    type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar usuario</button></center></div>

</form>    
@stop
