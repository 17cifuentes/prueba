@extends($_SESSION[DATA]['layouts'])

@section('contenido')
        <div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                <div style='background: url({{ asset(Auth::user()->img) }}) center no-repeat;' class="profile-pic" id="profile_pic"></div>
                <div class="rela-block profile-name-container">
                    <div class="rela-block user-name"><h2 style="margin-top: -0.7em">{{ Auth::user()->name }}</h2></div>
                    <div class="rela-block user-desc" id="user_description">{{ Auth::user()->email }} </div><br><br>
                    <form id="formEditarFuncion" data-accion="Actualizar perfil" action="{{ url('Usuarios/actulizaPerfil') }}" enctype="multipart/form-data" method="post" class="needs-validation" novalidate>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-12">
                                <label>Nombre</label>
                                <input type="text" value="{{ Auth::user()->name }}" class="form-control" name="name" required="">    
                            </div>
                            <div class="col-md-12">
                                <label>Correo</label>
                                <input type="text" value="{{ Auth::user()->email }}" class="form-control" name="email">    
                            </div>
                            <div class="col-md-12">
                                <label>Cambiar Contraseña </label>
                                <input type="password" class="form-control" name="password">    
                            </div>              
                            <div class="col-md-12">
                                <label>Cambiar foto</label>
                                <input type="file"  class="form-control" name="foto"> <br>
                            </div>  
                            <center><input type="submit" name="" class="btn btn-success" value="+ Actualizar perfil">           </center>
                        </form>
                </div>
            </div>
    </div>
@stop