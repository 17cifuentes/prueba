<div class="modal" id="registroUsuario">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <!-- Modal Header --> 
      <div class="modal-header">
        <h4 class="modal-title">Registro </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        @include('core.Usuarios.FormCrearUsuario')
      </div>
      <!-- Modal footer --> 
      <div class="modal-footer">
        <a type="button" href="{{ url('/ingresar/'.$id.'/0') }}" class="btn btn-info btn-md"> Iniciar Sesion </a>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button
          >

      </div>

    </div>
  </div>
</div>