@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<style type="text/css">
	#file-preview{
		width: 18em;	
		height: 18em;	
		border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color  }}
	}
</style>
<div class="profile-card">
    <div class="profile-body col-lg-12">
    	<br><br><br><br>
    	<div class="image-area" >
    	@if($data['img'] != '') 
    	<center>
    		<div id="file-preview-zone" >
    			<img style="border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color }};" src="{{ asset($data['img']) }}" class="img img-responsive" alt="AdminBSB - Profile Image" />
            </div>
         </center>
    	@else 
    	<center>
    		<div id="file-preview-zone" >
    			<center><img style="border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color }};" src="{{ asset('core/img/Formulario/subirimg.png') }}" class="img img-responsive" alt="AdminBSB - Profile Image"  /></center>
    		</div>
    	</center>
    	@endif
    	<hr>     
    	<div id="divInputLoad">
    		<div id="divFileUpload">
                
    		</div>
		</div>
      	</div>
    </div>
    <div class="profile-footer col-lg-12"><br>
        {!! $form!!}
    </div>
@stop