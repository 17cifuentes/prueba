<div class="table-responsive">
    <table class="sftDataTable  table table-striped dataTable js-exportable {{ $table['class'] }}" id="tableModel">
        <thead>
            <tr id="tableHead">
                <th>#</th>
                @foreach($table["colum"] as $key => $colum)
                    <?php 
                        $p = strstr($key, '-',true);
                    ?>
                    <th class="item{{ $key }}" >
                        @if($p == "html")
                            <?php 
                                $n = strstr($key, '-',false);
                                $nombre = str_replace('-',"", $n);
                            ?>
                            {{$nombre}}
                            <?php $indice = $nombre?>
                        @elseif($p == "") 
                            {{ $key }}     
                             <?php $indice = $key?>
                        @endif
                    </th>
                @endforeach
            </tr>
        </thead>
        <tbody id="tableBody">
            <?php 
                $c = count($table["colum"]);
                if($table["items"] != null){
                    $f = count($table["items"][$indice]);
                }else{
                    $f ="";
                }
            ?>
            @for($x = 0;$x < $f;$x++)
                <tr id="line{{ $x }}">
                    <td>{{ $x+1 }}</td> 
                    @foreach ($table["colum"] as $key => $value)
                        <?php $p = strstr($key, '-',true)?>
                         @if($p == "html")
                            <?php 
                                $n = strstr($key, '-',false);
                                $nombre = str_replace('-',"", $n);
                            ?>
                            <td class="item{{$key}}">
                                {!!$table["items"][$nombre][$x]!!}
                            </td>
                        @elseif($p == "")
                            <td class="item{{$key}}">
                                {!!$table["items"][$key][$x]!!}
                            </td>
                        @endif
                    @endforeach
                </tr>
            @endfor
        </tbody>
    </table>
    <input type="hidden" id="dataTable" value='{{ json_encode($table["items"]) }}'>
</div>
