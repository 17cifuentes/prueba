@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	<form id="idFormEditarCliente" enctype="multipart/form-data" method="POST" action="/Clientes/editarClienteProcess"  class="needs-validation" novalidate>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">	

	<legend>Datos del cliente</legend>
	<input  type="hidden"  name="id" value="{{$cliente[0]->id}}">

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Nombre*</label>
		<input type="text" class="form-control" id="cliente_nombre" name="cliente_nombre" value="{{ $cliente[0]->cliente_nombre}}" required=""> 
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>					
			
		<div class="col-lg-6">
			<div class="form-group">
				<label>Descripción*</label>
				<div class="form-line">
			    	<input type="text" class="form-control" id="cliente_descripcion" name="cliente_descripcion" value="{{ $cliente[0]->cliente_descripcion}}" required=""> 
			                     	
				</div>
			</div>
		</div>	
			
		<div class="col-lg-6">
			<div class="form-group">
				<label>Dirección*</label>
				<div class="form-line">
					<input type="text" class="form-control" id="direccion" name="direccion" value="{{ $cliente[0]->direccion}}" required=""> 

				</div>
			</div>
		</div>	
			
		<div class="col-lg-6">
			<div class="form-group">
				<label>Télefono*</label>
				<div class="form-line">
					<input type="number" class="form-control" min="1" name="telefono" value="{{ $cliente[0]->telefono}}" required="">                                     
				</div>
			</div>
		</div>	
			
		<div class="col-lg-6">
			<div class="form-group">
				<label>Correo*</label>
				<div class="form-line">
					<input type="email" class="form-control" id="correo" name="correo" value="{{ $cliente[0]->correo}}" required=""> 

				</div>
			</div>

		</div>	
			

		</fieldset>
		<div class="col-lg-12">
			<center>
				<button type="submit" class="btn btn-success">
				<i class="material-icons">send</i>&nbsp;&nbsp; Editar cliente</button>
			</center>
		</div>
	</div>
</form>
@stop