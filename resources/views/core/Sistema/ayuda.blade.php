@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h4>Softheory quiere brindarte una experiencia de usuario magnifica, por eso habilitamos números de contacto directo, donde podrás generar peticiones, sugerencias, reportes de sistema entre otros.</h4>.
</div>
@if($_SESSION['data']["conf"][0]->estructura_admin == 2)
<div class="row clearfix">
    <div class="col-md-4">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <a href="#">
                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/icon/avatar-01.jpg">
                        </a>
                        <div class="media-body">
                            <h2 class="text-light display-6">Teléfonico</h2>
                            <p>Contacto directo con Softheroy</p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="#">
                            <i class="fa fa-envelope-o"></i> Soporte: 346 80 11
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#">
                            <i class="fa fa-envelope-o"></i> Comercial: 316 723 55 57
                        </a>
                    </li>
                </ul>
            </section>
        </aside>
    </div>
        <div class="col-md-4">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <a href="#">
                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/icon/avatar-01.jpg">
                        </a>
                        <div class="media-body">
                            <h2 class="text-light display-6">Chat</h2>
                            <p>Contacto directo con Softheroy</p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="#">
                            <i class="fa fa-envelope-o"></i> <a href="http://softheory.com/" target="_blank">Chat Soporte</a>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#">
                            <i class="fa fa-envelope-o"></i> <a href="https://api.whatsapp.com/send?phone=573167235557" target="_blank">WhatsApp Comercial</a>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>
    </div>
            <div class="col-md-4">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <a href="#">
                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/icon/avatar-01.jpg">
                        </a>
                        <div class="media-body">
                            <h2 class="text-light display-6">Redes sociales</h2>
                            <p>Contacto directo con Softheroy</p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a  href="https://www.facebook.com/softheory" target="_blank"><i class="fa fa-envelope-o"></i>Facebook</a></li>
                    <li class="list-group-item"><a href=https://www.facebook.com/softheory" target="_blank"><i class="fa fa-envelope-o"></i>Instagram</a></li>
                    <li class="list-group-item"><a href="https://twitter.com/softheory" target="_blank"><i class="fa fa-envelope-o"></i>Twitter</a></li>
                    <li class="list-group-item"><a href="https://www.linkedin.com/in/cristian-cifuentes-513982138/" target="_blank"><i class="fa fa-envelope-o"></i>Linkedin</a></li>
                </ul>
            </section>
        </aside>
    </div>
</div>
@else
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="card">
        <div class="header bg-orange ">
            <h2>
                 Telèfonico <small>Contacto directo con Softheory</small>
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);">Soporte: 346 80 11</a></li>
                        <li><a href="javascript:void(0);">Comercial: 316 723 55 57</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="body" style="text-align: center;"> 
            - <em>Para ver nùemeros de contacto clic en los tres puntos</em> -
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="card">
        <div class="header bg-red">
            <h2>
                 Chat <small>Contacto directo con Softheory</small>
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="http://softheory.com/" target="_blank">Chat Soporte</a></li>
                        <li><a href="https://api.whatsapp.com/send?phone=573167235557" target="_blank">WhatsApp Comercial</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="body" style="text-align: center;">
            - <em>Para ver nùemeros de contacto clic en los tres puntos</em> -
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="card">
        <div class="header bg-green">
            <h2>
                 Redes sociales <small>Contacto directo con Softheory</small>
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="https://www.facebook.com/softheory" target="_blank">Facebook</a></li>
                        <li><a href=https://www.facebook.com/softheory" target="_blank">Instagram</a></li>
                        <li><a href="https://twitter.com/softheory" target="_blank">Twitter</a></li>
                        <li><a href="https://www.linkedin.com/in/cristian-cifuentes-513982138/" target="_blank">Linkedin</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="body" style="text-align: center;">
            - <em>Para ver nùemeros de contacto clic en los tres puntos</em> -
        </div>
    </div>
</div>
@endif

@stop
