<div class="{{ $col }}">
	<div class="form-group">
		<label>{{ $label }}</label>
		<?php 	
			$rel = $data["relacion"];
			unset($data["relacion"]);
			$con = count((array)$data);
		?>
		<select class="form-control show-tick" name="{{ $name }}">
			<option value="">Seleccione</option>
				@for($x = 0;$x < $con;$x++)
					<option value="{{ $data[$x][$rel[0]] }}">{{ $data[$x][$rel[1]] }}</option>
				@endfor
		</select>   
	</div>
</div>