<div class="{{ $col }}">
    @if($type == "hidden")
    			<input type="{{ $type }}" class="form-control " name="{{ $name }}" value="{{ $valueInput }}">
    @else
    	<div class="form-group">
    		<label>{{ $label }} </label>
        	<div class="form-line">
                 @if($type == 'number')
                    @if($type == $subtype)
                        @include('core.Formulario.inputs.number')
                    @endif
                    @if($subtype == "telefono")
                        @include('core.Formulario.inputs.telefono')
                    @endif
                    
                 @else
                    <input type="{{ $type }}"  class="form-control" id="{{ $name }}"  name="{{ $name }}" value="{{ $valueInput }}" {{ $atributes }}> 
                 @endif
            	
        	</div>
    	</div>
    @endif		
</div>	
	
