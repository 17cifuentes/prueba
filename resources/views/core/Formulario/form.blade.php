
<form id="@if(isset($form['idForm'])){{ $form['idForm'] }} @endif" enctype="multipart/form-data" class="@if(isset($form['col'])) {{ $form['col'] }}  @endif " method="{{ $form['method'] }}" action="{{ $form['action'] }}">
	<div class="col-lg-12">
@if($form['method'] == "POST")
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
@endif