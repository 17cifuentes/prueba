<fieldset class="form-group {{ $data['col'] }}" id="{{ $data['id']}}">
		<legend>{{ $data["nombre"] }}</legend>
		
		@if($data[ADD] == true)
        <div id="conten{{ $data['id']}}">
			<div class="col-lg-5">
				<div class="form-group">
    				<label>Nombre</label>
        			<div class="form-line">
            			<input type="text" class="form-control " name="nombre[]" value="">
        			</div>
    			</div>
    		</div>
    		<div class="col-lg-5">
				<div class="form-group">
    				<label>Valor</label>
        			<div class="form-line">
            			<input type="text" class="form-control " name="valor[]" value="">
        			</div>
    			</div>
    		</div>
			<div class="col-lg-2">
				<div class="form-group">
    				<a class="btn btn-info" onclick="addRow('{{ $data['id']}}')" href="javascript::void()"> + </a>
    			</div>
    		</div>
        </div>
		@endif