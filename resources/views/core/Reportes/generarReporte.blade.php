@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form id="idFormEditarCliente" enctype="multipart/form-data" method="POST" action="/Reportes/generarReporte"  class="needs-validation" data-accion="Generar reporte" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">	
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Módulo*</label>
		<select required="" class="form-control" name="modulo" onchange="getItemModulo(this.value)">
			<option value="">Seleccione</option>
			@foreach($modulos as $mod)
				<option @if(isset($datos['modulo']) and $datos['modulo'] == $mod->modulos->id) selected @endif value="{{ $mod->modulos->id }}">{{ $mod->modulos->mod_nombre }}</option>
			@endforeach
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Opción*</label>
		<select required="" class="form-control" name="opc">
			<option value="">Seleccione</option>
			@foreach($opcReporte as $opc)
				<option @if(isset($datos['opc']) and $datos['opc'] == $opc->valor) selected @endif value="{{ $opc->valor }}">{{ $opc->nombre }}</option>
			@endforeach
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Item</label>
		<select  class="form-control" name="item" id="item">
			<option value="">Seleccione</option>
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Operaciones</label>
		<select  class="form-control" name="operacion" id="operacion">
			<option value="">Seleccione</option>
			@foreach($operaciones as $op)
				<option value="{{ $op->valor
				 }}">{{ $op->nombre }}</option>
			@endforeach
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Valor</label>
		<input type="text" name="valor" class="form-control">
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Tipo reporte*</label>
		<select class="form-control" name="tipo" required="">
			<option value="">Seleccione</option>
			<option @if(isset($datos['tipo']) and $datos['tipo'] == 'Reporte') selected @endif value="Reporte" >Reporte</option>
			<option @if(isset($datos['tipo']) and $datos['tipo'] == 'Grafico') selected @endif value="Grafico">Grafico</option>
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Tipos de gráficos*</label>
		<select class="form-control" name="tipoGrafico" >
			<option value="">Seleccione</option>
			@foreach($graficos as $graf)
			<option value="{{ $graf->valor }}">{{ $graf->nombre }}</option>
			@endforeach
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
    		{{CAMPO_REQUERIDO}}
		</div>
	</div>						
	<div class="col-lg-12">
		<center>
			<button type="submit"  class="btn btn-success" type="button" data-toggle="modal" data-target="#RadarModal"
                data-chartnombre="" data-charttipo="radar" data-chartcontenedor="container">
			<i class="far fa-paper-plane"></i>&nbsp;&nbsp; Generar reporte</button>
		</center>
	</div>	
	@if(isset($datos['tituloGrafico']))
		<input type="hidden" id="tituloGrafico" value="{{ $datos['tituloGrafico'] }}">
		<input type="hidden" id="tipoGrafico" value="{{ $datos['tipoGrafico'] }}">
		<input type="hidden" id="contenedorGrafico" value="{{ $datos['contenedorGrafico'] }}">

		<input type="hidden" id="valorGrafico" value="{{ json_encode($datos['valorGrafico']) }}">
		<input type="hidden" id="dataGrafico" value="{{ json_encode($datos['dataGrafico']) }}">

		<script type="text/javascript">
			window.addEventListener('load', getData, false);
			function getData(){
				getChartData();
			}
		</script>
	@endif

	<div id="contenedor" class="col-lg-12"></div>	
	@if(isset($table))
		<div class="col-lg-12">
			<hr>
			{!! $table !!}	
			<script type="text/javascript">
				window.addEventListener('load', getData, false);
				function getData(){
					startDataTable();
				}
			</script>
		</div>
	@endif
		</fieldset>

	</div>
</form>
@stop
