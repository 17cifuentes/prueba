@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<ul class="nav nav-tabs" style="margin-top:-2.6em">
  <li class="active" id="contactosList"><a data-toggle="tab" href="#home"><i class="material-icons">face</i> Contactos</a></li>
  <li  id="chatUser"><a data-toggle="tab" href="#menu1"><i class="material-icons" >chat</i> Conversaciones</a></li>
  <li  id="chatUser"><a data-toggle="tab" href="#menu1"><i class="material-icons" >chat</i> Chat</a></li>
</ul>
<div class="tab-content">
  <div id="home" class="tab-pane fade in  active ">
    @foreach($contacts as $contac)
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                    <div class="card" style="height: 6em;">
                        <div class="header">
                            <div class="col-lg-4 col-md-2 col-sm-2 col-xs-4" style="margin-top: -1em">
                                <img width="70" src="{{ asset($contac->contacto->img)}}" style="color: black;border-color:black;border-style: solid;border-width: 1px;border-radius: 10px 10px" class="img img-responsive">     
                            </div>
                            <h2 class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
                                {{$contac->contacto->name}} <small>{{$contac->contacto->email}}</small>
                            </h2>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="margin-top: 0.5em">
                                <a href="javascript::void()" class="chatBtn" data-id="{{$contac->contacto->id}}" data-chat="{{ $contac->id }}"><i class="material-icons" style="width: 6em" >chat</i> </a>
                            </div>
                            <input type="hidden" id="chatNum{{ $contac->id }}" value="{{ $contac->msg }}">
                        </div>
                        
                    </div>
                </div>
            @endforeach
  </div>
  <div id="menu1" class="tab-pane fade">
    <div class="messages">
            <input type="hidden" id="contactoId" value="">
            <input type="hidden" id="chatId" value="">
            <ul id="msgChat"></ul>
        </div>
                <div class="message-input" style="padding: 1em">
            <div class="wrap">
            <input style="background: rgba(202,195,195,0.5);color:black;border-radius: 10px 10px" type="text" placeholder="Esctibe un mensaje aquí" />
            <button onclick="newMessage()" class="btn-success" style="border-radius: 10px 10px;"><i style="padding:0.2em" class="material-icons">send</i></button>
            </div>
        </div>
  </div>
</div>
@stop 
