@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<div class="" style="padding: 1em">

    <div class="row clearfix">

        <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">web</i>
                </div>
                <a href="{{url('carritoCompras/crearSubMenu')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Crear Submenú</div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">toc</i>
                </div>
                <a href="{{url('carritoCompras/listarSubMenu')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Listar Submenú</div>
                    </div>
                </a>
            </div>
        </div>

    </div>
</div>
@stop