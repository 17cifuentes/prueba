@extends($_SESSION[DATA]['layouts'])

@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                <form action="{{ url('carritoCompras/editarSubMenuProcess') }}" enctype="multipart/form-data" method="post" id="formEditarSubMenu" data-accion="Editar SubMenú" class="needs-validation" novalidate>

                     <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="id" id="id" value="{{$datos[0]->id}}">


                            <div class="col-md-6 mb-3 form-line">
                             <label for="validationCustom01"> Cliente * </label>
                                <select class="form-control show-tick" name="cliente_id" readonly="readonly" disabled="">
                                    @foreach($cliente as $cli)
                                    <option value="{{ $datos[0]->cliente_id }}" @if($datos[0]->cliente_id == $cli->id ) selected @endif> {{ $cli->cliente_nombre }}  </option>
                                    @endforeach
                                </select>   
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}
                            </div>
                        </div>
                                                
                        <div class="col-md-6 mb-3 form-line">
                             <label for="validationCustom01"> SubMenu *</label>
                                <select class="form-control show-tick" name="menu_id" required="" disabled="">
                                    @foreach($menu as $men)
                                    <option value="{{$datos[0]->menu_id}}"> {{ $men->nombre }} 
                                    </option>
                                    @endforeach
                                </select>
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 mb-3 form-line">
                             <label for="validationCustom01"> Nombre SubMenù Carrito Compra * </label>
                                    <input type="text" value="{{ $datos[0]->nombre }}" min="1" maxlength="250" class="form-control" name="nombre"  required="">    
                                
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}dd
                            </div>
                        </div>
                        <div class="col-md-6 mb-3 form-line">
                             <label for="validationCustom01"> Estado subMenú * </label>
                                <select class="form-control show-tick" name="estado_subMenu" required="">
                                    <option value="">Seleccione</option>
                                    <option value="Activo" @if($datos[0]->estado_subMenu == 'Activo') selected @endif> Activo </option>
                                    <option value="Inactivo" @if($datos[0]->estado_subMenu == 'Inactivo') selected @endif> Inactivo </option>
                                </select>   
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}
                            </div>
                        </div>
                        </div>
                        
                <div class="col-lg-12"><center><button id=""
                type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar submenú</button></center></div>

            </form>
        </div>
</div>
@stop