<div class="modal" tabindex="-1" role="dialog" id="modalAgregarProductoDomicilio" name="modalAgregarProductoDomicilio">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <legend>Agregar Nuevo Producto</legend> -->
        <b><h4>Agregar Nuevo Producto</h4></b>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="domicilio_cliente" name="domicilio_cliente">
        @include('Modulos.ShopCart.modals.agregarNuevoProductoDomicilio')

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" onclick="agregarNewProductDomi()"><b>Agregar Producto</b></button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>