<div class="modal" tabindex="-1" role="dialog" id="modalsInfoUsuario" name="modalAgregarProductoDomicilio">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    	<b><h4>Datos del usuario</h4></b>
    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
	    <div class="modal-body">
	        @include('Modulos.ShopCart.modals.InfoUsuario')
		</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>