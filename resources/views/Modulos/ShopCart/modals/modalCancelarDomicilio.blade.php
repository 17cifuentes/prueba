<!-- Modal -->
<div class="modal fade" id="modalCancelarDomicilio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cancelar Domicilio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <input type="hidden" name="idDomicilio" id="idDomicilio">
        @include('Modulos.ShopCart.modals.cancelarDomicilio')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info" onclick="cancelarDomicilioMotivo()">Domicilio Producto</button>
      </div>
    </div>
  </div>
</div>