<div class="modal" tabindex="-1" role="dialog" id="modalExitoContraentrega">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <legend>Agregar Nuevo Producto</legend> -->
        <b><h4>Información de domicilio</h4></b>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h3>Los datos presentados, los enviamos a tu correo para que tengas la información de tu domicilio.</h3><em>No olvides revisar tu bandeja de span - correo no deseado - promociones</em><hr>
          <div id="datosDomicilio"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>