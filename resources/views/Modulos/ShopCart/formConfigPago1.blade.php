@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	   <form action="{{ url('carritoCompras/newConfigPagoProccess') }}" enctype="multipart/form-data" method="post" data-accion="Configurar PaYu" id="formpayu" class="needs-validation" novalidate>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" class="needs-validation" novalidate>

        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">MerchanId *</label>
            <input class="form-control" name="merchantId"  type="text" value="" required="" >
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>

        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">accountId *</label>
            <input class="form-control" name="accountId" id="accountId"  type="text" value="" required="">
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Descripción *</label>
            <input name="description" type="text"  class="form-control" value="" id="description" required="" >    
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>

        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Moneda *</label>
            <input name="currency" class="form-control"  type="text" value="" required="" >
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Correo cuenta *</label>
            <input name="buyerEmail" class="form-control"   type="text"  value="" required="" >
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Api Key *</label>
            <input name="api_key" class="form-control"   type="text"  value="" id="api_key" required="">
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>

        <input type="hidden" name="responseUrl" id="responseUrl" value="{{$urlRespuesta}}">
        
        <div class="col-lg-12">
            <center><input  class="btn btn-success" type="submit"></center>
        </div>
	</form>
@stop