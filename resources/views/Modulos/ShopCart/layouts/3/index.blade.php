@if(isset($_SESSION['data']["conf"][0]->title))
        <?php
            $titulo = $_SESSION[DATA]["conf"][0]->title;
            $icono = $_SESSION[DATA]["conf"][0]->ico;
        ?>
    @else
        <?php 
            $titulo = $_SESSION[DATA][CLIENTE_TITLE];
            $icono = "";
        ?>
    @endif
    <input type="hidden" value="{{ $_SESSION['data']['conf'][0]->navbar_color }}" id="colorCliente">
    <input type="hidden" id="productosCart" value="{{ json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]) }}">
@include(PATH_MOD_SHOPCART_HEADER)
@include(PATH_MOD_SHOPCART_NAVBAR)
@include(PATH_MOD_SHOPCART_NAVBARMENU)
@include(PATH_MOD_SHOPCART_NAVBARCATEGORIAS)
@include(PATH_MOD_SHOPCART_NAVCART)
@include(PATH_MOD_SHOPCART_PRODUCTOS)
@include(PATH_MOD_SHOPCART_BENEFICIOS)
@include(PATH_MOD_SHOPCART_MODAL)
@include(PATH_MOD_SHOPCART_FOOTER)
