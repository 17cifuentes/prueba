<div class="modal" id="modalPeticionDomicilio" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Realizar Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
          Selecciona el método por el cual deseas hacer el pedido
      </div>
      
      
      <div class="modal-footer">
        <center>
        <button type="button" class="btn btn-primary btn-lg" id="Contraentrega" onclick="registerProductsDatabases(this.id)"> Contraentrega</button>

        <button type="button" class="btn btn-link btn-lg" id="Online" onclick="registerProductsDatabases(this.id)"> Online </button>
        </center>
      </div>
    </div>
  </div>
</div>