


<!-- Feature Section Start -->
<div class="feature-section section mb-60">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <!-- Feature Start -->
                <div class="feature feature-shipping">
                    <div class="feature-wrap">
                        <div class="icon"><img src="{{ asset('assetsShopCart/images/icons/feature-van.png')}}" alt="Feature"></div>
                        <h4>DOMICILIOS</h4>
                        <p>{{ $titulo }}</p>
                    </div>
                </div><!-- Feature End -->
            </div>
            
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <!-- Feature Start -->
                <div class="feature feature-guarantee">
                    <div class="feature-wrap">
                        <div class="icon"><img src="{{ asset('assetsShopCart/images/icons/feature-walet.png')}}" alt="Feature"></div>
                        <h4>DIFRENTES MEDIOS DE PAGO</h4>
                        <p>{{ $titulo }}</p>
                    </div>
                </div><!-- Feature End -->
            </div>
            
            <div class="col-lg-4 col-md-6 col-12 mb-30">
                <!-- Feature Start -->
                <div class="feature feature-security">
                    <div class="feature-wrap">
                        <div class="icon"><img src="{{ asset('assetsShopCart/images/icons/feature-shield.png')}}" alt="Feature"></div>
                        <h4>COMPRA SEGURO</h4>
                        <p>{{ $titulo }}</p>
                    </div>
                </div><!-- Feature End -->
            </div>
            
        </div>
    </div>
</div><!-- Feature Section End -->