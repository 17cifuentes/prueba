    <!-- Header Category Start -->
    <div class="header-category-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <!-- Header Category -->
                    <div class="header-category">
                        <!-- Category Toggle Wrap -->
                        <div class="category-toggle-wrap d-block d-lg-none">
                            <!-- Category Toggle -->
                            <button class="category-toggle">Categorias <i class="ti-menu"></i></button>
                        </div>
                        
                        <!-- Category Menu -->
                        <nav class="category-menu">
                            <ul>
                                @foreach($_SESSION[CATEGORIES] as $cat)
                                    <li ><a class="catColor" href="category-1.html">{{ $cat->cat_nombre }}</a></li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div><!-- Header Category End -->