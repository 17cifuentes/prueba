<body onload="init()">
<!-- Header Section Start -->
<div class="header-section section">
    <!-- Header Top Start -->
    <div class="header-top header-top-one header-top-border pt-10 pb-10">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col mt-10 mb-10">
                    <!-- Header Links Start -->
                    <div class="header-links">
                        <a href="store.html" ><span>{{ $titulo }}</span></a>
                    </div><!-- Header Links End -->
                </div>

                <div class="col order-12 order-xs-12 order-lg-2 mt-10 mb-10">
                    <!-- Header Advance Search Start -->
                    <div class="header-advance-search">
                        
                        <form action="#">
                            <div class="input"><input type="text" placeholder="Buscar Productos"></div>
                            <div class="select">
                                <select class="nice-select">
                                    <option>Todas</option>
                                    @foreach($_SESSION[CATEGORIES] as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->cat_nombre }}
                                    @endforeach
                                </select>
                            </div>
                            <div class="submit"><button><i class="icofont icofont-search-alt-1"></i></button></div>
                        </form>
                        
                    </div><!-- Header Advance Search End -->
                </div>

                <div class="col order-2 order-xs-2 order-lg-12 mt-10 mb-10">
                    <!-- Header Account Links Start -->
                    <div class="header-account-links">
                        @if(Auth::user())
                            <a href="{{url('home')}}"><img width="40" height="35" src="{{ asset(Auth::user()->img) }}" style="border: 1px solid black;border-radius: 100%;">
                                <span>{{Auth::user()->name}}</span>
                            </a>
                        @else
                            <a href="{{ url('ingresar/'.$id.'/0')}}"><i class="icofont icofont-user-alt-5" style="background: white"></i>
                                <span>Ingresar</span>
                            </a>
                        @endif
                    </div><!-- Header Account Links End -->
                </div>

            </div>
        </div>
    </div><!-- Header Top End -->