
<!-- Best Sell Product Section Start -->
<div class="product-section section mb-60">
    <div class="container">
        <div class="row">
            <div class="row mb-50">

                    <div class="col">
                <br>       
                        <!-- Shop Top Bar Start -->
                        <div class="shop-top-bar">

                            <!-- Product View Mode -->
                            <div class="product-view-mode">
                                <a class="active" href="#" data-target="grid"><i class="fa fa-th"></i></a>
                                <a href="#" data-target="list"><i class="fa fa-list"></i></a>
                            </div>

                            <!-- Product Showing -->
                            <div class="product-showing">
                                <p>Productos</p>
                                <select name="showing" class="nice-select" id="cantProducto">
                                    <option value="1">8</option>
                                    <option value="2">12</option>
                                    <option value="3">16</option>
                                    <option value="4">20</option>
                                    <option value="5">24</option>
                                </select>
                            </div>

                            <!-- Product Pages -->
                            <div class="product-pages">
                                <p style="margin-left: 3em">Página 1 de {{ count($_SESSION[SHOPCART_PRODUCTOS]) }}</p>
                            </div>

                        </div><!-- Shop Top Bar End -->
                        
                    </div>
                </div>
            <div class="col-12 mb-40">
                <br>
                <div class="section-title-one" data-title="PRODUCTOS"><h1>PRODUCTOS</h1></div>
            </div><!-- Section Title End -->
            <div class="shop-product-wrap grid row">
                @foreach($_SESSION[SHOPCART_PRODUCTOS] as $producto)
                    <div class="col-xl-3 col-lg-4 col-md-6 col-12 pb-30 pt-10">
                        <!-- Product Start -->
                        <div class="ee-product">
                        <!-- Image -->

                            <div class="image">

                                <a href="javascript::void()" class="img"><img src="{{ asset($producto->img_producto)}}" height="300" alt="Product Image"></a>

                                 <div class="wishlist-compare">
                                    <br><br>
                                    <a href="#" data-tooltip="Vista rapida"><i class="ti-eye"></i></a>

                                    @if($permisoFavorito )
                                        <a data-url="{{ url('Favoritos/addToFavoritos') }}" data-tooltip="Favorito" data-id="{{ $producto->id }}" onclick="addToFavoritos(this)" @foreach($proFavoritos as $proFav)@if($proFav->id_rel == $producto->id) data-favorito="{{ $producto->id }}" style='background-color: {{ $_SESSION['data']['conf'][0]->navbar_color }};' @endif @endforeach><i class="ti-heart">
                                        </i></a>
                                    @endif
                                   
                                </div>
                                <div class="wishlist-compare">      
                                    @if($permisoPromociones)
                                        @foreach($allPromociones as $pro)
                                            @if($pro->inicio <= $hoy)
                                                @if($hoy <=  $pro->inicio)
                                                    @if($pro->id_producto == $producto->id)
                                                        <span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">
                                                                {{$pro['mensaje']}}
                                                        </span>
                                                    @endif
                                                @endif
                                            @endif    
                                        @endforeach
                                    @endif
                                </div>
                                <a href="#" dataid="{{ $producto->id }}" class="add-to-cart"><i class="ti-shopping-cart"></i><span>AÑADIR</span></a>

                            </div>

                            <!-- Content -->
                            <div class="content">

                                <!-- Category & Title -->
                                <div class="category-title">

                                    <a href="#" class="cat">{{ $producto->cat_nombre}}</a>
                                    <h5 class="title"><a href="single-product.html">{{$producto->pro_nombre}}</a></h5>

                                </div>

                                <!-- Price & Ratting -->
                                <div class="price-ratting">

                                    <h5 class="price">$ {{number_format($producto->pro_precio_venta)}}</h5>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>

                                </div>

                            </div>

                        </div><!-- Product End -->
                        
                        <!-- Product List Start -->
                        <div class="ee-product-list">

                            <!-- Image -->
                            <div class="image">

                                <a href="single-product.html" class="img"><img src="{{ asset($producto->img_producto)}}" height="300" alt="Product Image"></a>

                            </div>

                            <!-- Content -->
                            <div class="content">

                                <!-- Category & Title -->
                                <div class="head-content">

                                    <div class="category-title">
                                        <a href="#" class="cat">{{ $producto->cat_nombre}}</a>
                                        <h5 class="title"><a href="single-product.html">{{ $producto->pro_nombre}}</a></h5>
                                    </div>

                                    
                                    <h5 class="price">$ {{number_format($producto->pro_precio_venta)}}</h5>

                                </div>
                                
                                <div class="left-content">
                                   
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    
                                    <div class="desc">
                                        <p>{{$producto->pro_descripcion}}</p>
                                    </div>
                                    


                                    <div class="actions">

                                        <a href="#" class="add-to-cart" dataid="{{ $producto->id }}"><i class="ti-shopping-cart"></i><span>AÑADIR</span></a>

                                        <div class="wishlist-compare">

                                            <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>

                                            @if($permisoFavorito )
                                                <a data-url="{{ url('Favoritos/addToFavoritos') }}" data-tooltip="Favorito" data-id="{{ $producto->id }}" onclick="addToFavoritos(this)" @foreach($proFavoritos as $proFav)@if($proFav->id_rel == $producto->id) data-favorito="{{ $producto->id }}" style='background-color: {{ $_SESSION['data']['conf'][0]->navbar_color }};' @endif @endforeach><i class="ti-heart">
                                                </i></a>
                                            @endif
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="right-content">
                                    <div class="specification">
                                    @if($permisoPromociones)
                                        @foreach($allPromociones as $pro)
                                            @if($pro->inicio <= $hoy)
                                                @if($hoy <=  $pro->inicio)
                                                    @if($pro->id_producto == $producto->id)
                                                        <span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">
                                                                {{$pro['mensaje']}}
                                                        </span>
                                                    @endif
                                                @endif
                                            @endif    
                                        @endforeach
                                    @endif
                                    <h5>Caracteristicas</h5>
                                        <ul>
                                            <?php $arr = [];  ?>    
                                                @foreach($producto->caracteristicas as $key=> $caracteristica)
                                                    @if( array_key_exists($caracteristica->sftCampo->campo_nombre, $arr))
                                                        <?php $arr[$caracteristica->sftCampo->campo_nombre].=" / ".$caracteristica->valor; ?>
                                                    @else
                                                        <?php $arr[$caracteristica->sftCampo->campo_nombre] = $caracteristica->valor; ?>
                                                    @endif
                                                @endforeach
                                            @foreach($arr as $key => $value)
                                                <li> {{$key}} : {{$value}}  </li>
                                            @endforeach
                                        </ul>
                                            @if($permisoInventario)   
                                                 @foreach ($inventario as $inv)
                                                    @if($inv->id_producto == $producto->id)
                                                        @if($inv->cantidad == "" or $inv->cantidad == 0)
                                                            <li>Cantidad:-</li>
                                                        @else
                                                            <li><span class="availability">Cantidad: <span>{{$inv->cantidad}}</span></span></li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                    </ul>
                                    </div>
                                    
                                </div>

                            </div>

                        </div><!-- Product List End -->
                        
                        </div>
            @endforeach
        </div>
    </div>
</div><!-- Best Sell Product Section End -->

