<!-- Footer Section Start -->
<div class="footer-section section bg-ivory">
   
    <!-- Footer Top Section Start -->
    <div class="footer-top-section section pt-90 pb-50">
        <div class="container">
           <div class="row">
                <div class="col-lg-6 col-md-6 col-12 mb-40">
                    <div class="footer-widget">
                       <center>
                        <h4 class="widget-title">CONTACT INFO</h4>
                        
                        <p class="contact-info">
                            <span>Direcciòn</span>
                            ########### <br>
                        </p>
                        <p class="contact-info">
                            <span>Telefono</span>
                            <a href="tel:01234567890">01234 567 890</a>
                        </p>
                        
                        <p class="contact-info">
                            <span>Web</span>
                            <a href="mailto:info@example.com">info@example.com</a>
                            <a href="#">www.example.com</a>
                        </p>
                        </center>
                    </div>
                </div><!-- Footer Widget End -->
                
                <!-- Footer Widget Start -->
                <div class="col-lg-6 col-md-6 col-12 mb-40">
                    <div class="footer-widget">
                       <div class="footer-logo">
                            <center>
                                <h4 class="widget-title">{{$titulo}}</h4><img src="{{ asset($_SESSION['data']['conf'][0]->img_logo)  }}" width="120" height="110" style="border: 1px solid black;border-radius: 100%;" alt="E&E - Electronics eCommerce Bootstrap4 HTML Template"></center>
                        </div>
                    </div>
                </div><!-- Footer Widget End -->
            </div>
            
        </div>
    </div><!-- Footer Bottom Section Start -->
   
    <!-- Footer Bottom Section Start -->
    <div class="footer-bottom-section section">
        <div class="container">
            <div class="row">
                
                <!-- Footer Copyright -->
                <div class="col-lg-6 col-12">
                    <div class="footer-copyright"><p>&copy; Copyright, 2019 Todos los derechos reservados por <a href="https://softheory.com/">Softheory - Agencia digital</a></p></div>
                </div>
                
                <!-- Footer Payment Support -->
                <div class="col-lg-6 col-12">
                    <div class="footer-payments-image"><img src="{{ asset('assetsShopCart/images/payment-support.png')}}" alt="Payment Support Image"></div>
                </div>
                
            </div>
        </div>
    </div><!-- Footer Bottom Section Start -->
    
</div><!-- Footer Section End -->

<!-- JS
============================================ -->

<!-- jQuery JS -->
<script src="{{ asset('Modulos/ShopCart/js/vendor/jquery-1.12.4.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{ asset('Modulos/ShopCart/js/popper.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('Modulos/ShopCart/js/bootstrap.min.js')}}"></script>
<!-- Plugins JS -->
<script src="{{ asset('Modulos/ShopCart/js/plugins.js')}}"></script>

<!-- Main JS -->
<script src="{{ asset('Modulos/ShopCart/js/main.js')}}"></script>

</body>

</html>