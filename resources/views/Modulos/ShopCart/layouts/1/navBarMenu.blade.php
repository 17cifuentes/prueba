
    <!-- Header Bottom Start -->
    <div class="header-bottom header-bottom-one header-sticky">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col mt-15 mb-15">
                    <!-- Logo Start -->
                    <div class="header-logo">
                        <a href=""><img width="60" height="60" src="{{ asset($_SESSION['data']['conf'][0]->img_logo)  }}" alt="Car Icon" style="border: 1px solid black;border-radius: 100%;"> </a>
                    </div><!-- Logo End -->
                </div>

                <div class="col order-12 order-lg-2 order-xl-2 d-none d-lg-block">
                    <!-- Main Menu Start -->
                    <div class="main-menu">
                        <nav>
                            <ul>
                                @foreach($_SESSION[MENU_SHOPCART] as $menu)
                                    <?php $x=0 ?> 
                                    @foreach($_SESSION[SUBMENU_SHOPCART] as $submenu)
                                        <?php $x++ ?>
                                        @if($x == 1)
                                            <!--Encabezado de sub-menús-->
                                            <li class="menu-item-has-children"><a href="blog-1-column-left-sidebar.html">{{$menu->nombre}}</a>
                                                <ul class="sub-menu">
                                        @endif
                                            <!--items de sub-menús-->
                                        @if($submenu->menu_id == $menu->id)
                                            <li><a href="">{{ $submenu->nombre }}</a></li>
                                        @endif
                                            <!--finalización de submenu de sub-menús-->
                                        @if($x == count($_SESSION[SUBMENU_SHOPCART]))
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                    @if($x == 0)
                                        <li><a href="#">{{$menu->nombre}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </nav>
                    </div><!-- Main Menu End -->
                </div>
                <div class="col order-2 order-lg-12 order-xl-12">
                    <!-- Header Shop Links Start -->
                    <div class="header-shop-links">
                        
                        <!-- Compare 
                        <a href="compare.html" class="header-compare"><i class="ti-control-shuffle"></i></a>
                        
                        <a href="wishlist.html" class="header-wishlist"><i class="ti-heart"></i> <span class="number">3</span></a>
                         Cart -->
                        <a href="" class="header-cart"><i class="ti-shopping-cart"></i> <span class="number" id="cantProCart">0</span></a>
                        @if(Auth::user())
                            <a class="" href="{{url('home')}} " style="border: 1px solid black;border-radius: 110px;padding: 0.3em;margin-top: -0.5em">
                                <i  class="icofont icofont-user-alt-5"></i>
                                <span > {{Auth::user()->name}}</span>
                            </a>
                        @else
                            <a href="{{ url('ingresar/'.$id.'/0')}}">
                                <i class="icofont icofont-user-alt-5"></i>
                                <span>Ingresar</span>
                            </a>
                        @endif
                        
                    </div><!-- Header Shop Links End -->
                </div>
                <!-- Mobile Menu -->
                <div class="mobile-menu order-12 d-block d-lg-none col"></div>

            </div>
        </div>
    </div><!-- Header Bottom End -->