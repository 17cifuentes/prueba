<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $titulo }} - Tienda Online</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon -->
    <link href="{{ asset($icono) }}" rel="shortcut icon" type="image/png">
    
    <!-- CSS
	============================================ -->
   
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('Modulos/ShopCart/css/bootstrap.min.css')}}">
    
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{ asset('Modulos/ShopCart/css/icon-font.min.css')}}">
    
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('Modulos/ShopCart/css/plugins.css')}}">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ asset('Modulos/ShopCart/css/style.css')}}">
    
    <!-- Modernizer JS -->
    <script src="{{ asset('Modulos/ShopCart/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    <!-- Archivos del modulo -->
    <link rel="stylesheet" href="{{ asset('Modulos/ShopCart/css/shopCart.css')}}">
    <script src="{{ asset('Modulos/ShopCart/js/shopCart.js')}}"></script>    
</head>