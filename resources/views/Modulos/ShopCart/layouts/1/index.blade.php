@if(isset($_SESSION['data']["conf"][0]->title))
        <?php
            $titulo = $_SESSION[DATA]["conf"][0]->title;
            $icono = $_SESSION[DATA]["conf"][0]->ico;
        ?>
    @else
        <?php 
            $titulo = $_SESSION[DATA][CLIENTE_TITLE];
            $icono = "";
        ?>
    @endif
    <input type="hidden" value="{{ $_SESSION['data']['conf'][0]->navbar_color }}" id="colorCliente">
    <input type="hidden" id="productosCart" value="{{ json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]) }}">
@include(PATH_MOD_SHOPCART_HEADER_1)
@include(PATH_MOD_SHOPCART_NAVBAR_1)
@include(PATH_MOD_SHOPCART_NAVBARMENU_1)
@include(PATH_MOD_SHOPCART_NAVBARCATEGORIAS_1)
@include('Modulos.ShopCart.FormCart')
@include(PATH_MOD_SHOPCART_PRODUCTOS_1)
@include(PATH_MOD_SHOPCART_BENEFICIOS_1)
@include(PATH_MOD_SHOPCART_MODAL_1)
@include(PATH_MOD_SHOPCART_FOOTER_1)
@include('Modulos.ShopCart.layouts.1.modalPeticionDomicilio')