<center><font face="impact" size="2" color="black"><div class="section-title-one" data-title="PRODUCTOS"><h3>PRODUCTOS</h3></div></font></center>
		<div class="container">
				<div class="product-one">
                    <center><div class="product-showing">
                            <font face="impact" size="2" color="black"><p>Productos</p></font>
                                <select name="showing" class="nice-select" id="cantProducto">
                                    <option value="1">8</option>
                                    <option value="2">12</option>
                                    <option value="3">16</option>
                                    <option value="4">20</option>
                                    <option value="5">24</option>
                                </select>
                    </div></center>
                    <br>
                    <br>
                    <div class="Col-lg-12">
                     @foreach($_SESSION[SHOPCART_PRODUCTOS] as $producto)
					<div class="col-md-3 product-left">
						<div class="product-main simpleCart_shelfItem">
							<a href="{{url('carritoCompras/descripcionProductos?id='.$producto->id.'&idCliente='.$id)}}" class="mask"><img class="img-responsive zoom-img" src="{{ asset($producto->img_producto)}}" width="150" height="150"/></a>
							<div class="product-bottom">
								<div class="category-title">
                                    <a href="#" class="cat">{{ $producto->cat_nombre}}</a>
                                    <h3 class="title"><a data-toggle="tooltip" title="ver detalle" href="{{ url('carritoCompras/descripcionProductos?id='.$producto->id.'&idCliente='.$id)}}">{{$producto->pro_nombre}}</a></h3>
                                </div>
                                <div class="price-ratting">
                                    <h5 class="price">$ {{number_format($producto->pro_precio_venta)}}</h5>
                                </div>

                                <div class="ee-product-list">
                                 <!-- Image -->
                                </div>

                                <div class="wishlist-compare">
                                            <a href="#" data-tooltip="Compare"><i class="ti-control-shuffle"></i></a>
                                           @if($permisoFavorito)
                                            <div ><a @foreach($proFavoritos as $proFav)
                                            @if($proFav->id_rel == $producto->id) 
                                            data-favorito="{{ $producto->id }}" style='background: {{ $_SESSION['data']['conf'][0]->navbar_color }} none repeat scroll 0 0;' 
                                            @endif @endforeach data-url="{{ url('Favoritos/addToFavoritos') }}" data-tooltip="Favorito" data-id="{{ $producto->id }}" onclick="addToFavoritos(this)" class="btn btn-default" data-toggle="tooltip" title="Agregar a favoritos" 
                                            ><i class="fa fa-heart"></i></a></div>

                                        @endif
                                        </div>

                                        <div class="right-content">
                                    <div class="specification">
                                    @if($permisoPromociones)
                                        @foreach($allPromociones as $pro)
                                            @if($pro->inicio <= $hoy)
                                                @if($hoy <=  $pro->inicio)
                                                    @if($pro->id_producto == $producto->id)
                                                        <span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">
                                                                {{$pro['mensaje']}}
                                                        </span>
                                                    @endif
                                                @endif
                                            @endif    
                                        @endforeach
                                    @endif
                                
                                        <a data-toggle="tooltip" title="Añadir" href="javascript:void(0)" onclick="addProToCart({{ $producto->id }})" dataid="{{ $producto->id }}" class="add-cart">AÑADIR AL CARRITO</a>
                                    <br>
                                            @if($permisoInventario)   
                                                 @foreach ($inventario as $inv)
                                                    @if($inv->id_producto == $producto->id)
                                                        @if($inv->cantidad == "" or $inv->cantidad == 0)
                                                            <li>Cantidad:-</li>
                                                        @else
                                                            <li><span class="availability">Cantidad: <span>{{$inv->cantidad}}</span></span></li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                    </ul>
                                </div>
                                    </div>
							</div>
						</div>
					</div>
					 @endforeach
					<div class="clearfix"></div>
				</div>					
			</div>
		</div>
	  </div>
    </div>

   <center><ul class="pagination pagination-lg">
        <li><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
        <li><a href="#">Página 1 de {{ count($_SESSION[SHOPCART_PRODUCTOS]) }}</a></li>
        <li><a href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
    </ul></center>
    <br>
    <br>
    <br>


    <!--Hola Mundo -->