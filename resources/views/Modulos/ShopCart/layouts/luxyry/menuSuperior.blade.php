<div class="top-header">
		<div class="container">
			<div class="top-header-main">
				<div class="col-md-6 top-header-left">
					<div class="drop">
						<div class="col order-2 order-xs-2 order-lg-12 mt-10 mb-10">
						<div  align="left" class="col-md-1 top-header-left">
						<div class="cart box_1">
						<a href="{{ url ('carritoCompras/carrito?id='.$_SESSION['data']['conf'][0]->id)}}">
							{{ $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] }}
								<img  align="left" src="{{ asset ('Modulos/ShopCart/luxyry/images/cart-1.png')}}"/>
						</a>
						<a href="{{ url ('carritoCompras/carrito?id='.$_SESSION['data']['conf'][0]->id)}}" class="simpleCart_empty">CARRITO</a>
					<!--<p><a href="javascript:;" class="simpleCart_empty">Compras</a></p>-->
					<div class="clearfix"></div>
					</div>
				</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

			<div align="right"class="header-account-links">
                        @if(Auth::user())
                            <a href="{{url('home')}}"><img width="50" height="50" src="{{ asset(Auth::user()->img) }}" style="border: 1px solid black;border-radius: 100%;">
                                <span>{{Auth::user()->name}}</span>
                            </a>
                        @else
                            <a href="{{ url('ingresar/'.$id.'/0')}}"><i class="icofont icofont-user-alt-10" style="background: white"></i>
                                <span>Ingresar</span>
                            </a>
                        @endif
                    </div><!-- Header Account Links End -->
				<div class="clearfix"></div>
			</div>
		</div>
	</div>	
	<div  class="logo">
		<a href="{{ url ('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->id)}}"><h1>Tienda</h1></a>
	</div>