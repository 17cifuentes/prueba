@extends('Modulos.ShopCart.layouts.luxyry.layoutsLuxyry')
@section('contenido')
<!--opciones primarias del carrito de compras-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="{{ url ('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->id)}}">Inicio</a></li>
					<li class="active">Revisar</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="ckeckout">
		<div class="container">
			 @include('Modulos.ShopCart.FormCart')
		</div>

	</div>

@stop