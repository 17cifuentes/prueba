<center><font face="impact" size="2" color="black"><div class="section-title-one" data-title="CATEGORIES"><h3>CATEGORÍAS</h3></div></font></center>
<div class="about"> 
		<div class="container">
			<div class="about-top grid-1">
				 @foreach($_SESSION[CATEGORIES] as $cat)
                <div class="col-md-3 about-left">
					<figure class="effect-bubba">
						<img class="img-responsive" src="{{ asset ('Modulos/ShopCart/luxyry/images/softheory.jpg')}}" alt=""/>
						<figcaption>
							<h2>{{ $cat->cat_nombre }}</h2>
							<p>{{ $cat->cat_descripcion }}</p>	
						</figcaption>			
					</figure>
				</div>
                 @endforeach
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!--about-end-->