@extends('Modulos.ShopCart.layouts.luxyry.layoutsLuxyry')
	@section('contenido')
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="{{ url ('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->id)}}">Inicio</a></li>
					<li class="active">Contacto</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<!--contact-start-->
	<div class="contact">
		<div class="container">
			<div class="contact-top heading">
				<h2>CONTACTO</h2>
			</div>
				<div class="contact-text">
				<div class="col-md-3 contact-left">
						<div class="address">
							<h5>Dirección</h5>
							<p>Softheory,
							<span> Calle 70 #7h bis 17</span>
							</p>
						</div>
						<div class="address">
							<h5>Info</h5>
							<p>Tel:+(57) 316 723 5557, 
							<span></span>
							Email: <a href="mailto:example@email.com">info@softheory.com</a></p>
						</div>
					</div>
					<div class="col-md-9 contact-right">
						<form>
							<input type="text" placeholder="Nombre" required="">
							<input type="text" placeholder="Telefono" required="">
							<input type="text"  placeholder="Correo" required="">
							<textarea placeholder="Mensaje" required=""></textarea>
							<div class="submit-btn">
								<input  href="{{ url ('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->id)}}" type="submit" value="Enviar">
							</div>
						</form>
					</div>	
					<div class="clearfix"></div>
				</div>
		</div>
	</div>
	<!--contact-end-->

	<!--map-start-->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6632.248000703498!2d151.265683!3d-33.7832959!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12abc7edcbeb07%3A0x5017d681632bfc0!2sManly+Vale+NSW+2093%2C+Australia!5e0!3m2!1sen!2sin!4v1433329298259" style="border:0"></iframe>
	</div>
	<!--map-end-->
@stop	