<!--CORE GENERAL -->
	@include('layouts.SFTHeadGenerals')
	@include('layouts.SFTConfHead')
	@include('layouts.SFTConfTitle')
	@include('layouts.SFTConfCss')
<!-- *************************************** -->
	
	@include('Modulos.ShopCart.layouts.luxyry.header')

	@include('Modulos.ShopCart.layouts.luxyry.menuSuperior')
	@include('Modulos.ShopCart.layouts.luxyry.opcionesHeader')
	@yield('contenido')
	@include('Modulos.ShopCart.layouts.luxyry.informacion')
	<!--CORE GENERAL -->
	@include('layouts.SFTGeneralScript')
	@include('layouts.SFTCoreScript')	
	@include('layouts.SFTConfScript')
	@include('Modulos.ShopCart.layouts.luxyry.footer')
<!-- *************************************** -->
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
</script>