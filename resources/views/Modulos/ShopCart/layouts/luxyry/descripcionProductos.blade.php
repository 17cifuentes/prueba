@extends('Modulos.ShopCart.layouts.luxyry.layoutsLuxyry')
@section('contenido')
@foreach($allProductos as $producto)
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="{{ url ('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->id)}}">Inicio</a></li>
					<li class="active">Descripción</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<!--start-single-->
	<div class="single contact">
		<div class="container">
			<div class="single-main">
				<div class="col-md-9 single-main-left">
				<div class="sngl-top">
					<div class="col-md-5 single-top-left">	
						<div class="product-images">
            <div class="main-image images">
                <img alt="big images" src="{{ asset($producto->img_producto)}}">
            </div>
        </div>
						<!-- FlexSlider -->
					</div>
						
					<div class="col-md-7 single-top-right">
						<div class="single-para simpleCart_shelfItem">
						<font face="impact" size="2" color="black">Nombre:</font>
							<h2>{{$producto->pro_nombre}}</h2>
							<div class="star-on">
								<ul class="star-footer">
										<li><a href="#"><i> </i></a></li>
										<li><a href="#"><i> </i></a></li>
										<li><a href="#"><i> </i></a></li>
										<li><a href="#"><i> </i></a></li>
										<li><a href="#"><i> </i></a></li>
									</ul>
								<div class="review">
									<a href="#"> TU OPINION ES IMPORTANTE </a>
									
								</div>
							<div class="clearfix"> </div>
							</div>
							
							
							<div class="category-title">
								<a href="#" class="cat">{{ $producto->cat_nombre}}</a>
								<div class="clearfix"></div>
							</div>

					<font face="impact" size="2" color="black"><h4>Valor:</h4></font>
                       <h5 class="price">$ {{number_format($producto->pro_precio_venta)}}</h5>
                    
					<font face="impact" size="2" color="black"><h4>Descripción:</h4></font>
						<div class="desc">
							<p>{{$producto->pro_descripcion}}</p>
						</div>

                        <font face="impact" size="2" color="black"><h4>Caracteristicas:</h4></font>
                        <ul>
                            <?php $arr = [];  ?>    
                        @foreach($producto->caracteristicas as $key=> $caracteristica)
                            @if( array_key_exists($caracteristica->sftCampo->campo_nombre, $arr))
                            <?php $arr[$caracteristica->sftCampo->campo_nombre].=" / ".$caracteristica->valor; ?>
                            @else
                            <?php $arr[$caracteristica->sftCampo->campo_nombre] = $caracteristica->valor; ?>
                            @endif
                        @endforeach

						@foreach($arr as $key => $value)
						 <p><li> {{$key}} : {{$value}}  </li></p>
                    	@endforeach
                
							
						 <div class="actions">
                            <a data-toggle="tooltip" title="Añadir" href="javascript:void(0)" onclick="addProToCart({{ $producto->id }})" dataid="{{ $producto->id }}" class="add-cart">AÑADIR AL CARRITO</a>
                        </div>
							
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>

			</div>
			</div>
		</div>
	</div>
	<!--end-single-->
@endforeach
@include('Modulos.ShopCart.layouts.JuniorHome.modalPeticionDomicilio')
@include('core.Usuarios.modalCrearUsuarioExterno')
@include('core.cargando')
@stop	