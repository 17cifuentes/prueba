<style type="text/css">

h1,h2,h3{
	font-family: Arial, sans-serif;
	font-weight:normal;
}
</style>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<section style="margin:5%">
		<center>
			<h1>POLÍTICA DE TRATAMIENTO DE DATOS PERSONALES</h1>
			<h2>IMPRONOVA</h2>
		</center>
		<h3>Introducción</h3>
		<p>
			La Constitución Política de Colombia consagra el derecho que tienen todas las personas a conocer, actualizar y rectificar la información que exista sobre ellas en bases de datos o
			archivos de entidades públicas o privadas. La constitución ordena a quienes tienen Datos
			Personales de terceros a respetar los derechos y garantías previstos en la misma cuando se
			recolecta, trata y circula esta información.
		</p>
		<p>
			La Constitución Política de Colombia consagra el derecho de cualquier persona a la
			información, de manera tal que reciba información veraz e imparcial.
		</p>
		<p>
			Impronova. (en adelante “La Compañía”) está comprometida con el cumplimiento de la
			regulación en materia de protección de Datos Personales y con el respeto de los derechos de los Titulares de la información. Por esta razón, adopta la presente Política de Tratamiento de Datos Personales (en adelante, “La Política”) obligatoria en todas las actividades que involucren Tratamiento de Datos Personales y de obligatorio cumplimiento por parte de La Compañía, sus Administradores, Colaboradores y los terceros con quien éste se relacione contractual o comercialmente.
		</p>
		<p>
			La Compañía, dentro del desarrollo de su objeto social, diariamente recibe, transmite y trata Datos Personales por lo cual se ha redactado la presente política a la cual la Compañía se adherirá estrictamente.
		</p>
		<h3>Objetivo</h3>
		<p>
			El objetivo de la presente Política es garantizar el adecuado cumplimiento de la Ley de
			Protección de Datos Personales aplicable, así como la definición de los lineamientos para la atención de consultas y reclamos de los Titulares de los Datos Personales sobre los que la Compañía realiza algún tipo de Tratamiento.
		</p>
		<h3>Alcance</h3>
		<p>
			Esta Política es obligatoria y de estricto cumplimiento por parte de la Compañía, sus
			colaboradores y los terceros quienes la representan o actúan por ella, o con los que la
			Compañía tiene algún tipo de vínculo, sea legal, comercial o convencional.
		</p>
		<p>
			Todos los Colaboradores de la Compañía, deben observar, respetar, cumplir y hacer cumplir
			esta Política.
		</p>
		<h3>Definiciones</h3>
		<p>
			Para claridad, a continuación se definen los términos relativos al derecho fundamental de
			protección de datos personales, importantes para la comprensión de la presente política de
			tratamiento de datos personales:
		</p>
		<h3>POLÍTICA DE TRATAMIENTO DE DATOS PERSONALES EDICIÓN 2.0 / 06.2017 3/15</h3>
		<p>
			a. Habeas data o protección de datos personales: es un derecho fundamental que otorga la
			potestad a su titular de determinar quién o cómo se administra la información personal que le concierne y, en ese sentido, otorga la facultad de conocer, actualizar, rectificar, autorizar, incluir y excluir información que es considerada como personal y que es administrada en la base de datos de una entidad pública o privada.
		</p>
		<p>
			b. Autorización: consentimiento previo, expreso e informado del titular del dato para llevar a cabo el tratamiento.
		<p>
			c. Consulta: solicitud del titular del dato o las personas autorizadas por éste o por la Compañía para conocer la información que reposa sobre ella en bases de datos o archivos.
		</p>
		<p>
			d. Bases de Datos: es el conjunto de datos personales que sean objeto de tratamiento.
		</p>
		<p>
			e. Dato personal: cualquier información vinculada o que pueda asociarse a una o varias personas naturales determinadas o determinables. Estos datos se clasifican en sensibles,
			públicos, privados y semiprivados.
		</p>
		<p>
			f. Dato personal privado: es el dato que por su naturaleza íntima o reservada sólo es relevante para la persona titular del dato.
		</p>
		<p>
			g. Dato personal semiprivado: es semiprivado el dato que no tiene naturaleza íntima, reservada,ni pública y cuyo conocimiento o divulgación puede interesar no sólo a su titular sino a cierto sector o grupo de personas o a la sociedad en general, como el dato financiero y crediticio de actividad comercial o de servicios a que se refiere el Titulo IV de la Ley 1266 de 2008.
		</p>
		<p>
			h. Dato personal público: es el dato calificado como tal según los mandatos de la Ley o de la Constitución Política y todos aquellos que no sean semiprivados o privados, de conformidad con la Ley 1266 de 2008. Son públicos, entre otros, los datos contenidos en documentos públicos, registros públicos, gacetas y boletines oficiales y sentencias judiciales debidamente ejecutoriadas que no estén sometidos a reserva, los relativos al estado civil de las personas, a su profesión u oficio y a su calidad de comerciante o de servidor público. Son públicos los datos personales existentes en el registro mercantil de las Cámaras de Comercio (artículo 26 del C.
			Co). Estos datos pueden ser obtenidos y ofrecidos sin reserva alguna y sin importar si hacen alusión a información general, privada o personal.
		</p>
		<p>
			i. Dato personal sensible: son “aquellos que afectan la intimidad del Titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición, así como los datos relativos a la salud, a la vida sexual y los datos biométricos (huellas dactilares, entre otros). La Ley 1581 de 2012 prohíbe el tratamiento de datos sensibles con excepción de los siguientes casos: (i) cuando el titular otorga su consentimiento, (ii) el Tratamiento es necesario para salvaguardar el interés vital del Titular y este se encuentre física o jurídicamente
			incapacitado; (iii) el tratamiento es efectuado en el curso de las actividades legítimas y con las debidas garantías por parte de una fundación, ONG, asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, religiosa o sindical, siempre que se refieran exclusivamente a sus miembros o a las personas que mantengan contactos regulares por razón de su finalidad, (iv) el Tratamiento se refiere a datos que sean necesarios para elreconocimiento, ejercicio o defensa de un derecho en un proceso judicial, y (v) el Tratamiento tenga una finalidad histórica, estadística o científica, en este último caso deben adoptarse las medidas conducentes a la supresión de identidad de los titulares.
		</p>
		<p>
			j. Datos personales de los niños, niñas y adolecentes (NNA): se debe tener en cuenta que
			aunque la Ley 1581 de 2012 prohíbe el tratamiento de los datos personales de los niños, niñas y adolescentes, salvo aquellos que por su naturaleza son públicos, la Corte Constitucional precisó que independientemente de POLÍTICA DE TRATAMIENTO DE DATOS PERSONALES EDICION 2.0 / 06.2017 4/15 la naturaleza del dato, se puede realizar el tratamiento de estos “siempre y cuando el fin que se persiga con dicho tratamiento responde al interés superior de los niños, niñas y adolescentes y se asegure sin excepción alguna el respeto a sus derechos prevalentes“.
		</p>
		<p>
			k. Encargado del tratamiento: persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, realice el tratamiento de datos personales por cuenta del Responsable del Tratamiento. La Compañía podrá realizar el tratamiento de sus datos personales a través de Encargados.
		</p>
		<p>
			l. Reclamo: solicitud del titular del dato o las personas autorizadas por éste o por la Ley para corregir, actualizar o suprimir sus datos personales
		</p>
		<p>
			m. Responsable del tratamiento: persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, decida sobre la base de datos y/o el Tratamiento de los datos.
		</p>
		<p>
			n. Titular del dato: es la persona natural cuyos datos personales sean objeto de tratamiento.
		</p>
		<p>
			o. Tratamiento: cualquier operación o conjunto de operaciones sobre datos personales tales como la recolección, almacenamiento, uso, circulación o supresión.
		</p>
		<p>
			p. Transferencia: envío de datos personales que realiza el Responsable o el Encargado desde Colombia a un Responsable que se encuentra dentro (transferencia nacional) o fuera del paí (transferencia internacional).
		</p>
		<p>
			q. Transmisión: tratamiento de datos personales que implica la comunicación de los mismos dentro (transmisión nacional) o fuera de Colombia (transmisión internacional) y que tiene por objeto la realización de un tratamiento por el Encargado por cuenta del Responsable.
		</p>
	</section>
</body>
</html>
