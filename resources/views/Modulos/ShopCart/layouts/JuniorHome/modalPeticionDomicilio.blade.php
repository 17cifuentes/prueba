<div class="modal" id="modalPeticionDomicilio" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Realizar Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
          Selecciona el método por el cual deseas hacer el pedido   
          <div class="col-12 pp calContent"></div>   
      </div>
      <div class="modal-body">
          Al escoger uno de los métodos, usted está aceptando la política de privacidad de datos de Impronova.
      <div>
        <center>
        <p id="termCondiciones">
          <a target="blank" href="{{url('carritoCompras/politicaPrivacidad')}}" >Ver política de privacidad
        </a>
        </p>
      </center>  
      </div>
      
        
      </div>
      <div class="modal-footer">
        <center>
        <button type="button" class="btn btn-primary btn-lg" id="Contraentrega" onclick="registerProductsDatabases(this.id)"> Contra entrega</button>

        <button type="button" class="btn btn-link btn-lg" id="Online" onclick="registerProductsDatabases(this.id)"> Online </button>
        </center>
      </div>
    </div>
  </div>
</div>