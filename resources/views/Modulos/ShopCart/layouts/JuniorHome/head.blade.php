
	<meta name="description" content="">
	<!-- Favicons -->

	<!-- Google font (font-family: 'Lobster', Open+Sans;) -->
	<link href="https://fonts.googleapis.com/css?family=Lobster+Two:400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Bubblegum+Sans" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{ asset('Modulos/ShopCart/JuniorHome/junior/css/bootstrap.min.css')}}">

	<link rel="stylesheet" href="{{ asset('Modulos/ShopCart/JuniorHome/junior/css/plugins.css')}}">
	<link rel="stylesheet" href="{{ asset('Modulos/ShopCart/JuniorHome/css/style.css')}}">

	<!-- Cusom css -->
   <link rel="stylesheet" href="{{ asset('Modulos/ShopCart/JuniorHome/junior/css/custom.css')}}">

	<!-- Modernizer js -->
	<script src="{{ asset('Modulos/ShopCart/JuniorHome/junior/js/vendor/modernizr-3.5.0.min.js')}}"></script>
