
<section class="junior__service bg-image--1 section-padding--bottom section--padding--xlg--top" style="margin-top:-4%">	
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12" id="bienvenido">
				<div class="section__title text-center" >
					<h2 class="title__line" style="margin-top:-8%">Bienvenido a {{$_SESSION['data']['conf'][0]->cliente_nombre}}</h2>
					<hr class="titleLine">
					<!-- <p>Nuestras categorias</p> -->
				</div>
			</div>
		<!-- Start Single Service -->
			<div id="categoriasOrden" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			@foreach($_SESSION[CATEGORIES] as $cat)
			<div class="col-lg-6 col-md-6 col-sm-6 col-12 md-mt-60 sm-mt-60" style="margin-top: 6em;">
				<div class="service bg--white border__color wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
					<div class="service__icon" >
						{!! $cat->sftIcono->formato !!}
					</div>
					<div class="service__details">
						
                        <h6><a onclick="Categoriashijos('{{$cat->id}}')" data-toggle="tooltip" title="ir a {{ $cat->cat_nombre }}" class="catColor">{{ $cat->cat_nombre }}</a></h6>
                    	<p>{{ $cat->cat_descripcion }}</p>
                    	<!-- href="{{url('carritoCompras/buscadorCategorias?id='.$id.'&idCat='.$cat->id)}}"-->
                    	<a onclick="Categoriashijos('{{$cat->id}}')" ><img class="proImg" src="{{asset($cat->sftFile->path)}}"></a> 
					</div>
				</div>
			</div>
			@endforeach	
			</div>
		</div>


	</div>
		
	<div class="container" >
	<div id="promosMes">
	@if(count($_SESSION[SHOPCART_PROMOCION_DEL_MES]) > 0) 
		<div class="col-lg-12 col-sm-12 col-md-12" style="margin-top: 13em">
			<div class="section__title text-center">
			<h2 class="title__line">Promos del mes</h2>
			<hr class="titleLine">
			<!-- <p>Nuestras categorias</p> -->
			</div>
		</div> 
	@endif
	</div>	 
	@foreach($_SESSION[SHOPCART_PROMOCION_DEL_MES] as $promos)
		@if($promos->inicio <= date('Y-m-d'))
			@if(date('Y-m-d') <=  $promos->fin)
                <div class="wrapper" >
                	<div @if(count($_SESSION[SHOPCART_PROMOCION_DEL_MES]) == 1) class="col-lg-12 col-sm-12 col-md-12" style="text-align: center"  @else class="col-lg-4 col-md-6 col-sm-6 col-12" @endif >		
					<div class="gallery wow fadeInUp ">
						<div class="">
							<a  href="{{ url('carritoCompras/mostrarProductoDetalle?id='.$promos->id_producto.'&idCliente='.$id)}}">
								<img class="proImg" data-toggle="tooltip" data-placement="bottom" title="Ver promoción" src="{{asset($promos->sftProducto->img_producto)}}" alt="Promocion del mes">
							</a>
						</div>
						<div class="courses__wrap">
							<div class="courses__date"><i class="fa fa-calendar"></i>Desde el {{$promos->inicio}} hasta el {{$promos->fin}} </div>
							<div class="courses__content">
								<h4><a >{{$promos->sftProducto->pro_nombre}}</a></h4>
								<span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">
	                                {{$promos['mensaje']}}
	                                                                
	                            </span>
								<!-- <ul class="rating d-flex">
									<li><span class="ti-star"></span></li>
									<li><span class="ti-star"></span></li>
									<li><span class="ti-star"></span></li>
									<li><span class="ti-star"></span></li>
									<li><span class="ti-star"></span></li>
								</ul> -->
							</div>
						</div>				
					</div>
				</div>	
                </div>
			@endif
		@endif    			
	@endforeach
		</div>


</section>
	
	
		