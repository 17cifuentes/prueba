<!-- Footer Area -->
		<footer id="footer" class="footer-area footer--2">
			
			<!-- .Start Footer Contact Area -->
			<div class="footer__contact__area bg__cat--2">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="footer__contact__wrapper d-flex flex-wrap justify-content-between">
								<div class="single__footer__address">
									<ul>
										<li><i class="fa fa-home"></i></li>
									</ul>						
									<div class="ft__contact__details">
										<p>{{$_SESSION['data']['conf'][0]->direccion}}</p>
									</div>
								</div>
								<div class="single__footer__address">
									<ul>
										<li><i class="fa fa-phone"></i></li>	
									</ul>
									<div class="ft__contact__details">
										<p>{{$_SESSION['data']['conf'][0]->telefono}}</p>
									</div>
								</div>
								<div class="single__footer__address">
									<ul>
										<li><i class="fa fa-envelope"></i></li>
									</ul>
									
									<div class="ft__contact__details">
										<p>{{$_SESSION['data']['conf'][0]->correo}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .End Footer Contact Area -->
			<div class="copyright  bg--theme">
				<div class="container">
					<div class="row align-items-center copyright__wrapper justify-content-center">
						<div class="col-lg-12 col-sm-12 col-md-12">
							<div class="coppy__right__inner text-center">
								<p>© <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright ©<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Creado por  <i class="fa fa-heart-o" aria-hidden="true"></i>  <a href="https://softheory.com" target="_blank">Softheory</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- //Footer Area -->