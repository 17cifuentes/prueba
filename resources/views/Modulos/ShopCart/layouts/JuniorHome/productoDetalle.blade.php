@extends('Modulos.ShopCart.layouts.JuniorHome.layoutAllJunior')
@section('contenido')
@foreach($allProductos as $producto)

<div class="modal-body">
    <div class="modal-product">
        <!-- Start product images -->
        <div class="product-images">
            <div class="main-image images">
                <img alt="big images" src="{{ asset($producto->img_producto)}}">
            </div>
        </div>
        <!-- end product images -->
        <div class="product-info">
            <h1>{{$producto->pro_nombre}}</h1>
            <div class="rating__and__review">
             <!--    <ul class="rating">
                    <li><span class="ti-star"></span></li>
                    <li><span class="ti-star"></span></li>
                    <li><span class="ti-star"></span></li>
                    <li><span class="ti-star"></span></li>
                    <li><span class="ti-star"></span></li>
                </ul> -->
<!-- 	                                    <div class="review">
                    <a href="#">4 customer reviews</a>
                </div> -->
            </div>
            <div class="price-box-3">
                <div class="s-price-box">
                    <span class="new-price">{{$producto->pro_precio_venta}}</span>
            </div>
            <div class="quick-desc">

                @if(isset($promociones))
                <span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">                                                       
                {{$promociones[0]['mensaje']}}</span>
                @endif
            

                 <p>{{$producto->pro_descripcion}}</p>
            </div>
            <div class="select__color">
                
            </div>
                <?php $arr = [];  ?>    
                @foreach($producto->caracteristicas as $key=> $caracteristica)
                
                    @if( array_key_exists($caracteristica->sftCampo->campo_nombre, $arr))
                        <?php $arr[$caracteristica->sftCampo->campo_nombre].=" / ".$caracteristica->valor; ?>
                    @else
                        <?php $arr[$caracteristica->sftCampo->campo_nombre] = $caracteristica->valor; ?>
                    @endif
                @endforeach
            <div class="social-sharing">
                	@foreach($arr as $key => $value)
                        <p><li> {{$key}} : {{$value}}  </li></p>
                    @endforeach
                
            </div>
            <!-- <div class="social-sharing">
                <div class="widget widget_socialsharing_widget">
                    <h3 class="widget-title-modal">Share this product</h3>
                    <ul class="dacre__social__link--2 bg--2 d-flex">
                        <li class="facebook"><a target="_blank" href="#" class="rss social-icon"><i class="zmdi zmdi-rss"></i></a></li>
                        <li class="linkedin"><a target="_blank" href="#" class="linkedin social-icon"><i class="zmdi zmdi-linkedin"></i></a></li>
                        <li class="pinterest"><a target="_blank" href="#" class="pinterest social-icon"><i class="zmdi zmdi-pinterest"></i></a></li>
                        <li class="tumblr"><a target="_blank" href="#" class="tumblr social-icon"><i class="zmdi zmdi-tumblr"></i></a></li>
                    </ul>
                </div>
            </div> -->

            <div class="addtocart-btn">
            	
                <a  data-toggle="tooltip" title="Añadir artículo" href="javascript:void(0)" class="add-to-cart" dataid="{{ $producto->id }}" onclick="addProToCart({{ $producto->id }})">Añadir</a>

            </div>
        </div><!-- .product-info -->
    </div><!-- .modal-product -->
</div>
@endforeach
@include('Modulos.ShopCart.layouts.JuniorHome.modalPeticionDomicilio')
@include('core.Usuarios.modalCrearUsuarioExterno')
@include('core.cargando')
@stop
