		<!-- Start Our Gallery Area -->
		<section class="junior__gallery__area bg--white section-padding--lg" style="margin-top: -10%">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center">

							<h2 class="title__line">Productos</h2>
                            <hr class="titleLine">
							<p>Nuestros productos</p>
						</div>
					</div>
				</div>
				<div class="row galler__wrap mt--40">
					<!-- Start Single Gallery -->
                    
					@foreach($_SESSION[SHOPCART_PRODUCTOS] as $producto)
					<div class="col-lg-6 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<center><a href="{{ url('carritoCompras/mostrarProductoDetalle?id='.$producto->id.'&idCliente='.$id)}}">
									<img class="proImg2" src="{{ asset($producto->img_producto)}}" alt="gallery images">
								</a></center>
							</div>
							<div class="left-content">

                                   <!--  <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>

                                    </div> -->
                                    
                                    <div class="desc">
                                        <!-- <p>{{$producto->pro_descripcion}}</p> -->
                                        
                                            @if($permisoInventario)   
                                                 @foreach ($inventario as $inv)
                                                    @if($inv->id_producto == $producto->id)
                                                        @if($inv->cantidad == "" or $inv->cantidad == 0)
                                                            <li>Cantidad:-</li>
                                                        @else
                                                            <li><span class="availability">Cantidad: <span>{{$inv->cantidad}}</span></span></li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                    </ul>
                                    </div>
                                </div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">

                                    <li><a  data-toggle="tooltip" title="Ver detalle" href="{{ url('carritoCompras/mostrarProductoDetalle?id='.$producto->id.'&idCliente='.$id)}}"><i class="fa fa-search"></i></a></li>
										@if($permisoFavorito)
                                        	<li @foreach($proFavoritos as $proFav)
                                            @if($proFav->id_rel == $producto->id) 
											data-favorito="{{ $producto->id }}" style='background: {{ $_SESSION['data']['conf'][0]->navbar_color }} none repeat scroll 0 0;' 
											@endif @endforeach data-url="{{ url('Favoritos/addToFavoritos') }}" data-tooltip="Favorito" data-id="{{ $producto->id }}" onclick="addToFavoritos(this)"><a data-toggle="tooltip" title="Agregar a favoritos" 
                                        	><i class="fa fa-heart"></i></a></li>

                                    	@endif

                                    	<li><a  data-toggle="tooltip" title="Agregar al carrito" href="javascript:void(0)" class="add-to-cart"  dataid="{{ $producto->id }}" data-toggle="popover" title="Añadir al carrito" data-content="?" onclick="addProToCart({{ $producto->id }})"><i class="fa fa-shopping-cart"></i></a></li>

                                    	<!-- <li><a href="#" data-tooltip="Compare"><i class="fa ti-control-shuffle"></i></a></li> -->

									</ul>

										@if($permisoPromociones)
                                        @foreach($allPromociones as $pro)
                                            @if($pro->inicio <= $hoy)
                                                @if($hoy <=  $pro->fin)
                                                    @if($pro->id_producto == $producto->id)

                                                        <span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">
                                                                {{$pro['mensaje']}}
                                                                
                                                        </span>
                                                    @endif
                                                @endif
                                            @endif    
                                        @endforeach
                                    @endif
									<div class="actions">							
                                        
                                        <div class="wishlist-compare">
                                    </div>
                                        <h4 class="gallery__title">{{$producto->pro_nombre}}</h4>

                                    </div>

								</div>
							</div>
						</div>	
                         
                          <div class="class__details">
                          <center>
                            <h4 class="gallery__title">{{$producto->pro_nombre}}</h4>
                            
                            <p>Precio: ${{number_format((float)$producto->pro_precio_venta)}}</p>
                                        <?php $arr = [];  ?>    
                                        @foreach($producto->caracteristicas as $key=> $caracteristica)
                                    
                                            @if( array_key_exists($caracteristica->sftCampo->campo_nombre, $arr))
                                            <?php $arr[$caracteristica->sftCampo->campo_nombre].=" / ".$caracteristica->valor; ?>
                                            @else
                                            <?php $arr[$caracteristica->sftCampo->campo_nombre] = $caracteristica->valor; ?>
                                            @endif
                                        @endforeach
                                <ul class="social-sharing">
                                        @foreach($arr as $key => $value)
                                            <p><li> {{$key}} : {{$value}}  </li></p>
                                        @endforeach
                                    
                                </ul>
                            <!-- <div class="class__btn">
                                <a class="dcare__btn btn__gray min__height-btn" href="class-details.html">Admission Now</a>
                            </div> --></center>
                        </div>
					</div>	

					@endforeach
                    
			</div>

		</section>
    @include('Modulos.ShopCart.layouts.JuniorHome.modalPeticionDomicilio')
    @include('core.Usuarios.modalCrearUsuarioExterno')
    @include('core.cargando')
		<!-- End Our Gallery Area -->