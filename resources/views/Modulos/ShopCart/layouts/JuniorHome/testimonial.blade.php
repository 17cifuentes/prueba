		<!-- Start Testimonial Area -->
		<section class="junior__testimonial__area bg-image--2 section-padding--lg">
			<div class="container">
				<div class="row">
					<div class="offset-lg-2 col-lg-8 col-md-12 col-sm-12">
						<div class="testimonial__container">
							<div class="tes__activation--1 owl-carousel owl-theme">
								<div class="testimonial__bg">
									<!-- Start Single Testimonial -->
									<div class="testimonial text-center">
										<div class="testimonial__inner">
											<div class="test__icon">
												<img src="{{ asset('Modulos/ShopScart/JuniorHome/junior/images/testimonial/icon/1.png')}}" alt="icon images">
											</div>
											<div class="client__details">
												<p>Lorem ipsum dolor t dolore magna aliqua. Ut enim ad minim veniam, quis nostexercitation ullamco laboris nisimollit anim id est lsunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatd.</p>
												<div class="client__info">
													<h6>Lora alica</h6>
													<span>Gardients of student</span>
												</div>
											</div>
										</div>
									</div>
									<!-- End Single Testimonial -->
								</div>
								<div class="testimonial__bg">
									<!-- Start Single Testimonial -->
									<div class="testimonial text-center">
										<div class="testimonial__inner">
											<div class="test__icon">
												<img src="{{ asset('Modulos/ShopCart/JuniorHome/junior/images/testimonial/icon/1.png')}}" alt="icon images">
											</div>
											<div class="client__details">
												<p>Kohinur ipsum dolor t dolore magna aliqua. Ut enim ad minim veniam, quis nostexercitation ullamco laboris nisimollit anim id est lsunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatd.</p>
												<div class="client__info">
													<h6>Kohinur alica</h6>
													<span>Gardients of student</span>
												</div>
											</div>
										</div>
									</div>
									<!-- End Single Testimonial -->
								</div>
								<div class="testimonial__bg">
									<!-- Start Single Testimonial -->
									<div class="testimonial text-center">
										<div class="testimonial__inner">
											<div class="test__icon">
												<img src="{{ asset('Modulos/ShopCart/JuniorHome/junior/images/testimonial/icon/1.png')}}" alt="icon images">
											</div>
											<div class="client__details">
												<p>Najnin ipsum dolor t dolore magna aliqua. Ut enim ad minim veniam, quis nostexercitation ullamco laboris nisimollit anim id est lsunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatd.</p>
												<div class="client__info">
													<h6>Najnin alica</h6>
													<span>Gardients of student</span>
												</div>
											</div>
										</div>
									</div>
									<!-- End Single Testimonial -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Testimonial Area -->