		<!-- Start Call To Action -->
		<section class="jnr__call__to__action bg-pngimage--3">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="jnr__call__action__wrap d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between align-items-center">
							<div class="callto__action__inner">
								<h2 class="wow flipInX" data-wow-delay="0.95s">How To Enroll Your Child In A class ?</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </p>
							</div>
							<div class="callto__action__btn">
								<a class="dcare__btn btn__white" href="#">Contact Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Call To Action -->