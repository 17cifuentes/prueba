
<header id="header" class="jnr__header header--one clearfix">
            <!-- Start Header Top Area -->
            <div class="junior__header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-lg-6 col-sm-12">
                            <div class="jun__header__top__left">

                                <ul class="top__address d-flex justify-content-start align-items-center flex-wrap flex-lg-nowrap flex-md-nowrap">

                                    <li><i class="fa fa-envelope"></i>{{$_SESSION['data']['conf'][0]->correo}}</li>
                                    <li><i class="fa fa-phone"></i><span>Contáctenos Ya <i class="fa fa-whatsapp"></i>: </span>{{$_SESSION['data']['conf'][0]->telefono}}</li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5 col-lg-6 col-sm-12">
                            <div class="jun__header__top__right">
                                <ul class="accounting d-flex justify-content-lg-end justify-content-md-end justify-content-start align-items-center">

                                   
                                    @if(Auth::user())
                                        <li><a href="{{url('home')}}"><img width="40" height="35" src="{{ asset(Auth::user()->img) }}" style="border: 1px solid black;border-radius: 100%;">
                                            <span>{{Auth::user()->name}}</span>
                                        </a></li>
                                    @else

                                        <li><a href="{{ url('ingresar/'.$_SESSION['data']['conf'][0]->id.'/0')}}"><i class="icofont icofont-user-alt-5" style="background: white"></i>
                                            <span>Ingresar</span>
                                        </a></li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Header Top Area -->
            <!-- Start Mainmenu Area -->
           
            <div class="header-category mainmenu__wrapper poss-relative header_top_line sticky__header ">
                <div class="container" >
                    <div id="top"></div>
                    <div class="row d-none d-lg-flex">
                        <div class="col-sm-4 col-md-6 col-lg-2 order-1 order-lg-1">
                            <div class="logo">
                              
                                <a data-toggle="tooltip" title="Ir a la página de inicio" href="{{url('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->cliente_id)}}"><img id="logoNormal" src="{{ asset($_SESSION['data']['conf'][0]->img_logo)  }}" alt="Car Icon" > </a>

                            </div>
                        </div>
                        <div class="col-sm-4 col-md-2 col-lg-9 order-3 order-lg-2">
                            <div class="mainmenu__wrap">
                                <nav class="mainmenu__nav">
                                    <ul class="mainmenu">
                                    @foreach($_SESSION[MENU_SHOPCART] as $menu)
                                    <?php $x=0 ?> 
                                        @foreach($_SESSION[SUBMENU_SHOPCART] as $submenu)
                                        <?php $x++ ?>
                                        @if($x == 1)
                                            <li class="drop"><a href="#">{{$menu->nombre}}</a>
                                                <ul class="dropdown__menu">
                                        @endif
                                        @if($submenu->menu_id == $menu->id)
                                            <li><a href="{{url('carritoCompras/buscarItemNavBar?id='.$_SESSION['data']['conf'][0]->cliente_id.'&subMeNombre='.$submenu->nombre )}}">{{ $submenu->nombre }}</a></li>
                                        @endif
                                        @if($x == count($_SESSION[SUBMENU_SHOPCART]))
                                                </ul>
                                            </li>
                                        @endif
                                        @endforeach
                                        @if($x == 0)
                                            <li><a href="{{url('carritoCompras/buscarItemNavBar?id='.$_SESSION['data']['conf'][0]->cliente_id.'&subMeNombre='.$menu->nombre )}}">{{$menu->nombre}}</a></li>
                                        @endif
                                        
                                @endforeach
                                       
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
                            <div class="shopbox d-flex justify-content-end align-items-center">
                                <a  data-toggle="tooltip" title="Mis artículos" class="minicart-trigger" href="">
                                    <i class="fa fa-shopping-basket" style="margin-top:6px"></i>
                                </a>
                                 <span data-toggle="tooltip" title="Mis artículos" class="minicart-trigger" class="number" id="cantProCart" style="margin-left: 2px"><center>0</center></span>
                            

                            </div>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="mobile-menu d-block d-lg-none">

                        <div class="logo">
                             <a href="{{url('carritoCompras/tienda?id='.$_SESSION['data']['conf'][0]->cliente_id)}}"><img id="logoMobile" width="70" height="60" src="{{ asset($_SESSION['data']['conf'][0]->img_logo)  }}" alt="Car Icon" > </a>
                             
                        </div>

                        <a class="minicart-trigger" href="">
                            <i class="fa fa-shopping-basket"></i>
                        </a>

                    </div>
                    
                </div>
            </div>
            <!-- End Mainmenu Area -->
        </header>
