		<!-- Start Courses Area -->
		<section class="dcare__courses__area section-padding--lg bg--white">

			<div class="container">
				<div class="row class__grid__page">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center">
							<h2 class="title__line">Categorias</h2>
							<p>Nuestros categorias</p>
						</div>
					</div>
					<!-- Start Single Courses -->
					@foreach($_SESSION[CATEGORIES] as $cat)
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="courses">
							<div class="courses__thumb">
								<a href="#">
									<img src="images/courses/1.jpg" alt="courses images">
								</a>
							</div>
							<div class="courses__inner">
								<div class="courses__wrap">
									<div class="courses__date"><i class="fa fa-calendar"></i>30th Dec, 2017</div>
									<div class="courses__content">
                             			<li ><a class="catColor" href="category-1.html">{{ $cat->cat_nombre }}</a></li>
                                	
										<p>Lor error sit volupta aclaud antium, toe ape sriam, ab illnv ritatis et quasi architecto beatae viliq.</p>
										<ul class="rating d-flex">
											<li><span class="ti-star"></span></li>
											<li><span class="ti-star"></span></li>
											<li><span class="ti-star"></span></li>
											<li><span class="ti-star"></span></li>
											<li><span class="ti-star"></span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					<!-- End Single Courses -->
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="dcare__pagination mt--80">
							<ul class="dcare__page__list d-flex justify-content-center">
								<li><a href="#"><span class="ti-angle-double-left"></span></a></li>
								<li><a class="page" href="#">Prev</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
								<li><a href="#">28</a></li>
								<li><a class="page" href="#">Next</a></li>
								<li><a href="#"><span class="ti-angle-double-right"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Start Subscribe Area -->