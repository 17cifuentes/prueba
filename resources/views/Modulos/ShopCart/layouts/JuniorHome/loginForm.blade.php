<!-- Login Form -->
<div class="login-wrapper">
    <div class="accountbox">
        <div class="accountbox__inner">
        	<h4>Login to Continue</h4>
            <div class="accountbox__login">
                <form action="#">
                    <input type="hidden" name="cliente_id" >
                    <div class="single-input">
                        <input type="email" placeholder="E-mail">
                    </div>
                    <div class="single-input">
                        <input type="password" placeholder="Password">
                    </div>
                    <div class="single-input text-center">
                        <button type="submit" class="sign__btn">SUBMIT</button>
                    </div>
                    <div class="accountbox-login__others text-center">
                        <ul class="dacre__social__link d-flex justify-content-center">
                            <li class="facebook"><a target="_blank" href="https://www.facebook.com/"><span class="ti-facebook"></span></a></li>
                            <li class="twitter"><a target="_blank" href="https://twitter.com/"><span class="ti-twitter"></span></a></li>
                            <li class="pinterest"><a target="_blank" href="#"><span class="ti-google"></span></a></li>
                        </ul>
                    </div>
                </form>
            </div>
            <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
        </div>
        <h3>Have an account ? Login Fast</h3>
    </div>
</div>
<!-- //Login Form -->