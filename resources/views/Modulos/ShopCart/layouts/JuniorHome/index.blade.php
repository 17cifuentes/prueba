@extends('Modulos.ShopCart.layouts.JuniorHome.layoutAllJunior')

@section('contenido')
	@include('Modulos.ShopCart.layouts.JuniorHome.categorias')
	<div id="productos">
		@include('Modulos.ShopCart.layouts.JuniorHome.productos')	
	</div>
	@include('Modulos.ShopCart.layouts.JuniorHome.modalPeticionDomicilio')
	@include('core.Usuarios.modalCrearUsuarioExterno')
	@include('core.cargando')
@stop