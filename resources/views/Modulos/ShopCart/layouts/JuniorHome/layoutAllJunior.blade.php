@include('layouts.SFTConfHead')
@include('layouts.SFTHeadGenerals')
@include('layouts.SFTConfCss')
@include('Modulos.ShopCart.layouts.JuniorHome.head')
@include('Modulos.ShopCart.layouts.JuniorHome.navBar')
@include('Modulos.ShopCart.layouts.JuniorHome.cartBox')
	<div class="wrapper" id="wrapper">
		<!-- Header -->
		@yield('contenido')
	</div>
	@include('Modulos.ShopCart.layouts.JuniorHome.footerIndex')
@include('layouts.SFTGeneralScript')
@include('Modulos.ShopCart.layouts.JuniorHome.footer')
@include('layouts.SFTCoreScript')
@include('layouts.SFTConfScript')
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
</script>