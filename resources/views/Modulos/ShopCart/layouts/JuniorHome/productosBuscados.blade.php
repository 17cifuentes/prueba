<!-- Start Our Gallery Area -->

		<section class="junior__gallery__area bg--white section-padding--lg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center">

							<h2 class="title__line">Productos</h2>
                            <hr class="titleLine">
							<p>Muestros productos</p>

						</div>
					</div>
				</div>

				<div class="row galler__wrap mt--40">
					<!-- Start Single Gallery -->
					@foreach($response as $producto )
                    
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a>
									<img src="{{ asset($producto->img_producto)}}" alt="gallery images">
								</a>
							</div>
							<div class="left-content">
              
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    
                                    <div class="desc">
                                       <p>{{$producto->pro_descripcion}}</p>
                                        <li>Cantidad:-</li>
                                    
                                        <li><span class="availability">Cantidad: <span></span></span></li>
                                                        
                                    </ul>
                                    </div>
                                </div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">

										
										
                                    	<li><a href="javascript::void()" class="add-to-cart" dataid="{{ $producto->id }}" onclick="addProToCart({{ $producto->id }})"><i class="fa fa-shopping-cart"></i></a></li>


                                        <span style="margin-top: 0.5em;font-size:13px" class="badge badge-danger badge-pill">
                                                                {{$producto['mensaje']}}
                                                        </span>
                                    	<!-- <li><a href="#" data-tooltip="Compare"><i class="fa ti-control-shuffle"></i></a></li> -->

									</ul>
										
									<div class="actions">							
                                        
                                        <div class="wishlist-compare">
                                    </div>
                                        <h4 class="gallery__title"><a href="#">{{$producto->pro_nombre}}</a></h4>

                                    </div>

								</div>
							</div>
						</div>	
					</div>	

				@endforeach

			</div>
		</section>
		<!-- End Our Gallery Area -->