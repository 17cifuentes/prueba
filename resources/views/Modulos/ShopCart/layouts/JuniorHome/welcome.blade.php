		<section class="junior__welcome__area section-padding--md bg-pngimage--2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section__title text-center">
							<h2 class="title__line">
								Bienvenido a {{$_SESSION['data']['conf'][0]->cliente_nombre}}</h2>
							<hr class="titleLine">
							<p>{{$_SESSION['data']['conf'][0]->cliente_descripcion}}</p>
						</div>
					</div>
				</div>
				</div>
			</div>
		</section>