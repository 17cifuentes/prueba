@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<b> Nombre del cliente: {{$domicilio[0]['name']}} </b>
<hr>
	<form method="post" >
		@if($domicilio[0]['domi_estado'] == $estadoCancelado[0]['id_estado'] || $domicilio[0]['domi_estado'] != $estado )
		@else
	 	<input type="hidden" name="" value="">
        <a type="button" class="btn btn-info" style="color: white" id="{{$_GET['id']}}"  onclick="addrNewProdDomi(this.id)"> <b> Agregar Nuevo Producto </b></a>
        @endif
        <hr>
         {!! $table !!}
	 </form>
	@if(isset($opc))
		<input type="hidden" id="ruta">
		<input type="hidden" value="{{ $opc }}" id="opcTable">
		<script type="text/javascript">
			window.addEventListener('load', calculos, false);
			function calculos() {	
				pintarOpc();
			}
		</script>
	@endif
@stop
