@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                
                    <form action="{{ url('carritoCompras/editarApartadosProcess') }}" enctype="multipart/form-data" method="post" data-accion="Editar Apartado" id="formEditarApartadosTienda" class="needs-validation" novalidate>

                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        <input type="hidden" name="id" id="id" value="{{$apartados->id}}">

                        <div class="col-md-6 mb-3 form-line">
                        <label for="validationCustom01">seleccione Apartado*</label>
                        <img src="{{ asset($apartados->apartado)}}" class="img img-reponsive img-circle" width="70" height="70">
                        <input type="file" name="apartado" class="form-control" accept="jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai"multiple >
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_REQUERIDO}}
                                </div>
                            </div>

                        <div class="col-md-6 mb-3 form-line">
                        <label for="validationCustom01">seleccione Valor del Apartado*</label>
                        <img src="{{ asset($apartados->valor_apartado)}}" class="img img-reponsive img-circle" width="70" height="70">
                        <input type="file" name="valor_apartado" class="form-control" accept="jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai" multiple />
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_REQUERIDO}}
                                </div>
                        </div>
                
                        
                <center> <input type="submit" class="btn btn-success"value="Editar Apartado"></center>

        </form>
    </div>
</div>

@stop