@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<div class="" style="padding: 1em">
    <div class="row clearfix">
        @if(Auth::user()->rol['id']== 1)
        <div class="col-lg-5 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">feedback</i>
                </div>
                <a href="{{url('carritoCompras/crearEstadoDomicilioCliente')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px"> Crear Estados Domicilio </div>
                    </div>
                </a>
            </div>
        </div>
        @endif
        <div class="col-lg-5 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">speaker_notes</i>
                </div>
                <a href="{{url('carritoCompras/listarEstadosCliente')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Listar Estados Domicilio
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-5 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">assignment turned in</i>
                </div>
                <a href="{{url('carritoCompras/listarDomicilios')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Listar Domicilios</div>
                    </div>
                </a>
            </div>
        </div>
        
        <div class="col-lg-5 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">assignment turned in</i>
                </div>
                <a href="{{url('carritoCompras/ordenarEstadosDomicilio')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Organizar Domicilios</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    
</div>
@stop