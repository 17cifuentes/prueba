@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                
                    <form action="{{ url('carritoCompras/crearApartadosProcess') }}" enctype="multipart/form-data" method="post" data-accion="Crear Apartado" id="formCrearApartadosTienda" class="needs-validation" novalidate>

                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="estructura_id" id="estructura_id" value="{{($estructura[0]['id'])}}">
                       
                        <div class="col-md-6 mb-3 form-line">
                        <label for="validationCustom01">seleccione Apartado*</label>
                        <input type="file" name="apartado" required="" class="form-control" accept="jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai" multiple/>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_REQUERIDO}}
                                </div>
                            </div>

                        <div class="col-md-6 mb-3 form-line">
                        <label for="validationCustom01">seleccione Valor del Apartado*</label>
                        <input type="file" name="valor_apartado" required="" class="form-control" accept="jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai" multiple/>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_REQUERIDO}}
                                </div>
                        </div>
                
                        
                <center> <input type="submit" class="btn btn-success" value="Crear Apartado"></center>

        </form>
    </div>
</div>

@stop