<div class="mini-cart-wrap">

    <!-- Mini Cart Top -->
    <div class="mini-cart-top">    
        
        <center><span class="titleCart">Carrito de compras</span></center><br><br>
        <font face="impact" size="3" color="#72706D"><button class="btn  float-right close-cart shopBoton btn-xs" >Cerrar<i class="icofont icofont-close"></i></button></font>
        <a href="javascript::void()" onclick="vaciarCart()" class="btn btn-info">Vaciar</a>
    </div>
    <!-- Mini Cart Products -->
    <ul class="mini-cart-products" id="listProCart">
        
    </ul>

    <!-- Mini Cart Bottom -->
    <div class="mini-cart-bottom">    
        
        <font face="impact" size="6" color="black"><h4 class="sub-total titleCart">Total: <span class="titleCart" id="totalCart">$0</span></h4></font>

        <div class="button ">
            <button class="btn btn-primary btn-lg btn-block" onclick="modalPeticionDomicilio()" > Comprar ya!</button>
        </div>
    </div>
    @include('Modulos.ShopCart.formShop')
</div><!-- Mini Cart Wrap End --> 