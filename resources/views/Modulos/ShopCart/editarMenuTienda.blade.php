@extends($_SESSION[DATA]['layouts'])

@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                    <form action="{{ url('carritoCompras/editarMenuProcess') }}" enctype="multipart/form-data" method="post" id="formEditarMenu" data-accion="Editar Menú" class="needs-validation" novalidate>
                        
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="id" value="{{ $menu[0]->id }}">
                        
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Nombre Menù Carrito Compra * </label>
                            <input type="text" value="{{ $menu[0]->nombre }}" min="1" maxlength="250" class="form-control" name="nombre" required="">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Cliente Nombre * </label>
                            <select class="form-control show-tick" name="cliente_id" required="">
                                    <option value="">Seleccione</option>
                                    @foreach($clientes as $cli)  
                                    <option value="{{$cli->id }}" @if($cli->id == $menu[0]->cliente_id) selected @endif> {{$cli->cliente_nombre }}  </option>    
                                     @endforeach                                       
                                </select>   
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}
                            </div>
                        </div>

                        <div class="col-md-12 mb-3 form-line">
                            <label for="validationCustom01">Estado Menú * </label>
                                <select class="form-control show-tick" name="estado_menu" required="">
                                <option value="">Seleccione</option>
                                <option value="Activo" @if($menu[0]->estado_menu == "Activo" ) selected @endif> Activo </option>
                                <option value="Inactivo" @if($menu[0]->estado_menu == "Inactivo" ) selected @endif> Inactivo </option>
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_SELECT}}
                                </div>  
                        </div>
                        
                <div class="col-lg-12"><center><button id=""
                type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Edita menú</button></center></div>
        </form>
    </div>
</div>

@stop