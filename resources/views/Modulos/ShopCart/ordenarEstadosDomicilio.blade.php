@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form action="{{ url('carritoCompras/editarOrdenEstadosDomicilioProcess') }}" enctype="multipart/form-data" method="post" data-accion="editar Orden Estados Domicilio" id="formeditarOrdenEstadosDomicilio" class="needs-validation" novalidate>
    <span><b> Recuerda: el estado que se encuentre en el ultimo lugar del orden que tengas sera el designado para cancelar los Domicilios </b></span>
    <hr>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-md-6 mb-3 form-line">
        <div class="form-group">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Orden</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($estadosDomicilio as $estados)
                    <tr>
                        <td>{{ $i }}</td>
                        <td> 
                            <input class="form-control" type="hidden" name="id_estado[]" id="nombre_estado" value="{{$estados->id_estado}}" readonly="">{{$estados->nombre_estado}}</td>
                        <td>
                            <select class="form-control show-tick" name="orden[]" required="">
                                @foreach($contador as $con)
                                <option value="{{$con}} " @if($estados->orden == $con ) Selected @endif> {{$con}} </option>
                                
                                @endforeach
                            </select>

                        </td>
                        <?php $i++; ?>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-4 mb-3 form-line">
        <label for="validationCustom01"> Nombre del estado para cancelar actual </label>
        <input type="text" value="{{$estadoCancelarDomi[0]['nombre_estado']}}" min="1" maxlength="250" class="form-control" readonly="">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-lg-12"><button id=""
    type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Ordenar estados domicilio </button></div>
 </form>   
@stop