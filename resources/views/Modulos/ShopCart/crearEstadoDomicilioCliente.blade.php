@extends($_SESSION[DATA]['layouts'])

@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
    <div class="rela-block profile-card" style="margin-top: 2em">
                
                    
        <form action="{{ url('carritoCompras/crearEstadoDomicilioClienteProcess') }}" enctype="multipart/form-data" method="post" data-accion="Crear Estado Domicilio" id="formCrearEstadoDomicilio" class="needs-validation" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-6 mb-4 form-line">
                    <label for="validationCustom01">Nombre del estado domicilio * </label>
                    <input type="text" value="" class="form-control" maxlength="38" 
                    name="nombre_estado" id="nombre_estado" required="">
                    <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                        {{CAMPO_REQUERIDO}}
                    </div>
            </div>        
                <div class="col-md-6 mb-3 form-line">
                    <label for="validationCustom01">Cliente </label>
                    <select class="form-control show-tick" name="cliente_id" required="">
                            <option value="">Seleccione</option>
                            @foreach($clientes as $cli)  
                            <option value="{{$cli->id }}"> {{$cli->cliente_nombre }}  </option>    
                             @endforeach                                       
                        </select>   
                    <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                        {{CAMPO_SELECT}}
                    </div>
                </div>

                <div class="col-lg-12"><center><button id=""
                type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear estado domicilio</button></center></div>

            </div>        
        </form>
    </div>
</div>
@stop