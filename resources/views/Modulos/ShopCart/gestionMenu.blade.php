@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<div class="" style="padding: 1em">
    <div class="row clearfix">
        <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">gavel</i>
                </div>
                <a href="{{url('carritoCompras/crearMenuTienda')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Crear Menú</div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">view_headline</i>
                </div>
                <a href="{{url('carritoCompras/listarMenu')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Lista de Menú</div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12">
            <div class="info-box-2 bg-indigo hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">input</i>
                </div>
                <a href="{{url('carritoCompras/gestionSubMenu')}}">
                    <div class="content">
                        <div class="text">Ingresar a:</div>
                        <div class="number" style="font-size: 20px">Gestion Submenú</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@stop