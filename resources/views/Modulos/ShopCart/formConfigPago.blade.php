@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	   <form action="{{ url('carritoCompras/configPagoProccess') }}" enctype="multipart/form-data" method="post" data-accion="Configurar PaYu" id="formpayu" class="needs-validation" novalidate>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" class="needs-validation" novalidate>

        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">MerchanId *</label>
            <input class="form-control" name="merchantId"  type="text" value="{{$pago[0]->merchantId}}" required="" >
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>

        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">accountId *</label>
            <input class="form-control" name="accountId" id="accountId"  type="text" value="{{$pago[0]->accountId}}" required="">
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Descripción *</label>
            <input name="description" type="text"  class="form-control" value="{{$pago[0]->description}}"  id="description" required="" >    
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>

        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Moneda *</label>
            <input name="currency"    class="form-control"  type="text"  value="{{$pago[0]->currency}}" required="" >
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Correo cuenta *</label>
            <input name="buyerEmail" class="form-control"   type="text"  value="{{$pago[0]->buyerEmail}}" required="" >
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Api Key *</label>
            <input name="api_key" class="form-control"   type="text"  value="{{$pago[0]->api_key}}" id="api_key" required="">
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>

        
        <div class="col-md-4 mb-3 form-line">
            <label for="validationCustom01">Url de respuesta *</label>
            <input class="form-control"   type="text"  value="{{$pago[0]->responseUrl}}" readonly="">
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_REQUERIDO}}
            </div>
        </div>
            
        <div class="col-lg-12"><center><button id=""
        type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Actualizar configuración</button></center></div>

	</form>
@stop