@extends($_SESSION[DATA]['layouts'])

@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
            <div class="rela-block profile-card" style="margin-top: 2em">
                
                    
             <form action="{{ url('carritoCompras/crearSubMenuProcess') }}" enctype="multipart/form-data" method="post" data-accion="Crear SubMenú" id="formEditarSubMenuTienda" class="needs-validation" novalidate>

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01">Cliente * </label>
                                <select class="form-control show-tick" name="cliente_id" required="" id="       selectCliente" onclick="cargarSelect(this.value)">
                                    <option value=""> Seleccione</option>
                                    @foreach($cliente as $cli)
                                    <option value="{{ $cli->id }}"> {{$cli->cliente_nombre }}  </option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_REQUERIDO}}
                                </div>
                        </div>
                        

                        <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Menù al que pertenece * </label>
                                <select name="menu_id" id="Menu" class="form-control"></select>
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                        </div>


                    <div class="col-md-6 mb-3 form-line">
                        <label for="validationCustom01">Nombre SubMenù Carrito Compra * </label>
                            <input type="text" value="" class="form-control" min="1" maxlength="250" name="nombre" required=""><div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                    </div>
                     
                    <div class="col-md-6 mb-3 form-line">
                            <label for="validationCustom01"> Estado Menú * </label>
                                <select name="estado_submenu" class="form-control" required="">
                                        <option value="">Seleccione</option>
                                        <option <value="Activo"> Activo </option>
                                        <option <value="Inactivo"> Inactivo</option>
                                </select>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                        {{CAMPO_SELECT}}
                                </div>
                        </div>

                <div class="col-lg-12"><center><button id=""
                type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear submenú</button></center></div>

            </form>
        </div>
    </div>
<script type="text/javascript" src="{{ asset('Modulos/ShopCart/js/shopCart.js') }}"></script>
@stop