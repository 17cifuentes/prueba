@extends($_SESSION[DATA]['layouts'])

@section('contenido')
<div class="rela-block container" style="margin-top: -1.5em;width: 100%">
    <div class="rela-block profile-card" style="margin-top: 2em">
        
        <form action="{{ url('carritoCompras/editarEstadoDomicilioClienteProcess') }}" enctype="multipart/form-data" method="post" data-accion="Editar Estado Domicilio" id="formEditarEstadoDomicilio" class="needs-validation" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_estado" id="id_estado" value="{{$_GET['id']}}">

            <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Nombre del estado domicilio * </label>
                <input type="text" value="{{$estado['0']['nombre_estado']}}" class="form-control" maxlength="38" 
                name="nombre_estado" id="nombre_estado" required="">
                <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
                <br>
                <input type="hidden" name="orden" value="{{$estado[0]['orden']}}">
            </div>

                <div class="col-lg-12"><button id=""
                type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar estado domicilio</button></div>
        
        </form>
    </div>
    
</div>

@stop