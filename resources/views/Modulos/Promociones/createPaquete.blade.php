@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form  method="POST" action="{{url('Promociones/crearPaqueteProcess')}}"id="formCrearPromociones" data-accion="Crear Paquete" class="needs-validation" novalidate>


        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Nombre *</label>
            <input type="text" value="" name="nombre" class="form-control" required/>
                <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{FECHA_INICIO_REQUERIDA}}
                </div>
            </div>
        <div class="col-md-6 mb-3 form-line">
            <label>Opción *</label>
            <div class="form-line">
             <select class="form-control" name="id_opcion" required="">
                <option value="">Seleccione</option>
                @foreach($opciones as $opc)
                
                 <option value="{{$opc->id}}">{{$opc->nombre_estado}}</option>
                
                @endforeach
             </select>
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>                        
            </div>
        </div>
        <br>
        <div class="col-md-6 mb-3 form-line">
            <label>Operación *</label>
            <div class="form-line">
             <select class="form-control" name="id_operacion" required="">
                <option value="">Seleccione</option>
                @foreach($operacion as $ope)

                 <option value="{{$ope->id}}">{{$ope->nombre_estado}}</option>
                }
                }
                
                @endforeach
             </select>
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>                        
            </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Valor de la operación *</label>
            <input type="number" value="" name="valor_operacion" class="form-control" required/>
                <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    
                </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
            <label>Tipo de promoción *</label>
            <div class="form-line">
             <select class="form-control" name="id_tipo_promocion" required="">
                <option value="">Seleccione</option>
                @foreach($tipoPromocion as $pro)
                 <option value="{{$pro->id}}">{{$pro->nombre_estado}}</option>
                
                @endforeach  
             </select>
            <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{CAMPO_SELECT}}
                </div>                        
            </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Valor de la Promoción *</label>
            <input type="number" value="" name="valor_promocion" class="form-control" required/>
                <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    
                </div>
        </div>

       <div class="col-lg-12 col-md-12 col-xs-12">
            <center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear promoción</button></center>
       </div>

</form>
  @stop 









