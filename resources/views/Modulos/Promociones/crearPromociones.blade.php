@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	<form  method="GET" action="{{url('Promociones/crearPromoProcess')}}"id="formCrearPromociones" data-accion="Crear Promociones" class="needs-validation" novalidate>

        <input type="hidden" name="id_producto" id="id_producto" value="{{ $producto }}">

		<div class="col-md-6 mb-3 form-line">
        	<label for="validationCustom01">Inicio*</label>
        	<input type="text" value="" name="inicio" class="form-control datepicker" required/>
            	<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{FECHA_INICIO_REQUERIDA}}
                </div>
		</div>

		<div class="col-md-6 mb-3 form-line">
        		<label for="validationCustom01">Fin*</label>
				<input type="text" value="" name="fin" class="form-control datepicker" required/>
				<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{FECHA_FIN_REQUERIDA}}
                </div>	
		</div>
        <br>

        <div class="col-md-12 mb-3 form-line">
            <label for="validationCustom01">Mensaje*</label>
            <textarea type="text" value="" name="mensaje" class="form-control" maxlength="200" required /></textarea>
                <div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}},{{NUM_MAX_CARACTERES}}
                </div>
        </div>


		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear promoción</button></center>

		 </form>
  @stop 









