@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<style type="text/css">
    #file-preview{
        width: 18em;    
        height: 18em;   
        border-radius: 15px 15px 15px 15px;
    }
</style>
<form action="{{ url(PATH_PRO_CREATEPROCCESS) }}" enctype="multipart/form-data" method="POST" data-accion="Crear producto" id="formCrearProducto" class="needs-validation" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="profile-body col-lg-12" style="margin-top: -7em">
        <br><br><br><br>
        <div class="image-area" >
        @if(isset($data) and $data != '' and $data['img'] != '') 

        <center>
            <div id="file-preview-zone" >
                <img class="" style="border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color }};" src="{{ asset($data['img']) }}" class="img img-responsive img-circle" alt="AdminBSB - Profile Image" />
            </div>
         </center>
        @else 
        <center>
            <div id="file-preview-zone" >
                <center><img id="img_default2" style="border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color }};" src="{{ asset('core/img/Formulario/subirimg.png') }}" class="img img-responsive" alt="AdminBSB - Profile Image"/></center>
            </div>
        </center>
        @endif
        <hr>     
        <div id="divInputLoad">
            <div id="divFileUpload">
                
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-4 mb-3 form-line">
        <label >Imagen del producto * 
        <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" title="{{POPOVER_INFO}}" data-content="{{PRODUCTO_INFO}}"></i>
</label>
        <input type="file" class="form-control" onclick="selectPhoto()" id="file-upload" name="file-upload" value="" accept=".jpg,.jpeg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.svg" required="">
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>

    </div>
    <div class="col-md-2 mb-3 form-line">
            <a  href="JavaScript:void(0);" class="btn btn-info" onclick="addCatToPro()" style="margin-top: 2em">+</a>
            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" title="{{POPOVER_INFO}}" data-content="{{PRODUCTO_INFO_MAS}}"></i>
        </div>
        <div class="col-md-6 mb-3 form-line">
        <label >Nombre *</label>
        <input type="text" class="form-control" id="pro_nombre" name="pro_nombre" value="" min="1" maxlength="200" required=""> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label >Categoria *</label>
        <select class="form-control show-tick" name="cat_id" required="">
            <option value="">Seleccione</option>
            @foreach($cats as $cat)
                <option value="{{ $cat->id }}">{{ $cat->cat_nombre }}</option>
            @endforeach
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label >Descripción *</label>
        <textarea type="text" class="form-control" id="pro_descripcion" name="pro_descripcion" value="" min="1" maxlength="200" required=""></textarea>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label >Costo *</label>
         <input type="number" class="form-control" min="1" maxlength="200" name="pro_precio_interno" value="" required=""> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label >Valor venta *
        </label>
        <input type="number" class="form-control" min="1" maxlength="200" name="pro_precio_venta" value="" required="">  
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div><br>
    </div>

    @if($permisoShopCart)
        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01"> En Venta *
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" title="{{POPOVER_INFO}}" data-content="{{PRODUCTO_INFO_OPC_VENTA}}"></i>
            </label>
                <select name="en_venta" class="form-control" required="">
                    <option value="">Seleccione</option>
                    <option <value="Activo"> Activo </option>
                    <option <value="Inactivo"> Inactivo</option>
                </select>
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>
        </div>
    @endif

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <legend>Caracteristicas del producto</legend>
    </div>
    <div class="col-md-5 mb-3 form-line">
        <label >Caracteristica
        <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" title="{{POPOVER_INFO}}" data-content="{!!PRODUCTO_INFO_OPC_CARACTERISTICAS!!}"></i>
        </label>
        <select id="campoId" class="{{SFT_SELECT2}} form-control basic-single" onchange="getCampoProducto(this.value)">
            <option value="">Seleccione</option>
            @foreach($campos as $campo)
                <option value="{{ json_encode($campo) }}">{{$campo->campo_nombre}}</option>
            @endforeach
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div><br>
    </div>
    <div id="contenValor"></div>
    <div class="col-md-2 mb-3 form-line">
        <a  href="JavaScript:void(0);" class="btn btn-info" onclick="addCatToPro()" style="margin-top: 2em">+ Agregar</a>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <table class="table">
            <thead>
                <th>Caracteristica</th>
                <th>Valor</th>
                <th>Eliminar</th>
            </thead>
            <tbody id="addCaract">
                
            </tbody>
        </table>
    </div>
    <input type="hidden" name="caractPro" id="caractPro">
    <input type="hidden" name="valorPro" id="valorPro">
        <div class="col-lg-12"><center><button id=""
        type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear producto</button></center></div>
</form>
@stop 