@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form action="{{ url('Productos/editarOrdenCategoriaProcess') }}" enctype="multipart/form-data" method="post" data-accion="editar Orden Estados Domicilio" id="formeditarOrdenEstadosDomicilio" class="needs-validation" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row"> 
        <div class="form-group">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Orden</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($lista as $categoria)

                    <tr>
                        <td>{{ $i }}</td>
                        <td> 
                            <input class="form-control" type="hidden" name="id_categoria[]" id="nombre_estado" value="{{$categoria['id']}}" readonly="">{{$categoria['cat_nombre']}}</td>
                        <td>
                            <select class="form-control show-tick" name="orden[]" required="">
                                @foreach($contador as $con)

                                <option value="{{$con}}" @if($categoria['orden'] == $con ) Selected @endif> {{$con}} </option>

                                @endforeach
                            </select>

                        </td>
                        <?php $i++; ?>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-lg-12"><button id=""
    type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Ordenar categorías </button></div>
 </form>   
@stop