@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<style type="text/css">
    #file-preview{
        width: 18em;    
        height: 18em;   
        border-radius: 15px 15px 15px 15px;
    }
</style>
<form action="{{ url('/Productos/editarProccess') }}" enctype="multipart/form-data" method="POST" data-accion="Editar producto" id="formEditarProducto" class="needs-validation" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="profile-body col-lg-12" style="margin-top: -7em">
        <br><br><br><br>
        <div class="image-area" >
        @if($data['img'] != '') 
        <center>
            <div id="file-preview-zone" >
                <img class="" id="img_default2" style="border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color }};" src="{{ asset($data['img']) }}" class="img img-responsive img-circle" alt="AdminBSB - Profile Image" width="150em" />
            </div>
         </center>
        @else 
        <center>
            <div id="file-preview-zone" >
                <center><img style="border: 2px solid {{ $_SESSION['data']['conf'][0]->navbar_color }};" src="{{ asset('core/img/Formulario/subirimg.png') }}" class="img img-responsive" alt="AdminBSB - Profile Image" width="150"/></center>
            </div>
        </center>
        @endif
        <hr>     
        <div id="divInputLoad">
            <div id="divFileUpload">
                
            </div>
        </div>
        </div>
    </div>

    <input type="hidden" name="id" id="id" value="{{ $producto->id }} ">

    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Imagen del producto</label>
        <input type="file" class="form-control" id="file-upload" name="file-upload" accept=".jpg,.jpeg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.svg" value="">
        <div class="valid-feedback">Ok!</div>
    </div>
    
    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Nombre *</label>
        <input type="text" class="form-control" id="pro_nombre" name="pro_nombre" min="1" maxlength="200" value="{{ $producto->pro_nombre }}" required=""> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Categoria *</label>
        <select class="form-control show-tick" name="cat_id" required="">
            <option value="">Seleccione</option>
            @foreach($categorias as $cat)
            <option value="{{ $cat->id }}" @if($cat->id == $producto->cat_id) 
            selected
            @endif>{{ $cat->cat_descripcion }}</option>
            @endforeach
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Descripción *</label>
        <textarea type="text" class="form-control" id="pro_descripcion" name="pro_descripcion"  min="1" maxlength="200" required=""> {{$producto->pro_descripcion}} </textarea>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>


    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Costo *</label>
         <input type="number" class="form-control" min="1" maxlength="200" name="pro_precio_interno" value="{{$producto->pro_precio_interno}}" required=""> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Valor venta *</label>
        <input type="number" class="form-control" min="1" maxlength="200" name="pro_precio_venta" value="{{$producto->pro_precio_venta}}" required="">  
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div><br>
    </div>
    @if($permisoShopCart)
        <div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">En Venta * </label>
                <select class="form-control show-tick" name="en_venta" required="">
                    <option value="">Seleccione</option>
                    <option value="Activo" @if($producto->en_venta == "Activo" ) selected @endif> Activo </option>
                    <option value="Inactivo" @if($producto->en_venta == "Inactivo" ) selected @endif> Inactivo </option>
                </select>
                <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{CAMPO_SELECT}}
                </div>  
        </div>
    @endif
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <legend>Caracteristicas del producto</legend>
    </div>
    <div class="col-md-5 mb-3 form-line">
        <label for="file-upload">Caracteristica</label>
        <select id="campoId" class="{{SFT_SELECT2}} form-control basic-single" onchange="getCampoProducto(this.value)">
            <option value="">Seleccione</option>
            @foreach($campos as $campo)
                <option value="{{ json_encode($campo) }}">{{$campo->campo_nombre}}</option>
            @endforeach
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div><br>
    </div>
    <div id="contenValor"></div>
    <div class="col-md-2 mb-3 form-line">
        <a  href="JavaScript:void(0);" class="btn btn-info" onclick="addCatToPro()" style="margin-top: 2em">+ Agregar</a>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <table class="table">
            <thead>
                <th>Caracteristica</th>
                <th>Valor</th>
                <th>Eliminar</th>
            </thead>
            <tbody id="addCaract">
                
            </tbody>
        </table>
    </div>
    <input type="hidden" name="caractPro" id="caractPro">
    <input type="hidden" name="valorPro" id="valorPro">
    <input type="hidden" value="{{ $caracteristicasPro }}" id="caracteristicasPro">
    <div class="col-lg-12"><center><button id=""
    type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar producto</button></center></div>
</form>
<script type="text/javascript">
    window.addEventListener('load', calculos, false);
        function calculos() {   
            getCaracProductos($("#caracteristicasPro").val());
        }
</script>
@stop 