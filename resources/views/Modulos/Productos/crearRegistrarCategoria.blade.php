@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form action="{{ url('Productos/crearCatProccess') }}" enctype="multipart/form-data" method="post" data-accion="Crear categoria" id="formCrearCategoria" class="needs-validation" novalidate>

<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Nombre Categoría *</label>
        <input type="text" class="form-control" id="cat_nombre" min="1" maxlength="200" name="cat_nombre" value="" required=""> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Tipo de Categoría *</label>
        <input type="text" class="form-control" id="cat_tipo" min="1" maxlength="200" name="cat_tipo" value="" required=""> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-12 mb-3 form-line">
        <label for="file-upload">Descripción de la Categoría *</label>
        <textarea type="text" class="form-control" id="cat_descripcion" min="1" maxlength="200" name="cat_descripcion" value="" required=""></textarea> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>

    <div class="col-md-12 mb-3 form-line">
        <label for="file-upload">Imagen *</label>
        <input required="" accept="jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai" type="file" name="img"> 
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
        </div>
    </div>
        <div class="col-lg-12"><center><button id=""
        type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear categoría</button></center></div>
  </form>
@stop