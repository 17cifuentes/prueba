@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<form  method="GET" action="{{url('Inventario/abastecerCantProccess')}}" id="formAbastecer" data-accion="Abastecer" class="needs-validation" novalidate>
	<input type="hidden" value="{{ $datos->id }}" name="id"  id="id" class="form-control"/>

		<div class="col-md-6 mb-3 form-line">
        	<label for="validationCustom01">Cantidad actual</label>
        		<input type="number" name="cant_actual" value="{{$datos->cantidad }}" disabled="" class="form-control"/>
                <div class="valid-feedback">Actual</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
	    </div>

		<div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Cantidad a abastecer*</label>
        	<input type="number" value="" min="1" name="cantidad" class="form-control" required=""/>
            <div class="valid-feedback">Abastecer</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}},{{ CAMPO_NUM_MAYOR_1 }}
            </div>
		</div>


		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Abastecer producto</button></center>

		 </form>
  @stop 
