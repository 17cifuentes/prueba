@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form  method="GET" action="{{url('Inventario/darDeBajaCantProcess')}}" id="formdarDeBaja" data-accion="Dar de baja" class="needs-validation" novalidate>
	<input type="hidden" value="{{$dates->id}}" name="id" class="form-control"/>

		<div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Cantidad actual</label>
        	<input type="number" name="cant_actual" value="{{$dates->cantidad }}" disabled="" class="form-control"/>
            <div class="valid-feedback">Actual</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
		</div>

		<div class="col-md-6 mb-3 form-line">
            <label for="validationCustom01">Cantidad a dar de baja*</label>
        	<input type="number" value="" min="1" name="cantidad" class="form-control" required=""/>
            <div class="valid-feedback">Dar de baja</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}},{{CAMPO_NUM_MAYOR_1}}
                </div>
		</div>

		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Dar de baja producto</button></center>
</form>

@stop 