@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	<form  method="GET" action="{{url('Inventario/crearProcess')}}"id="formCrearInventario" data-accion="Crear Inventario" class="needs-validation" novalidate>

		<div class="col-md-12 mb-3 form-line">
        		<label for="validationCustom01">Seleccione Producto*</label>
				<select type="text" name="id_producto" class="form-control basic-single" required="">
					<center><option value="">Seleccione</option>
					@foreach($productos as $prod)
					<option value="{{ $prod->id }}">{{$prod->pro_nombre}}  </option>
					@endforeach
				</select>
				<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{SELECT_PRODUC}}
                </div>	
		</div>

		<div class="col-md-6 mb-3 form-line">
			<label for="validationCustom01">Cantidad*</label>
        	<input type="number" value="" name="cantidad" class="form-control"  min="1" max="11111111111" required/>
            	<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}},{{CAMPO_MIN}},{{CAMPO_MAX}}
                </div>
		</div>

		<div class="col-md-6 mb-3 form-line">
        	<label for="validationCustom01">Nivel de stock*</label>
        	<input type="number" value="" name="nivel_stock" class="form-control"  min="1" max="11111111111" required=""/>
            	<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}},{{CAMPO_MIN}},{{CAMPO_MAX}}
                </div>
		</div>

		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Registrar producto</button></center>

		 </form>
  @stop 









