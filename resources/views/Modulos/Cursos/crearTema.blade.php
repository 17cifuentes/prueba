@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form enctype="" class="needs-validation" method="POST" action="{{url('Cursos/crearTemaProccess')}}" data-accion="Crear tema" id="crearTema" novalidate>
	<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
	<center><legend>Crear tema</legend></center>
<input type="hidden" name="cliente_id" value="{{ $cliente }}">

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Nombre *</label>
		<input type="text" class="form-control" id="nombre_tema" name="nombre_tema" min="1" maxlength="255" required>   
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>
	</div>

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Descripción *</label>
		<textarea type="text" class="form-control" id="descripcion_tema" name="descripcion_tema" min="1" maxlength="255" required></textarea> 
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>                    	
	</div>

	<div class="col-md-12 mb-6 form-line">
        <label for="validationCustom01"> Estado tema*</label>
        <select name="estado_tema" class="form-control" required="">
            <option value="">Seleccione</option>
            <option <value="Activo"> Activo </option>
            <option <value="Inactivo"> Inactivo</option>
        </select>
            <div class="valid-feedback">Ok!</div>
            <div class="invalid-feedback">
                {{CAMPO_SELECT}}
            </div>
       </div>   

	<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear tema</button></center>

</form>

@stop