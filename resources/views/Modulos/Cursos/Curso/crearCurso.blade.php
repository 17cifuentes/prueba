@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form class="needs-validation" enctype="multipart/form-data" method="POST" action="{{url('Cursos/crearCursoProccess')}}" data-accion="Crear Curso" id="crearCurso" novalidate>
	<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
	<center><legend><b>Crear Curso</b></legend></center>

	<div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Imagen Curso</label>
        <input type="file" class="form-control" id="file-upload" name="file-upload" accept=".jpg,.jpeg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.svg" required="" value="">
        <div class="valid-feedback">Ok!</div>
    	<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>
		<br>
		<div class="col-md-8 mb-3 form-line">
			<div id="file-preview-zone" >
                <img style="width: 10em" />
            </div>
		</div>
	</div>

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Nombre del curso *</label>
		<input type="text" class="form-control" id="nombre_curso" name="nombre_curso" maxlength="200" required>   
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>
	</div>

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Descripción del curso *</label>
		<textarea type="text" class="form-control" id="descripcion_curso" name="descripcion_curso" maxlength="250" required=""></textarea>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>                    	
	</div>

	<div class="col-md-6 mb-3 form-line">
        <label for="file-upload">Encargado *</label>
        <select id="encargado" name="encargado" class="{{SFT_SELECT2}} form-control basic-single" required="">
            <option value="">Seleccione</option>
            @foreach($usuarios as $usuario)
            <option value="{{$usuario['id']}}">{{$usuario['name']}}</option>
            @endforeach
        </select>
        <div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div><br>
    </div>

	<div class="col-md-6 mb-3 form-line">
		<label for="file-upload"> <b>Categorias Curso *<b> </label>
		<select multiple="" id="id_categoria" name="id_categoria[]" class="{{SFT_SELECT2}} form-control basic-single" required="">
			@foreach($categorias as $categoria)
			<option value="{{$categoria['id']}}">{{$categoria['cat_nombre']}}</option>
	       	@endforeach
       	</select>
		<div class="valid-feedback">Ok!</div>
        <div class="invalid-feedback">
            {{CAMPO_SELECT}}
        </div>
    </div>

</form>
<div class="col-lg-12">
		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear Curso</button></center>
</div>

@stop