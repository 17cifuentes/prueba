<div class="modal fade" id="modalCategoriasCurso" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenter"> <b>Categoria</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group" id="categoriasCurso" ></div>
        <!--<input type="text" name="categoriasCurso" id="categoriasCurso">-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>