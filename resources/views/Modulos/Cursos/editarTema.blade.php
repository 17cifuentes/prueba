@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<form enctype="" class="needs-validation" method="POST" action="{{url('Cursos/editarTemaProccess')}}" data-accion="Editar tema" id="editarTema" novalidate>
	<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
	<center><legend>Crear Tema</legend></center>
<input type="hidden" name="cliente_id" value="{{ $cliente }}">
<input type="hidden" name="id" value="{{ $temas->id }}">

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Nombre *</label>
		<input type="text" class="form-control" id="nombre_tema" name="nombre_tema" min="1" maxlength="255" required value="{{ $temas->nombre_tema }}">   
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>
	</div>

	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Descripción *</label>
		<input type="text" class="form-control" id="descripcion_tema" name="descripcion_tema" min="1" maxlength="255" value="{{ $temas->descripcion_tema }}" required>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>                    	
	</div>

	<div class="col-md-12 mb-6 form-line">
		<label for="validationCustom01"> Estado tema*</label>
		<select name="estado_tema" class="form-control" required="">
		<option value="">Seleccione</option>
		<option <value="Activo" @if($temas->estado_tema == 'Activo' )selected @endif > Activo </option>
		<option <value="Inactivo"@if($temas->estado_tema == 'Inactivo' )selected @endif > Inactivo</option>
		</select>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_SELECT}}
		</div>
	</div>  

	<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar tema</button></center>

</form>

@stop