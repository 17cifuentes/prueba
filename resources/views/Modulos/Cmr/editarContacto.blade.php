@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="au-card recent-report col-lg-12">
           <form id="idForm" enctype="multipart/form-data" class="col-lg-12 needs-validation" method="POST" action="{{ url('Crm/editarContactoProcess') }}" data-accion="Editar este contacto" novalidate>
            <input required="" type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="col-lg-12">
                    <fieldset class="form-group col-lg-12 " id="">
                    <legend>Datos del contacto</legend>
                    <input required="" type="number" class="hidden" name="id" value="{{$contactByID[0]->id}}">
                    <div class="col-lg-6">
                        <div class="form-group">
                                <label>Nombre *</label>
                            <div class="form-line">
                                <input required="" type="text"  class="form-control" id="nombre" maxlength="250" minlength="1" name="nombre" value="{{$contactByID[0]->nombre}}" >
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_REQUERIDO}}
                                </div>                         
                            </div>
                        </div>        
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Apellido *</label>
                            <div class="form-line">
                            <input required="" type="text"  class="form-control" id="apellido" maxlength="250" minlength="1" name="apellido" value="{{$contactByID[0]->apellido}}" > 
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>
                            </div>
                            
                        </div>            
                    </div>  

                    <div class="col-lg-6">
                        <div class="form-group">
                                <label>Dirección </label>
                            <div class="form-line">
                                <input required="" type="text"  class="form-control" id="direccion" maxlength="250" minlength="1" name="direccion" value="{{$contactByID[0]->direccion}}" >
                                <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>                
                            </div>
                             
                        </div>     
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Télefono </label>
                            <div class="form-line">
                                <input type="number" min="1111111" max="99999999999" class="form-control" min="1"   name="telefono" value="{{$contactByID[0]->telefono}}"  required="">  <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_NUM_MIN_5}}
                                    {{CAMPO_NUM_MAX_12}}
                                </div>                                       
                            </div>
                            
                        </div>
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Correo </label>
                            <div class="form-line">
                                <input required="" type="email"  class="form-control" id="correo" maxlength="250" minlength="1" name="correo" name="correo" value="{{$contactByID[0]->correo}}" > <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_EMAIL}}
                            </div>                   
                            </div>
                            
                        </div>         
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Resumen </label>
                            <textarea readonly="" value="" required="" class="form-control show-tick" type="text" name="resumen" maxlength="250" minlength="1">{{$contactByID[0]->resumen}}</textarea>
                            <div class="valid-feedback">Ok!</div>
                        <div class="invalid-feedback">
                            {{CAMPO_TEXT_MAX_250}}
                        </div> 
                        </div>
                         
                    </div>                 
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Estado </label>
                            <div class="form-line">
                             <select  class="form-control" name="estado_contacto" required="">
                                 <option>Seleccione</option>
                                 <option value="Pendiente" @if($contactByID[0]->estado_contacto == "Pendiente") selected @endif>Pendiente</option>
                                 <option value="Rechazado" @if($contactByID[0]->estado_contacto == "Rechazado") selected @endif>Rechazado </option>
                                 <option value="Nueva llamada" @if($contactByID[0]->estado_contacto == "Nueva llamada") selected @endif>Nueva llamada</option>
                             </select>
                                                    
                                </div>
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_SELECT}}
                                </div>
                            </div>
                                
                    </div>  
                </fieldset>
                <div class='col-lg-12'>
                    <center>
                        <button type='submit' class='btn btn-success'><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar contacto</button>
                    </center>
                </div>
                </div>
                    
            </form>
        </div>
    </div>
</div>
@stop
