@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="au-card recent-report col-lg-12">
           <form id="idForm" enctype="multipart/form-data" class="needs-validation col-lg-12" method="POST" action="{{ url('Crm/createContactProcess') }}" data-accion="Crear el contacto" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">                
                    <div class="col-lg-12">
                   
                    <fieldset class="form-group col-lg-12 " id="">
                    <legend>Datos del contacto</legend>
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                                <label>Nombre *</label>
                            <div class="form-line">
                                <input required="" type="text"  class="form-control" id="nombre" maxlength="250" minlength="1" name="nombre" value="" >
                                <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>                         
                            </div>
                            
                        </div>        
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Apellido *</label>
                            <div class="form-line">
                            <input required="" required="" type="text"  class="form-control" id="apellido" maxlength="250" minlength="1" name="apellido" value="" >
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div> 
                            </div>
                        </div>            
                    </div>  

                    <div class="col-lg-6">
                        <div class="form-group">
                                <label>Dirección *</label>
                            <div class="form-line">
                                <input required="" type="text"  class="form-control" id="direccion"  name="direccion" value="" maxlength="250" minlength="1">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>                
                            </div>
                            
                        </div>     
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Télefono *</label>
                            <div class="form-line">
                                <input type="number" min="1111111" max="99999999999" class="form-control"  name="telefono"  required="">
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_NUM_MIN_5}}
                                    {{CAMPO_NUM_MAX_12}}
                                    
                                </div>                                        
                            </div>
                            
                        </div>
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Correo *</label>
                            <div class="form-line">
                                <input required="" type="email"  class="form-control" id="correo" maxlength="250" minlength="1" name="correo" value="" >
                                <div class="valid-feedback">Ok!</div>
                                <div class="invalid-feedback">
                                    {{CAMPO_EMAIL}}
                                </div>                    
                            </div>
                            
                        </div>         
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Estado *</label>
                            <div class="form-line">
                             <select class="form-control" name="estado_contacto" required="">
                                 <option value="">Seleccione</option>
                                 <option value="Pendiente">Pendiente</option>
                                 <option value="Rechazado">Rechazado </option>
                                 <option value="Nueva llamada">Nueva llamada</option>
                             </select>
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_SELECT}}
                            </div>                        
                            </div>
                        </div>
                                
                    </div>  
                </fieldset>
                <div class='col-lg-12'>
                    <center>
                        <button type='submit' class='btn btn-success'><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear contacto</button>
                    </center>
                </div>
                </div>
                    
            </form>
        </div>
    </div>
</div>
@stop
