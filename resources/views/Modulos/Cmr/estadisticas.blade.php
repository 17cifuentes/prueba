@extends($_SESSION[DATA]['layouts'])
@section('contenido')




<div  class="coll-lg-12 col-md-12 col-xs-12">
	<center><h6>Estadisticas de contactos</h6></center>
	<hr>
	<input type="hidden" id="numCharts" value="1">
	<div class="col-lg-6 col-xs-6 col-md-6">
		
		<div class="" id="1">
			<div class="panel panel-default active">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
							<i class="fa fa-caret-right"></i> Llamadas al mes
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in">
					<div class="panel-body">
						<div id="contenedor" class="col-lg-12"></div>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-xs-6 col-md-6">
		
		<div class="" id="1">
			<div class="panel panel-default active">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">
							<i class="fa fa-caret-right"></i> Llamadas por usuarios
						</a>
					</h4>
				</div>
				<div id="collapsetwo" class="panel-collapse collapse in">
					<div class="panel-body">
						<div id="contenedor" class="col-lg-12"></div>	
						<table class="table">
							<thead>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Total llamadas</th>
							</thead>
							<tbody>
								@foreach($call_for_con as $call=>$cfc)
								<tr>
									<td>{{$cfc->nombre}}</td>
									<td>{{$cfc->apellido}}</td>
									<td>{{$cfc->total}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div  class="coll-lg-12 col-md-12 col-xs-12">
	
	<input type="hidden" id="numCharts" value="1">
	<div class="col-lg-6 col-xs-6 col-md-6">
		
		<div class="" id="1">
			<div class="panel panel-default active">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapsethree">
							<i class="fa fa-caret-right"></i> Contactos por estado
						</a>
					</h4>
				</div>
				<div id="collapsethree" class="panel-collapse collapse in">
					<div class="panel-body">
						<div id="contenedor" class="col-lg-12"></div>
						<table class="table">
							<thead>
								<th>Estado</th>
								<th>Cantidad</th>
							</thead>
							<tbody>

								@foreach($tot_con_for_est as $tote=>$tcfe)
								<tr>
									<td>{{$tcfe->estado_contacto}}</td>
									<td>{{$tcfe->contactos}}</td>
								</tr>
								@endforeach

							</tbody>
						</table>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-xs-6 col-md-6">
		
		<div class="" id="1">
			<div class="panel panel-default active">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
							<i class="fa fa-caret-right"></i> Tiempo total por contacto
						</a>
					</h4>
				</div>
				<div id="collapsefour" class="panel-collapse collapse in">
					<div class="panel-body">
						<div id="contenedor" class="col-lg-12"></div>	
						<table class="table">
							<thead>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Tiempo</th>

							</thead>
							<tbody>

								@foreach($tot_calls_for_con as $totc=>$tcfc)
								<tr>
									<td>{{$tcfc->nombre}}</td>
									<td>{{$tcfc->apellido}}</td>
									<td>{{$tcfc->suma}}</td>
								</tr>
								@endforeach

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div  class="coll-lg-12 col-md-12 col-xs-12">
	
	<input type="hidden" id="numCharts" value="1">
	<div class="col-lg-6 col-xs-6 col-md-6">
		
		<div class="" id="1">
			<div class="panel panel-default active">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapsefive">
							<i class="fa fa-caret-right"></i> Tiempo total de llamadas
						</a>
					</h4>
				</div>
				<div id="collapsefive" class="panel-collapse collapse in">
					<div class="panel-body">
						<div id="contenedor" class="col-lg-12"></div>
						<table class="table">
							<thead>
								<th>Tiempo</th>
							</thead>
							<tbody>
								<tr>
									<td>{{$tot_calls[0]->suma}}</td>
								</tr>
							</tbody>
						</table>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="tituloGrafico1" value="Número de llamadas">
<input type="hidden" id="tipoGrafico1" value="column">
<input type="hidden" id="contenedorGrafico1" value="contenedor">
<input type="hidden" id="valorGrafico1" value="{{ json_encode([$allToday,$oneWeeks, $twoWeeks, $fourWeeks])}}">
<input type="hidden" id="dataGrafico1" value="{{json_encode(['Hoy','Hace 6 días','Hace 15 días','Hace 30 días'])}}">

    


<script type="text/javascript">
	window.addEventListener('load', getData, false);
	function getData(){
		getChartDataContact();
	}
</script>


@stop