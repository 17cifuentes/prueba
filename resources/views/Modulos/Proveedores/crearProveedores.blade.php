@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	<form enctype="multipart/form-data" method="POST" action="{{url('Proveedores/crearProveedorProcess')}}" id="formCrearProveedor"  data-accion="Crear proveedor" class="needs-validation" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="col-md-6 mb-3 form-line">
        	<label for="validationCustom01">Nombre del proveedor*</label>
        		<input type="text" value="" name="nombre_proveedor" class="form-control" maxlength="200" required >
        		<div class="valid-feedback">Digitada</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}},{{CAMPO_TEXT_MAX_200}}
                </div>
             </div>

		<div class="col-md-6 mb-3 form-line">
    			<label for="validationCustom01">Cedula responsable*</label>
        			<input type="number" value="" name="cedula_responsable" class="form-control" max="9999999999999" min="10000000" required>
        	<div class="valid-feedback">Digitada</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}},{{CAMPO_NUM_MIN_8}}
                </div>
             </div>

            	
		<div class="col-md-6 mb-3 form-line">
	    		<label for="validationCustom01">NIT*</label>
	        	<input type="number" value="" name="nit" class="form-control"  max="9999999999999" min="10000000" required>
	          <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}},{{CAMPO_NUM_MIN_8}}
                </div>
             </div>

			<div class="col-md-6 mb-3 form-line">
		    	<label for="validationCustom01">Nombre del responsable*</label>
		        <input type="text" value="" name="nombre_responsable" class="form-control" maxlength="200"  required>
		      <div class="valid-feedback">Digitado</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}}
                </div>
             </div>

			<div class="col-md-6 mb-3 form-line">
		    	<label for="validationCustom01">Camara y Comercio*</label>
		        	<input type="file" value="" name="camara_comercio" class="form-control" required  accept=".jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai">
		        <div class="valid-feedback">Seleccionado</div>
                	<div class="invalid-feedback">
                   		{{ARCHIVO_REQUERIDO}}
                	</div>
             	</div>

					<div class="col-md-6 mb-3 form-line">
						<label for="validationCustom01">RUT*</label>
		        		<input type="file" value="" name="rut" class="form-control" required  accept=".jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai">
		            <div class="valid-feedback">Seleccionado</div>
               			<div class="invalid-feedback">
                   			{{ARCHIVO_REQUERIDO}}
                		</div>
             		</div>
		    		
		    	<center><div class="col-md-12 mb-3 form-line">
		    		<label for="validationCustom01">Estado Proveedor*</label>
		    		<select value="" name="estado_proveedor" class="form-control" required>
               <option value="">seleccione</option>
					     <option value="Activo">Activo</option>
					     <option value="Inactivo">Inactivo</option>
                <div class="valid-feedback">Estado seleccionado</div>
                    <div class="invalid-feedback">
                        {{CAMPO_REQUERIDO}}
                    </div>
                </div>
					 </select>
				</div></center>	

		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear proveedor</button></center>

  @stop 








