@extends($_SESSION[DATA]['layouts'])
@section('contenido')
		<form enctype="multipart/form-data" method="post" action="{{url('Proveedores/editarProveedorProcess')}}" id="formEditarProveedor" data-accion="Editar proveedor" class="needs-validation" novalidate>
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" id="id" value="{{$proveedor->id}}">
			
			<div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Nombre del proveedor*</label>
                <input type="text" value="{{$proveedor->nombre_proveedor}}" name="nombre_proveedor" class="form-control" required> 
                <div class="valid-feedback">Digitada</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}},{{CAMPO_TEXT_MAX_200}}
                </div>
             </div>

             <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Cedula responsable*</label>
                <input type="number" value="{{$proveedor->cedula_responsable}}" name="cedula_responsable" class="form-control" required max="99999999999" min="10000000">
                <div class="valid-feedback">Digitada</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}},{{CAMPO_NUM_MIN_8}}
                </div>
             </div>

             <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">NIT*</label>
               <input type="number" value="{{$proveedor->nit}}" name="nit" class="form-control" max="99999999999" min="10000000" required>
                <div class="valid-feedback">Digitada</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}},{{CAMPO_NUM_MIN_8}}
                </div>
             </div>

             <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Nombre del responsable*</label>
              <input type="text" value="{{$proveedor->nombre_responsable}}" name="nombre_responsable" class="form-control" required>
                <div class="valid-feedback">Digitada</div>
                <div class="invalid-feedback">
                   	{{CAMPO_REQUERIDO}}
                </div>
             </div>
             
             <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">Camara y Comercio*</label>
                <img src="{{ asset($proveedor->camara_comercio) }}" class="img img-reponsive img-circle" width="70" height="70">
               <input type="file" class="form-control" name="camara_comercio" id="camara_comercio" accept=".jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai"> 
                <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                    {{ARCHIVO_REQUERIDO}}
                </div>
             </div>


             <div class="col-md-6 mb-3 form-line">
                <label for="validationCustom01">RUT*</label>
                <img src="{{ asset($proveedor->rut) }}" class="img img-reponsive img-circle" width="70" height="70">
               <input type="file" class="form-control" name="rut" id="rut" accept=".jpeg,.jpg,.gif,.png,.tiff,.tif,.raw,.bmp,.psd,.pdf,.eps,.svg,.ai"> 
                <div class="valid-feedback">Ok!</div>
                <div class="invalid-feedback">
                   	{{ARCHIVO_REQUERIDO}}
                </div>
             </div>


				 <center><div class="col-md-12 mb-3 form-line">
		    		<label for="validationCustom01">Estado Proveedor*</label>
		        	<select name="estado_proveedor" class="form-control" required>
					     <option value="Activo" @if($proveedor->estado_proveedor == 'Activo') selected @endif>Activo</option>
					     <option value="Inactivo"@if($proveedor->estado_proveedor == 'Inactivo') selected @endif>Inactivo</option>
               <div class="valid-feedback">Estado Actualizado</div>
                <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
					 </select>
		</div></center>
		    		
		<center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Editar proveedor</button></center>

		 </form>
  @stop 








