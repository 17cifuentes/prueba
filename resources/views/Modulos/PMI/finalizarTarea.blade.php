@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<center><legend><b>Finalizar tarea</b></legend></center>
	<form enctype="" class="needs-validation" method="GET" action="{{url('PMI/finalizarTareaProccess')}}" id="idFinalizarTareaProyecto"  data-accion="Finalizar tarea" novalidate>

  <input type="hidden" name="id" id="id" value="{{ $_GET['id'] }}">

	<center><div class="col-md-12 mb-3 form-line">
          <label for="validationCustom01">Descripción</label>
          <textarea type="text"   name="finalizar" id="finalizar" class="form-control input-modal" min="5" maxlength="250"  required /></textarea>   
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}  ,  {{CAMPO_TEXT_MAX_250}}
          </div>
        </div></center>  

        <center><button type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Finalizar tarea</button></center>
      </form>

@stop