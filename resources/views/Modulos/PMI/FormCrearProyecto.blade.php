<form enctype="" class="needs-validation" method="POST" action="{{url('PMI/createProccess')}}" data-accion="Crear proyecto" id="crearFormPMI" novalidate>
	<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
	<legend>Datos del proyecto</legend>
	
	<div class="col-md-6 mb-3 form-line">
		<input type="hidden" name="cliente" value="{{ $cliente }}">
		<label for="validationCustom01">Nombre *</label>
		<input type="text" class="form-control input-modal" id="pmi_nombre" name="pmi_nombre" min="1" maxlength="50" value="{{ old('pmi_nombre') }}" required pattern="[A-Za-z0-9 ]+" title="Registre nombre sin caracteres especiales">   
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>
	</div>	
	
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Descripción *</label>
		<input type="text" class="form-control input-modal" id="pmi_descripcion" name="pmi_descripcion" min="1" maxlength="250" value="{{ old('pmi_descripcion') }}" required>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>                    	
	</div>

	<center>
		<div class="col-md-4 mb-3 form-line">
			<label for="validationCustom01">Responsable *</label>
			<select name="responsable" id="responsable" class="form-control input-modal show-tick {{SFT_SELECT2}}" required="">
				<option value="">Seleccione</option>
				
				@foreach($usuarios as $usu)
				<option value="{{$usu->id}}">{!! $usu->name !!}</option>
				@endforeach
				
			</select>
			<div class="valid-feedback">Ok!</div>
			<div class="invalid-feedback">
				{{CAMPO_SELECT}}
			</div>                    	
		</div>
	</center>	
	
	<div class="col-md-4 mb-3 form-line">
		<label for="validationCustom01">Fecha inicio *</label>
		<input type="text"  class="form-control input-modal datepicker" id="fec_inicio" name="fec_inicio" maxlength="250" value="{{old('fec_inicio')}}" required="" >
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_DATA_MAYOR_HOY}}
		</div> 
	</div>

	<div class="col-md-4 mb-3 form-line">
		<label for="validationCustom01">Fecha final *</label>
		<input type="text" class="form-control input-modal datepicker" id="fec_fin" name="fec_fin" maxlength="250" value="{{old('fec_fin')}}" required="" >
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_DATA_MAYOR_HOY}}
		</div> 
	</div>	
	
	<span id="contenidoCampos"></span>

	<div class="col-lg-12"><center><button id="btnCrearUsuario"
		type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear proyecto</button></center></div>

	</form>