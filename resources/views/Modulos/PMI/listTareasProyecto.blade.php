@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<b> Nombre del proyecto:  {{$proyecto->pmi_nombre}}  </b>
<hr>
	<form method="post" >
	 	<input type="hidden" name="" value="">
        <a type="button" class="btn btn-info" style="color: white" id="id_proyecto" value="{{$_GET['id']}}" onclick="modalagregarTareaProyecto({{$_GET['id']}})"> <b> Crear Tarea </b></a>
        <hr>
         {!! $table !!}
	 </form>
	@if(isset($opc))
		<input type="hidden" id="ruta">
		<input type="hidden" value="{{ $opc }}" id="opcTable">
		<script type="text/javascript">
			window.addEventListener('load', calculos, false);
			function calculos() {	
				pintarOpc();
			}
		</script>
	@endif
@stop
