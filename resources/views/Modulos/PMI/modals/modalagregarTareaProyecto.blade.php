<div class="modal" id="modalagregarTareaProyecto">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal body -->
      <div class="modal-body">
        <center><legend><b>Datos del proyecto</b></legend></center>
    <form enctype="" class="needs-validation"  id="formmodalagregarTareaProyecto" novalidate>
        
        <input type="hidden" name="proyecto_id" id="proyecto_id" >

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Nombre del proyecto</label>
          <input type="text" class="form-control input-modal" name="pmi_nombre" id="pmi_nombre" min="1" maxlength="50" readonly="">   
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
          </div>
        </div>  
        
        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Descripción del proyecto</label>
          <textarea class="form-control" rows="3" id="pmi_descripcion" maxlength="250" readonly=""></textarea>
            </textarea>
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
          </div>                      
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Fecha inicio</label>
          <input type="date" class="form-control input-modal" name="fec_inicio" id="fec_inicio" maxlength="250" readonly="">
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_DATA_MAYOR_HOY}}
          </div> 
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Fecha final </label>
          <input type="date" class="form-control input-modal" name="fec_fin" maxlength="250" id="fec_fin" readonly="">
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_DATA_MAYOR_HOY}}
          </div> 
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Nombre tarea *</label>
          <input type="text" class="form-control input-modal" name="nombre_tarea" id="nombre_tarea" min="1" maxlength="50" required="">   
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
          </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Prioridad *</label>
          <select class="form-control input-modal" name="prioridad" id="prioridad" min="1" maxlength="50"  required="">
            
          </select>
        </div>
        
        <div class="col-md-12 mb-3 form-line">
           <center> <label for="validationCustom01">Descripcion Tarea *</label> </center> 
            <textarea class="form-control" rows="5" maxlength="250" id="tarea_descripcion" name="tarea_descripcion"></textarea>
        </div>
        <hr>
        <center>
          <div class="col-md-7 mb-3 form-line">
            <label for="validationCustom01"> Responsable *</label>
            <select class="form-control input-modal" name="responsable_tarea" id="responsable_tarea" required="">
            </select>
          </div>
        </center> 
        </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-success" onclick="agregarTareaProyecto()">Agregar Tarea</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
    </form>
    </div>
  </div>
</div>