<style>
	table{
		margin: 10;	
	}	
	th, td{
		padding: 3px;
	}
</style>

<table>
	<caption><h3>Historial informe de proyecto</h3></caption>
	
	<thead>
		@foreach($clienteDatos as $cliente)
		<tr>
			<th>Nombre</th>
			<td>{{$cliente['cliente_nombre']}}</td>
		</tr>
		<tr>
			<th>Descripción</th>
			<td>{{$cliente['cliente_descripcion']}}</td>
		</tr>
		<tr>
			<th>Telefono</th>
			<td>{{$cliente['telefono']}}</td>
		</tr>
		<tr>
			<th>Correo</th>
			<td>{{$cliente['correo']}}</td>
		</tr>
		@endforeach
	</thead>
</table>
<br>
<hr>

<table border="1px" width="100%">
	<tr align="center">
		<th colspan="8">DATOS PROYECTO</th>
	</tr>
	<tr>
		<th> # </th>
		<th>Nombre</th>
		<th>Descripción</th>
		<th>Responsable</th>
		<th>Inicio</th>
		<th>Finalización</th>
		<th>Creación</th>
		<th>Actualización</th>
	</tr>
	@foreach($project as $data)
	<tr>
		<td>{{$data['id']}}</td>
		<td>{{$data['pmi_nombre']}}</td>
		<td>{{$data['pmi_descripcion']}}</td>
		<td>{{$manager['name']}}</td>
		<td>{{$data['fec_inicio']}}</td>
		<td>{{$data['fec_fin']}}</td>
		<td>{{$data['created_at']}}</td>
		<td>{{$data['updated_at']}}</td>
	</tr>
	@endforeach
</table>

<table border="1px" width="100%">
	<tr align="center">
		<th colspan="5">DATOS RESPONSABLE</th>
	</tr>
	<tr>
		<th>#</th>
		<th>Nombre</th>
		<th>Identificación</th>
		<th>Correo</th>
		<th>Telefono</th>
	</tr>
	
	<tr>
		<td>{{$manager['id']}}</td>
		<td>{{$manager['name']}}</td>
		<td>{{$manager['cedula']}}</td>
		<td>{{$manager['email']}}</td>
		<td>{{$manager['telefono']}}</td>
	</tr>
	
</table>

<table border="1px" width="100%">
	<tr align="center">
		<th colspan="7">DATOS TAREAS</th>
	</tr>
	<tr>
		<th> # </th>
		<th>Tarea</th>
		<th>Estado</th>
		<th>Finalizar</th>
		<th>Desasignar</th>
		<th>Creación</th>
		<th>Asignado</th>
	</tr>
	@foreach($tareas as $datos)
	<tr>
		<td>{{$datos['id']}}</td>
		<td>{{$datos['nombre_tarea']}}</td>
		<td>{{$datos['nombre_estado']}}</td>
		<td>{{$datos['finalizar']}}</td>
		<td>{{$datos['desasignar']}}</td>
		<td>{{$datos['fecha_tarea']}}</td>
		<td>{{$datos['name']}}</td>
	</tr>
	@endforeach
</table>
<br>
<hr>
<footer>
	<div align="center">
		Documento impreso por SFT-PLATFORM - Softheory, todo es posible
	</div>	
</footer>