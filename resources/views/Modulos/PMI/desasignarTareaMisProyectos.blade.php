@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<center><legend><b>Datos de la tarea</b></legend></center>
    <form enctype="" class="needs-validation" method="POST" action="{{url('PMI/desasignarTareaProyectoProccess')}}" id="idDesasignarTareaProyecto"  data-accion="Desasignar Tarea" novalidate>
        <input type="hidden" name="id_tarea" id="id_tarea" value="{{$tarea[0]['id']}}" >
        <input type="hidden" name="proyecto_id" id="proyecto_id" value="{{$proyecto->id}}" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Nombre del proyecto</label>
          <input type="text" class="form-control input-modal" value="{{$proyecto->pmi_nombre }}" min="1" maxlength="50" readonly="" >   
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
          </div>
        </div>  
        
        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Descripción del proyecto</label>
          <textarea class="form-control" rows="3" maxlength="250" readonly="">{{$proyecto->pmi_descripcion}}</textarea>
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
          </div>                      
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Fecha inicio</label>
          <input type="date" class="form-control input-modal" maxlength="250" value="{{$proyecto->fec_inicio}}" readonly="">
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_DATA_MAYOR_HOY}}
          </div> 
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Fecha final </label>
          <input type="date" class="form-control input-modal" maxlength="250" value="{{$proyecto->fec_fin}}" readonly="">
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_DATA_MAYOR_HOY}}
          </div> 
        </div>
        
        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Nombre tarea *</label>
          <input type="text" class="form-control input-modal" name="nombre_tarea" id="nombre_tarea" min="1" value="{{$tarea[0]['nombre_tarea']}}" maxlength="50" required="" readonly>   
          <div class="valid-feedback">Ok!</div>
          <div class="invalid-feedback">
            {{CAMPO_REQUERIDO}}
          </div>
        </div>

        <div class="col-md-6 mb-3 form-line">
          <label for="validationCustom01">Prioridad *</label>
          <select class="form-control input-modal" name="prioridad" id="prioridad" min="1" maxlength="50"  required="" readonly>
            @foreach($prioridad as $priorida)
              <option value="{{$priorida['id']}}" @if($priorida['id'] == $tarea[0]['prioridad'])selected @endif> {{$priorida['nombre_estado']}} </option> 
           @endforeach
          </select>
        </div>
        
        <div class="col-md-12 mb-3 form-line">
           <center> <label for="validationCustom01">Descripcion Tarea *</label> </center> 
            <textarea class="form-control" rows="5" maxlength="250" readonly id="tarea_descripcion" name="tarea_descripcion">{{$tarea[0]['tarea_descripcion']}}</textarea>
        </div>

        
          <div class="col-md-7 mb-3 form-line">
            <label for="validationCustom01"> Responsable *</label>
            <select class="form-control input-modal" name="responsable_tarea" id="responsable_tarea" required="">
              @foreach($usuarios as $usuario)
              <option value="{{$usuario['id']}}" @if($usuario['id'] == $tarea[0]['responsable_tarea'])selected @endif> {{$usuario['name']}} </option> 
              @endforeach
            </select>
          </div>

       <center>
        <div class="col-lg-12"><center><button
            type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Desasignar tarea</button></center>
          </div>
        </center>   
    
</form>
@stop