<form enctype="" class="needs-validation" method="POST" action="{{url('Punto/createProccess')}}" data-accion="Crear proyecto" id="crearFormPMI" novalidate>
	<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
	<legend>Datos del proyecto</legend>
	
	<div class="col-md-6 mb-3 form-line">
		
		<label for="validationCustom01">Nombre punto*</label>
		<input type="text" class="form-control input-modal" id="pun_nombre" name="pun_nombre" min="1" maxlength="50" value="{{ old('pun_nombre') }}" required pattern="[A-Za-z0-9 ]+" title="Registre nombre sin caracteres especiales">   
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>
	</div>	
	
	<div class="col-md-6 mb-3 form-line">
		<label for="validationCustom01">Tipo *</label>
		<input type="text" class="form-control input-modal" id="pun_tipo" name="pun_tipo" min="1" maxlength="250" value="{{ old('pun_tipo') }}" required>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>                    	
	</div>

	<div class="col-md-4 mb-3 form-line">
		<label for="validationCustom01">Posición *</label>
		<input type="text" class="form-control input-modal" id="pun_posicion" name="pun_posicion" min="1" maxlength="250" value="{{ old('pun_posicion') }}" required>
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_REQUERIDO}}
		</div>                    	
	</div>
	
	<div class="col-md-4 mb-3 form-line">
		<label for="validationCustom01">Descripción *</label>
		<input type="text"  class="form-control input-modal datepicker" id="pun_descripcion" name="pun_descripcion" maxlength="250" value="{{old('pun_descripcion')}}" required="" >
		<div class="valid-feedback">Ok!</div>
		<div class="invalid-feedback">
			{{CAMPO_DATA_MAYOR_HOY}}
		</div> 
	</div>

	<center>
		<div class="col-md-4 mb-3 form-line">
			<label for="validationCustom01">Estado *</label>
			<select name="estado" id="estado" class="form-control input-modal show-tick {{SFT_SELECT2}}" required="">
				<option value="">Seleccione</option>
				@foreach($estado as $est)
				
				<option value="{{$est['id']}}">{{$est['nombre_estado']}}</option>
				
				@endforeach
			</select>
			<div class="valid-feedback">Ok!</div>
			<div class="invalid-feedback">
				{{CAMPO_SELECT}}
			</div>                    	
		</div>
	</center>
	<input type="hidden" name="cliente_id" value="{{ $cliente }}">
	<span id="contenidoCampos"></span>

	<div class="col-lg-12"><center><button id="btnCrearPunto"
		type="submit" class="btn btn-success"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear punto</button></center></div>

	</form>