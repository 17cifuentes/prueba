<div class="modal fade" id="modalPunto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header" id="pun_nombre">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12 mb-3 form-line">



          <p>Producto</p>
          <select name="producto" id="producto" class="{{SFT_SELECT2}}"></select>

          <p>Cantidad</p>
          <input type="number" name="cantidad" id="cantidad" class="form-control col-md-6" value="1">

          <button type="button" class="btn btn-success btn-sm" onclick="agregarProducto()">Agregar</button>

        </div>

        <div class="col-md-12 mb-3 form-line">

          <table class="table">
            <thead>
              <th>Producto</th>
              <th>Cantidad</th>
              <th>Valor</th>
            </thead>
            <tbody id="lista_productos">
              <tr>
                
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th></th>
                <th>Subtotal</th>
                <th>Total</th>
              </tr>
              <tr id="operacion">
                
              </tr>
            </tfoot>
          </table>
          <input type="hidden" name="id_producto[]" id="id_producto">
        </div>  
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="nuevaModal()">Añadir</button>
      </div>
    </div>
  </div>
</div>
