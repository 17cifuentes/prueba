@extends($_SESSION[DATA]['layouts'])
@section('contenido')
	<form  method="GET" action="{{url('Tareas/crearTareasProcess')}}" id="formCrearTareas" data-accion="Crear Tareas" class="needs-validation" novalidate>

			<div class="col-md-6 mb-3 form-line">
				<label for="validationCustom01">Nombre*</label>
        			<input type="text" value="" name="nombre" class="form-control" required>
            	<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                    </div>
				</div>

		<div class="col-md-6 mb-3 form-line">
	    		<label for="validationCustom01">Estado*</label>
	    			<select name="estado" class="form-control" required>
					     <option>Seleccione</option>
					     <option value="Activo">Activo</option>
					     <option value="Inactivo">Inactivo</option>
					 </select>
	    			<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                    </div>
			</div>
			
		<div class="col-md-12 mb-3 form-line">
    		<label for="validationCustom01"> Descripción*</label>
        	<textarea type="text" value="" name="descripcion" class="form-control" required></textarea>
            	<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                    </div>
        </div>


		<center><button type="submit" class="btn btn-success"><i class="material-icons">send</i>&nbsp;&nbsp; Registrar</button></center>

		 </form>
  @stop 