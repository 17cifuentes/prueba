@extends($_SESSION[DATA]['layouts'])
@section('contenido')
		<form  method="GET" action="{{url('Tareas/editarTareasProcess')}}" data-accion="Editar Tareas" class="needs-validation" novalidate>
		<input type="hidden" name="id" id="formEditarTareas" value="{{ $Tareas->id }}">

			<div class="col-md-6 mb-3 form-line">
					<label for="validationCustom01">Nombre*</label>
        			<input type="text" value="{{$Tareas->nombre}}" name="nombre" class="form-control" required>
        		<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                    </div>
			</div>

			<div class="col-md-6 mb-3 form-line">
	    			<label for="validationCustom01">Estado*</label>
	        		<select value="{{$Tareas->estado}}" name="estado" class="form-control" required>
					     <option>Seleccione</option>
					     <option value="Activo" @if($Tareas->estado == 'Activo') selected @endif>Activo</option>
					     <option value="Inactivo"@if ($Tareas->estado == 'Inactivo') selected @endif>Inactivo</option>
					 </select>
				<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                    </div>
			</div>

		<div class="col-md-12 mb-3 form-line">
    			<label for="validationCustom01">Descripción*</label>
        			<textarea type="text" value="" name="descripcion" class="form-control" required>{{$Tareas->descripcion}}</textarea>
        		<div class="valid-feedback">Ok!</div>
                    <div class="invalid-feedback">
                    {{CAMPO_REQUERIDO}}
                </div>
		</div>

		<center><button type="submit" class="btn btn-success"><i class="material-icons">send</i>&nbsp;&nbsp; Editar</button></center>

		 </form>
  @stop 








