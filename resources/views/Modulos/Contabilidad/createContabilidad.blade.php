@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="">
           <form id="idForm" enctype="multipart/form-data" class="needs-validation col-lg-12" method="POST" action="{{ url('Contabilidad/createCuentaProcess') }}" data-accion="Crear cuenta" novalidate>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">                
                    <div class="col-lg-12">
                   
                    <fieldset class="form-group col-lg-12 " id="">
                    <legend>Ingrese los siguientes datos:</legend>
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                                <label>Nombre *</label>
                            <div class="form-line">
                                <input required="" type="text"  class="form-control" id="nombre" maxlength="250" minlength="1" name="nombre" value="" >
                                <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>                         
                            </div>
                            
                        </div>        
                    </div>  
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Descripción *</label>
                            <div class="form-line">
                            <input required="" required="" type="text"  class="form-control" id="apellido" maxlength="250" minlength="1" name="descripcion" value="" >
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div> 
                            </div>
                        </div>            
                    </div>  

                    <div class="col-lg-6">
                        <div class="form-group">
                                <label>Código *</label>
                            <div class="form-line">
                                <input required="" type="number"  class="form-control" id="direccion"  name="codigo" value="" maxlength="250" minlength="1">
                            <div class="valid-feedback">Ok!</div>
                            <div class="invalid-feedback">
                                {{CAMPO_REQUERIDO}}
                            </div>                
                            </div>
                            
                        </div>     
                    </div>  
                
                   <div class="col-lg-6">
                    <div class="form-group">
                        <label>Estado *</label>
                        <div class="form-line">
                         <select class="form-control" name="tipo_estado" required="">
                            <option value="">Seleccione</option>
                            @foreach($tipoEstados as $estados) 
                             <option value="{{$estados->id}}">{{$estados->nombre_estado}} </option>
                            @endforeach
                         </select>
                        <div class="valid-feedback">Ok!</div>
                        <div class="invalid-feedback">
                            {{CAMPO_SELECT}}
                        </div>                        
                        </div>
                    </div>              
                    </div>  
                </fieldset>
                <div class='col-lg-12'>
                    <center>
                        <button type='submit' class='btn btn-success'><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Crear cuenta</button>
                    </center>
                </div>
                </div>
                    
            </form>
        </div>
    </div>
</div>
@stop
