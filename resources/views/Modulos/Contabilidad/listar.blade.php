@extends($_SESSION[DATA]['layouts'])
@section('contenido')


<?php
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
$total_1N= 0; 
$total_2N=0; 
$total_3N= 0;

?>


    <div class="col-md-12 table-responsive">
      <div class="card">
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px">Gestión de Nómina</th>
            <th align="center" valign="middle">
			<a href="{{ url('Contabilidad/reservado') }}" class="btn btn-success" title="Ver nómina quincenal" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Quincenal</a>
			</th>			
			
				</tr>
			</thead>	
			</table>	
				</div>
		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
		<thead>
            <tr>
              <th>NOMBRE</th>
			  <th>Cargos</th>
              <th align="center">Salario</th>
			  <th align="center">Contrato</th>
			  <th align="center">{{ $info[0]->pensioon }}% Pensión $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center">{{ $info[0]->salud }}% Salud $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center">4% Caja de Compensación $</th> <!-- COMPENSACIÓN FAMILIAR         -->			  
			  <th align="center">ARL $</th> <!-- 0.52% 1.04% 2.44% 4.35% 6.96%         -->
			  <th align="center">{{ $info[0]->cesantiia }}% Cesantía $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center">{{ $info[0]->intereses }}% Intereses Cesantía $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center">{{ $info[0]->prima }}% Prima $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center">{{ $info[0]->vacaciones }}% Vacaciones $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center">Auxilio de Transporte $</th>
			  <th align="center">Comisiones $</th>
			  <th align="center">Otros Pagos $</th>
			  <th align="center">Total Seguridad Social Mensual</th>			  
			  <th align="center">RESERVA PROVISIÓN MENSUAL PRESTACIONES SOCIALES</th>
			  <th align="center">Costo Total sin Provisión</th>
	  
			  <th>Editar</th>
			  
            </tr>
          </thead>
          <tbody>
          @foreach ($usuarios as $usuario)
		@if ($usuario->user_estado == "Activo" && $usuario->nombre != "admin" && $usuario->rol != "Cliente")

            <tr>
              <td>
			  <a href="mailto:
		{{ ($usuario->email) }}
		?subject=
		Tirilla%20Nómina
		&body=
		{{ ucwords($usuario->nombre) }}, la siguiente es la tirrilla de nómina
		">
			  {{ ucwords($usuario->nombre) }}
				</a> <?php $total_1= 0; $total_2=0; $total_3= 0;?>
			  </td>

			  <td>{{ ucwords($usuario->rol['rol_nombre']) }}</td>
              <td align="right"><span class="label label-primary">$ {{ number_format($usuario->valor_a_pagar,0) }}</span> <?php $total_3+= $usuario->valor_a_pagar;$total_3N+=$total_3;?> </td>
			  <td>
			  @if($usuario->contrato == 0)
			  <span class="label label-success">Servicios</span>
			  @else
			  <span class="label label-primary">Laboral</span>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->pensioon/100,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->pensioon/100;?> 
			  @else
			  0 <?php $total_1+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->salud/100,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->salud/100;?> 
			  @else
			  0 <?php $total_1+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.04,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.04;?> 
			  @else
			  0 <?php $total_1+= 0;?>
			  @endif
			  </td>			  
			  <td align="right">
			  @if($usuario->contrato == 1)
				  @if ($usuario->arl_categoria == 1)
				  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.00522,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.00522;?>
				  @elseif ($usuario->arl_categoria == 2)
				  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0104,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0104;?>
				  @elseif ($usuario->arl_categoria == 3)
				  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0244,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0244;?>
				  @elseif ($usuario->arl_categoria == 4)
				  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0435,0) }} <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0435;?>
				  @elseif ($usuario->arl_categoria == 5)
				  {{ number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0696,0) }}	<?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0696;?>
				  @endif
				  <span class="label label-info">Categoría {{ $usuario->arl_categoria }}</span>
			  @else
			  0 <?php $total_1+= 0;?>
			  @endif				  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->cesantiia/100,0),0) }} <?php $total_2+= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->cesantiia/100,0);?> 
			  @else
			  0 <?php $total_2+= 0;?>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->intereses/100,0),0) }} <?php $total_2+= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->intereses/100,0);?> 
			  @else
			  0 <?php $total_2+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->prima/100,0),0) }} <?php $total_2+= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->prima/100,0);?> 
			  @else
			  0 <?php $total_2+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format($usuario->valor_a_pagar*$info[0]->vacaciones/100,0) }} <?php $total_2+= $usuario->valor_a_pagar*$info[0]->vacaciones/100;?> 
			  @else
			  0 <?php $total_2+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
				  @if ($usuario->valor_a_pagar <= $usuario->valor_a_pagar*2)
				  {{ number_format($info[0]->auxilio,0) }} <?php $total_3+= $info[0]->auxilio;?>
				  @else
				  0
				  @endif
			  @else
			  0 <?php $total_2+= 0;?>
			  @endif				  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format($usuario->comisiones,0) }} <?php $total_3+= $usuario->comisiones?> 
			  @else
			  0 <?php $total_3+= 0;?>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format($usuario->otros,0) }} <?php $total_3+= $usuario->otros?> 
			  @else
			  0 <?php $total_3+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  <span class="label label-primary"><?php echo "$ ".number_format($total_1,0);$total_1N+=$total_1;?></span><?php $total_3+= $total_1;?> 
			  @else
			  0 <?php $total_3+= 0;$total_1N+= 0;?>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  <span class="label label-warning"><?php echo "$ ".number_format($total_2,0);$total_2N+=$total_2;?></span>
			  @else
			  0 <?php $total_2N+= 0;?>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  <span class="label label-info"><?php echo "$ ".number_format($total_3,0);?></span>
			  @else
			  0 
			  @endif			  
			  </td>
			  
			  <td align="center" valign="middle">

				<a onclick="editarNomina({{$usuario}})" class="btn-sm btn-success" title="Editar el registro de los otros gastos {{ ucwords($usuario->nombre) }}"> Editar</a>
									  
			  </td>	
		  
            </tr>		
		
		@endif
          @endforeach
          
          </tbody>
        </table>
		</div> 
		
			
		
      </div>
    </div>
	
    <div class="col-md-12" style="margin-top: 10%;">
      <div class="card">
		
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px"> Reporte de Nómina <?php echo $meses[date('n')-1] ?>:  </th>
				</tr>
			</thead>	
			</table>	
				</div>		
		
        <div class="card-body">
          <div class="row">
            <div class="col-md-6" align="right">
              <label>Total Salarios Mensuales </label>
              <input type="text" style="text-align:right;" readonly class="form-control" name="SalarioMensual" value="$ {{ number_format($total_3N,0) }}" maxlength="250">
              <label>Total Seguridad Social Mensual</label>
              <input type="text" style="text-align:right;" name="Seguridad" readonly class="form-control" value="$ {{ number_format($total_1N,0) }}" maxlength="250">
            </div>
            <div class="col-md-6" align="right">
              <label>Total reserva de Provisiones Sociales Mensuales</label>
              <input type="text" style="text-align:right;" name="Salario" readonly class="form-control" value="$ {{ number_format($total_2N,0) }}" >
              <label>Total Costo de Nómina</label>
              <input type="text" style="text-align:right;" name="GNómina" readonly class="form-control" value="$ {{ number_format($total_3N + $total_1N,0) }}" >			  
            </div>
          </div>
        </div>
      </div>
    </div>			
  </div>

@stop