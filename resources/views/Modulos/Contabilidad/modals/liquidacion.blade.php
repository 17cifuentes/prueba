	<div class="modal recent-report" tabindex="-1" id="liquidar" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title" id="myModalLabel">Desprendible de Nómina</h4>
		  </div>
	  <form id="formLiquidacion" method="POST" action="{{ url('/Contabilidad/pdf') }}">		  
		<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
		<div class="card-body">
			<div class="row">
			<div class="col-md-12">
			<label>Identificación </label>
			<input type="text"  class="form-control" id="Identidad" name="Identidad" maxlength="250" value="">  																						
			</div>
			</div>
		  <div class="row">
			<div class="col-md-6">
			<label>Desde </label>
			<input type="hidden" class="form-control date"  value="" name="Id">           									
			<input type="text"  id="FechaInicio" class="form-control datepicker" name="FechaInicio" value="<?php echo date('Y-m-d'); ?>">
			<label>Nombre: </label>									
			<input readonly type="text" readonly id="Nombre" class="form-control date" name="Nombre" value="" >
			<label>Sueldo: </label>									
			<input readonly type="text" style="text-align:right;" readonly id="Sueldo" class="form-control date" name="Sueldo" value="" >									
			</div>
			<div class="col-md-6">
			<label>Hasta</label>	
			<input type="text"  id="FechaFinal" class="form-control datepicker" name="FechaFinal" value="<?php echo date('Y-m-d'); ?>">
			<label>Cargo: </label>									
			<input readonly type="text" readonly id="Cargo" class="form-control date" name="Cargo" value="" >
			<label>Contrato: </label>
			<input readonly type="text" readonly class="form-control date"  id="contrato" name="Contrato" value="" >
								
			</div>
		  </div>
		<div class="row">
		<div class="row">
		<div class="col-md-6">
		<label>Deducción Pensión $ </label>
		</div>
		<div class="col-md-6">									
			<input readonly type="text" style="text-align:right;" readonly id="Pension" class="form-control date" name="Pensión" value="" >
		</div>
		</div>
		<div class="row">
		<div class="col-md-6">
			<label>Deducción Salud $ </label>
		</div>
		<div class="col-md-6">
			<input readonly type="text" style="text-align:right;" readonly id="Salud" class="form-control date" name="Salud" value="" >
		</div>									
		</div>
		<div class="row">
		<div class="col-md-6">								
			<label>Auxilio de Transporte $ </label>	
		</div>
		<div class="col-md-6">		

			<input readonly type="text" style="text-align:right;" readonly id="auxilio" class="form-control date" name="Auxilio" value="" >
		</div>
		</div>
		<div class="row">
		<div class="col-md-6">								
			<label>Comisiones $ </label>	
		</div>
		<div class="col-md-6">									
			<input readonly type="text" style="text-align:right;" readonly id="Comisiones" class="form-control date" name="Comisiones" value="" >
		</div>
		<div class="row">
		<div class="col-md-6">								
			<label>Otros Pagos, extras $ </label>	
		</div>
		<div class="col-md-6">									
			<input readonly type="text" style="text-align:right;" readonly id="otrosPagos" class="form-control date" name="Otros Pagos" value="{" >
		</div>
		</div>								
		</div>								
		</div>
		</div>								  	
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary">Ver Desprendible</button> 
		  </div>
	</form>		  
		</div>
	  </div>
	</div>	
		