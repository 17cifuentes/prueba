<!-- ModalBODY -->

	<div class="modal fade" id="desprendibleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title" id="myModalLabel">Generar Desprendibles de Nómina</h4>
		  </div>
	  <form target="_blank" action="{{ url('Contabilidad/pdf_todos') }}" method="POST">		  
		<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
		<div class="card-body">
		  <div class="row">
			<div class="col-md-6">
			<label>Desde </label>
			<input type="text"  id="FechaInicio" class="form-control datepicker" name="FechaInicio" value="<?php echo date('Y-m-d'); ?>">
			</div>
			<div class="col-md-6">
			<label>Hasta</label>	
			<input type="text"  id="FechaFinal" class="form-control datepicker" name="FechaFinal" value="<?php echo date('Y-m-d'); ?>">
			</div>
            <div class="col-md-6">
			<label>Acciones</label>	
			<select class="form-control" name="opc" required="">
			    <option value="">Seleccione</option>
			    <option value="1">Solo ver</option>
			    <option value="2">Ver y Pagar</option>
			</select>
			</div>									
		  </div>

		</div>								  	
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary">Generar Desprendibles</button> 
		  </div>
	</form>		  
		</div>
	  </div>
	</div>			
<!-- Modal -->	