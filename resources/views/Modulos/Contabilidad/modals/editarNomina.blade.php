
	<div class="modal" id="editNomina" tabindex="-1" value="" role="dialog" aria-labelledby="myModalLabel">
			  <form id="formEditNomina" action="" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
				  <div class="col-md-6">
			  <div class="card">
				<div class="card-header">
				  <h2>Detalles de Otros Pagos para</h2><h2 id="nombreUsuario"> </h2>
				</div>
				<div class="card-body">
				  <div class="row">
					<div class="col-md-6">
					<label>Contrato*</label>
					  <select required="" class="select" name="contrato">
						<option value="1"> Laboral</option>
						<option value="0"> Servicios</option>
					  </select>	
					  <br>
					<label>Comisiones $</label>
					<input type="number" id="venci1" class="form-control date" name="Comisioon" value="">
					<input type="hidden" id="venci2" class="form-control date" name="Id" value="">								
					<br>
					<label>Otros pagos, extras, compensatorios $</label>
					<input type="number"  id="venci3" class="form-control date" name="Otros" value="{{ $usuario->otros }}">
					<label>Categoría ARL *</label>
					  <select required="" class="select" name="ARL">
						<option value="1"> Categoría 1 </option>
						<option value="2"> Categoría 2 </option>
						<option value="3"> Categoría 3 </option>
						<option value="4"> Categoría 4 </option>
						<option value="5"> Categoría 5 </option>
					  </select>
					
					</div>
				  <center><a class="btn btn-success" value="" onclick="actualizarPagos()">Actualizar Pagos</a></center>
				  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
					
				  </div>
				</div>
			  </div>
				  </div>


			  </form>
	</div>
