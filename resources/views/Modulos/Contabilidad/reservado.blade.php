@extends($_SESSION[DATA]['layouts'])
@section('contenido')

<?php
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
$total_1N= 0; 
$total_2N=0; 
$total_3N= 0;

?>


    <div class="col-md-12 table-responsive">
      <div class="card">
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px">Nómina Quincenal</th>
            <th align="center" valign="middle">
			<a href="{{ url('Contabilidad/listarNomina') }}" class="btn btn-success" title="Ver nómina mensual" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Mensual</a>
			</th>
            <th align="center" valign="middle">
	  			
			<a target="_blank" onclick="desprendibles()"  class="btn btn-danger" title="Ver Desprendibles"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Desprendibles</a>		
					
			</th>			
			
				</tr>
			</thead>	
			</table>	
				</div>
		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
		<thead>
            <tr>
			  <th>Desprendibles</th>
              <th>NOMBRE</th>
			  <th>Cargos</th>
              <th align="center">Salario</th>
			  <th align="center">Contrato</th>
			  <th align="center">{{ 16-$info[0]->pensioon }}% Pensión $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center">{{ 12.5 -$info[0]->salud }}% Salud $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center">Auxilio de Transporte $</th>
			  <th align="center">Comisiones $</th>
			  <th align="center">Otros Pagos $</th>
			  <th align="center">Descontar Seguridad Social Quincenal</th>			  
			  <th align="center">Total Pago</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($usuarios as $usuario)
		@if ($usuario->user_estado == "Activo" && $usuario->nombre != "admin" && $usuario->rol != "Cliente")
            <tr>
			<td>
			<center>
						<!-- Modal -->			
			<a type="button" class="btn-sm btn-info " onclick='liquidar({{$usuario}},{{$info[0]->pensioon/100}},{{$info[0]->salud/100}},{{$info[0]->auxilio}})' title="Liquidar Días {{ $usuario->nombre }}" data-toggle="tooltip" data-placement="left">Liquidar</a>
						
			</center>

			</td>
              <td>
			  <a href="mailto:
		{{ ($usuario->email) }}
		?subject=
		Tirilla%20Nómina
		&body=
		{{ ucwords($usuario->nombre) }}, la siguiente es la tirrilla de nómina
		">
			  {{ ucwords($usuario->nombre) }}
				</a> <?php $total_1= 0; $total_2=0; $total_3= 0;?>
			  </td>
			  <td>{{ ucwords($usuario->rol['rol_nombre']) }}</td>
              <td align="right"><span class="label label-primary">$ {{ number_format($usuario->valor_a_pagar/2,0) }}</span> <?php $total_3+= $usuario->valor_a_pagar/2;$total_3N+=$total_3;?> </td>
			  <td>
			  @if($usuario->contrato == 0)
			  <span class="label label-success">Servicios</span>
			  @else
			  <span class="label label-primary">Laboral</span>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.16-$info[0]->pensioon/100)/2),0) }} <?php $total_1-= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.16-$info[0]->pensioon/100)/2);?> 
			  @else
			  0 <?php $total_1+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.125-$info[0]->salud/100)/2),0) }} <?php $total_1-= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.125-$info[0]->salud/100)/2);?> 
			  @else
			  0 <?php $total_1+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
				  @if ($usuario->valor_a_pagar <= $usuario->valor_a_pagar*2)
				  {{ number_format($info[0]->auxilio/2,0) }} <?php $total_3+= $info[0]->auxilio/2;?>
				  @else
				  0
				  @endif
			  @else
			  0 <?php $total_2+= 0;?>
			  @endif				  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format($usuario->comisiones,0) }} <?php $total_3+= $usuario->comisiones?> 
			  @else
			  0 <?php $total_3+= 0;?>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  {{ number_format($usuario->otros,0) }} <?php $total_3+= $usuario->otros?> 
			  @else
			  0 <?php $total_3+= 0;?>
			  @endif
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  <span class="label label-primary"><?php echo "$ ".number_format($total_1,0);$total_1N+=$total_1;?></span><?php $total_3+= $total_1;?> 
			  @else
			  0 <?php $total_3+= 0;$total_1N+= 0;?>
			  @endif			  
			  </td>
			  <td align="right">
			  @if($usuario->contrato == 1)
			  <span class="label label-info"><?php echo "$ ".number_format($total_3,0);?></span>
			  @else
			  0 
			  @endif			  
			  </td>
 
            </tr>		
		
		@endif
          @endforeach
          </tbody>
        </table>
		</div> 
		
			
		
      </div>
    </div>
	
    <div class="col-md-12">
      <div class="card">
		
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px"> Reporte de Nómina Quincenal <?php echo $meses[date('n')-1] ?>:  </th>
				</tr>
			</thead>	
			</table>	
				</div>		
		
        <div class="card-body">
          <div class="row">
            <div class="col-md-12" align="right">
              <label>Total Salarios Quincenales </label>
              <input type="text" style="text-align:right;" readonly class="form-control" name="SalarioMensual" value="$ {{ number_format($total_3N,0) }}" maxlength="250">
              <label>Total Seguridad Social Quincenal a Descontar</label>
              <input type="text" style="text-align:right;" name="Seguridad" readonly class="form-control" value="$ {{ number_format($total_1N*-1,0) }}" maxlength="250">			  
			  </div>
          </div>
        </div>
      </div>
    </div>			
	
  </div>
 
 <script>
     $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });
 </script>
 
@stop