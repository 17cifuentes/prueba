<div class="modal" id="eventoReserva" style="height: 60%">
	<div class="">
		<div class="">
			<div class="modal-header">
			<h4 class="modal-title">Registrar evento</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<!-- Modal body -->
		<div class="modal-body">
			<input type="hidden" id="ruta" value="">
			<input type="hidden" id="urlBaseEvento" value="{{url('reservas/crearEventoProccess')}}">
			<input type="hidden" id="urlDeleteEven" value="{{url('reservas/eliminarEvento')}}">
			<form enctype="multipart/form-data" method="POST" action="{{url('reservas/crearEventoProccess')}}" id="formCrearEvento"  data-accion="Crear evento" class="needs-validation" novalidate>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="col-md-12 mb-3 form-line">
					  	<label for="validationCustom01">Nombre del evento*</label>
						<input type="text" value="" name="nombre" id="nombreEvento" class="form-control" maxlength="200" required >
						<div class="valid-feedback">Ok</div>
						<div class="invalid-feedback">
							{{CAMPO_REQUERIDO}},{{CAMPO_TEXT_MAX_200}}
						</div>
					</div>
					<div class="col-md-12 mb-3 form-line">
				 		<label for="validationCustom01">Servicios o Productos*</label>
					  	<select value="" name="id_producto_servicio" id="id_producto_servicio" class="form-control" required>
							<option value="">seleccione</option>
				  		</select>
				 		<div class="valid-feedback">ok</div>
						 <div class="invalid-feedback">
							  {{CAMPO_REQUERIDO}},{{CAMPO_DATA_MAYOR_HOY}}
						 </div>
					</div><br>
					<div class="col-md-6 mb-3 form-line">
				 		<label for="validationCustom01">Fecha inicio*</label>
					  	<input type="text" class="form-control datepicker" data-date="" data-date-format="Ymd" required name="fechaInicio" id="fechaInicio">
				 		<div class="valid-feedback">ok</div>
						 <div class="invalid-feedback">
							  {{CAMPO_REQUERIDO}},{{CAMPO_DATA_MAYOR_HOY}}
						 </div>
					 </div>
					 <div class="col-md-6 mb-3 form-line">
				 		<label for="validationCustom01">hora Inicio*</label>
				 		<div class="dataTimeSft input-append date">
    						<input data-format="hh:mm:ss" type="text" name="horaInicio" id="horaInicio">
    						<span class="add-on">
      								<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
    						</span>
  						</div>
				 		<div class="valid-feedback">ok</div>
						 <div class="invalid-feedback">
							  {{CAMPO_REQUERIDO}}
						 </div>
					 </div>
					 <div class="col-md-12 col-lg-12 col-xs-12"><hr></div>
					 <div class="col-md-6 mb-3 form-line">
				 		<label for="validationCustom01">Fecha fin*</label>
					  	<input type="text" class="form-control datepicker" data-date="" data-date-format="Ymd" required name="fechaFin" id="fechaFin">
				 		<div class="valid-feedback">ok</div>
						 <div class="invalid-feedback">
							  {{CAMPO_REQUERIDO}},{{CAMPO_DATA_MAYOR_HOY}}
						 </div>
					 </div>
					 <div class="col-md-6 mb-3 form-line">
				 		<label for="validationCustom01">hora fin*</label>
				 		<div class="dataTimeSft input-append date">
    						<input data-format="hh:mm:ss" type="text" name="horaFin" id="horaFin">
    						<span class="add-on">
      								<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
    						</span>
  						</div>
				 		<div class="valid-feedback">ok</div>
						 <div class="invalid-feedback">
							  {{CAMPO_REQUERIDO}}
						 </div>
					 </div>
					<div class="col-md-12 mb-3 form-line">
				 		<label for="validationCustom01">Tipo de evento*</label>
					  	<select value="" name="tipoEvento" id="tipoEvento" class="form-control" required>
							<option value="">seleccione</option>
							<option value="default">Transparente(Normal)</option>
							<option value="important">Rojo(Urgente)</option>
							<option value="chill">Rosa(Importante)</option>
							<option value="info">Azul(Informativo)</option>
							<option value="success">verde(Secundario)</option>
				  		</select>
				 		<div class="valid-feedback">ok</div>
						 <div class="invalid-feedback">
							  {{CAMPO_REQUERIDO}},{{CAMPO_DATA_MAYOR_HOY}}
						 </div>
					</div><br>

		 <center>
		 	<div class="col-lg-12 col-md-12 col-xs-12" id="opctionEvento">
		 		<button id="btnCrearEvento" type="submit" class="btn btn-success btn-sm">Crear evento</button>
		 		
		 	</div>

		 </center>

		  </form>

			</div>

			<!-- Modal footer -->
			<div class="modal-footer col-lg-12 col-md-12 col-xs-12">
				<div class="col-md-6 col-xs-6 col-lg-6">
					<button  id="btnEliminarEvento" class="float-left btn btn-danger btn-sm hidden">Eliminar evento</button>	
				</div>
				<div class="col-md-6 col-xs-6 col-lg-6">
					<button type="button" class="float-right btn btn-default" data-dismiss="modal">Cancelar</button>		
				</div>
			
			</div>

		 </div>
	  </div>
	</div>