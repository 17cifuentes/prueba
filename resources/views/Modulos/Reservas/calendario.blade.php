@extends($_SESSION[DATA]['layouts'])
@section('contenido')
<div class="collg-12 col-md-12 col-xs-12">
	<div class="col-lg-2 col-md-4 col-xs-6">
		<a href="{{url('reservas/listarEventos')}}" class="btn btn-primary">Eventos</a>
	</div>
	<div class="col-lg-2 col-md-4 col-xs-6">


		<div class="col-lg-12"><center><button
		type="submit" class="btn btn-success openEvento"><i class="far fa-paper-plane"></i>&nbsp;&nbsp; Nuevo evento</button></center></div>


</button>
	</div>
</div>
<div id='wrap'>
	<div id='calendar'></div>
</div>
<input type="hidden" id="eventosCalendar" value="{{ json_encode($eventos) }}">
<input type="hidden" id="listProductos" value="{{ json_encode($productos) }}">
@stop