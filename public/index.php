<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

define('LARAVEL_START', microtime(true));

/*INGLES
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

/*ESPAÑOL
| ------------------------------------------------- -------------------------
| Registrar el cargador automático
| ------------------------------------------------- -------------------------
|
| Composer proporciona un cargador de clases conveniente y generado automáticamente para
| nuestra aplicacion Sólo necesitamos utilizarlo! Simplemente lo requeriremos
| en el script aquí para que no tengamos que preocuparnos por el manual
| cargando cualquiera de nuestras clases mas adelante. Se siente muy bien para relajarse.
|
*/
require __DIR__.'/../vendor/autoload.php';

/*INGLES
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/
/*ESPAÑOL
| ------------------------------------------------- -------------------------
| Enciende las luces
| ------------------------------------------------- -------------------------
|
| Necesitamos iluminar el desarrollo de PHP, así que encendamos las luces.
| Esto arranca el marco y lo prepara para su uso, luego
| cargará esta aplicación para que podamos ejecutarla y enviarla
| Las respuestas vuelven al navegador y deleitan a nuestros usuarios.
|
*/
$app = require_once __DIR__.'/../bootstrap/app.php';

/*INGLES
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

/*ESPAÑOL
| ------------------------------------------------- -------------------------
| Ejecutar la aplicación
| ------------------------------------------------- -------------------------
|
| Una vez que tengamos la aplicación, podremos manejar la solicitud entrante.
| a través del núcleo, y enviar la respuesta asociada de nuevo a
| El navegador del cliente permitiéndoles disfrutar de la creatividad.
| Y maravillosa aplicación que hemos preparado para ellos.
|*/
$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
