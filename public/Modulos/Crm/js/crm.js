var  contador_s=0;
var  contador_m=0;
var  contador_h=0;

function iniciarCronomretro()
{
	$("#iniciarCronomretro").addClass('disabled');
	seg = $("#seg").val();
	min = $("#min").val();
	horas=$("#hors").val();
	
	cronometro = setInterval(
		function(){
			if(contador_s == 60)
			{
				contador_s = 0;
				contador_m++;
				$("#min").text(contador_m);
				if(contador_m == 60)
				{
					contador_h++;
					$("#hors").text(contador_h);
					contador_m =0;
				}
			}
			$("#seg").text(contador_s);
			contador_s++;
			$("#tiempo_de_contacto").val(contador_h+":"+contador_m+":"+contador_s);
		},1000);
	
}

function terminarCronomretro()
{	
	$("#terminarCronomretro").addClass('disabled');
	$("#tiempo_de_contacto").val(contador_h+":"+contador_m+":"+contador_s);
	clearInterval(cronometro);
}
function getChartDataContact(){
            
        var nombre = $('#tituloGrafico1').val();
        var tipo = $('#tipoGrafico1').val();
        var contenedor = $('#contenedorGrafico1').val();
        var valorGrafico = $('#valorGrafico1').val();
        var dataGrafico = $('#dataGrafico1').val();    
        data = JSON.parse(dataGrafico);
        valor = JSON.parse(valorGrafico);
        getChart(nombre, tipo, contenedor, valor, data);
}