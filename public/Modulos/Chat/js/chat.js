$(".chatBtn").click(function(){
    idContacto = $(this).data('id');
    idChat= $(this).data('chat');
    $.ajax({
        type: 'get',
        url: "/chat/getMsgChat", 
        data: "id="+idChat+"&idContacto="+idContacto,
        success: function(result){
            data = JSON.parse(result);
            $("#contactoId").val(idContacto);
            $("#chatId").val(idChat);
            chat = JSON.parse(data.msg)
            putMsg(chat);
            changeTabToChat();
            $(".messages").scrollTop($(".up").offset().top);
        }
    });


});
//función para enviar mensaje y mostrarlo en pantalla
function newMessage() {
    message = $(".message-input input").val();
    contactoId = $("#contactoId").val();
    chatId = $("#chatId").val();
    if($.trim(message) == '') {
        return false;
    }
     $.ajax({
        type: 'get',
        url: "/chat/sendMsg", 
        data: "id="+chatId+"&msg="+message+"&contacto="+contactoId,
        success: function(result){
            $("#msgChat li .up").remove();
            data = JSON.parse(result);
            chat = JSON.parse(data.msg);
            putMsg(chat);
            $(".wrap input").val("");
            $(".messages").scrollTop($(".up").offset().top);
        }
    });
    
};

$('.submit').click(function() {
  newMessage();
});

$(window).on('keydown', function(e) {
  if (e.which == 13) {
    newMessage();
    return false;
  }
});
//funcion par aponer agregar mensaje
function putMsg(chat){
$("#msgChat").html("");
$.each(chat[0].usu, function( index, value ) {
                if(chat[0].usu[index] != ""){

                    html = '<li class="sent">\
                        <img src="'+urlBase+data.imgUser+'" alt="" />\
                        <p style="font-size:1.1em">'+chat[0].usu[index]+'<br><em style="font-size:0.5em">'+chat[0].timeUsu[index]+'</em></p>\
                    </li>';
                    $("#msgChat").append(html);
                }
                if(chat[0].contacto[index] != undefined){
                    html = '<li class="replies">\
                        <img src="'+urlBase+data.imgContacto+'" alt="" />\
                        <p style="font-size:1.1em">'+chat[0].contacto[index]+'<br><em style="font-size:0.5em">'+chat[0].timeContacto[index]+'</em></p>\
                    </li>';
                    $("#msgChat").append(html);
                }
            });
            $("#msgChat").append("<li class='up'></li>");
}
//funcion para cambiar de tab contactos a tab chat
function changeTabToChat(){
    $("#contactosList").removeClass('active');
    $("#chatUser").addClass('active');
    $("#home").removeClass('active');
    $("#home").removeClass('in');
    $("#menu1").addClass('active');
    $("#menu1").addClass('in');    
}
//vienen por default deprurar
function starChat(){
    $(".messages").animate({ scrollTop: $(document).height() }, "fast");

    $("#profile-img").click(function() {
        $("#status-options").toggleClass("active");
    });

    $(".expand-button").click(function() {
      $("#profile").toggleClass("expanded");
        $("#contacts").toggleClass("expanded");
    });    
}


$("#status-options ul li").click(function() {
    $("#profile-img").removeClass();
    $("#status-online").removeClass("active");
    $("#status-away").removeClass("active");
    $("#status-busy").removeClass("active");
    $("#status-offline").removeClass("active");
    $(this).addClass("active");
    
    if($("#status-online").hasClass("active")) {
        $("#profile-img").addClass("online");
    } else if ($("#status-away").hasClass("active")) {
        $("#profile-img").addClass("away");
    } else if ($("#status-busy").hasClass("active")) {
        $("#profile-img").addClass("busy");
    } else if ($("#status-offline").hasClass("active")) {
        $("#profile-img").addClass("offline");
    } else {
        $("#profile-img").removeClass();
    };
    
    $("#status-options").removeClass("active");
});
//var reviewChats = window.setInterval(putChats,8000);
/*
function putChats() {
    contactoId = $("#contactoId").val();
    chat = $("#chatNum"+contactoId).val();
    
    /*$.ajax({
        type: 'get',
        url: "/chat/searchChat", 
        success: function(result){
            $("#msgChat li .up").remove();
            data = JSON.parse(result);
            chat = JSON.parse(data.msg);
            putMsg(chat);
            //$(".wrap input").val("");
            //$(".messages").scrollTop($(".up").offset().top);
        }
    });    
}*/
//# sourceURL=pen.js

