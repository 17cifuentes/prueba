
function modalagregarTareaProyecto(id_dato){

	var url = '/PMI/asignarTareasProyecto?id='+id_dato;

	$.ajax({
		type: 'get',
		url: url,
		success :function(datos){
			var dato = JSON.parse(datos);
			if(dato == "" || dato == null){
				showNotification("Ocurrio un error, Reintentelo o comuniquese con el administrador","error"); 
			}
			$('#proyecto_id').val(dato['proyecto']['id']);
			$('#pmi_nombre').val(dato['proyecto']['pmi_nombre']);
			$('#pmi_descripcion').val(dato['proyecto']['pmi_descripcion']);
			$('#fec_inicio').val(dato['proyecto']['fec_inicio']);
			$('#fec_fin').val(dato['proyecto']['fec_fin']);
			html = "";
              $.each(dato['usuarios'], function( index,     value ) {
                html +=  "<option value='"+value.id+"'>"+value.name+"</option>";
              });
            $("#responsable_tarea").html(html);
            html2 = "";
              $.each(dato['prioridad'], function( index,     value ) {
                html2 +=  "<option value='"+value.id+"'>"+value.nombre_estado+"</option>";
              });
            $("#prioridad").html(html2);
            $('#modalagregarTareaProyecto').modal('show');
     	}
	})
}

function agregarTareaProyecto(){

	var id_proyecto = $('#proyecto_id').val();
	var nombre_tarea = $('#nombre_tarea').val();
	var prioridad = $('#prioridad').val();
	var tarea_descripcion = $('#tarea_descripcion').val();
	var responsable_tarea = $('#responsable_tarea').val();
		if( id_proyecto == null || id_proyecto.length == 0 || /^\s+$/.test(id_proyecto) ) {
			showNotification("Ocurrio un error, Faltan datos. Reintentelo o comuniquese con el administrador","error"); 
			return false;
		}
		if (!/^([0-9])*$/.test(id_proyecto)){
	      		showNotification("Ocurrio un error, No modificar los datos enviados. Reintentelo o comuniquese con el administrador","error"); 
	      		return false;
		}

		if(prioridad == null || prioridad.length == 0 || /^\s+$/.test(prioridad)){
			showNotification("Ocurrio un error, La prioridad es obligatoria. Reintentelo o comuniquese con el administrador","error"); 
			return false;
		}

		if(nombre_tarea == null || nombre_tarea.length == 0 || /^\s+$/.test(nombre_tarea)){
	  		showNotification("Ocurrio un error, El nombre de la tarea es obligatorio. Reintentelo o comuniquese con el administrador","error");
	  		$('#nombre_tarea').focus();
	  		return false;
	  	}
	  	var Max_Length = 50;
		var length = $('#nombre_tarea').val().length;
		if (length > Max_Length) 
		{
  			showNotification("Ocurrio un error, Limite de caracteres excedido. Reintentelo o comuniquese con el administrador","error");
  			$('#nombre_tarea').focus();
  			return false;
		}

	  	if(tarea_descripcion == null || tarea_descripcion.length == 0 || /^\s+$/.test(tarea_descripcion)){
	  		showNotification("Ocurrio un error, La descripcion de la tarea es obligatoria. Reintentelo o comuniquese con el administrador","error");
	  		$('#tarea_descripcion').focus();
	  		return false;
	  	}
	  	var Max_Length = 250;
		var length = $('#tarea_descripcion').val().length;
		if (length > Max_Length) 
		{
  			showNotification("Ocurrio un error, Limite de caracteres excedido. Reintentelo o comuniquese con el administrador","error");
  			$('#tarea_descripcion').focus();
  			return false;
		}

	  	if(responsable_tarea == null || responsable_tarea.length == 0 || /^\s+$/.test(responsable_tarea) )
	  	{
	  		showNotification("Ocurrio un error, el responsable de la tarea es obligatorio. Reintentelo o comuniquese con el administrador","error");	
	  		$('#responsable_tarea').focus();
	  		return false;
	  	}
		
		form = $( "#formmodalagregarTareaProyecto" ).serialize();
		var url = "asignarTareasProyectoProcess";
		$.ajax({
				type: 'get',
				url : url,
				data : form,
				contentType: 'application/json; chshowNotification("Ocurrio un error, el responsable de la tarea es obligatoria debes dejarlo como minimo Sin Asignar. Reintentelo o comuniquese con el administrador","error");arset=utf-8',
				success : function(respuesta){
					if(respuesta)
					{
						
						alertMsg('Notificación',"Se registo la tarea correctamente",'success');
						$("#modalagregarTareaProyecto").modal("hide");
						setTimeout(function(){
  							window.location.reload();
						}, 2000);
						
					}else{
						alertMsg('Notificación',"Ocurrio un error, Reintentelo o comuniquese con el administrador",'warning');
						$("#modalagregarTareaProyecto").modal("hide");
						
					}
					
				}

			}) 	
	}

