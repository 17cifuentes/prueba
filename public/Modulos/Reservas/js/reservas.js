function showDataEvent(identificador){
		$.ajax({
		type: 'get',
		url: "/reservas/getEvent", 
		data: "id="+identificador,
		success: function(result){
			var event = JSON.parse(result);
			$("#eventoReserva").css("height","50%");
			$("#eventoReserva modal").css("height","50%");
			var html = "";
			for (var i = 0; i < event.productos.length ; i++) {
				html += "<option value='"+event.productos[i].id+"'>"+event.productos[i].pro_nombre+"</option>"
				
			}
			$("#id_producto_servicio").html(html);			
			putDataEvent(event);
			$("#eventoReserva").modal("show");		
	    }
	});
}