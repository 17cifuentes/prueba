
/**
 * Disparador, esta funcion agrega estilos a los articulos seleccionados en 
 *  el carrito de compras.
 * @category GeneralController
 * @package Modulo Tienda
 * @version 1.0
 * @author Samuel Beltran
 */
$( document ).ready(function() {
    getFonts();
    pintarCarrito();
    getStyleJunior();
    $(".drop a").css("color","black");
});
function ordenarPlandilla(){
		ids = $("#idHides").val();
		per = JSON.parse(ids);
		$( per ).each(function(indice, elemento) { 
            if(elemento == "bienvenido"){
				$("#promosMes").css("margin-top","-10em");            	
				$("#categoriasOrden").css("margin-top","-10em");            	
            }
        });
}
function getStyleJunior(){
	$(".imgCart").css({
		'width':'8em',
	    'border-radius': '100px',
        'margin-top': '18%'

		//'float' : 'left' 
	    //'margin':'5% 0px 6% 20%'
	});

	$(".form-control").css({
		'width': '20%'
	});

	$(".title").css({
		'margin-top' : '1%',
		'display': 'block'
	});

	$(".price").css({
		'font-family': 'sans-serif',
		'display': 'block'
	});

	$("#listProCart li").addClass("col-lg-12");
	$(".image").addClass("col-lg-4");
	$(".content").addClass("col-lg-6");
	$(".qty input").css("width","60%");
	$("#listProCart li button").addClass("col-lg-2");
	$("#listProCart li button").addClass("btn btn-info");
	$("#listProCart li a").css("margin-top","1em");
   	$("#listProCart li").css("margin","8% 0 1% 0");
	$("#listProCart li button").css("margin-top","25%");
	$(".mini-cart-bottom").addClass("col-lg-12");
	$(".mini-cart-bottom h4").addClass("col-lg-6");
	$(".close-cart").addClass("hidden");
	$(".mini-cart-top span").css("font-family","sans-serif");
	var colorCliente = $('#colorCliente').val();

	$('.header-category').css({'background-color':colorCliente,'color':'white'});
	var URLdomain = window.location.host;
	if(URLdomain != "goodlicor.com" ){
		$('i').css({'color':colorCliente});
	}
	$('.gallery__zoom li a i').css('color','white')
	$('.single__footer__address li').css({'color':colorCliente,'margin-right':'1em','font-size': '26px'});
	$('.bg--theme').css({'background':'#666666'});
	$('.titleLine').css({	'width': '43%',
								    'margin-bottom': '3%',
								    'margin-left': 'auto',
								    'margin-right': 'auto',
								    'height': '4px',
								    'background-color': '#666666'
									});
	$('#logoNormal').css({	
						    'width': '100%',
						    'height': '100%',
						    
					        

	});

	$('.logo').css({'padding-left':'23px'});
	$('.fa-shopping-basket').css({ 'color':'#4444449e'});  
	$('.proImg').css({ 'width':'200',"height":"200"}); 
	$('.proImg2').css({ 'width':'370',"height":"310"}); 
	$('.border__color').css({'border-bottom':'5px solid',colorCliente});
	$('.service__icon').css({'background': "#a8ce38"});
}

$(function(){
	$(".fa-angle-up").click(function(){
		console.log("funcionara");
		//$('.container').animate({scrollTop:0},'1500');
		$("#top").focus();
		$(".container").scrollTop($("#top").offset().top);

	});

});
        
    
