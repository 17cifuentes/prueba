//eventos shopcart
//funcioneS

/*
  *Autor: Cristian Campo
  *Modulo: Tienda (ShopCart)
  *Versiòn: 1.0
  *Entrada:id_domi(shopcart.js->listProdDomicilios)
  *Salida: Nueva pestaña con detalle del producto
  *Descripciòn: Consulta el id de un producto a través del id_domi
*/
function detalleProducto(id){
	$.ajax({
		url :'/carritoCompras/detalleProducto',
		data : "id="+id,
		success:function(data){
			data = JSON.parse(data);
			window.open('/carritoCompras/mostrarProductoDetalle?id='+data[0]+'&idCliente='+data[1]);
		}
	});
}
function getFonts(){
	font = $("#idfonts").val();
	$("h2").attr("style",font);
	$("h1").attr("style",font);
	$("h3").attr("style",font);
	$("h4").attr("style",font);
	$("h5").attr("style",font);
	$("p").attr("style",font);
	$("a").attr("style",font);
}
function cargarSelect(idCliente){

	var url = '/carritoCompras/consultaMenu?id='+idCliente;
	
	$.ajax({
		type:'get',
		url:url,
		success:function(xd){
			var menu = JSON.parse(xd);
			console.log(menu[0].nombre);
			console.log(menu[0].id);
			html = "";
			for (var i = 0; i < menu.length; i++) {
				html += "<option value="+menu[i].id+">"+menu[i].nombre+"</option>"
			}
			$("#Menu").html(html);
		}

	});
}

function init(){
		$(".drop a").css("color","red");
        var colorCliente = $('#colorCliente').val();
        $('button').css({'background-color':colorCliente,'color':'white'});
        $('.header-category').css({'background-color':colorCliente,'color':'white'});
        $(".catColor").css('color','#CAC3C3');
        $('.number').css({'background-color':colorCliente,'color':'white'});
        $('#scrollUp').css({'background-color':colorCliente,'color':'white'});
        $('.sub-menu').css('border-top','3px solid '+colorCliente);
        $(".add-to-cart").css({'background-color':colorCliente,'color':'white'});
        $("i").css('color','#202020');
        pintarCarrito();
 }
/*Funciones*/
function addProToCart(idPro){
	$.ajax({
		url: "/carritoCompras/addProductToCart", 
		data: "idProducto="+idPro,
		success: function(result){
			$("#productosCart").val(result);
        	pintarCarrito();
        	showNotification('Producto agregado correctamente','success');
    	}
	});
}
function removeProToCart(idPro){
	$.ajax({
		url: "/carritoCompras/removeProToCart", 
		data: "idProducto="+idPro,
		success: function(result){
			$("#productosCart").val(result);
        	pintarCarrito();
        	showNotification('Producto removído correctamente','success');
    	}
	});
}
function pintarCarrito(){
	cart = JSON.parse($("#productosCart").val());
	var html = '';
	//console.log(cart[1][0].pro_nombre);
	var getUrl = window.location.origin;
	//Crear listado de carrito
	var x = 0;
	//Agregar a la UI cantidad y listado de productos
	$.each(cart, function(y, item) {

    		if(y != 0){
    			var precioVenta = cart[y][0].pro_precio_venta.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",");
    				html += '\
				<li>\
	            	<a class="image"><img class="imgCart" src="'+getUrl+'/'+cart[y][0]["img_producto"]+'" alt="'+cart[y][0].pro_nombre+'"></a>\
	            	<div class="content">\
	                	<a href="#" class="title">'+cart[y][0].pro_nombre+'</a>\
	                	<span class="price">Precio: $'+precioVenta+'</span>\
	                	<span class="qty">Cantidad:<input min="1" id="cantPro'+cart[y][0].id+'" onchange="changeCantProCart('+cart[y][0].id+')" type="number" class="form-control" value="'+cart[y].pro_cantidad_cart+'"></span>\
	            	</div>\
	            	<button class="remove shopBoton" onclick="removeProToCart('+cart[y][0].id+')"><i class="fa fa-trash-o"></i></button>\
	        	</li>';
	        	
    		}
    		x= y;
    		
	});
	
	$("#cantProCart").html(x);
	$("#listProCart").html(html);
	//se ajusta url de respuesta 
	url = $('#urlResponse').val();
	newUrl = url+"/"+cart[0].referenceCode;		
	$("#urlResponse").val(newUrl);

		if(cart.length == 0){
		//Si es cero agregar cero a opciones
		$("#totalCart").html('$0');
		$("#amount").val(0);
		$("#cantProCart").html('0');
	}else{
		//pintar total de carrito
		$("#totalCart").html('$'+cart[0].totalCart);
		var res = cart[0].totalCart.replace(",", "");
		//total para pagos
		$("#amount").val(res);
		//si existe los codigos para pagos pintarlos
		if(cart[0].referenceCode.length > 0 && cart[0].signature.length > 0){
			$("#referenceCode").val(cart[0].referenceCode);
			$("#signature").val(cart[0].signature);
		}
		//crear url de respuesta
		var URLactual = window.location.href;
		console.log(URLactual);
	}
	setTimeout(getPlantillaStyle(),2000);
}
function getPlantillaStyle(){
	plantilla = $("#platillaNum").val();
	if(plantilla == 1){

	}
	if(plantilla == 5){
		getStyleLuxyry();
	}
	if(plantilla == 3){
		getStyleJunior();
	}
}
function checks(){
	if($('.add-to-cart').hasClass('added')){
       $('.add-to-cart').removeClass('added').find('i').removeClass('ti-check').addClass('ti-shopping-cart').siblings('span').text('AÑADIR'); 
    }else{
       $('.add-to-cart').addClass('added').find('i').addClass('ti-check').removeClass('ti-shopping-cart').siblings('span').text('AÑADIDO'); 
    }
}
function vaciarCart(){
	$.ajax({
		url: "/carritoCompras/emptyCart", 
		success: function(result){
			$("#productosCart").val(result);
        	pintarCarrito();
        	showNotification('Carrito vaciado correctamente','success');
    	}
	});
}
function changeCantProCart(idPro){
	cantPro = $("#cantPro"+idPro).val();
	$.ajax({
		url: "/carritoCompras/newCantProCart", 
		data: 'idProducto='+idPro+'&cantPro='+cantPro,
		success: function(result){
			$("#productosCart").val(result);
        	pintarCarrito();
    	}
	});
}

//funcion para enviar el id del producto por url
  function addToFavoritos(item){
        uri = $(item).data('url');
        id= $(item).data('id');
        idFav = $(item).data("favorito");
        url = uri+"?id="+id+"&opc="+idFav;
        window.location.href= url;

   }

function modalPeticionDomicilio()
{
	usuId = $("#usuId").val();
	if(usuId == ""){
		alertMsg('Notificación',"Primero Debes Iniciar Sesion",'warning');
		idCliente = $("#idCliente").val();
		$.ajax({
			url: "/Usuarios/crearUsuarioExterno",
			data: "idCliente="+idCliente,
			success: function(res){
				respuesta = JSON.parse(res);
				html = "<option value=''>Selecciones</option>";
				$.each(respuesta.departamentos, function(y, item) {
    				html += '<option value='+item.id+'>'+item.nombre+'</option>';	
				});
				$("#departamento_id").html(html);
				html = "<option value=''>Selecciones</option>";
				$.each(respuesta.clientes, function(y, item) {
    				html += '<option value='+item.id+'>'+item.cliente_nombre+'</option>';	
				});
				$("#cliente_id").html(html);
				
				$(".cartbox-wrap").removeClass("is-visible");
				$(".input-modal").css("width","100%");
				$("#btnCrearUsuario").attr("type","button");
				$("#btnCrearUsuario").attr("onclick","registroExternoAjax()");
				$("#registroUsuario").modal("show");
			}
		})

	}else if(usuId != ""){

		validarProductosCarrito();
		$(".cartbox-wrap").removeClass("is-visible");
		$(".mini-cart-wrap").removeClass("open");
	}
	validarPromocion();
}

function validarProductosCarrito()
{
	$.ajax({
		url: "/carritoCompras/validarProductos",
		success: function(res)
		{
			if(res == false){
				alertMsg('Notificación',"Debes agregar productos antes de ir a comprar",'warning');
				$("#modalPeticionDomicilio").modal("hide");
			}else{
				$("#modalPeticionDomicilio").modal("show");
			}

		}

	}) 
}

function registerProductsDatabases(dato)
{
	if(dato == "Online"){
		pintarCarrito();
		$("#formpago").submit();
		LimpiarCarrito();
	}else{
	$.ajax({
			url: "/carritoCompras/registerProductsDatabases/"+dato,
			success: function(res){
				domi = JSON.parse(res);
				if(dato == "Online")
				{
					$('#formpago').submit();
					$("#modalPeticionDomicilio").modal("hide");
					LimpiarCarrito();
				}else{
					html = "<h4>Codigo de referencia:</h4>"+domi.referencia+"<br>";
					html += "<h4>Tipo transacción:</h4> Contra-entrega"+"<br>";
					html += "<h4>Identificador de domicilio:</h4>"+domi.id+"<br>";
					html += "<h4>Fecha pedido:</h4>"+domi.created_at+"<br>";
					$("#datosDomicilio").html(html);
					$("#modalExitoContraentrega").modal("show");
				}
				$("#modalPeticionDomicilio").modal("hide");
				LimpiarCarrito();
			}
		})
	}
}

function LimpiarCarrito(){
	$.ajax({
		url: "/carritoCompras/emptyCart", 
		success: function(result){
			$("#productosCart").val(result);
        	pintarCarrito();
        }
	});
	showNotification('Su compra se genero correctamente','success');
}

function registroExternoAjax(){
	form = $("#crearFormUsu").serialize();
	idCliente = $("#idCliente").val();
	$.ajax({
		url: "/Usuarios/createProccess",
		data: form+"&externo=1&idCliente="+idCliente,
		success: function(res){
			if(res == "true"){
				//$('#formpago').submit();
				window.location.reload();
				alert("Has Iniciado Session Ya Puedes Generar Tu Compra");
			}else{
				alert("Error en los datos");
			}
		}
	})
} 


function registroShopCartAjax()
{ 
	idCliente = $("#idCliente").val();
	$.ajax({
			url: "/Usuarios/crearUsuarioExterno",
			data: "idCliente="+idCliente,
			success: function(res){
				respuesta = JSON.parse(res);
				html = "<option value=''>Selecciones</option>";
				$.each(respuesta.departamentos, function(y, item) {
    				html += '<option value='+item.id+'>'+item.nombre+'</option>';	
				});
				$("#departamento_id").html(html);
				html = "<option value=''>Selecciones</option>";
				$.each(respuesta.clientes, function(y, item) {
    				html += '<option value='+item.id+'>'+item.cliente_nombre+'</option>';	
				});
				$("#cliente_id").html(html);
				
				$(".cartbox-wrap").removeClass("is-visible");
				$(".input-modal").css("width","100%");
				$("#btnCrearUsuario").attr("type","button");
				$("#btnCrearUsuario").attr("onclick","registroExternoShopCart()");
				$("#registroUsuario").modal("show");
			}
		})
}

function registroExternoShopCart(){
	form = $("#crearFormUsu").serialize();
	idCliente = $("#idCliente").val();
		$.ajax({
		url: "/Usuarios/createProccess",
		data: form+"&externo=1&idCliente="+idCliente,
		success: function(res){
			if(res == "true"){
				window.location.href = "/home";
			}else{
				
				showNotification("No se pudo crear la cuenta <br>1.Revise los campos <br>2.Puede que el correo ya exista","error"); 
			}
		}
	})
} 

function validarPromocion(){
		idCliente = $("#idCliente").val();

		$.ajax({
		url: "/Promociones/calculatePromocion",
		data: "idCliente="+idCliente,
		success: function(res)
		{
			resultado = JSON.parse(res);
			html = '<p>Valor inicial:</p><br><p>Descuento:</p><br><p>valor a pagar: </p><br>';
			$(".calContent").html(html);
		}

	})
}

function cancelarProducto($identificador)
{
	var  url ="/carritoCompras/domiciliomotivosCancell";

	$.ajax({
		url: url,
		success: function(datos){
			$('#modalcancelarProducto').modal('show');
			data = JSON.parse(datos);	
			html = "";
            $.each(data, function( index,     value ) {
            html +=  "<option value='"+value.motivos+"'>"+value.motivos+"</option>";
              });
            $("#estadosUsuario").html(html);
            $("#idProducDomi").val($identificador);
			
		}


	})
}
function cancelarProductoMotivo()
{
	$('#modalcancelarProducto').modal('hide');
	var motivoCancelar = $("#estadosUsuario").val();
    var idProductoDomi = $("#idProducDomi").val();
    
    if(motivoCancelar == null || motivoCancelar.length == 0 || /^\s+$/.test(motivoCancelar) ) 
    {
    	$("#estadosUsuario").focus();
		showNotification("Ocurrio un error, Debe seleccionar el motivo. Reintentelo o comuniquese con el administrador","error"); 
		return false;
	}
	if(idProductoDomi == null || idProductoDomi.length == 0 || /^\s+$/.test(idProductoDomi))
	{
		showNotification("Ocurrio un error, Faltan Datos. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}
		
	  url = "/carritoCompras/cancelarProductoDomicilio";
      $.ajax({
      	url: url,
      	data : "idProducDomi="+idProductoDomi+"&motivo="+motivoCancelar,
      	success:function(respuesta){
      		var respuesta1 = JSON.parse(respuesta);
      		if(respuesta1 == true)
      		{
      			
				alertMsg('Notificación',"Se cancelo el producto correctamente",'success');
					
					setTimeout(function(){
  							window.location.reload();
					}, 1000);
      		}else{
      			alertMsg('Notificación',"Ocurrio un error, Reintentelo o comuniquese con el administrador",'warning');
      		}
      	}
      })
      
    consultarProductosDomi(idProductoDomi);
}

function consultarProductosDomi(idProductoDomi)
{
	var url= "/carritoCompras/consultarProductoAjax";

	$.ajax({
		url:url,
		data:"idProducDomi="+idProductoDomi,
		success: function(){
			setTimeout(function(){
				window.location.reload();
			}, 1000);
		}
	})
	
}
/*
	Funcion para llamar a la modal $("#modalAgregarProductoDomicilio"), y seleccionar 
	el producto a agregar en el domicilio y la cantidad
*/
function addrNewProdDomi($id)
{
	var url = "/carritoCompras/productsCliente";

	$.ajax({
		url : url,
		data: "idDomicilio="+$id,
		success: function(productos){
			product= JSON.parse(productos);
			
			html = "";
            $.each(product, function( index,     value ) {
            html +=  "<option value='"+value.id+"'>Producto: "+value.pro_descripcion+"</option>";
              });
            $("#productosCliente").html(html);
            $("#domicilio_cliente").val($id);
            $("#productCantidadDomi").val(1);
            $("#modalAgregarProductoDomicilio").modal("show");
		}
	})
}
/*
	funcion que recibe los datos de la modal $("#modalAgregarProductoDomicilio") y recibe
	id del domicilio el id del producto y la cantidad del producto para hacer la insercion
	en la base de datos del nuevo producto

*/
function agregarNewProductDomi()
{
	$("#modalAgregarProductoDomicilio").modal("hide");
	var  idDomicilio = $("#domicilio_cliente").val();
	var  idproducto = $("#productosCliente").val();
	var  cantidadProd = $("#productCantidadDomi").val();

	if(idDomicilio == null || idDomicilio.length == 0 || /^\s+$/.test(idDomicilio) ) 
	{
		showNotification("Ocurrio un error, Faltan Datos. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}
	if(idproducto == null || idproducto.length == 0 || /^\s+$/.test(idproducto) ) 
	{
		showNotification("Ocurrio un error, Faltan Datos. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}
	if(cantidadProd == null || cantidadProd.length == 0 || /^\s+$/.test(cantidadProd) ) 
	{
		showNotification("Ocurrio un error, Debes agregar una cantidad. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}
	if(isNaN(cantidadProd) == true)
	{
		showNotification("Ocurrio un error, Cantidad no especificada. Reintentelo o comuniquese con el administrador","error"); 
		return false;		
	}
	if(cantidadProd == 0 || cantidadProd < 0 ) 
	{
		showNotification("Ocurrio un error, La cantidad debe ser mayor que 0. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}

	var url= "/carritoCompras/agregarNuevoProductoDomicilio";
	$.ajax({
		url: url,
		data: "idPro="+idproducto+"&cantidadProd="+cantidadProd+"&idDomicilio="+idDomicilio,
		success: function(respuesta){
			var res = JSON.parse(respuesta);
			if(res != false){
				alertMsg('Notificación',"Se agrego el producto correctamente",'success');
						setTimeout(function(){
  							window.location.reload();
						}, 1000);
			}else{
				alertMsg('Notificación',"Ocurrio un error, Reintentelo o comuniquese con el administrador",'warning');
			}
		}
	})
}

function Categoriashijos(identificador){
	$.ajax({
		type: 'get',
		url: '/Productos/identificarHijos',
		data: "id="+identificador,
		success: function(result){
			cat = JSON.parse(result);
			html = "";
			$.each(cat, function(index,value) {
				html += '<div class="col-lg-6 col-md-6 col-sm-6 col-12 md-mt-60 sm-mt-60" style="margin-top: 6em;">\
				<div class="service bg--white border__color" style="visibility: visible; animation-name: fadeInUp;">\
					<div class="service__icon" style="background: black">\
						'+value.formato+'\
					</div>\
					<div class="service__details">\
                        <h6><a  data-toggle="tooltip" title="ir a '+value.cat_nombre+'" class="catColor" href="/carritoCompras/buscadorCategorias?id='+value.cliente_id+'&idCat='+value.id+'">'+value.cat_nombre+'</a></h6>\
                    	<p>'+value.cat_descripcion+'</p>\
                    	<a href="/carritoCompras/buscadorCategorias?id='+value.cliente_id+'&idCat='+value.id+'" ><img class="proImg" src="/'+value.path+'"></a> \
					</div>\
				</div>\
			</div>';			
			  });
			if(html == ""){
				url = '/carritoCompras/buscadorCategorias?id='+$("#idCliente").val()+'&idCat='+identificador;
				window.location.href= url;				
			}else{
				$("#result").html(html);
				$("#padreModal").modal("show");	
			}
			
		}      
	});
}
/*
	Funcion para llamar una modal $("#modalCancelarDomicilio").modal, y traer los
	estados por los cuales se desea cancelar el domicilio del cliente
*/

function cancelarDomicilio($id)
{
	var  url ="/carritoCompras/domiciliomotivosCancell";
	$.ajax({
		url: url,
		success: function(datos){
			data = JSON.parse(datos);	
			html = "";
            $.each(data, function( index,     value ) {
            html +=  "<option value='"+value.motivos+"'>"+value.motivos+"</option>";
              });
            $("#estadosUsuarioCancelarDomi").html(html);
            $("#idDomicilio").val($id);
            $("#modalCancelarDomicilio").modal("show");
        }


	})
}
/*	
	Funcion para recibir los datos de la modal $("#modalCancelarDomicilio").modal 
	y insertarlos en la base datos para cancelar el domicilio especificando el
	motivo por el cual se cancelo.
*/

function cancelarDomicilioMotivo()
{
	$("#modalCancelarDomicilio").modal("hide");

	var idDomicilio = $("#idDomicilio").val();
	var motivo = $("#estadosUsuarioCancelarDomi").val();

	if(idDomicilio == null || idDomicilio.length == 0 || /^\s+$/.test(idDomicilio) ) 
	{
		showNotification("Ocurrio un error, Faltan Datos. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}
	if(isNaN(idDomicilio) == true)
	{
		showNotification("Ocurrio un error, Datos incorrectos. Reintentelo o comuniquese con el administrador","error"); 
		return false;		
	}
	if(motivo == null || motivo.length == 0 || /^\s+$/.test(motivo) ) 
	{
		showNotification("Ocurrio un error, Faltan Datos. Reintentelo o comuniquese con el administrador","error"); 
		return false;	
	}
	
	var url = "/carritoCompras/cancelarDomicilio"
	$.ajax({
		url : url,
		data: "idDomicilio="+idDomicilio+"&cancela_motivo="+motivo,
		success: function(respuesta){
			var res = JSON.parse(respuesta);
			if(res == false)
			{
				alertMsg('Notificación',"Ocurrio un error, Reintentelo o comuniquese con el administrador",'warning');	
			}else{
				alertMsg('Notificación',"Se cancelo el domicilio correctamente",'success');
						setTimeout(function(){
  							window.location.reload();
						}, 1000);
			}
			
		}

	})

}
function datosUsuario($id)
{
	var url = "/carritoCompras/consultUserForOrder"
	$.ajax({
		url: url,
		data: "idDomicilio="+$id,
		success: function(infoUser){
			var usu = JSON.parse(infoUser);
			console.log(usu);
			$("#Usuario").val(usu.name);
			$("#email").val(usu.email);
			$("#telefono").val(usu.telefono);
			$("#direccion").val(usu.direccion);
			$("#modalsInfoUsuario").modal("show");
		}

	})
}


