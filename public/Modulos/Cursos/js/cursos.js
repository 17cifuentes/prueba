/*
    Funcion Real Time para cargar la imagen en tiempo real usada en la funcion de crear Curso
    JR
*/

var filePreview = document.createElement('img');
function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
 
            reader.onload = function (e) {
                
                filePreview.id = 'file-preview';
                //e.target.result contents the base64 data from the image uploaded
                filePreview.src = e.target.result;
                var previewZone = document.getElementById('file-preview-zone');
                previewZone.appendChild(filePreview);
            }
 
            reader.readAsDataURL(input.files[0]);
        }
    }
 
    var fileUpload = document.getElementById('file-upload');
    fileUpload.onchange = function (e) {
        readFile(e.srcElement);
}
/*
    Funcion para taer todas las categorias en una modal del listar de cursos
    JR
*/
function categoriasCurso(idCurso)
{
    var url= '/Cursos/categoriasCursoId';
    
    $.ajax({
        url: url,
        data: "curso_id="+idCurso,
        success:function(users){
          
            usuarios = JSON.parse(users);
            html = "";
            $.each(usuarios, function( index,     value ) {
                html += "<li><b>" +value.cat_nombre+"</b></li>";
            });
            $('#categoriasCurso').html(html);
            $('#modalCategoriasCurso').modal('show');
        }
    })
    
}