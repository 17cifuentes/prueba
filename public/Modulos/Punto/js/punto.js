/*
*Autor: Cristian Campo
*Modulo: Punto
*Versiòn: 1.0
*Descripciòn: Abre ventana modal y muesta los datos de las consultas
*/ 
function modalPunto(id){
	
	modalExterno = $('#modalPunto').modal("show");

	$.ajax({
		url:'/Punto/punto',
		type:'GET',
		data:'id='+id,
		success:function(resultado){

			resultado = JSON.parse(resultado);

			$("#pun_nombre").html(
				'<h5 class="modal-title">'+resultado.pun_nombre+'</h5>'
				);
		},
	});

	getProductos();
}


/*
*Autor: Cristian Campo
*Modulo: Punto
*Versiòn: 1.0
*Descripciòn: Consulta todos los productos.
*/
function getProductos(){

	$.ajax({
		
		url:'/Productos/datosProductos',
		type:'GET',
		success:function(resultado){

			resultado = JSON.parse(resultado);
			
			html='';

			$.each(resultado, function(index,value) {
				
				html+='<option value="'+value.id+'">'+value.pro_nombre+'</option>';
            });

            $("#producto").html(html);
		},
	});
}


/*
*Autor: Cristian Campo
*Modulo: Punto
*Versiòn: 1.0
*Descripciòn: Añade un producto a la compra.
*/
subtotal=0;
total = 0;


function agregarProducto(){

	punto = $("#punto").val();
	producto = $("#producto").val();
	cantidad = $("#cantidad").val();

	
	$.ajax({
		/*
		*Autor: Cristian Campo
		*Descripciòn: Consulta los datos un producto en especifico.
		*/
		
		url:'/Punto/producto',
		type:'GET',
		data:'id='+producto,
		success:function(resultado){

			resultado = JSON.parse(resultado);

			if (cantidad>0) {

				valorPorProducto = parseInt(resultado.pro_precio_venta)*cantidad;

			}else{
				cantidad=1;
				valorPorProducto = parseInt(resultado.pro_precio_venta)*cantidad;
			}

			subtotal+=valorPorProducto;

			$("#lista_productos").append(

					'<tr>'
						+'<td>'+resultado.pro_nombre+'</td>'
						+'<td>'+cantidad+'</td>'
						+'<td>'+valorPorProducto+'</td>'
					+'</tr>'
				);

			$("#id_producto").append(resultado.id);

			$("#operacion").html(

				'<td></td>'
				+'<td>'+subtotal+'</td>'
				+'<td>'+0+'</td>'
			);
		},
	});
}
