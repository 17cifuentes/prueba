/**
* Abre la modal con el formulario para editar una nomina
* category: contabilidad.js
* package: Modulo Contabilidad
* version: 1.0
* author: Samuel Beltran
*/
function editarNomina(usuario)
{
	$('#editNomina').val(usuario['id_plantilla']);
	$('#nombreUsuario').text(usuario['nombre']);
	$('#venci').val(usuario['comisiones']);
	$('#venci1').val(usuario['comisiones']);
	$('#venci2').val(usuario['id_plantilla']);
	$('#venci3').val(usuario['otros']);																																																																																																																																																																																																																																																																												
	$("#editNomina").modal("show");
	
}

/**
* Esta funcion enviara la informacion capturada desde el formulario
* y la envia a otra funcion para que esta sea actualizada en la base
* de datos. Por ultimo se recargara la pagina.
* category: contabilidad.js
* package: Modulo Contabilidad
* version: 1.0
* author: Samuel Beltran
*/
function actualizarPagos()
{

	form = $("#formEditNomina").serialize();
	$.ajax({
		type:'get',
		url: "/Contabilidad/updateCuota",
		data: form,
		success: function(res){
			if (res >= 1)
			{
				location.reload(true);
				return false;	
			}
			
		}
	})
}

/**
* Esta funcion abre la modal liquidacion
* category: contabilidad.js
* package: Modulo Contabilidad
* version: 1.0
* author: Samuel Beltran
*/
function liquidar(usuario,infoPension,infoSalud,infoLiq)
{
	var contrato;
	var auxilio;

	var comisiones = usuario['comisiones'];
	var sueldo = usuario['valor_a_pagar'];
	var otros = usuario['otros'];
	var computePension = (sueldo + comisiones + otros) * (0.16 - infoPension);
	var computeSalud = (sueldo + comisiones + otros) * (0.125 - infoSalud); 

	if (usuario['contrato'] == 0) 
	{
	 	contrato = "Prestación de Servicios";
	 	auxilio = 0;
	} else
	{
	    contrato = "Labor Contratada";
	    auxilio = infoLiq;
	}

	$('#Identidad').val(usuario['identidad']);
	$('#Id').val(usuario['id_personal']);
	$('#Nombre').val(usuario['nombre']);
	$('#Sueldo').val(sueldo);
	$('#Cargo').val(usuario['rol']['rol_nombre']);
	$('#Nmobre').val(usuario['nombre']);
	$('#Pension').val(Math.round(computePension));
	$('#Salud').val(Math.round(computeSalud));
	$('#contrato').val(contrato);
	$('#auxilio').val(auxilio);
	$('#Comisiones').val(comisiones);
	$('#otrosPagos').val(otros);

	$("#liquidar").modal("show");
	
}

function desprendibles()
{
	
	$("#desprendibleModal").modal("show");
}