/*
Inicializa el calendario
*/
$(".openEvento").click(function(){
    deleteDataEvent();
    var productos = JSON.parse($("#listProductos").val());
    html = "<option value=''>Selecciones</option>";
    
    $.each(productos, function(index, value) {
        html += '<option value='+value.id+'>'+value.pro_nombre+'</option>';   
    });
    $("#lsProductos").html(html);
    $("#eventoReserva").modal("show");
})
function deleteEvent(id){
    console.log(id);
    url = $("#urlDeleteEven").val()+"?id="+id;
    alerta(url,"Eliminar evento");
}
var eventsBD;
function initCalendar(){
    //quit<ar solo de ejemplo
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    eventsEj =  [
                {
                    title: 'Lunch',
                    start: new Date(y, m, 1),
                    end: new Date(y, m, 1),
                    allDay: false,
                    className: 'important'
                }];

    eventsBD = JSON.parse($("#eventosCalendar").val());
    events = formatEvents(eventsBD);
    getCalendar(events,'calendar');
}
//Da el formato de los datos de la BD a lso que resive la libreria
function formatEvents(events){
    eventos=[];
    $.each(events, function( index, value ){
        eventos.push({
                id:value.id,
                title:value.title,
                start:value.fecha_inicio +" "+ value.hora_inicio,
                end:value.fecha_fin +" "+ value.hora_fin,
                allDay:false,
                className:value.className
            });
    });

    return eventos;
}
//Instancia del calendario
function getCalendar(events,Idcontendor){
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    /* initialize the calendar
        -----------------------------------------------------------------*/
        var calendar =  $('#'+Idcontendor).fullCalendar({
            lang: 'es',
            header: {
                left: 'title',
                center: 'agendaDay,agendaWeek,month',
                right: 'prev,next today'
            },
            editable: true,
            firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
            selectable: true,
            defaultView: 'month',

            axisFormat: 'h:mm',
            columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
            allDaySlot: false,
            selectHelper: true,
            select: function(start, end, allDay) {
                //$("#eventoReserva").modal("show");
            },
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            },
            eventClick: function(info) {
                $.each(eventsBD, function( index, value){
                    if(value.id == info.id){
                        showDataEvent(value);
                    }
                });
            },

            events: events,
        });


}
function showDataEvent(event){
    putDataEvent(event);
    $("#eventoReserva").modal("show");
}
function putDataEvent(event){
    var url = $("#formCrearEvento").attr("action");
    $("#formCrearEvento").attr("action",url+"?id="+event.id);
    $("#formCrearEvento").data("accion","Actualizar evento");
    $("#btnEliminarEvento").removeClass("hidden");
    $("#btnEliminarEvento").attr("onclick","deleteEvent("+event.id+")");
    $("#btnCrearEvento").text("Actualizar evento");
    $("#nombreEvento").val(event.title);
    $("#fechaInicio").val(event.fecha_inicio);
    $("#fechaFin").val(event.fecha_fin);
    $("#horaInicio").val(event.hora_inicio);
    $("#horaFin").val(event.hora_fin);
    $("#tipoEvento").val(event.className);
    $("#id_producto_servicio").val(event.id_producto_servicio).change();
}
function deleteDataEvent(){
    url = $("#urlBaseEvento").val();
    $("#formCrearEvento").attr("action",url);
    $("#btnEliminarEvento").addClass("hidden");
    $("#btnCrearEvento").text("Crear evento");
    $("#nombreEvento").val("");
    $("#fechaInicio").val("");
    $("#fechaFin").val("");
    $("#horaInicio").val("");
    $("#horaFin").val("");
    $("#tipoEvento").val("");
}

