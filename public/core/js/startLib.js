/*INICIO DE LIBRERIA DATATABLE*/
function startDataTable(){
    $('.sftDataTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'colvis',
            'excel',
            'print',
            'pdfHtml5'
        ]
    });
}
/*FIN DE LIBRERIA DATATABLE*/
/*INCIO DE LIBRERIA SELECT2*/
function startSelect2(){
    $('.sftSelect2').select2();
}
/*FIN DE LIBRERIA SELECT2*/
