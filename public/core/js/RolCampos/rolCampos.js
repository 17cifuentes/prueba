
function cargarDatosRolCampos(id){
	$.ajax({
		type: 'get',
		url: "/Usuarios/getCampos", 
		data: "id="+id,
		success: function(result){
            campos = JSON.parse(result);
            html = "";   
            $.each(campos, function( index, value ) {
                var atributos = JSON.parse(value.funcion_validacion);
                attr = '';
                $.each(atributos, function( index, value ) {
                    $.each(value, function( dex, val ) {
                        attr += String(dex)+"='"+val+"' ";
                    });
                });
                html +='<div class="col-lg-6 col-md-6"><label for="'+value.campo_nombre+'">'+value.campo_nombre+'</label>';
                html += '<input type="'+value.campo_tipo+'" value="" '+attr+' class="form-control" name="'+value.id_campo+'" id="id'+value.campo_nombre+'">';
                html += '<div class="valid-feedback">Ok!</div><div class="invalid-feedback">'+value.mensaje_validacion+'</div></div>';
            });
            $("#contenidoCampos").html(html);
        }      
	});
}
function cargarDatosRol(id){
    $.ajax({
        type: 'get',
        url: "/Usuarios/getUsuRoles", 
        data: "id="+id,
        success: function(result){

            roles = JSON.parse(result);
            rol = roles;
            console.log(rol);
            
            htmlRol = '<option value="">Seleccione</option>';
            for (var i = 0; i < rol.length; i++) {
                htmlRol += '<option value="'+roles[i].id+'">'+roles[i].rol_nombre+'</option>'
            }
            $("#selectRol").html(htmlRol);
        }      
    });
}
function addCamposDatosRol(campos,datos){
    camp = JSON.parse(campos);
    var html ="";
    $.each(camp, function( index, value ) {
        html += getCampoGenerals(value,0,0,'noTable');
    })
    data = JSON.parse(datos);
    $("#contenDataRol").html(html);
    $.each(data, function( index,value) {
        $('#Identificador'+value.id_campo).val(value.valor);
    })
}