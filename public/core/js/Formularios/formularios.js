function addCamposForm(){
	url = $("#formModel").data('url');
	idForm = $("#formModel").data('id');
	baseUrl = $("#baseUrl").val();
	if(url == ""){
		$("#formModel").attr('action',baseUrl+"/Form/save/"+idForm);
	}
	var campos = JSON.parse($("#camposForm").val());
	var html = '';
	$.each(campos, function( index, value ) {
		attr = getAttrCampo(value.funcion_validacion)
		datos = $("#dataForm").val();
		if(datos){
			data = JSON.parse(datos);
			$("#btnSubmitModel").val('Modificar');
			$("#coreTitlePage").html('Modificar');
			$("#formModel").attr('action',baseUrl+"/Form/update/"+idForm);
			$("#formModel").attr('data-accion','Modificar');
			$("#formModel").append("<input type='hidden' name='id' value='"+data[0]['id']+"'>");
			html += getCampoGenerals(value,datos,value.columna,0);
		}else{
			html += getCampoGenerals(value,datos = 0,col = 0,0);
		}
	});
	$("#contenCampos").html(html);
}

function getCampo(tipo,value,attr,datos = 0,col = 0,table){
	if(datos == 0){
		valor = "";
	}else{
		data = JSON.parse(datos);
		valor = data[0][col];
	}
	if(table == 'noTable'){
		identificador = 'Identificador'+value.id;
	}else{
		identificador = value.columna;
	}
	if(tipo == "text" || tipo == "hidden" || tipo == "number" || tipo == "date" || tipo == "file" ){
		html = '<input type="'+value.campo_tipo+'" value="'+valor+'" '+attr+' class="form-control" name="'+identificador+'" id="'+identificador+'">';
		return html;
	}
	if(tipo == "select"){
		html = '<select class="form-control" '+attr+' id="'+identificador+'" name="'+identificador+'">\
					<option value="">Seleccione</option>';
		
		if(value.data != ""){
			data = JSON.parse(value.data);
			$.each(data[0], function( index, val ) {
				var selected = "";
				if(val[0] == valor){
					selected = "selected";
				}
				html += '<option '+selected+' value="'+val[0]+'">'+val[1];+'</option>';
			});
			html += '</select>';
			return html;
		}else if(value.dataUrl!=""){
			html2 = '';
			$.ajax({
				type: 'get',
				url: value.dataUrl, 
				success: function(result){
					dataUrl = JSON.parse(result);
        			$.each(dataUrl, function( index, val ) {
						html2 += '<option  value="'+val.id+'">'+val.cat_nombre+'</option>';
					});
					$("#"+identificador).append(html2);
				}      
			});
		}
		return html;
	}	
}

function getCampoGenerals(value,datos = 0,col = 0,table = 0){
	attr = getAttrCampo(value.funcion_validacion);
	if(!attr.indexOf("required")){
		nombre = value.campo_nombre+"*";
	}else{
		nombre = value.campo_nombre;
	}
	html ='<div class="col-lg-6 col-md-6">\
	    		<label for="'+value.campo_nombre+'">'+nombre+'</label>';
			html += getCampo(value.campo_tipo,value,attr,datos,col,table);	        	
	        console.log(html);
	        html +=	'<div class="valid-feedback">Ok!</div>\
	        	<div class="invalid-feedback">\
	           		'+value.mensaje_validacion+'\
	        	</div>\
	   		</div>';
	   		
	return html;
}
function getAttrCampo(data){
		var json = JSON.parse(data);
		attr = '';
		$.each(json,function( index, value ) {
			$.each(value, function( dex, val ) {
				attr += String(dex)+"='"+val+"' ";
			});
		});
		return attr;
}