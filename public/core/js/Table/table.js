function pintarOpc(){
	var opc = JSON.parse($("#opcTable").val());
	var data = JSON.parse($("#dataTable").val());
	$.each(opc, function(i, item) {
		if(i.substr(0,4) == 'val-'){
			accion = i.substr(4,i.length);
		}else if(i.substr(0,4) == 'fun-'){
			accion = i.substr(4,i.length);
		}else{
			accion = i;
		}
		$("#tableHead").append("<th>"+accion+"</th>");
	});
	x=0;
	y = 0;
	$.each(data.id, function(i, item) {
		$.each(opc, function(i2, item2) {
			if(item2.indexOf("?") > 0){
				separador = "&";
			}else{
				separador = "?";
			}
			if(i2.substr(0,4) == 'val-'){
				evento = 1;
				accion = i2.substr(4,i2.length);
				data = null;
			}else if(i2.substr(0,4) == 'fun-'){
				evento = 2;
				accion = i2.substr(4,i2.length)
				data = item2;
			}else if(i2.substr(0,4) == 'hml-'){
				evento = 3;
				accion = i2.substr(4,i2.length)
				data = item2;
			}else{
				evento = 0;
				accion = i2;
				data = null;
			}
			$("#line"+x).append("<td><a data-datainfo='"+item+"' data-evento='"+accion+"' onclick='eventoMsg("+'"'+accion.replace(/ /g, "")+'"'+","+evento+","+y+","+'"'+data+'"'+","+item+")' id='opc"+accion.replace(/ /g, "")+y+"'  class='btn btn-default' data-url='/"+item2+separador+"id="+item+"'>"+accion+"</a></td>");
			y++;
		});
		x++;
	});
	$(".itemid").addClass("hidden");
	startDataTable();
}
