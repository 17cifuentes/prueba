function getChartDataDash(num){
    for (var i = 0;i < num; i++) {
        var nombre = $('#tituloGrafico'+i).val();
        var tipo = $('#tipoGrafico'+i).val();
        var contenedor = $('#contenedorGrafico'+i).val();
        var valorGrafico = $('#valorGrafico'+i).val();
        var dataGrafico = $('#dataGrafico'+i).val();    
        data = JSON.parse(dataGrafico);
        valor = JSON.parse(valorGrafico);
        getChart(nombre, tipo, contenedor, valor, data);
    }
}
function getChartData(){
    var nombre = $('#tituloGrafico').val();
    var tipo = $('#tipoGrafico').val();
    var contenedor = $('#contenedorGrafico').val();
    var valorGrafico = $('#valorGrafico').val();
    var dataGrafico = $('#dataGrafico').val();
    
    data = JSON.parse(dataGrafico);
    valor = JSON.parse(valorGrafico);
    getChart(nombre, tipo, contenedor, valor, data);
}
/************ FUNCIONES DE GRAFICOS **********/
function getChart(nombre, tipo, contenedor, valor, data) {
    if (tipo == 'radar') {
        getGraficoRadar(nombre, data, valor, contenedor);
    }
    if (tipo == 'spline' || tipo == 'column' || tipo == 'pie' || tipo == 'area' || tipo == 'scatter') {
        datos = arrayToChartGeneral(data, valor);
        getChartBasic(nombre, tipo, datos, contenedor);
    }

}

function arrayToChartGeneral(data, valor) {
    var obj = {};
    var arr = [];
    $.each(data, function (index, value) {
        obj['name'] = value;
        obj['y'] = valor[index];
        obj['drilldown'] = value;
        arr.push(obj);
        obj = {};
    });
    return arr;
}

/**********GRAFICOS********************/
function getChartBasic(nombre, tipo, datos, contenedor) {
// Create the chart
    Highcharts.chart(contenedor, {

        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            panning: true,
            panKey: 'shift',
            zoomType: 'xy',
            type: tipo
        },
        title: {
            text: nombre
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Escala'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            },
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },

        series: [
            {
                name: nombre,
                colorByPoint: true,
                data: datos
            }
        ],
        drilldown: {
            series: []
        }
    });
}

function getGraficoRadar(nombre, data, valor, contenedor) {
    Highcharts.chart('container', {

        chart: {
            polar: true,
            type: 'area'
        },

        title: {
            text: nombre,
            x: -80
        },

        pane: {
            size: '120%'
        },

        xAxis: {
            categories: controlData,
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle'
        },

        series: [{
            name: 'Calificación actual',
            data: valorData,
            pointPlacement: 'on'
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom'
                    },
                    pane: {
                        size: '70%'
                    }
                }
            }]
        }

    });
}

$(document).on("change", "input[type=radio][name=typeGraphic]", function (event) {
    var value = $(this).val();
    $(".datosGrafico").data('charttipo', value);
});