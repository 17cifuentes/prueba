function eventoMsg(id,evento,y,data,identificador){
	item = $("#opc"+id+y);
	if(evento == 1){
		alerta(item.data('url'),item.data('evento'));	
	}else if(evento == 2){
		window[data](identificador);
	}else{
		console.log(item);
		window.location.href = item.data('url');
	}
}
function alertMsg(accion,descripcion,type){
	Swal.fire(
  		accion,
  		descripcion,
  		type
	)
}
function alerta(url = '',accion,form = '',fun = ''){
	const swalWithBootstrapButtons = Swal.mixin({
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		buttonsStyling: false,
	})
	swalWithBootstrapButtons.fire({
		title: '¿Seguro desea '+accion+'?',
		text: "Notificación!",
		type: 'warning',
		width: '35%',
		showCancelButton: true,
		confirmButtonText: 'Si,'+accion+'!',
		cancelButtonText: 'No,'+accion+'!',
		reverseButtons: true
	}).then((result) => {
	if (result.value) {
		swalWithBootstrapButtons.fire(
		    'Notificación!',
		    'Proceso '+accion+' iniciado.',
		    'success'
		)
		
		$("#ruta").val(url);
		if(form == '' && url != ''){
			setTimeout("reFresh()", 1000);
		}else if(form != '' && url == ''){
			$("#"+form).submit();
		}else if(form == '' && url == '' && fun != ''){
			fun();
		}
	}else if (result.dismiss === Swal.DismissReason.cancel){
		swalWithBootstrapButtons.fire(
		    'Notificación',
		    'Proceso '+accion+' cancelado:)',
		    'error'
		)
	}
	})
	$('.swal2-cancel ').css('margin-right','1em')
}
function reFresh(url){
	$(location).attr('href',$("#ruta").val());
}