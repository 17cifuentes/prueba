function showNotification(text,type,shortUrl = null) 
{
  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
    if(type == 'success'){
    	toastr.success(text,'Notificación')	
    } 
    if(type == 'error'){
    	toastr.error(text,'Notificación')		
    }
    $(".toast-title").css("font-size","15px");
    $(".toast-message").css("font-size","15px");

    var pathname = window.location.search;
    var hash = window.location.hash;
    if(shortUrl != null){
      var res = pathname.substring(7,pathname.length);
      var url = "?"+res;
      history.pushState(null,"",url);  
    }
    
}
