function putChecksRoles(){
    checks = JSON.parse($("#visualizar").val());
    $.each(checks, function(index, valor) {
        $('#check'+valor.id).attr("checked", "checked");
    })  
}
function cargarRolModulo(id){
	$.ajax({
		type: 'get',
		url: "/Permisos/getModuloRol", 
		data: "id="+id,
		success: function(result){

			data = JSON.parse(result);
			modulos = data.modulos;
			roles = data.roles;
            
			console.log(roles);
        	htmlModulo = '<option value="">Seleccione</option>';
        	htmlRol = '<option value="">Seleccione</option>';
        	for (var i = 0; i < modulos.length; i++) {
                
        		htmlModulo += '<option value="'+modulos[i].id_modulo+'">'+modulos[i].mod_nombre+'</option>'
        	
        	}
        	for (var i = 0; i < roles.length; i++) {
        		htmlRol += '<option value="'+roles[i].id+'">'+roles[i].rol_nombre+'</option>'
        	}
        	$("#selectModulo").html(htmlModulo);
        	$("#selectRole").html(htmlRol);
    	}
	});
}

function cargarRolModuloFun(id){
    $.ajax({
        type: 'get',
        url: "/Permisos/getModuloFun", 
        data: "id="+id,
        success: function(result){
            data = JSON.parse(result);
            fun = data.funciones;
            htmlfun = '<option value="">Seleccione</option>';
            console.log(fun);

            for (var i = 0; i < fun.length; i++) {
                htmlfun += '<option value="'+fun[i].id+'">'+fun[i].fun_menu_nombre+'</option>'
            
            }
            $("#selectFunciones").html(htmlfun);
            
        }
    });
}