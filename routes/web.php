<?php
// MODULO PUNTO
Route::prefix('Punto')->group(function(){
    Route::get('crearPunto',PATH_PUNTO.'@crearPunto');
    Route::post('createProccess',PATH_PUNTO.'@createProccess');
    Route::get('lstPuntos',PATH_PUNTO.'@lstPuntos');
    Route::get('editarPunto',PATH_PUNTO.'@editarPunto');
    Route::post('editarProccess',PATH_PUNTO.'@editarProccess');
    Route::get('eliminarPunto',PATH_PUNTO.'@eliminarPunto');
    Route::get('cambiarEstado',PATH_PUNTO.'@cambiarEstado');
    Route::get('punto',PATH_PUNTO.'@punto');
    Route::get('producto',PATH_PUNTO.'@producto');


});
//MODULO PMI
Route::prefix('PMI')->group(function(){

    Route::get('crearProyecto',PATH_PMI.'@crearProyecto');
    Route::post('createProccess',PATH_PMI.'@createProccess');
    Route::get('listarProyecto',PATH_PMI.'@listarProyecto');
    Route::get('editarProyecto',PATH_PMI.'@editarProyecto');
    Route::post('editProccess',PATH_PMI.'@editProccess');
    Route::get('eliminarProyecto',PATH_PMI.'@eliminarProyecto');
    Route::get('listTareasProyecto',PATH_PMI.'@listTareasProyecto');
    Route::get('asignarTareasProyecto',PATH_PMI.'@asignarTareasProyecto');
    Route::get('asignarTareasProyectoProcess',PATH_PMI.'@asignarTareasProyectoProcess');
    Route::get('editarTareaProyecto',PATH_PMI.'@editarTareaProyecto');
    Route::post('editarTareaProyectoProccess',PATH_PMI.'@editarTareaProyectoProccess');
    Route::get('eliminarTareaProyecto',PATH_PMI.'@eliminarTareaProyecto');
    Route::get('misProyectos',      PATH_PMI.'@misProyectos');
    Route::get('listTareasMisProyectos',      PATH_PMI.'@listTareasMisProyectos');
    Route::get('desasignarTareaMisProyectos',PATH_PMI.'@desasignarTareaMisProyectos');
    Route::post('desasignarTareaProyectoProccess',PATH_PMI.'@desasignarTareaProyectoProccess');
    Route::get('finalizarTarea',PATH_PMI.'@finalizarTarea');
    Route::get('finalizarTareaProccess',PATH_PMI.'@finalizarTareaProccess');
    Route::get('historial',PATH_PMI.'@historial');
    
});
//MODULOS RESERVAS
 Route::prefix('reservas')->group(function () {
    Route::get('calendario',PATH_CTRL_RESERVAS.'@calendario');
    Route::get('eliminarEvento',PATH_CTRL_RESERVAS.'@eliminarEvento');
    Route::post('crearEventoProccess',PATH_CTRL_RESERVAS.'@crearEventoProccess');
    Route::get('listarEventos',PATH_CTRL_RESERVAS.'@listarEventos');
    Route::get('getEvent',PATH_CTRL_RESERVAS.'@getEvent');
});
//RUTAS MODULO RUTAS
 Route::prefix('Rutas')->group(function () {
    Route::get('crearRutas',PATH_CTRL_RUTAS.'@crearRutas');
    Route::get('crearRutasProcess',PATH_CTRL_RUTAS.'@crearRutasProcess');
    Route::get('listarRutas',PATH_CTRL_RUTAS.'@listarRutas');
    Route::get('cambiarEstadoRutas',PATH_CTRL_RUTAS.'@cambiarEstadoRutas');
    Route::get('editarRutas',PATH_CTRL_RUTAS.'@editarRutas');
    Route::get('editarRutasProcess',PATH_CTRL_RUTAS.'@editarRutasProcess');
    Route::get('eliminarRutas',PATH_CTRL_RUTAS.'@eliminarRutas');
    Route::get('crearPunto',PATH_CTRL_PUNTOS.'@crearPunto');
    Route::get('crearPuntoProcess',PATH_CTRL_PUNTOS.'@crearPuntoProcess');
    Route::get('listarPunto',PATH_CTRL_PUNTOS.'@listarPunto');
    Route::get('cambiarEstadoPuntos',PATH_CTRL_PUNTOS.'@cambiarEstadoPuntos');
    Route::get('editarPunto',PATH_CTRL_PUNTOS.'@editarPunto');
    Route::get('editarPuntoProcess',PATH_CTRL_PUNTOS.'@editarPuntoProcess');
    Route::get('eliminarPuntos',PATH_CTRL_PUNTOS.'@eliminarPuntos');
});
 Route::prefix('Cursos')->group(function(){
    Route::get('crearCurso',PATH_CURSOS.'@crearCurso');
    Route::post('crearCursoProccess',PATH_CURSOS.'@crearCursoProccess');
    Route::get('listarCursos',PATH_CURSOS.'@listarCursos');
    Route::get('categoriasCursoId',PATH_CURSOS.'@categoriasCursoId');
    ///
    Route::get('crearTemas',PATH_TEMAS.'@crearTemas');
    Route::post('crearTemaProccess',PATH_TEMAS.'@crearTemaProccess');
    Route::get('ListarTemas',PATH_TEMAS.'@ListarTemas');
    Route::get('editarTema',PATH_TEMAS.'@editarTema');
    Route::post('editarTemaProccess',PATH_TEMAS.'@editarTemaProccess');
    Route::get('eliminarTema',PATH_TEMAS.'@eliminarTema');
    Route::get('cambiarEstadoTema',PATH_TEMAS.'@cambiarEstadoTema');
});
 
//RUTAS CLIENTE MAPXI
Route::prefix('Mapxi')->group(function () {
	//RUTAS MAPXI USUARIOS
    Route::get('/Usuario/lstUsuario','Custom\Mapxi\Usuarios\MapxiUsuariosController@lstUsuario');
    //FIN RUTAS MAPXI USUARIO
});
//RUTAS MODULO PRODUCTOS
Route::prefix('Productos')->group(function () {
    Route::get('datosProductos',PATH_CTRL_PRODUCTOS.'@datosProductos');
    Route::get('identificarHijos',PATH_CTRL_PRODUCTOS.'@identificarHijos');
    Route::get('borrarCategoria',PATH_CTRL_PRODUCTOS.'@borrarCategoria');
    Route::get('create',PATH_CTRL_PRODUCTOS.'@create');
    Route::get('caracteristicas',PATH_CTRL_PRODUCTOS.'@listCaracterisitcas');
    Route::post('createProccess',PATH_CTRL_PRODUCTOS.'@createProccess');
    Route::get('bloquesProductos',PATH_CTRL_PRODUCTOS.'@bloquesProductos');
    Route::get('lstCategorias',PATH_CTRL_PRODUCTOS.'@lstCategorias');
    Route::get('listarPadres',PATH_CTRL_PRODUCTOS.'@listarPadres');
    Route::get('identificarPadre',PATH_CTRL_PRODUCTOS.'@identificarPadre');
    Route::get('lst',PATH_CTRL_PRODUCTOS.'@lstProductos');
    Route::get('createCategoria',PATH_CTRL_PRODUCTOS.'@createCategoria');
    // Pruebas Para EDITAR forma Normal 
    //Autor Javier
    Route::get('editarProductos',PATH_CTRL_PRODUCTOS.'@editarProductos');
    Route::get('estadoProducto',PATH_CTRL_PRODUCTOS.'@estadoProducto');
    Route::post('editarProccess',PATH_CTRL_PRODUCTOS.'@editarProccess');
    Route::get('eliminarProducto',PATH_CTRL_PRODUCTOS.'@eliminarProducto');
    Route::get('eliminarCaracteristica',PATH_CTRL_PRODUCTOS.'@eliminarCaracteristica');
    Route::get('crearCategoria',PATH_CTRL_PRODUCTOS.'@crearCategoria');
    Route::post('crearCatProccess',PATH_CTRL_PRODUCTOS.'@crearCatProccess');
    Route::get('estadosCancelarProducto',PATH_CTRL_PRODUCTOS.'@estadosCancelarProducto');
    Route::get('organizarCategorias',PATH_CTRL_PRODUCTOS.'@organizarCategorias');
    Route::post('editarOrdenCategoriaProcess',PATH_CTRL_PRODUCTOS.'@editarOrdenCategoriaProcess');

    Route::get('getIconos',PATH_CTRL_PRODUCTOS.'@getIconos');
});/***********************************************************************/
//FIN RUTAS CLIENTE MAPXI
/***********************************************************************/
Route::prefix('chat')->group(function () {
    Route::get('sendMsg',PATH_CHAT.'\ChatController@sendMsg');  
    Route::get('panelChat',PATH_CHAT.'\ChatController@panelChat');  
    Route::get('searchChat',PATH_CHAT.'\ChatController@searchChat');  
});
Route::prefix('Proveedores')->group(function () {
    Route::get('crearProveedor',PATH_CTRL_PROVEEDORES.'@crearProveedor');
    Route::post('crearProveedorProcess',PATH_CTRL_PROVEEDORES.'@crearProveedorProcess');
    Route::get('listarProveedores',PATH_CTRL_PROVEEDORES.'@listarProveedores');
    Route::get('cambiarEstadoProveedor',PATH_CTRL_PROVEEDORES.'@cambiarEstadoProveedor');
    Route::get('editarProveedor',PATH_CTRL_PROVEEDORES.'@editarProveedor');
    Route::post('editarProveedorProcess',PATH_CTRL_PROVEEDORES.'@editarProveedorProcess');
    Route::get('eliminarProveedores',PATH_CTRL_PROVEEDORES.'@eliminarProveedores');
});
/***********************************************************************/
Route::prefix('Inventario')->group(function () {
    Route::get('crearInventario',PATH_CTRL_INVENTARIO.'@crearInventario');
    Route::get('crearProcess',PATH_CTRL_INVENTARIO.'@crearProcess');
    Route::get('listarInventario',PATH_CTRL_INVENTARIO.'@listarInventario');
    Route::get('abastecerCant',PATH_CTRL_INVENTARIO.'@abastecerCant');
    Route::get('abastecerCantProccess',PATH_CTRL_INVENTARIO.'@abastecerCantProccess');
    Route::get('editarAbastecerProcess',PATH_CTRL_INVENTARIO.'@editarAbastecerProcess');
    Route::get('editUserProcess',PATH_CTRL_INVENTARIO.'@editUserProcess');
    Route::get('darDeBajaCantProcess',PATH_CTRL_INVENTARIO.'@darDeBajaCantProcess');
    Route::get('darDeBajaCant',PATH_CTRL_INVENTARIO.'@darDeBajaCant');
    Route::get('saveHistoricoInventario',PATH_CTRL_INVENTARIO.'@saveHistoricoInventario');
    Route::get('getInventario',PATH_CTRL_INVENTARIO.'@getInventario');
    Route::get('insertlistarHistorial',PATH_CTRL_INVENTARIO.'@insertlistarHistorial');
    Route::get('listarHistorial',PATH_CTRL_INVENTARIO.'@listarHistorial');
});

/***********************************************************************/
//RUTAS MODULO SHOPCART

//FIN RUTAS CLIENTE MAPXI
/***********************************************************************/

Route::prefix('carritoCompras')->group(function () {
    Route::get('carrito',PATH_CTRL_SHOPCART.'@carrito');
    Route::get('descripcionProductos',PATH_CTRL_SHOPCART.'@descripcionProductos');
    Route::get('contacto',PATH_CTRL_SHOPCART.'@contacto');
    Route::get('crearApartados',PATH_CTRL_SHOPCART.'@crearApartados');
    Route::post('crearApartadosProcess',PATH_CTRL_SHOPCART.'@crearApartadosProcess');
    Route::get('listarApartados',PATH_CTRL_SHOPCART.'@listarApartados');
    Route::get('editarApartados',PATH_CTRL_SHOPCART.'@editarApartados');
    Route::post('editarApartadosProcess',PATH_CTRL_SHOPCART.'@editarApartadosProcess');
    Route::get('eliminarApartados',PATH_CTRL_SHOPCART.'@eliminarApartados');
    Route::get('addProductToCart',PATH_CTRL_SHOPCART.'@addProductToCart');
    Route::get('removeProToCart',PATH_CTRL_SHOPCART.'@removeProToCart');
    Route::get('emptyCart',PATH_CTRL_SHOPCART.'@emptyCart');
    Route::get('newCantProCart',PATH_CTRL_SHOPCART.'@newCantProCart');
    Route::get('tienda',PATH_CTRL_SHOPCART.'@tienda');
    Route::get('configPagos',PATH_CTRL_SHOPCART.'@ConfigPagos');
    Route::post('newConfigPagoProccess',PATH_CTRL_SHOPCART.'@newConfigPagoProccess');
    Route::post('configPagoProccess',PATH_CTRL_SHOPCART.'@configPagoProccess');
    ///this is temporally whil I make the global function
    Route::get('buscadorCategorias',PATH_CTRL_SHOPCART.'@buscadorCategorias');
    Route::get('buscarItemNavBar',PATH_CTRL_SHOPCART.'@buscarItemNavBar');   

    //Modulos ShopCart Menú Submenú
    Route::get('gstShopCartMenu',PATH_CTRL_SHOPCART.'@gstShopCartMenu');
    Route::get('crearMenuTienda',PATH_CTRL_SHOPCART.'@crearMenuTienda');
    Route::post('crearMenuProcess',PATH_CTRL_SHOPCART.'@crearMenuProcess');
    Route::get('listarMenu',PATH_CTRL_SHOPCART.'@listarMenu');
    Route::get('editarMenuTienda',PATH_CTRL_SHOPCART.'@editarMenuTienda');
    Route::post('editarMenuProcess',PATH_CTRL_SHOPCART.'@editarMenuProcess');
    Route::get('cambiarEstadoMenu',PATH_CTRL_SHOPCART.'@cambiarEstadoMenu');
    Route::get('eliminarMenuid',PATH_CTRL_SHOPCART.'@eliminarMenuid');
    
    //SubMenu
    Route::get('gestionSubMenu',PATH_CTRL_SHOPCART.'@gestionSubMenu');
    Route::get('crearSubMenu',PATH_CTRL_SHOPCART.'@crearSubMenu');
    Route::get('consultaMenu',PATH_CTRL_SHOPCART.'@consultaMenu');
    Route::post('crearSubMenuProcess',PATH_CTRL_SHOPCART.'@crearSubMenuProcess');
    Route::get('listarSubMenu',PATH_CTRL_SHOPCART.'@listarSubMenu');
    Route::get('cambiarEstadoSubMenu',PATH_CTRL_SHOPCART.'@cambiarEstadoSubMenu');
    Route::get('editarsubMenuTienda',PATH_CTRL_SHOPCART.'@editarsubMenuTienda');
    Route::post('editarSubMenuProcess',PATH_CTRL_SHOPCART.'@editarSubMenuProcess');
    Route::get('eliminarSubMenuid',PATH_CTRL_SHOPCART.'@eliminarSubMenuid');

    //Domicilios
    Route::get('detalleProducto',PATH_CTRL_SHOPCART.'@detalleProducto');
    Route::get('gestionDomicilios',PATH_CTRL_SHOPCART.'@gestionDomicilios');
    Route::get('listarDomicilios',PATH_CTRL_SHOPCART.'@listarDomicilios');
    Route::get('domicilioDespachado',PATH_CTRL_SHOPCART.'@domicilioDespachado');
    Route::get('domicilioEntregado',PATH_CTRL_SHOPCART.'@domicilioEntregado');
    Route::get('domicilioCancelado',PATH_CTRL_SHOPCART.'@domicilioCancelado');
    Route::get('mostrarProductoDetalle',PATH_CTRL_SHOPCART.'@mostrarProductoDetalle');
    Route::get('listProdDomicilio',PATH_CTRL_SHOPCART.'@listProdDomicilio');
    Route::get('domiciliomotivosCancell',PATH_CTRL_SHOPCART.'@domiciliomotivosCancell');
    Route::get('cancelarProductoDomicilio',PATH_CTRL_SHOPCART.'@cancelarProductoDomicilio');
    Route::get('consultarProductoAjax',PATH_CTRL_SHOPCART.'@consultarProductoAjax');
    Route::get('productsCliente',PATH_CTRL_SHOPCART.'@productsCliente');
    Route::get('agregarNuevoProductoDomicilio',PATH_CTRL_SHOPCART.'@agregarNuevoProductoDomicilio');
    Route::get('cancelarDomicilio',PATH_CTRL_SHOPCART.'@cancelarDomicilio');
    Route::get('consultUserForOrder',PATH_CTRL_SHOPCART.'@consultUserForOrder');
    //ResponsePagos
    Route::get('registerProductsDatabases/{tipoDomicilio}',PATH_CTRL_SHOPCART.'@registerProductsDatabases');
    Route::get('responseUrl/{codRef}',PATH_CTRL_SHOPCART.'@responseUrl');

    //DomiciliosInteractivos
    Route::get('crearEstadoDomicilioCliente',PATH_CTRL_SHOPCART.'@crearEstadoDomicilioCliente');
    Route::post('crearEstadoDomicilioClienteProcess',PATH_CTRL_SHOPCART.'@crearEstadoDomicilioClienteProcess');
    Route::get('listarEstadosCliente',PATH_CTRL_SHOPCART.'@listarEstadosCliente');
    Route::get('editarEstadoDomicilioCliente',PATH_CTRL_SHOPCART.'@editarEstadoDomicilioCliente');
    Route::post('editarEstadoDomicilioClienteProcess',PATH_CTRL_SHOPCART.'@editarEstadoDomicilioClienteProcess');
    Route::get('ordenarEstadosDomicilio',PATH_CTRL_SHOPCART.'@ordenarEstadosDomicilio');
    Route::post('editarOrdenEstadosDomicilioProcess',PATH_CTRL_SHOPCART.'@editarOrdenEstadosDomicilioProcess');
    Route::get('eliminarEstadoDomicilioCliente',PATH_CTRL_SHOPCART.'@eliminarEstadoDomicilioCliente');
    Route::post('compararOrdenDomicilio',PATH_CTRL_SHOPCART.'@compararOrdenDomicilio');
    //validar CARRTIO
    Route::get('validarProductos',PATH_CTRL_SHOPCART.'@validarProductos');
    Route::get('cambiarEstadoDomicilio',PATH_CTRL_SHOPCART.'@cambiarEstadoDomicilio');
    Route::get('politicaPrivacidad',PATH_CTRL_SHOPCART.'@politicaPrivacidad');
    
});

/*Route::fallback(function(){
    return redirect("/ingresar/7/8");
})->name('fallback');*/

Route::prefix('Inventario')->group(function () {
    Route::get('crearInventario',PATH_CTRL_INVENTARIO.'@crearInventario');
    Route::get('crearProcess',PATH_CTRL_INVENTARIO.'@crearProcess');
    Route::get('listarInventario',PATH_CTRL_INVENTARIO.'@listarInventario');
    Route::get('abastecerCant',PATH_CTRL_INVENTARIO.'@abastecerCant');
    Route::get('abastecerCantProccess',PATH_CTRL_INVENTARIO.'@abastecerCantProccess');
    Route::get('editarAbastecerProcess',PATH_CTRL_INVENTARIO.'@editarAbastecerProcess');
    Route::get('editUserProcess',PATH_CTRL_INVENTARIO.'@editUserProcess');
    Route::get('darDeBajaCantProcess',PATH_CTRL_INVENTARIO.'@darDeBajaCantProcess');
    Route::get('darDeBajaCant',PATH_CTRL_INVENTARIO.'@darDeBajaCant');
    Route::get('saveHistoricoInventario',PATH_CTRL_INVENTARIO.'@saveHistoricoInventario');
    Route::get('getInventario',PATH_CTRL_INVENTARIO.'@getInventario');
    Route::get('insertlistarHistorial',PATH_CTRL_INVENTARIO.'@insertlistarHistorial');
    Route::get('listarHistorial',PATH_CTRL_INVENTARIO.'@listarHistorial');
});
/***********************************************************************/




Route::prefix('Promociones')->group(function () {
    Route::get('crearPromociones',PATH_CTRL_PROMOCIONES.'@crearPromociones');
    Route::get('crearPromoProcess',PATH_CTRL_PROMOCIONES.'@crearPromoProcess');
    Route::get('listarPromociones',PATH_CTRL_PROMOCIONES.'@listarPromociones');
    Route::get('eliminarPromocion',PATH_CTRL_PROMOCIONES.'@eliminarPromocion');
    Route::get('createPaquete',PATH_CTRL_PROMOCIONES.'@createPaquete');
    Route::post('crearPaqueteProcess',PATH_CTRL_PROMOCIONES.'@crearPaqueteProcess');
    Route::get('listPaquetes',PATH_CTRL_PROMOCIONES.'@listPaquetes');
    Route::get('eliminarPaquete',PATH_CTRL_PROMOCIONES.'@eliminarPaquete');
    Route::get('calculatePromocion',PATH_CTRL_PROMOCIONES.'@calculatePromocion');
    
    
    
});
Route::prefix('Favoritos')->group(function () {
    Route::get('lstFavoritos',PATH_FAVORITOS.'\favoritoController@lstFavoritos');
    Route::get('addToFavoritos',PATH_FAVORITOS.'\favoritoController@addToFavoritos');
    Route::get('lstMisFavoritos',PATH_FAVORITOS.'\favoritoController@lstMisFavoritos');
});



/***********************************************************************/

/*
    DESDE AQUI COMIENZAN LAS RUTAS DEL CORE NO MODIFICAR NADA.
*/
/***********************************************************************/

//RUTAS CORE
//Funciones hecho JR Pruebas
Route::prefix('chat')->group(function () {
    Route::get('sendMsg',PATH_CHAT.'\ChatController@sendMsg');  
    Route::get('panelChat',PATH_CHAT.'\ChatController@panelChat');  
    Route::get('getMsgChat',PATH_CHAT.'\ChatController@getMsgChat');  
});
Route::prefix('Form')->group(function () {
    Route::get('{id}','Formularios\FormularioController@generateForm');
    Route::get('save/{id}','Formularios\FormularioController@FormSave');
    Route::post('save/{id}','Formularios\FormularioController@FormSave');
    Route::get('update/{id}','Formularios\FormularioController@FormUpdate');
    Route::post('update/{id}','Formularios\FormularioController@FormUpdate');
    Route::get('editar/{columna}/{entidad}/{formId}','Formularios\FormularioController@generateformEdit');
});
Route::prefix('Table')->group(function () {
    Route::get('{id}','Table\TableController@generateTable');
});
Route::prefix('Gen')->group(function () {
    Route::get('cambiarEstado','General\GeneralController@cambiarEstado');
    Route::get('getMunicipioById','General\GeneralController@getMunicipioById');
});
Route::prefix('Reportes')->group(function () {
    Route::get('crearReporte','Reportes\ReportesController@crearReporte');
    Route::post('generarReporte','Reportes\ReportesController@generarReporte');
    Route::get('getReporteMod','Reportes\ReportesController@getReporteMod');
});
//Funciones hecho JR Pruebas
Route::prefix('Funciones')->group(function () {
    Route::get('listarFunciones','Funciones\FuncionesController@listarFunciones');
    Route::get('crearFunciones','Funciones\FuncionesController@crearFunciones');
    Route::post('crearFuncion','Funciones\FuncionesController@crearFuncion');
    Route::get('cambiarEstadoFuncion','Funciones\FuncionesController@cambiarEstadoFuncion');
    Route::get('editarFunciones','Funciones\FuncionesController@editarFunciones');
    Route::post('editarFuncionProcess','Funciones\FuncionesController@editarFuncionProcess');
    Route::get('eliminarFuncion','Funciones\FuncionesController@eliminarFuncion');  
});
//Fin hecho JR Pruebas
Route::prefix('Modulos')->group(function () {
    Route::get('/'.PATH_MODULO_LST,'Modulos\ModulosController@lstModulos');
    Route::get('createModulo','Modulos\ModulosController@createModulo');
    Route::get('editarModulo','Modulos\ModulosController@editarModulo');  
    Route::post('createModuloProcess','Modulos\ModulosController@createModuloProcess');
    Route::post('editarModuloProcess','Modulos\ModulosController@editarModuloProcess');
    Route::get('cambiarEstado','Modulos\ModulosController@cambiarEstado');
});

Route::prefix('Permisos')->group(function () {
    Route::get('createPermisosMod','Permisos\PermisosController@createPermisosMod'); 
    Route::get('lstPermisosModulos','Permisos\PermisosController@lstPermisosModulos');
    Route::get('getModuloRol','Permisos\PermisosController@getModuloRol'); 
    Route::post('createPermisosModProcess','Permisos\PermisosController@createPermisosModProcess');
    Route::get('verPermisosModulos','Permisos\PermisosController@verPermisosModulos'); 
    Route::get('borrarPermisoModulo','Permisos\PermisosController@borrarPermisoModulo');
    Route::get('listPermisosFunciones','Permisos\PermisosController@listPermisosFunciones');
    Route::get('verPermisosFunciones','Permisos\PermisosController@verPermisosFunciones');
    Route::get('borrarPermisoFunciones','Permisos\PermisosController@borrarPermisoFunciones');
    Route::get('createPermisosFun','Permisos\PermisosController@createPermisosFun'); 
    Route::get('getModuloFun','Permisos\PermisosController@getModuloFun');   
    Route::post('createPermisosFunProcess','Permisos\PermisosController@createPermisosFunProcess');
    Route::get('createPModulos','Permisos\PermisosController@createPModulos');
    Route::post('createPModulosProcess','Permisos\PermisosController@createPModulosProcess');
    Route::get('checkPermisosModulos','Permisos\PermisosController@checkPermisosModulos');
    Route::get('listPermisosRoles','Permisos\PermisosController@listPermisosRoles');
    Route::get('verPermisosRoles','Permisos\PermisosController@verPermisosRoles');
    Route::get('listFuncionesRoles','Permisos\PermisosController@listFuncionesRoles');
    Route::get('VisualizarRoles','Permisos\PermisosController@VisualizarRoles');
    Route::post('updateFunRoles','Permisos\PermisosController@updateFunRoles');
    
});

Route::prefix('Clientes')->group(function () {
    Route::get('configuracion','Cliente\ClientesController@configuracionForm');
    Route::post('confiProcess','Cliente\ClientesController@confiProcess');
    Route::get('create','Cliente\ClientesController@create');
    Route::get('configCliente','Cliente\ClientesController@configCliente');
    Route::get('createProcess','Cliente\ClientesController@createProcess');
    Route::get('lstClientes','Cliente\ClientesController@lstClientes');
    //nuevas rutas
    Route::get('editarCliente','Cliente\ClientesController@editarCliente');
    Route::post('editarClienteProcess','Cliente\ClientesController@editarClienteProcess');
    Route::get('cambiarEstado','Cliente\ClientesController@cambiarEstadoCliente');
    Route::get('borrarCliente','Cliente\ClientesController@borrarCliente');
});

Route::prefix('Roles')->group(function () {
    Route::get('crearRol','Roles\RolesController@crearRol');
    Route::post('crearRolProcess','Roles\RolesController@crearRolProcess');
    Route::get('lstRoles','Roles\RolesController@lstRoles');
    Route::get('editarRoles','Roles\RolesController@editarRoles');
    Route::post('editarRolesProcess','Roles\RolesController@editarRolesProcess');
    Route::get('cambiarEstado','Roles\RolesController@cambiarEstado');
});

Route::prefix('Menu')->group(function () {
    Route::get('/{idModulo}','Menu\MenuController@menu');
});
Route::prefix('Usuarios')->group(function () {
    Route::get('crearUsuarioExterno',    PATH_ROUTE_USER.'@crearUsuarioExterno');
    Route::get('perfil',                 PATH_ROUTE_USER.'@perfil');
    Route::get('actulizaPerfil',        PATH_ROUTE_USER.'@actulizaPerfil');
    Route::get('ayuda',                  PATH_ROUTE_USER.'@ayuda');
    Route::get('lstUsuarios',            PATH_ROUTE_USER.'@lstUsuarios');
    Route::get('logOut',                 PATH_ROUTE_USER.'@logOut');
    Route::get('crearUsuario',           PATH_ROUTE_USER.'@crearUsuario');
    Route::get('createProccess',         PATH_ROUTE_USER.'@createProccess');
    Route::get('createProccessInt',      PATH_ROUTE_USER.'@createProccessInt');
    Route::get('estadisticasUsuarios',   PATH_ROUTE_USER.'@estadisticasUsuarios');
    Route::get('getCampos',              PATH_ROUTE_USER.'@getCampos');
    Route::get('getUsuRoles',            PATH_ROUTE_USER.'@getUsuRoles');
    /* Pruebas Para EDITAR forma Normal 
    Autor Javier
    */
    Route::get('recuperarContrasena',   PATH_ROUTE_USER.'@recuperarContrasena');
    Route::get('editarUsuarios',        PATH_ROUTE_USER.'@editarUsuarios');
    Route::get('eliminarUsuario',       PATH_ROUTE_USER.'@eliminarUsuario');
    Route::post('cambioPassword',       PATH_ROUTE_USER.'@cambioPassword');
    Route::get('data/{codigoInicial}',  PATH_ROUTE_USER.'@data');
    Route::post('actulizaUsuarioProcess',PATH_ROUTE_USER.'@actulizaUsuarioProcess');
    Route::post('recuperarContrasenaProccess',PATH_ROUTE_USER.'@recuperarContrasenaProccess');
    
    //fin Prueba
});
    Route::get('/ingresar/{idcliente}/{val}', 'Cliente\ClientesController@ingresar');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/',function(){
    return redirect('ingresar/7/0');

});
Route::get('/login',function(){
    return redirect('ingresar/1/0');
});

Route::prefix('Favoritos')->group(function () {
    Route::get('lstFavoritos',PATH_FAVORITOS.'\favoritoController@lstFavoritos');
    Route::get('addToFavoritos',PATH_FAVORITOS.'\favoritoController@addToFavoritos');
    Route::get('lstMisFavoritos',PATH_FAVORITOS.'\favoritoController@lstMisFavoritos');
});

Route::prefix('Crm')->group(function () {
    Route::get('createContacto',PATH_CRM.'\ContactoController@createContacto');
    Route::post('createContactProcess',PATH_CRM.'\ContactoController@createContactProcess');
    Route::get('listContactos',PATH_CRM.'\ContactoController@listContactos');
    Route::get('cambiarEstado',PATH_CRM.'\ContactoController@cambiarEstado');
    Route::get('borrarContacto',PATH_CRM.'\ContactoController@borrarContacto');
    Route::get('editarContacto',PATH_CRM.'\ContactoController@editarContacto');
    Route::post('editarContactoProcess',PATH_CRM.'\ContactoController@editarContactoProcess');
    Route::get('contactarContacto',PATH_CRM.'\ContactoController@contactarContacto');
    Route::post('contactarContactoProcess',PATH_CRM.'\ContactoController@contactarContactoProcess');
    Route::get('historialContacto',PATH_CRM.'\ContactoController@historialContacto');
    Route::get('parteHora',PATH_CRM.'\ContactoController@parteHora');
    Route::get('SumaHoras',PATH_CRM.'\ContactoController@SumaHoras');
    Route::get('estadisticas',PATH_CRM.'\ContactoController@estadisticas');
    

});

Route::prefix('Contabilidad')->group(function(){
    Route::get('createCuenta',PATH_CONTABILIDAD.'\ContabilidadController@createCuenta');
    Route::post('createCuentaProcess',PATH_CONTABILIDAD.'\ContabilidadController@createCuentaProcess');
    Route::get('listCuenta',PATH_CONTABILIDAD.'\ContabilidadController@listCuenta');
    Route::get('editCuenta',PATH_CONTABILIDAD.'\ContabilidadController@editCuenta');
    Route::post('editarCuentaProcess',PATH_CONTABILIDAD.'\ContabilidadController@editarCuentaProcess');
    Route::get('eliminarCuenta',PATH_CONTABILIDAD.'\ContabilidadController@eliminarCuenta');
    Route::get('listarNomina',PATH_CONTABILIDAD.'\ContabilidadController@listarNomina');
    Route::get('reservado',PATH_CONTABILIDAD.'\ContabilidadController@reservado');
    Route::get('updateCuota',PATH_CONTABILIDAD.'\ContabilidadController@updateCuota');
    Route::post('pdf',PATH_CONTABILIDAD.'\ContabilidadController@pdf');
    Route::post('pdf_todos',PATH_CONTABILIDAD.'\ContabilidadController@pdf_todos');
    

});

Auth::routes();

//FIN RUTAS CORE
/***********************************************************************/