<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigPagos extends Model
{
    protected $table = "tn_sft_conf_pagos";
}
