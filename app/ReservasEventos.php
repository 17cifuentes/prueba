<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasEventos extends Model
{
    protected $table = 'tn_sft_reservas_events';
    public $fields = ['id','titulo','fecha_inicio','fecha_fin','hora_inicio','hora_fin','tipo_evento','cliente_id','id_producto_servicio'];

    public function sftCliente(){
    	return $this->hasOne("App\Cliente","id","cliente_id");
    }

    public function sftProductos(){
    	return $this->hasOne("App\Productos","id","id_producto_servicio");
    }
}
