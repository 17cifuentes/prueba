<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campos extends Model
{
    protected $table = 'tn_sft_campos';
    public $fields = ['id','campo_nombre','campo_tipo','funcion_validacion','mensaje_validacion','data','razon','dataUrl'];
}
