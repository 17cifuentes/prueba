<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivosCliente extends Model
{
	protected $table = 'sft_motivos_cliente';
}
