<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $table = 'tn_sft_categoria_productos';
     public $fields = ['id','cat_nombre','cat_tipo','cat_descripcion','cliente_id','img','padre','icono'];

    public function sftFile(){
    	return $this->hasOne("App\SftFiles","id","img");
    }
    public function sftIcono(){
    	return $this->hasOne("App\Iconos","id","icono");	
    }
    public function sftCategoriasCursos()
    {
    	return $this->hasMany("App\CategoriasCurso","id","id_categoria");	
    }

}
