<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportesOperaciones extends Model
{
    protected $table = 'reportes_operaciones';
}
