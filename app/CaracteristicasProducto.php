<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaracteristicasProducto extends Model
{
    protected $table = 'tn_sft_val_caracteristicas_producto';
    public $fields = ['id','id_pro','campo_id'];
    public function sftProducto(){
    	return $this->hasOne('App\Productos','id','id_pro');
    }
    public function sftCampo(){
    	return $this->hasOne('App\Campos','id','campo_id');
    }
}

