<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatContactos extends Model
{
    protected $table = 'tn_sft_mod_chat_contactos';

    public function user(){
    	return $this->hasOne('App\User','id','id_usu');
    }
    public function contacto(){
    	return $this->hasOne('App\User','id','contacto_id');
    }
}
