<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasCurso extends Model
{
     protected $table = 'tn_sft_categorias_curso';
     public $fields = ['id','id_curso','id_categoria','created_at','updated_at'];

	     public function sftCursos()
	     {
	        return $this->hasOne("App\Cursos","id","id_curso");
	     }
	     public function sftCategorias()
	     {
	     	return $this->hasOne("App\Categorias","id","id_categoria");
	     }
}
