<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SftFiles extends Model
{
    protected $table = 'tn_sft_files';
    public $fields = ['id','modulo','opcion','idRel','nombre','descripcion','tipoDoc','fExpedicion','size','estado','seguro','cliente_id','path'];
}
