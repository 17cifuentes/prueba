<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Gst\GstModulos;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstReportes;
use Auth;
use App\Http\Controllers\Cliente\ClientesController;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $gstModulos;
    private $gstCliente;
    private $data;
    private $usuarioController;
    private $gstReportes;
    public function __construct(GstReportes $gstReportes,GstModulos $gstModulos, GstCliente $gstCliente)
    {
        $this->middleware(MIN_AUTH);
        $this->gstModulos = $gstModulos;
        $this->gstCliente = $gstCliente;
        $this->gstReportes = $gstReportes;
        session_start();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    

    public static function index()
    {
        $cliente = GstCliente::getCliente(Auth::user()->cliente_id);
    
       if ($cliente[0]->estado_cliente == "Activo")
       {
            $_SESSION[DATA] = ClientesController::configuration();
            $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME]; 
            $_SESSION[DATA][CLIENTE_TITLE] = TITLE_INICIO_DASH;
            $reportes = GstReportes::getReportsDash();
            return view(PATH_DASH,[SFT_LIB=>[SFT_DATATABLE,SFT_GRAFICOS],SFT_LIB_MOD=>[SFT_LIB_MOD_REPORTES],'reportes'=>$reportes]);
        }else{
            $cliente = Auth::user()->cliente_id;
            Auth::logout();
            session_destroy();
            return redirect('/ingresar/'.$cliente.'/'.'1');
        }
        
        
    }
    
}
