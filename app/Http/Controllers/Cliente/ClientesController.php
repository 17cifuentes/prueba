<?php

namespace App\Http\Controllers\Cliente;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controllers\Controller;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\Formulario;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstModulos;
use App\Http\Gst\GstGeneral;
use App\Http\Clases\Table;
use App\FuncionesRoles;
use App\ClientesMod;
use App\Formularios;
use App\ModulosRol;
use App\Favoritos;
use App\SFTConf;
use App\Cliente;
use App\Listas;
use App\Roles;
use App\User;
use Auth;
use URL;



//use Illuminate\Http\Request;
//use Validator;


/*
    *Autor: Cristian Cifuentes
    *Modulo: Cliente
    *Versiòn: 1.0
    *Descripciòn: Controlador para clientes en el sistema.
    */
class ClientesController extends Controller
{
    private $gstCliente;
    private $gstGeneral;
    public function __construct(GstGeneral $gstGeneral,GstCliente $gstCliente) 
    {
       $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => ['ingresar']]);
       session_start();
       $this->gstCliente = $gstCliente;
       $this->gstGeneral = $gstGeneral;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:
    *Salida: Matriz con configuraciones del cliente.
    *Descripciòn: Funcion para controlar las configuraciones de cada cliente en el sistema, vizual *y funcional.
    */
    public static function configuration($id = null){
        $data[CLIENTE_TITLE] = INICIO;
        if($id == null){
          $id = Auth::user()->cliente_id;
        }
        if(isset(Auth::user()->rol_id)){
            $data[CLIENTE_MENU] = GstModulos::getModulosCliente(Auth::user()->rol_id);
        }
        
        $data[CLIENTE_CONF] = GstCliente::getConfCliente($id);
        switch ($data[CLIENTE_CONF][0]->estructura_admin) {
            case 2:
                $data['layouts'] = 'layouts.2.layoutAll2';
                break;
            case 4:
                $data['layouts'] = 'layouts.4.layoutAll4';
                break;
        }
        switch ($data[CLIENTE_CONF][0]->estructura_usuario) {
            case 5:
                $data[CLIENTE_CONF][0]['layoutsUsu'] = 'Modulos.ShopCart.layouts.luxyry';
                break;
            case 3:
                $data[CLIENTE_CONF][0]['layoutsUsu'] = 'Modulos.ShopCart.layouts.JuniorHome';
                break;
            case 1:
                $data[CLIENTE_CONF][0]['layoutsUsu'] = 'Modulos.ShopCart.layouts.1';
                break;
        }
        return $data;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:$idCliente = identificador del cliente a ingresar
    *Salida: Matriz de configuraciones del cliente.
    *Descripciòn: Funcion para controlar las configuraciones de cada cliente en el sistema, vizual *y funcional.
    */
    public static function ingresar($idCliente,$val = 0)
    {
        $_SESSION[DATA][CLIENTE_CONF] = GstCliente::getConfCliente($idCliente);
    	if(count($_SESSION[DATA][CLIENTE_CONF]) == 0)
    	{
    		$_SESSION[DATA][MSG] = CLIENTE_NO_EXISTE;
    		$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
    	}else{
            if($val == 1){
                $_SESSION[DATA][MSG] = MSG_CLIENTE_INACTIVO;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            }else if($val == 2 ){
                $_SESSION[DATA][MSG] = MSG_VAL_CHAR;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                
            }else if($val == 3 ){
                $_SESSION[DATA][MSG] = MSG_FORM_UNFOUND;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                
            }else if($val == 4 ){
                $_SESSION[DATA][MSG] = MSG_FORM_SIN_NUMERAR;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

            }else if($val == 5 ){
                $_SESSION[DATA][MSG] = MSG_FORM_UNFOUND;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            }
            else if($val == 6 ){
                $_SESSION[DATA][MSG] = MSG_USU_EXITOSO;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

            }else if($val == 7 ){
                $_SESSION[DATA][MSG] = MSG_UNLOG;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

            }else if($val == 8 ){
                $_SESSION[DATA][MSG] = MSG_NO_ANSWER;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            }else if($val == 9){
                $_SESSION[DATA][MSG] = MSG_TIENDA_SIN_CONFIGURAR;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            }
            else{
                $_SESSION[DATA][MSG] = CLIENTE_BIENVENIDO.$_SESSION[DATA][CLIENTE_CONF][0][TN_NOMBRE_CLIENTE];
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;    
            }

    		
    	}
    	$_SESSION[DATA][CLIENTE_TITLE] = TITLE_INGRESO; 
        
        return view(PATH_LOGIN,[ID=>$idCliente,SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);   
    }

    /**
     * Función para crear botones en el formulario de configuraciòn.
     * @category ClientesController o clientes.php 
     * @package Modulo Clientes
     * @version 1.0
     * @author Cristian Cifuentes ,Jasareth obando:opcion de logo.
     */
    public function formConfig(){
        $form = new Formulario;
        
        $field = $form->setFielset(COL_LG_12,CLIENTE_CONF_PLATAFORMA_FIELD1);
        $field = $form->setInput($field,"",INPUT_TEXT,COL_LG_4,CONF_TITULO_PESTANA,TN_CONF_TITULO_PESTANA,'','');
        $field = $form->setInput($field,"",INPUT_TEXT,COL_LG_4,CONF_NAVBAR_TITULO,TN_CONF_NAVBAR_TITULO,'','');
        $field = $form->setInput($field,"",INPUT_FILE,COL_LG_4,CONF_ICONO_PESTANA,TN_CONF_ICONO_PESTANA,'','');
        $field = $form->setInput($field,"",INPUT_COLOR,COL_LG_4,CONF_NAVBAR_COLOR,TN_CONF_NAVBAR_COLOR,'','');
        $field = $form->setInput($field,"",INPUT_COLOR,COL_LG_4,CONF_BREADCUMD_COLOR,TN_CONF_BREADCUMD_COLOR,'','');
        $field = $form->setInput($field,"",INPUT_FILE,COL_LG_4,CONF_LOGO,TN_CONF_LOGO,'','');
        $field = $form->setInput($field,"",INPUT_FILE,COL_LG_4,CONF_MENU_DERECHO,TN_CONF_MENU_DERECHO,'','');
        $field = $form->setInput($field,"",INPUT_FILE,COL_LG_4,CONF_IMG_LOGIN,TN_CONF_IMG_LOGIN,'','');
        if(Auth::user()->cliente_id == 1){
                $search = [
                [ID=>0,NOMBRE=>NO],
                [ID=>1,NOMBRE=>SI]
            ];
            $search[RELACION] = [ID,NOMBRE];
            $field= $form->setInput($field,"",INPUT_SELECT,COL_LG_4,CONF_BUSCADOR,TN_CONF_BUSCADOR,'',$search);
            $navbar_notification = [
                [ID=>0,NOMBRE=>NO],
                [ID=>1,NOMBRE=>SI]
            ];
            $navbar_notification[RELACION] = [ID,NOMBRE];
            $field= $form->setInput($field,"",INPUT_SELECT,COL_LG_4,CONF_NOTIFICACIONES,TN_CONF_NOTIFICACIONES,'',$navbar_notification);

            $slide_rigth = [
                [ID=>0,NOMBRE=>NO],
                [ID=>1,NOMBRE=>SI]
            ];
            $slide_rigth[RELACION] = [ID,NOMBRE];
            $field= $form->setInput($field,"",INPUT_SELECT,COL_LG_4,CONF_MENU_DERECHO_OPC,
                TN_CONF_MENU_DERECHO_OPC,'',$slide_rigth);
        }
        

        $form->addField($form,$field);
        return $form;
    }
    public function configuracionForm()
    {
        PermisosController::getPermisos();
        $_SESSION[DATA][CLIENTE_TITLE] = CLIENTE_CONF_PLATAFORMA;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,CLIENTE_CONF_PLATAFORMA=>URL::previous()]; 
        $form = $this->formConfig();
        $form->col = COL_LG_12;
        $form->action = CONF_URL_PROCESS;
        $form->method = POST;
        $form = $form->render($form);
        return view(PATH_FORM,compact(FORM));
    }

     /**
     * Función configProcess que registra los datos de la configuraciòn en el sistema.
     * @category ClientesController o clientes.php 
     * @package Modulo Clientes
     * @version 1.0
     * @author Cristian Cifuentes , Jasareth obando: registro del logo.
     */

    public function confiProcess(){
        //dd(TN_CONF_ICO);
        if(in_array(SELECCIONE,$_POST)){
            $_SESSION[DATA][MSG] = MSG_CAMPOS_INCORRECTOS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(URL_CONF_CLIENTE);
        }else{
            if(TN_CONF_ICO == 0 ){
            }
            if($_FILES[TN_CONF_ICO][NAME]){
               $_POST[TN_CONF_ICO] = FuncionesGenerales::cargararchivo($_FILES[TN_CONF_ICO],PATH_CONF_ICO,ICO_DEFAULT);
           }else{
                unset($_POST[CONF_ICO]);
           }
           if($_FILES[TN_CONF_ICO] = 0){

            }
           if($_FILES[TN_CONF_IMG_LOGIN][NAME]){
               $_POST[TN_CONF_IMG_LOGIN] = FuncionesGenerales::cargararchivo($_FILES[TN_CONF_IMG_LOGIN],PATH_CONF_ICO,ICO_DEFAULT);
           }else{
                unset($_POST[CONF_ICO]);
           }
           if($_FILES[TN_CONF_IMG_SLIDE][NAME]){
                $_POST[TN_CONF_IMG_SLIDE] = FuncionesGenerales::cargararchivo($_FILES[TN_CONF_IMG_SLIDE],PATH_CONF_SLIDE,IMG_SLIDE_DEFAULT);
           }else{
                unset($_POST[TN_CONF_IMG_SLIDE]);
           }
           if($_FILES[TN_CONF_LOGO][NAME]){
               $_POST[TN_CONF_LOGO] = FuncionesGenerales::cargararchivo($_FILES[TN_CONF_LOGO],PATH_CONF_ICO,ICO_DEFAULT);
            }else{
                unset($_POST[TN_CONF_LOGO]);
            }
            //dd($_FILES);
            FuncionesDB::Editar(TN_CONF,TN_USU_CLIENTE_ID,Auth::user()->cliente_id,$_POST);
            $_SESSION[DATA] = ClientesController::configuration();
            return redirect(URL_CONF_CLIENTE);
           
        }
    }

    
public function create(){
        $_SESSION[DATA][CLIENTE_TITLE] = CREAR_CLIENTES;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CLIENTES=>'/Menu/3',CREAR_CLIENTES=>""]; 
        $form = new Formulario;
        $field = $form->setFielset(COL_LG_12,FIELD1_CREAR_CLIENTES);
        $field = $form->setInput($field,"",INPUT_TEXT,COL_LG_4,CLIENTE_NOMBRE,TN_NOMBRE_CLIENTE);
        $field = $form->setInput($field,"",INPUT_TEXT,COL_LG_4,CLIENTE_DESCRIPCION,TN_CLIENTE_DESCRIPCION);
        $field = $form->setInput($field,"",INPUT_TEXT,COL_LG_4,CLIENTE_DIRECCION,TN_CLIENTE_DIRECCION,TN_CLIENTE_DIRECCION);
        $field = $form->setInput($field,"",INPUT_NUMBER,COL_LG_4,CLIENTE_TELEFONO,TN_CLIENTE_TELEFONO);
        $field = $form->setInput($field,"",INPUT_EMAIL,COL_LG_4,CLIENTE_CORREO,TN_CLIENTE_CORREO);
        $form->addField($form,$field);
        $form->col = COL_LG_12;
        $form->action = url(CLIENTE_URL_PROCESS);
        $form->idForm = FORM_ID;
        $form->method = GET;
        
        $form = $form->render($form);
        return view(PATH_FORM,compact(VAR_FORM));
    }

    public function createProcess(){
        $_GET[TN_ESTADO_CLIENTE] = 'Activo';
        $this->gstCliente->saveCliente($_GET);
        $_SESSION[DATA][MSG] = MSG_CLIENTE_CREATE_EXITOSAMENTE;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect(CLIENTE_URL_CREATE); 
    }
    
    public function configCliente(){
        $_SESSION[DATA][CLIENTE_TITLE] = CLIENTE_CONF_PLATAFORMA;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CLIENTES=>URL::previous(),CONFIGURACIONES=>""]; 
        $form = $this->formConfig();

        $clientes = $this->gstCliente->getAllClientes()->toArray();
        $clientes[RELACION] = [ID,TN_NOMBRE_CLIENTE];
        $field = $form->setFielset(COL_LG_12,TITLE_CLIENTE_CONF);
        $form2 = new Formulario;
        $form2= $form2->setInput($field,"",INPUT_SELECT,COL_LG_4,CONF_BUSCADOR,TN_CONF_BUSCADOR,'',$clientes);
        $form->fieldSet[] = $form2;
        $form->col = COL_LG_12;
        $form->action = CONF_URL_PROCESS;
        $form->method = POST;
        $form = $form->render($form);

        return view(PATH_FORM,compact(FORM));
    }

/**
 * Controlador de clientes: listado de clientes
 * @category Clientes
 * @package SGD
 * @version 1.0
 * @author Cristian Cifuentes <ccifuentes@nexura.com>
 */

    public function lstClientes(){
        $table = new Table;
        $table->setColum($table,
        [
            CLIENTE_ID=>TN_ID_CLIENTE,
            CLIENTE_NOMBRE=>TN_NOMBRE_CLIENTE,
            CLIENTE_DIRECCION=>TN_CLIENTE_DIRECCION,
            CLIENTE_TELEFONO=>TN_CLIENTE_TELEFONO,
            CLIENTE_CORREO=>TN_CLIENTE_CORREO,
            FECHA=>CREATED_AT,
            CLIENTE_TITLE_ESTADO=>TN_ESTADO_CLIENTE

        ]);
        $opciones = 
            [
                //nuevas lineas de codigo
                EDITAR=> CLIENTE_URL_EDITAR,
                'val-'.CAMBIAR_ESTADO=>CLIENTE_URL_CAM_EST,
                'val-'.TITLE_ELIMINAR=>CLIENTE_URL_DELETE
            ];
        $table->setItems($table,$this->gstCliente->getAllClientes()->toArray());
        $table = $table->dps($table);
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CLIENTES=>URL::previous(),CLIENTE_TITLE_LISTAR=>""]; 
        $_SESSION[DATA][OPC_FUNCIONES] = null;
        $_SESSION[DATA][CLIENTE_TITLE] = CLIENTE_TITLE_LISTAR;
        return view(PATH_LIST,[TABLE=>$table,'opc'=>json_encode($opciones)]);
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:
    *Salida: formulario con los datos del cliente seleccionado.
    *Descripciòn: La funcion muestra al usuarion un formulario
    * con los datos del cliente.
    */
    public function editarCliente(){
       
        $cliente = $this->gstCliente->getCliente($_GET['id']);
                
        $_SESSION[DATA][CLIENTE_TITLE] = CLIENTE_TITLE_EDITAR_CLIENTE;
            $_SESSION[DATA][BREAD] = [INICIO =>PATH_HOME,MENU_MODULOS=>'/Menu/1',CLIENTE_TITLE_LISTAR_CLIENTE => RUTAS_LISTAR_MODULOS, CLIENTE_TITLE_EDITAR_CLIENTE =>''];

        return redirect(URL_EDITAR_CLIENTE, compact('cliente'));
    }

      /*
    *Autor: Samuel Beltran
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:
    *Salida: Devuelve al usuario a la vista de la lista
    * de clientes registrados.
    *Descripciòn: La funcion permite que los datos de un
    *cliente previamente registrado puedan ser editados/actualizados.
    *Una vez sean editados correctamente, nos devolvera a la vista de
    * la lista.
    */
    public function editarClienteProcess(Request $request)
    {
        $this->gstGeneral->validateData($request);
        $this->gstCliente->processEditCliente($_POST[TN_ID_CLIENTE],$_POST);
        return redirect(URL_EDIT_CLIENTE);
    }

     /*
    *Autor: Samuel Beltran
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:
    *Salida: Devuelve la vista con las listas de clientes con 
    * su nuevo cambio de estado.
    *Descripciòn: Funcion para hacer un cambio de estado de "Activo" a 
    * "Inactivo" y viceversa.
    */
    public function cambiarEstadoCliente()
    {
       $response=$this->gstCliente->estCliente($_GET[TN_ID_CLIENTE],TN_ESTADO_CLIENTE);
       
       if ($response) {

            $_SESSION[DATA][MSG] = MSG_CLIENTE_CAMBIAR_ESTADO;

            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

            
        }else{

            $_SESSION[DATA][MSG] = MSG_CLIENTE_NO_ACTUALIZADO_ESTADO;

            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

            
        }
        return redirect(URL_EDIT_CLIENTE);
    }
         /*
    *Autor: Samuel Beltran
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:
    *Salida: Devuelve la vista con las listas de clientes con 
    * su nuevo cambio de estado.
    *Descripciòn: Funcion para hacer un cambio de estado de "Activo" a 
    * "Inactivo" y viceversa.
    */
    public static function listarClienteTable()
    {
       return $this->gstCliente->getAllClientes()->toArray();
    }

                 /*
    *Autor: Samuel Beltran
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:
    *Salida: 
    *Descripciòn: Funcion para borrar un cliente.
    */
    public function borrarCliente()
    {
        
        $rolAtributte = FuncionesGenerales::getByAtribbute(new Roles,TN_CONF_ID_CLIENTE,$_GET['id']);
        $usuAtributte = FuncionesGenerales::getByAtribbute(new User,'cliente_id',$_GET['id']);
        
        
        foreach ($rolAtributte as $rol) {
            FuncionesGenerales::deleteByElement(new ModulosRol,'id_rol',$rol['id']);
            FuncionesGenerales::deleteByElement(new FuncionesRoles,'rol_id',$rol['id']);
        }

        foreach ($usuAtributte as $usu ) {
           FuncionesGenerales::deleteByElement(new Favoritos,'usuario_emisor',$usu['id']);
           FuncionesGenerales::deleteByElement(new Favoritos,'usuario_resector',$usu['id']);

        }

        FuncionesGenerales::deleteByElement(new User,TN_CONF_ID_CLIENTE,$_GET['id']);
        FuncionesGenerales::deleteByElement(new Listas,TN_CONF_ID_CLIENTE,$_GET['id']);
        FuncionesGenerales::deleteByElement(new ClientesMod,TN_CLIENTE_ID,$_GET['id']);
        FuncionesGenerales::deleteByElement(new Formularios,TN_CONF_ID_CLIENTE,$_GET['id']);
        FuncionesGenerales::deleteByElement(new SFTConf,TN_CONF_ID_CLIENTE,$_GET['id']);
        FuncionesGenerales::deleteByElement(new Roles,'cliente_id',$_GET);
        FuncionesGenerales::deleteByElement(new Cliente,CLIENTE_ID,$_GET['id']);
        
        return redirect(URL_TABLE_1);
    }
}
