<?php

namespace App\Http\Controllers\Menu;
use App\Http\Controllers\Cliente\ClientesController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Gst\GstModulos;
use App\Http\Gst\GstFunciones;
use App\Http\Gst\GstCliente;
use Auth;
/*
*Autor: Cristian Cifuentes
*Modulo: Menu
*Versiòn: 1.0
*Descripciòn: Controllador menus del sistema.
*/
class MenuController extends Controller
{
	private $data;
	private $gstModulos;
  private $gstCliente;
  private $gstFunciones;
  /*
    *Autor: Cristian Cifuentes
    *Modulo: Menu
    *Versiòn: 1.0
    *Entrada: GstFunciones = Gestion de funciones
    *Salida: 
    *Descripciòn: Constructor donde se inyectan dependencias a utilziar
    */
  public function __construct(GstFunciones $gstFunciones,GstModulos $gstModulos) 
  {
    $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
    session_start();
    $this->gstFunciones = $gstFunciones;
    $this->gstModulos = $gstModulos;
  }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Menu
    *Versiòn: 1.0
    *Entrada: $idModulo = identificador del modulo
    *Salida: vista de menu con las funciones del modulo
    *Descripciòn: Funcion que carga el menu de funciones de cada modulo
    */
  public function menu($idModulo)
  {
    $_SESSION[DATA][CLIENTE_FUNCIONES] =  $this->gstFunciones->getFuncionesMod($idModulo);
    $modulo = $this->gstModulos->getModulo($idModulo);
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU.$modulo[0]->mod_nombre =>PATH_MENU.$idModulo];
    $_SESSION[DATA][CLIENTE_TITLE] = MENU.$modulo[0]->mod_nombre;
    return view(CLIENTE_MENU,[DATA=>$_SESSION[DATA]]);
  }
}




