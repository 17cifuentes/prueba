<?php

namespace App\Http\Controllers\Usuarios;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controllers\Controller;
use App\Http\Gst\GstCambioPassword;
use App\Http\Gst\GstUsuarios;
use Illuminate\Http\Request;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstGeneral;
use App\Http\Gst\GstCampos;
use App\Http\Gst\GstRoles;
use App\Http\Clases\Table;
use Auth;
use URL;



//use App\Http\Controllers\Cliente\ClientesController;
//use App\Http\Clases\Formulario; 
//use App\Roles;
//use Validator;
//use App\User;



/*
*Autor: Cristian Cifuentes
*Modulo: Modulos
*Versiòn: 1.0
*Descripciòn: Controllador Usuarios del sistema.
*/
class UsuarioController extends Controller
{
	private $data;
    private $gstUsuario;
    private $gstCliente;
    private $gstRoles;
    private $gstCampos;
    private $GstCambioPassword;
    private $gstGeneral;
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:GstUsuarios = clase de gestion de usuarios, GstCliente = clase de gestion de cliente, *GstRoles = clase de gestion de roles
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Constructor para inyeccion de dependencias.
    */

	public function __construct(GstGeneral $gstGeneral,FuncionesGenerales $classFunGenerales,                        GstUsuarios $gstUsuario, GstCliente $gstCliente, 
                                GstRoles $gstRoles, GstCampos $gstCampos, 
                                GstCambioPassword $gstCambioPassword) 
    {
       $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => [EXC_REG_EXT,
                                                        EXC_REC_PASS,
                                                        EXC_REC_PASS_PRO,
                                                        EXC_CHA_PASS,
                                                        EXC_DATA,
                                                        EXC_CRE_USU_EXT,
                                                        EXC_GET_USU_ROL,
                                                        EXC_CRE_PRO,
                                                        EXC_GET_CAMP]
                                                    ]);

       session_start();
       $this->gstRoles = $gstRoles;
       $this->gstCampos = $gstCampos;
       $this->gstGeneral = $gstGeneral;
       $this->gstUsuario = $gstUsuario;
       $this->gstCliente = $gstCliente;
       $this->classFunGenerales = $classFunGenerales; 
       $this->gstCambioPassword= $gstCambioPassword;
   	}
    /*
    *Autor: Javier R
    *Modulo: Usuarios
    *Entrada: id: Identificador De Usuario Para Editar
    *Salida: return Vistar Editar
    *Versiòn: 1.0
    *Descripciòn: Metodo Para Mostrar El Usuario a Editar
    */
    public function editarUsuarios()
    {
         $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menù Usuarios'=>'/Menu/5','Listar usuarios'=>'lstUsuarios','Editar Usuarios'=>'#'];
         if(Auth::user()->rol_id == 1 )
         {
            $clientes = $this->gstCliente->getAllClientes();  
         }else
         {
           $cliente = Auth::user()->cliente_id;
            $clientes = $this->gstCliente->getCliente($cliente);
         }
        $usuario = $this->gstUsuario->getUser($_GET[TN_USU_ID]);
        $datosRol = $this->gstRoles->gatDatosRol($usuario->rol_id,$usuario->id);
        $campos = $this->getCampos($usuario->rol_id);
        $departamentoQuery = $this->classFunGenerales->getWorldbyParent(48);
        $municipioUsu = $this->gstGeneral->getMunicipioById($usuario['departamento_id']);
        
        return view('core.Usuarios.editarUsuario',['datosRol'=>$datosRol,'municipioUsu'=>$municipioUsu,'clientes'=>$clientes,'usuario'=>$usuario,"campos"=>$campos,'departamentoQuery'=>$departamentoQuery,SFT_LIB=>[SFT_SELECT2]]);
    }
    /*
    *Autor: Javier R
    *Modulo: Usuarios
    *Entrada: id: Identificador De Usuario Para Modificar
    *Salida: return listar Usuarios
    *Versiòn: 1.0
    *Descripciòn: Metodo Para Editar Usuario
    */

    public function actulizaUsuarioProcess()
    {
        if(!isset($_POST[TN_USU_ID]) or !isset($_POST['name']) or !isset($_POST['email']))
        {
            $_SESSION[DATA][MSG] = MSG_ERROR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return $this->lstUsuarios();
        }
        if($_POST[TN_USU_PASSWORD] != "" or $_POST[TN_USU_PASSWORD] != null)
        {
          if(strlen($_POST[TN_USU_PASSWORD]) < 6)
            {
                $_SESSION[DATA][MSG] = 'Error, La Contraseña debe ser mayor a 6 caracteres';
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return $this->lstUsuarios();   
            }  
        }
        $validar =$this->gstUsuario->validarDatos($_POST[TN_USU_ID],$_POST['name'],$_POST['email'],$_POST[TN_USU_PASSWORD]);
        if($validar != true)
        {
            $_SESSION[DATA][MSG] = MSG_ERROR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return $this->lstUsuarios();   
        }
        foreach ($_POST as $key => $value) {
             if(substr($key,0,13) == 'Identificador'){
                $this->gstRoles->updateDataRol($_POST[TN_USU_ID],substr($key,13),$value);
                unset($_POST[$key]);
             }
             
        }
        unset($_POST['_token']);  
        $id = $_POST[TN_USU_ID];
        unset($_POST[TN_USU_ID]);
        if($_POST[TN_USU_PASSWORD] == ''){
            unset($_POST[TN_USU_PASSWORD]);
        }else{
           $_POST[TN_USU_PASSWORD] = bcrypt($_POST[TN_USU_PASSWORD]); 
        }
        if($_FILES['foto']['name'] != ''){
            
            $_POST['img'] = FuncionesGenerales::cargararchivo($_FILES['foto'],'core/img/','default.jpg');
        }
        $data = $this->gstUsuario->actualizarUsuario($_POST,$id);
        if($data != false)
        {
            $_SESSION[DATA][MSG] = MSG_USU_EXITOSO_ACTUALIZADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            return redirect(FUN_LST_USUARIOS); 
        }else
        {
            $_SESSION[DATA][MSG] = MSG_ERROR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(FUN_LST_USUARIOS);
        }
        
    }
    /*
    *Autor: Javier R
    *Modulo: Usuarios
    *Entrada: id: Identificador De Usuario Para Eliminar Usuario
    *Salida: return listar Usuarios
    *Versiòn: 1.0
    *Descripciòn: Metodo Para Eliminar Usuario
    */
    public function eliminarUsuario()
    {

        $respuesta =$this->gstUsuario->eliminarUsuario($_GET[TN_USU_ID]);
        if($respuesta)
        {
            $_SESSION[DATA][MSG] = 'Borrado exitoso';
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            return redirect(FUN_LST_USUARIOS);    
        }
        else{
            $_SESSION[DATA][MSG] = MSG_ERROR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(FUN_LST_USUARIOS);      
        }
    }

    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo para listar los usuarios pertenecientes a un clientes
    */
    public function lstUsuarios()
    {
        //PermisosController::getPermisos();
        $_SESSION[DATA][CLIENTE_TITLE] = USU_VIEW_LST_TITLE;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_USUARIOS=>'/Menu/5',LST_USUARIOS=>"#"];  
        $tableUsu = new Table;
        $tableUsu->setColum($tableUsu,
            [
                ID=>TN_USU_ID,
                USU_NOMBRE=>TN_USU_NOMBRE,
                USU_CORREO=>TN_USU_CORRE,
                USU_ROL =>TN_USUARIO_ROL_NAME,
                USU_HTML_FOTO=>['<img src="/-img" width="80em">','img']
            ]
        );
        $opciones = 
            [
                EDITAR_USU=> FUN_EDIT_USER,
                VAL_DELETE=> FUN_DELETE_USER
            ];
        if(Auth::user()->cliente_id == 1){
            $datos = $this->gstUsuario->getAllUsers()->toArray();
        }else{
            $datos = $this->gstUsuario->getUserClient()->toArray();
        }
        //dd($datos);
        $tableUsu->setItems($tableUsu,$datos);
        $tableUsu->setClass($tableUsu,'sftDataTable');
        $tableUsu = $tableUsu->dps($tableUsu);
        return view(PATH_LIST,[
            TABLE=>$tableUsu,
            'opc'=>json_encode($opciones),
            //SFT_LIB=>[SFT_DATATABLE]
        ]);
    }

    public function getUsuRoles()
    {
        $roles = $this->gstRoles->getDatosByAtributte(TN_USU_CLIENTE_ID,$_GET[TN_USU_ID]);
         return json_encode($roles);
    }

    public function getCampos($id = 0)
    {     
        if($id == 0){
            $idRol = $_GET[TN_USU_ID];
        }else{
            $idRol = $id;
        }
        $campos = $this->gstCampos->camposJoin($idRol);
        return json_encode($campos);
    }
     /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo para mostrar formulario de creaciòn de usuario
    */
    public function crearUsuario()
    {
        $_SESSION[DATA][CLIENTE_TITLE] = CREAR_USUARIOS;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_USUARIOS=>'/Menu/5',CREAR_USUARIOS=>"#"];  
        if(Auth::user()->rol_id == 1 )
         {
            $clientes = $this->gstCliente->getAllClientes();  
         }else
         {
           $cliente = Auth::user()->cliente_id;
            $clientes = $this->gstCliente->getCliente($cliente);
         }
        $departamentoQuery = $this->classFunGenerales->getWorldbyParent(48);
         
        return view('core.Usuarios.crearFormUsuario',['clientes'=>$clientes,'departamentoQuery'=>$departamentoQuery,SFT_LIB=>[SFT_SELECT2]]);

    }
     /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo para mostrar formulario de creaciòn de usuario
    */
    public function crearUsuarioExterno()
    {
        //dd($_GET);
        $data['clientes'] = $this->gstCliente->getCliente($_GET['idCliente']);
        $data['departamentos'] = $this->classFunGenerales->getWorldbyParent(48);
        return json_encode($data);
    }    
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:por get = datos del formualario
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo para persistir un usaurio en el sistema
    */
    public function createProccess(Request $request)
    {
        
        $response = $this->gstUsuario->getUserByAttribute('email',$_GET['email'])->toArray();
        if ($response) {
        
        }else{
            $this->gstGeneral->validateData($request);
            foreach ($_GET as $key => $value) {
                if(is_numeric($key)){
                    $idCampo[] = (int)$key;
                    $valor[] = $value;
                    unset($_GET[$key]);
                }
            }
            if(isset($_GET['externo'])){
                $externo = 1;
                $idCliente = $_GET['idCliente'];
                unset($_GET['externo']);
                unset($_GET['idCliente']);
            }
            $roles = $this->gstRoles->registroExterno($idCliente);
            $_GET['rol_id'] = $roles[0]['id'];
            $_GET['cliente_id'] = $idCliente;
            $_GET[TN_USU_IMG] = "core/img/images.jpg";
            $_GET[TN_USU_PASSWORD] = bcrypt($_GET[TN_USU_PASSWORD]);
            
            $usuario = $this->gstUsuario->saveUsuario($_GET);
            $_SESSION[DATA][MSG] = MSG_USU_EXITOSO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            
            if(isset($idCampo)){
                for($x = 0;$x < count($idCampo);$x++) {
                    $data['id_campo'] = $idCampo[$x];
                    $data['valor'] = $valor[$x];
                    $data['usuario_id'] = $usuario->id;
                    $this->gstRoles->saveDatosRol($data);
                }
            }
            if(isset($externo)){
                Auth::login($usuario);
                $res = "false";
                if(isset(Auth::user()->id)){
                    $res = "true";
                }
                return $res; 
                
            }else{
                return redirect(FUN_LST_USUARIOS);  
            }
            
        }
        
        
    }
    public function createProccessInt(Request $request)
    {
        
        $response = $this->gstUsuario->getUserByAttribute('email',$_GET['email'])->toArray();
        if ($response) {
            
            $_SESSION[DATA][MSG] = 'El correo ya existe';

            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

            return redirect(PATH_URL_USU_CREAR);
        }else{
            $this->gstGeneral->validateData($request);
            foreach ($_GET as $key => $value) {
                if(is_numeric($key)){
                    $idCampo[] = (int)$key;
                    $valor[] = $value;
                    unset($_GET[$key]);
                }
            }
            if(isset($_GET['externo'])){

                $externo = 1;
                $idCliente = $_GET['idCliente'];
                unset($_GET['externo']);
                unset($_GET['idCliente']);
            }
            $_GET[TN_USU_IMG] = "core/img/images.jpg";
            $_GET[TN_USU_PASSWORD] = bcrypt($_GET[TN_USU_PASSWORD]);
            $_GET['cliente_id'] = $_GET['idCliente'];
            $usuario = $this->gstUsuario->saveUsuario($_GET);
            $_SESSION[DATA][MSG] = MSG_USU_EXITOSO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            
            if(isset($idCampo)){
                for($x = 0;$x < count($idCampo);$x++) {
                    $data['id_campo'] = $idCampo[$x];
                    $data['valor'] = $valor[$x];
                    $data['usuario_id'] = $usuario->id;
                    $this->gstRoles->saveDatosRol($data);
                }
            }
            if(isset($externo)){
                Auth::login($usuario);
                $res = "false";
                if(isset(Auth::user()->id)){
                    $res = "true";
                }
                return $res; 
                
            }else{
                return redirect(FUN_LST_USUARIOS);  
            }
            
        }
        
        
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo que un usuario salga del sistema
    */
    public function logOut()
    {
        $cliente = Auth::user()->cliente_id;
        Auth::logout();
        session_destroy();
        unset($_SESSION[DATA]);
        unset($_SESSION[DATA]['funciones']);
        unset($_SESSION[DATA]['bread']);
        unset($_SESSION[DATA]['title']);
        return redirect(URL_INGRESO.$cliente.'/0');
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo que un usuario salga del sistema
    */
    public function perfil()
    {
        $_SESSION[DATA][CLIENTE_TITLE] = PERFIL_USUARIOS;
        $_SESSION[DATA][BREAD] = [INICIO=>URL::previous(),PERFIL_USUARIOS=>""];
        return view(PATH_PERFIL);
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Metodo que un usuario salga del sistema
    */
    public function ayuda()
    {
        $_SESSION[DATA][CLIENTE_TITLE] = TITLE_AYUDA;
        $_SESSION[DATA][BREAD] = [INICIO=>URL::previous(),$_SESSION[DATA][CLIENTE_TITLE]=>""];
        return view(PATH_CORE_AYUDA);
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada: GET -> Arreglo con datos de perfil
    *Salida: vista de perfil
    *Versiòn: 1.0
    *Descripciòn: Metodo para actualizar datos de perfil
    */
    public function actulizaPerfil()
    {
        unset($_POST[TOKEN]);
        $_POST[TN_USU_IMG] = $this->classFunGenerales->cargararchivos($_FILES[FOTO],PATH_FOTO_USU,'');
        if($_POST[TN_USU_IMG] == PATH_FOTO_USU){
           $_POST[TN_USU_IMG] = Auth::user()->img; 
        }
        $_POST[TN_USU_PASSWORD] = bcrypt($_POST[TN_USU_PASSWORD]);
        $this->gstUsuario->actualizarUsuario($_POST,Auth::user()->id);
        $_SESSION[DATA][MSG] = MSG_USU_EXITOSO_ACTUALIZADO;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect(PATH_URL_USU_PERFIL);
    }
    /*
     * Funcion para redirigir a la vista donde se va a escribir la direccion del correo
     electronico a recuperar contraseña
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
     */

    public function recuperarContrasena()
    {
        if(!isset($_GET['msg'])){
            $_GET['msg'] = 'Recuperar Contraseña '.$_SESSION[DATA][CLIENTE_CONF][0][TN_NOMBRE_CLIENTE];
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS; 

        }
        $_SESSION[DATA][MSG] = $_GET['msg'];
        return view('auth.recuperarContrasena');
    }

    /*
     * Funcion para revisar si el correo enviado coinciden con los que se tienen en los registros de las bases de datos, tambien sera el indicado de enviar el correo con una contraseña para cambio de contraseña
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
     */
    public function recuperarContrasenaProccess()
    {
        unset($_POST['_token']);
        $usuario = $this->gstUsuario->consultaEmail($_POST);
        if(count($usuario) > 0)
        {
            $data=[];
            $codigoInicial = FuncionesGenerales::generarCodigo(20);
            $codigoFinal = $usuario[0]->id.".".$codigoInicial;
            $data['id_usuario']= $usuario[0]->id;
            $data['codigo']= $codigoInicial; 
            $this->gstCambioPassword->insertarCodigo($data);
            return view('Auth.confirmaCodigo',['usuario'=>$usuario[0]->name,'codigoInicial'=>$codigoFinal]); 
            /*
            $codigoFinal = $usuario[0]->id.$codigoInicial;
            $usuario['codigo'] = $codigoFinal;
            
            FuncionesGenerales::correo('working',$usuario,$usuario[0]->email,'Recuperar Contraseña','coco','info@softheory.com','ococ');
            ;
            $msg = 'Se envió correo';
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS; 
            */
        }else{
            $msg = 'Error el correo no existe en nuestros registros';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR; 
        }
        return redirect('Usuarios/recuperarContrasena?msg='.$msg);
    }
    /*
     * Funcion para recibir el codigo que se le envio por medio del correo y compararlo con el que se tiene en la base de datos tambien sera el encargado de redirigir al la vista del cambio de contraseña en el caso de que los codigos sean iguales.
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
     */
    public function data($codigoInicial)
    {
        if(!isset($codigoInicial))
        {
            $msg = 'Error al cambiar contraseña vuelva a realizar el proceso';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Usuarios/recuperarContrasena?msg='.$msg);
        }
        $indice = strpos($codigoInicial,'.');
        $usuId = substr($codigoInicial,0,$indice);
        $codigo = substr($codigoInicial,$indice+1,strlen($codigoInicial));

        $usuario = $this->gstUsuario->getUser($usuId);
        if($usuario)
        {
        $consultaCodigo= $this->gstCambioPassword->consultaCodigo($codigo);
        if($consultaCodigo)
            {
                if(!isset($_GET['msg']) or !$_GET['msg']){
                    $_GET['msg'] = 'Recuperar Contraseña '.$_SESSION[DATA][CLIENTE_CONF][0][TN_NOMBRE_CLIENTE];
                    $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
                }
                
                $_SESSION[DATA][MSG] = $_GET['msg'];
                
                return view('Auth.correoRecuperacionPass',['usuario'=>$usuario,'codigo'=>$codigoInicial]);
            }else{
                $msg = MSG_ERROR_CHA_PASS;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Usuarios/recuperarContrasena?msg='.$msg);
            }        
        }else{
            $msg = MSG_ERROR_CHA_PASS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Usuarios/recuperarContrasena?msg='.$msg);   
        }
    }
    /*
     * Funcion encargada para recibir la nueva contraseña y su confirmacion de contraseña, sera el encargado de modificar la contraseña y borrar el codigo que se le envio al cliente 
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
    */
    public function cambioPassword()
    {
        
        if(isset($_POST['codigo']) && isset($_POST[TN_USU_CLIENTE_ID]) && isset($_POST[TN_USU_PASSWORD]) && isset($_POST['confPasword']) && isset($_POST[TN_USU_ID]))
        {
            if(strlen($_POST['confPasword']) > 40 or strlen($_POST[TN_USU_PASSWORD]) > 40)
            {
                $msg= 'Error al cambiar contraseña, demasiados caracteres';
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Usuarios/data/'.$_POST['codigo'].'?msg='.$msg);
            }

            if($_POST[TN_USU_PASSWORD] != $_POST['confPasword'])
            {
                $msg= 'Error al cambiar contraseña, las contraseñas no coiciden';
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Usuarios/data/'.$_POST['codigo'].'?msg='.$msg);
            }
                unset($_POST['_token']);
                $pass = bcrypt($_POST[TN_USU_PASSWORD]);
                $data=$this->gstCambioPassword->deleteCod($_POST[TN_USU_ID]);
            if($data)
            {
                $cambiarContraseña = $this->gstUsuario->cambioPassword($_POST[TN_USU_ID],$pass);
                if($cambiarContraseña)
                {
                    $cliente = $_POST[TN_USU_CLIENTE_ID];
                    return redirect(URL_INGRESO.$cliente.'/0');
                    $_SESSION[DATA][MSG] = MSG_USU_CAMBIO_CONTRASEÑA;
                    $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
                }else{
                    $msg = MSG_USU_ELIMINO_CODIGO;
                    $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                    $cliente = $_POST[TN_USU_CLIENTE_ID];
                    return redirect(URL_INGRESO.$cliente.'/0');
                }

            }else{
                $msg = MSG_ERROR_CHA_PASS;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Usuarios/recuperarContrasena?msg='.$msg);
            }

        }else{
            $msg = MSG_ERROR_CHA_PASS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Usuarios/recuperarContrasena?msg='.$msg);   
        }
        
        /*si pierde, condigo unico de recuperacion activado
        por favor realce el proceso de recuperacion en este momento
        el codigo sera desactivado al recargar la pagina o pasado 10 min*/
    }

}

