<?php

namespace App\Http\Controllers\Permisos;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Cliente\ClientesController;
use Illuminate\Http\Request;
use App\Http\Gst\GstModulos;
use App\Http\Gst\GstFunciones;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstPermisos;
use App\Http\Clases\Table;
use Auth;
use URL;
use App\ClientesMod;
use App\Roles;
use App\Http\Gst\GstRoles;
/*
*Autor: Cristian Cifuentes
*Modulo: Permisos
*Versiòn: 1.0
*Descripciòn: Controllador de permisos dentro del sistema
*/
class PermisosController extends Controller
{
  private $gstPermisos;
  private $gstModulos;
  private $gstFunciones;
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Usuarios
    *Entrada:GstUsuarios = clase de gestion de usuarios, GstCliente = clase de gestion de cliente, *GstRoles = clase de gestion de roles
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Constructor para inyeccion de dependencias.
    */

  public function __construct(GstPermisos $gstPermisos, GstModulos $gstModulos, GstFunciones $gstFunciones, GstCliente $gstCliente,GstRoles $gstRoles) 
    {
      $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
       session_start();
       $this->gstPermisos = $gstPermisos;
       $this->gstModulos = $gstModulos;
       $this->gstFunciones = $gstFunciones;
       $this->gstCliente = $gstCliente;
       $this->gstRoles = $gstRoles;

    }

    public static function getPermisos()
    {
      $data = gstPermisos::getPermisosFunciones(Auth::user()->rol_id)->toArray();
      $pased = false;
      if(empty($data)){
        PermisosController::sinPermisos();
      }else{
        for ($i=0; $i < count($data); $i++) { 
          if($data[$i]['fun_ruta'] == $_SERVER["REQUEST_URI"]){
            $pased = true;
          }
        }
      }
      if($pased == false){
        PermisosController::sinPermisos();
      }
    }

    public static function sinPermisos(){
      $_SESSION[DATA][MSG] = "No cuenta con permisos para realizar esta funciòn";
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        header('Status: 301 Moved Permanently', false, 301);
        header('Location: '.URL::previous());
        exit();
    }


    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Esta funcion  obtiene todos los registros de
    * la tabla "tn_sft_modulos" y de "tn_sft_roles" segun sus respectivos "id"
    * y lo codifica con la funcion "json_encode()".
    */
       public function getModuloRol()
    {
      $data['modulos'] =  $this->gstModulos->getModuloDetalle($_GET[ID]);
      $data['roles'] =  $this->gstModulos->getDatosByAtribute(new Roles,TN_CONF_ID_CLIENTE,$_GET[ID]);

      return json_encode($data);
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida:Formulario con 3 "seleccione".
    *Versiòn: 1.0
    *Descripciòn: Funcion que al seleccionar un cliente,
    * este nos muestra los permisos que se pueden
    * seleccionar en las casillas de "rol" y "modulo".
    */
    public function createPermisosMod()
    {
      $clientes = $this->gstCliente->getAllClientes();
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                              MENU_PERMISOS=>PATH_MENU_DOS,
                              'Crear P. Roles'=>'']; 
      $_SESSION[DATA][CLIENTE_TITLE] = 'Crear Permisos/Roles';
      return view('core.Permisos.crearPermisosMod',['clientes'=>$clientes,SFT_LIB_MOD=>['Permisos']]);
    }
    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Devuelve a la vista para crear otro permiso.
    *Versiòn: 1.0
    *Descripciòn: funcion que evalua si el permiso que se quiere
    * crear ya se encuentra registrado en la base de datos. Si hay 
    * regitros ya creados,nos enviara un mensaje de error, y deja al 
    * usuario en la vista actual. Si la 
    * funcion no encuentra ningun registro, este procede a crear uno nuevo
    * con los datos que el usuarion escogio de las casillas "seleccione", y 
    * muestra un mensaje de registro exitoso.
    */
    public function createPermisosModProcess()
    {   
      if(isset($_POST['id_cliente']) && isset($_POST['id_rol']) && isset($_POST['id_modulo']))
      {
        if(count($this->gstPermisos->getIdModRoles($_POST[TN_DET_MOD_ROL_ID],$_POST[TN_MOD_ID_MOD])) > 0 ){
          $_SESSION[DATA][MSG] = MSG_TIENE_PERMISOS;
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      }else{
          $response = $this->gstPermisos->crearDetModClientes($_POST); 
          if($response){
            $_SESSION[DATA][MSG] = MSG_PERMISO_CREADO_EXITOSO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            return redirect('/Permisos/createPermisosMod');
          } 
          
      }
       return redirect('/Permisos/createPermisosMod'); 
      }else{
        $_SESSION[DATA][MSG] = 'No se puede crear el permiso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('/Permisos/createPermisosMod');
      }
      
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Muestra una lista con el nombre de los modulos,descripcion
    * y su id.
    *Versiòn: 1.0
    *Descripciòn: Funcion para listar los registros de la tabla "tn_sft_modulos"
    * con las opciones "gestionar" y "ver permisos".
    */
    public function lstPermisosModulos()
    {
      $tablePer = new Table;
      $tablePer->setColum($tablePer,
        [
          'id'=>ID,
          MODULOS_NOMBRE=>TN_MODULOS_NOMBRE,
          MODULOS_DESCRICION=>TN_MODULOS_DESCRIPCION,
        ]);
      $opciones = [
        GESTIONAR=>'Modulos/lstModulos',
        VER_PERMISOS=>'Permisos/verPermisosModulos'
      ];
      
      $tablePer->setItems($tablePer,$this->gstModulos->getModulos()->toArray());
      $tablePer = $tablePer->dps($tablePer);
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PERMISOS=>PATH_MENU_DOS,TITLE_LISTA_PERMISO_MODULO=>'lstPermisosModulos']; 
      $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LISTA_PERMISO_MODULO;
      return view (PATH_LIST,[TABLE=>$tablePer,'opc'=>json_encode($opciones)]);
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: lista con el id, el nombre del modulo con su respectivo rol,
    * y un boton de eliminar.
    *Versiòn: 1.0
    *Descripciòn: Funcion para listar los roles que tiene un modulo, sacados
    * de la tabla "tn_sft_detalle_modulo_rol","tn_sft_roles" y "tn_sft_modulos", y
    * muetra el boton de eliminar.
    */
    public function verPermisosModulos()
    {

      $all = $this->gstPermisos->getAllModRoles($_GET);
      if (count($all) > 0)
      {
        $tableVerPer = new Table;
        $tableVerPer->setColum($tableVerPer,
        [
          ID=>ID,
          MODULO=>TN_MODULOS_NOMBRE,
          TITTLE_ROL_NOMBRE=>TN_ROL_NOMBRE
  
        ]);
        $opciones = [
          'val-'.TITLE_ELIMINAR=>"Permisos/borrarPermisoModulo"
        ];
  
        $datos = $this->gstPermisos->getsDetalleModuloRolJoin($_GET['id'])->toArray();
        $tableVerPer->setItems($tableVerPer,$datos);
        $tableVerPer = $tableVerPer->dps($tableVerPer);
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                                  TITLE_MENU_PERMISOS=>PATH_MENU_DOS,
                                  TITLE_LISTA_PERMISO_MODULO=>'/Permisos/lstPermisosModulos',
                                  VER_PERMISOS_MODULOS=>''
                                ]; 
      $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LISTA_PERMISO_MODULO;
        return view (PATH_LIST,[TABLE=>$tableVerPer,'opc'=>json_encode($opciones)]);
      }   
      else{
          $_SESSION[DATA][MSG] = 'Este rol no cuenta con permisos';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('/Permisos/lstPermisosModulos');       
          }
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Muestra nuevamente la lista modelos una vez eliminado el deseado.
    *Versiòn: 1.0
    *Descripciòn: Esta funcion elimina un permiso con el id seleccionado, y muestra
    * un mensaje de eliminado exitoso.
    */
    public function borrarPermisoModulo()
    {
      $modDetId = $this->gstPermisos->getModuloDetalleID($_GET['id']);
      $this->gstPermisos->deletePermiso($_GET['id']);
      $_SESSION[DATA][MSG] = 'Permiso eliminado exitosamente';
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect('/Permisos/verPermisosModulos?id='.$modDetId[0]['id']);
    }

    public function listPermisosFunciones()
{
      $tablePerFu = new Table;
      $tablePerFu->setColum($tablePerFu,
      [
        'id'=>ID,
        'Nombre'=>FUN_MENU_NOMBRE,
        'Descripcion'=>'fun_descripcion',
        'Ruta'=>'fun_ruta'
        
      ]);
      $opciones = [
        'Gestionar'=>'Funciones/listarFunciones',
        'Ver Permisos'=>'Permisos/verPermisosFunciones'
      ];

      $tablePerFu->setItems($tablePerFu,$this->gstFunciones->getAllFunciones()->toArray());
      $tablePerFu = $tablePerFu->dps($tablePerFu);
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Permisos'=>'/Menu/2','Lista de Permisos/Funciones'=>'Permisos/listPermisosFunciones']; 
    $_SESSION[DATA][CLIENTE_TITLE] = 'Lista de Permisos/Funciones';
      return view (PATH_LIST,[TABLE=>$tablePerFu,'opc'=>json_encode($opciones)]);
    }

       /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: lista que muestra el "id", el nombre del rol, nombre de la funcion,
    * y un boton que puede eliminar un item de la lista.
    *Versiòn: 1.0
    *Descripciòn: Funcion que evalua si hay permisos en la base de datos. Si 
    * se encuentran registros este procede a mostrar el rol y la funcion junto
    * con el boton de eliminar. Si no hay registros, esta funcion permanece en
    * la vista mostrando con ella un mensaje de permisos no encontrados.
    */
    public function verPermisosFunciones()
    {
      $all = $this->gstPermisos->getAllFunRoles($_GET['id']);
      if(count($all) > 0){
        $taVerPerFun = new Table;
        $taVerPerFun->setColum($taVerPerFun,
          [
          'id'=>ID,
          'Rol Nombre'=>'rol_nombre',
          'nombre Funcion'=>FUN_MENU_NOMBRE
          ]);
          $opciones = [
            'val-'.TITLE_ELIMINAR=>"Permisos/borrarPermisoFunciones"
           ];

          $datos = $this->gstPermisos->getDetalleRolFunciones($_GET['id']);
          $taVerPerFun->setItems($taVerPerFun,$datos);
          $taVerPerFun = $taVerPerFun->dps($taVerPerFun);
          $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                                    'Menú Permisos'=>'/Menu/2',
                                    'Lista de Permisos/Funciones'=>'/Permisos/listPermisosFunciones',
                                    'Ver Permisos/Funciones'=>'']; 
          $_SESSION[DATA][CLIENTE_TITLE] = 'Ver Permisos/Funciones';
            return view (PATH_LIST,[TABLE=>$taVerPerFun,'opc'=>json_encode($opciones)]);
        }else{
          $_SESSION[DATA][MSG] = 'La función no cuenta con permisos';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('/Permisos/listPermisosFunciones');
      }
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Elimina un permiso de alguna funcion, y devuele a la vista de la lista.
    *Versiòn: 1.0
    *Descripciòn: Esta funcion elimina un permiso con el id seleccionado, y muestra
    * un mensaje de eliminado exitoso.
    */
    public function borrarPermisoFunciones()
    {
      $funId = $this->gstPermisos->getDetaRolFunId($_GET['id'])->toArray();
      $this->gstPermisos->deletePermisoFun($_GET['id']);
      $_SESSION[DATA][MSG] = 'Permiso eliminado exitosamente';
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect('/Permisos/verPermisosFunciones?id='.$funId[0]['id']);
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Esta funcion  obtiene todos los registros de
    * la tabla "tn_sft_funciones" segun sus respectivo "id", y lo
    * codifica con la funcion "json_encode()".
    */
    public function getModuloFun()
    {
      $data['funciones'] = $this->gstFunciones->getFuncionesMod($_GET['id']);
      return json_encode($data);
    }


    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida:Formulario con 4 "seleccione".
    *Versiòn: 1.0
    *Descripciòn: Funcion que al seleccionar un cliente,
    * este nos muestra los permisos que se pueden
    * seleccionar en las casillas de "rol", "modulo" y "funciones".
    */
    public function createPermisosFun()
    {
      $clientes = $this->gstCliente->getAllClientes();
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                              'Menú Permisos'=>'/Menu/2',
                              'Crear P. Funciones'=>'']; 
      $_SESSION[DATA][CLIENTE_TITLE] = 'Crear Permisos/Funciones';
      return view('core.Permisos.crearPermisosFun',['clientes'=>$clientes,SFT_LIB_MOD=>['Permisos']]);
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Devuelve a la vista para crear otro permiso.
    *Versiòn: 1.0
    *Descripciòn: funcion que evalua si el permiso que se quiere
    * crear ya se encuentra registrado en la base de datos. Si hay 
    * regitros ya creados,nos enviara un mensaje de error, y deja al 
    * usuario en la vista actual. Si la 
    * funcion no encuentra ningun registro, este procede a crear uno nuevo
    * con los datos que el usuarion escogio de las casillas "seleccione", y 
    * muestra un mensaje de registro exitoso.
    */
    public function createPermisosFunProcess()
    {  
      if(isset($_POST['id_cliente']) && isset($_POST['rol_id']) && isset($_POST['id_modulo']) && isset($_POST['funcion_id']))
      {
        if (count($this->gstFunciones->getRolFuncionsIds($_POST['rol_id'],$_POST['funcion_id'])) > 0)
        {
          $_SESSION[DATA][MSG] = 'La función ya cuenta con permisos';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('/Menu/2');
        }
            $response = $this->gstFunciones->insertRolFunciones($_POST);

            if ($response)
            {
             
              $_SESSION[DATA][MSG] = 'Función creada exitosamente';
              $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
              return redirect('/Menu/2');
            }else{

              $_SESSION[DATA][MSG] = PERMISO_NO_PUDO_CREAR;
              $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
              return redirect('/Menu/2');     
           }   

      }else{
        $_SESSION[DATA][MSG] = PERMISO_NO_CREADO;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('/Menu/2'); 
      }
    }

    public function createPModulos()
    {
      $modulos = $this->gstModulos->getModulos();
      $clientes = $this->gstCliente->getAllClientes();
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                              MENU_PERMISOS=>PATH_MENU_DOS,
                              TITLE_MODULOS_PERMISO_CREAR=>'']; 
      $_SESSION[DATA][CLIENTE_TITLE] = TITLE_CREAR_PERMISO_MODULO;
      return view('core.Permisos.crearPModulos',['clientes'=>$clientes,'modulos'=>$modulos,SFT_LIB=>[SFT_SELECT2]]);
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Devuelve a la vista para crear otro permiso.
    *Versiòn: 1.0
    *Descripciòn: funcion que evalua si el permiso que se quiere
    * crear ya se encuentra registrado en la base de datos. Si hay 
    * regitros ya creados,nos enviara un mensaje de error, y deja al 
    * usuario en la vista actual. Si la 
    * funcion no encuentra ningun registro, este procede a crear uno nuevo
    * con los datos que el usuarion escogio de las casillas "seleccione", y 
    * muestra un mensaje de registro exitoso.
    */
    public function createPModulosProcess()
    {  
    if(isset($_POST['id_cliente']) && isset($_POST['id_modulo']))
    {
      $deModulos = $this->gstPermisos->getIdModClientes($_POST['id_cliente'],$_POST['id_modulo']);
 
      if( count($deModulos) > 0 )
      {
          $_SESSION[DATA][MSG] = MSG_TIENE_PERMISOS;
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      }
      $response = $this->gstPermisos->crearPModulosClientes($_POST);
      if($response)
      {   
          $_SESSION[DATA][MSG] = MSG_PERMISO_CREADO_EXITOSO;
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
          return redirect('/Permisos/createPModulos');
      }else
      {
        $_SESSION[DATA][MSG] = PERMISO_NO_PUDO_CREAR;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('/Permisos/createPModulos');
      }
    } else{
      $_SESSION[DATA][MSG] = PERMISO_NO_CREADO;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('/Permisos/createPModulos');
    } 
      
        

    }

      /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: Devuelve true o false.
    *Versiòn: 1.0
    *Descripciòn: Esta funcion es para rvisar si un cliente tiene 
    * o no permisos de un modulo.
    */
    public static function checkPermisosModulos($idModulo)
    {
        $gstPermisos = new GstPermisos;        
        if (is_numeric($idModulo)) 
        {
          if(Auth::user()){
            $checkPermisos =$gstPermisos->queryPermisos($idModulo);  
          }
          if (empty($checkPermisos) or $checkPermisos == null or count($checkPermisos) == 0)
          {
              return false;

            }else {
               return true;

            }
        }else {
          echo(MSG_MODULO_NO_NUMERO);
          
        }
        
    }
    
    public function listPermisosRoles()
    {

      $tablePerRol = new Table;
      $tablePerRol->setColum($tablePerRol,
        [
          ID=>ID,
          TITTLE_ROL_NOMBRE=>TN_ROL_NOMBRE,
          DESCRIPCION=>TN_ROL_DESCRIPCION,
          'Cliente'=>'cliente_nombre'
          
        ]);
      $opciones = [
        GESTIONAR=>'Roles/lstRoles',
        VER_PERMISOS=>'Permisos/verPermisosRoles'
      ];

      $tablePerRol->setItems($tablePerRol,$this->gstRoles->getRolesClienteJoin()->toArray());
      $tablePerRol = $tablePerRol->dps($tablePerRol);
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PERMISOS=>PATH_MENU_DOS,TITLE_LISTA_P_ROLES=>URL::previous()]; 
      $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LISTA_P_ROLES;
      return view (PATH_LIST,[TABLE=>$tablePerRol,'opc'=>json_encode($opciones)]);
    }

       /*
    *Autor: Samuel Beltran
    *Modulo: Permisos
    *Entrada:
    *Salida: 
    *Versiòn: 1.0
    *Descripciòn: Funcion para listar los roles que tiene un modulo, sacados
    * de la tabla "tn_sft_detalle_modulo_rol","tn_sft_roles" y "tn_sft_modulos", y
    * muetra el boton de eliminar.
    */
    public function verPermisosRoles()
    {
      $all = $this->gstPermisos->getAllRolesMod($_GET['id']);
      if (count($all) > 0)
      {
        $tableVerPer = new Table;
        $tableVerPer->setColum($tableVerPer,
        [
          ID=>ID,
          MODULO=>TN_MODULOS_NOMBRE,
          TITTLE_ROL_NOMBRE=>TN_ROL_NOMBRE
  
        ]);
        $opciones = [
          'val-'.TITLE_ELIMINAR=>"Permisos/borrarPermisoModulo"
        ];
  
        $datos = $this->gstPermisos->getsDetalleRolesModulosJoin($_GET['id'])->toArray();
        $tableVerPer->setItems($tableVerPer,$datos);
        $tableVerPer = $tableVerPer->dps($tableVerPer);
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                                  TITLE_MENU_PERMISOS=>PATH_MENU_DOS,
                                  TITLE_LISTA_P_ROLES=>'/Permisos/listPermisosRoles',
                                  VER_PERMISOS_ROLES=>VER_PERMISOS_ROLES
                                ]; 
      $_SESSION[DATA][CLIENTE_TITLE] = VER_PERMISOS_ROLES;
        return view (PATH_LIST,[TABLE=>$tableVerPer,'opc'=>json_encode($opciones)]);
      }   
      else{
          $_SESSION[DATA][MSG] = 'Este rol no cuenta con permisos';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('/Permisos/listPermisosRoles');       
          }
    }

 /**
 * Función que lista todas las funciones existentes
 * de la base de datos.
 * @category PermisosController
 * @package Modulo Permisos
 * @version 1.0
 * @author Samuel Beltran
 */
  public function listFuncionesRoles()
  {
    $tableVerFunRoles = new Table;
    $tableVerFunRoles->setColum($tableVerFunRoles,
    [
      ID=>ID,
      TITLE_FUNCIONES=>FUN_MENU_NOMBRE,  
    ]);
    $opciones = [
      VISUALIZAR=>"Permisos/VisualizarRoles"
    ];

    $allFunciones = $this->gstFunciones->getAllFunciones()->toArray();
    $tableVerFunRoles->setItems($tableVerFunRoles,$allFunciones);
    $tableVerFunRoles = $tableVerFunRoles->dps($tableVerFunRoles);
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                              TITLE_MENU_PERMISOS=>'/Menu/2',TITLE_LISTA_P_FUN_ROLES=>'']; 
    $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LISTA_PERMISO_MODULO;
    return view (PATH_LIST,[TABLE=>$tableVerFunRoles,'opc'=>json_encode($opciones)]);
  }

 /**
 * Función que trae todos los roles y muestra en checkbox(checked)
 * los que que tienen permisos.
 * @category PermisosController
 * @package Modulo Permisos
 * @version 1.0
 * @author Samuel Beltran
 */
  public function VisualizarRoles()
  {
    if (!isset($_GET[ID]) || !filter_var($_GET[ID],FILTER_VALIDATE_INT)) {
      $_SESSION[DATA][MSG] = MSG_FUN_NO_ENCONTRADA;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_PERMISOS_LISTA_FUNCIONESROLES);   
    }
    $funcionById = $this->gstFunciones->getFuncionById($_GET[ID]);
    if (!count($funcionById) > 0) {
      $_SESSION[DATA][MSG] = MSG_FUN_NO_ENCONTRADA;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_PERMISOS_LISTA_FUNCIONESROLES);
    }else{
      $allFunciones = $this->gstFunciones->getFuncion($_GET[ID]);
      $jsonRoles = json_decode($allFunciones[ROLES]);
      foreach ($jsonRoles as $roles){
        $queryRol[] = $this->gstRoles->getRolesByID($roles)->toArray()[0];
      }
      $tableVerFunRoles = new Table;
      $tableVerFunRoles->setColum($tableVerFunRoles,
        [
          ID=>ID,
          TITLE_ROL=>TN_ROL_NOMBRE,
          'html-'.TITLE_PERMISOS =>['<input id="check-id" type="checkbox" name="roles[]" value="-id">', ID]
        ]);
        $opciones = [

        ];

        $allRoles = $this->gstRoles->getAllRoles()->toArray();
        $tableVerFunRoles->setItems($tableVerFunRoles,$allRoles);
        $tableVerFunRoles = $tableVerFunRoles->dps($tableVerFunRoles);
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,
                                  TITLE_MENU_PERMISOS=>'/Menu/2',
                                  TITLE_LISTA_P_FUN_ROLES=>PATH_PERMISOS_LISTA_FUNCIONESROLES,
                                  TITLE_VISUALIZAR_ROLES_FUNCIONES=>'']; 

        $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LISTA_PERMISO_MODULO;
        return view (PATH_VISUALIZAR_ROLES,[SFT_LIB_MOD=>[SFT_LIB_MOD_PERMISOS],
                    TABLE=>$tableVerFunRoles,'opc'=>json_encode($opciones),
              'check'=>json_encode($queryRol),IDFUNCION=>$_GET[ID]]);
    }
  }

  /**
  * Función que edita un item de la lista de roles de una funcion.
  * @category PermisosController
  * @package Modulo Permisos
  * @version 1.0
  * @author Samuel Beltran
  */
  public function updateFunRoles()
  {
    if (count($_POST) <= 2) {
      $_SESSION[DATA][MSG] = MSG_PERMISOS_NO_SELECCIONADO;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_PERMISOS_VISUALIZAR_ROLES.'?id='.$_POST[IDFUNCION]);
    }else{
      $jsonRoles = json_encode($_POST[ROLES]);
      $response = $this->gstFunciones->updateColumnValue($_POST[IDFUNCION],$jsonRoles,ROLES);
      if (!$response) {
        $_SESSION[DATA][MSG] = MSG_NO_ACTUALIZA;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect(PATH_PERMISOS_LISTA_FUNCIONESROLES);
      }
      $_SESSION[DATA][MSG] = MSG_FUNCION_ACTUALIZADA;
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect(PATH_PERMISOS_LISTA_FUNCIONESROLES);  
    }
    
    
  }
}

