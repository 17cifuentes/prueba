<?php

namespace App\Http\Controllers\Roles;
use App\Http\Controllers\Controller;
use App\Http\Clases\Table;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstRoles;
use App\Roles;


//use App\Http\Controllers\Cliente\ClientesController;
//use Illuminate\Http\Request;
//use URL;
//use Auth;
/*
*Autor: Cristian Cifuentes
*Modulo: Modulos
*Versiòn: 1.0
*Descripciòn: Controllador Roles del sistema.
*/
class RolesController extends Controller
{
	private $gstRoles;
	private $table;
	private $gstCliente;

	public function __construct(GstRoles $gstRoles, Table $table,GstCliente $gstCliente )
	{
		$this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);

		session_start();

		$this->gstRoles = $gstRoles;

		$this->table = $table;

		$this->gstCliente = $gstCliente;

	}

	/*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada:
	*Salida: Vista de formulario para ser llenado
	*Descripciòn: Funcion que muestra un formulario con 
	*	dos campos vacios y dos "seleccione"
	*/
	public function crearRol()
	{
		$_SESSION[DATA][BREAD] = 	[INICIO=>PATH_HOME,
									 MENU_ROLES=>'../Menu/4',
	    							 CRE_ROL=>""]; 

	    $_SESSION[DATA][CLIENTE_TITLE] = CRE_ROL;

		$dataRoles = $this->gstRoles->getAllRoles();

		$dataClientes = $this->gstCliente->getAllClientes();

		return view('core.Roles.crearRoles', compact('dataRoles','dataClientes'));
	}

		/*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada:
	*Salida: Muestra el menu de roles despues del Registro de un rol
	*Descripciòn: Funcion que evalua si el nombre del rol a crear 
	* ya existe en la tabla,inserta datos en la tabla "tn_sft_roles"
	* y finalmente retorna a la vista de menu.
	*/
	public function crearRolProcess()
	{
		if(isset($_POST[TN_ROL_NOMBRE]) && isset($_POST[TN_ROL_DESCRIPCION]) && isset($_POST[TN_USU_CLIENTE_ID]) && isset($_POST[TN_EST_ROL]))
		{
			if(strlen($_POST[TN_ROL_NOMBRE]) > 250 || strlen($_POST[TN_ROL_DESCRIPCION]) > 250) 

			{
				$_SESSION[DATA][MSG] = MSN_LIM_CHAR;

		        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

		        return redirect(FUN_ROL_CRE_ROL);

			}else{

				$dataRoles = $this->gstRoles->findRole($_POST[TN_ROL_NOMBRE]);

				if(count($dataRoles) > 0){

					$_SESSION[DATA][MSG] = ERR_ROL;

			        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

				}else{
					$this->gstRoles->insertRol();

					$_SESSION[DATA][MSG] = SUCC_ROL;

			        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
				}
				return redirect('/Menu/4');	
				}
		}else{

			$_SESSION[DATA][MSG] = ERR_CRE_ROL;

	        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

	        return redirect(FUN_ROL_CRE_ROL);
		}
	}

		/*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada:
	*Salida: Muestra una lista de los roles
	*Descripciòn: Funcion para listar los roles con id, nombre
	* descripcion, cliente, estado, y dos botones para cambiar estado
	* y para editar el registro.
	*/
    public function lstRoles()
    {
	    $tableRoles = new Table;

	    $tableRoles->setColum($tableRoles,
	      [
			'id'=>'id',
	        'Nombre'=>TN_ROL_NOMBRE,
	        'descripcion'=>TN_ROL_DESCRIPCION,
	        'cliente'=>TN_NOMBRE_CLIENTE,
	        'Estado' => TN_EST_ROL
	      ]);
	    $opciones = 
	    [
	        'Editar'=>FUN_ROL_EDI_ROL,
	        'val-'.'Cambiar Estado'=>FUN_ROL_CHA_EST
	    ];
	    $tableRoles->setItems($tableRoles,$this->gstRoles->getRolesClienteJoin()->toArray());
	    $tableRoles = $tableRoles->dps($tableRoles);
	    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_ROLES=>'../Menu/4',
	    	LST_ROL=>""]; 
	    $_SESSION[DATA][CLIENTE_TITLE] = LST_ROL;
	    return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableRoles,'opc'=>json_encode($opciones)]);
    }


    /*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada: 
	*Salida: vista de formalario
	*Descripciòn: Esta funcion muestra un formulario
	*con datos en sus campos, y permite al usuario
	* actualizar los campos.
	*/
    public function editarRoles()
    {
	    	if(!isset($_GET['id']))
	    	{
	    		$_SESSION[DATA][MSG] = MSG_UNF_ROL;

	        	$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

	        	return redirect(FUN_ROL_LST_ROL);

	    	}
	    	if(!filter_var($_GET['id'],FILTER_VALIDATE_INT)){

	    		$_SESSION[DATA][MSG] = MSG_NUM_ROL;

	        	$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

	        	return redirect(FUN_ROL_LST_ROL);
	    	}

	    	$rol = $this->gstRoles->getRole($_GET['id']);

	    	if($rol == null){

	    		$_SESSION[DATA][MSG] = MSG_UNF_ROL;

	        	$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

	        	return redirect(FUN_ROL_LST_ROL);

	    	}else{
    			$clientes = $this->gstCliente->getAllClientes();

		    	$_SESSION[DATA][BREAD] = 
			    [
			    	INICIO=>PATH_HOME,

			    	MENU_ROLES=>'../Menu/4',

			    	LST_ROL=>FUN_ROL_LST_ROL,

			    	EDI_ROL=>""
			    ]; 

			    $_SESSION[DATA][CLIENTE_TITLE] = EDI_ROL;

		    	return view ('core.Roles.editarRoles', compact('rol','clientes'));
	    	}
	    	
	    
    }

        /*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada: 
	*Salida: Muestra la vista del listar despues de actualizar los datos
	*Descripciòn: Esta funcion actualiza los datos de la tabla "tn_sft_roles",
	* finalmente devuelve al usuario a la vista de la lista, y muestra un mensaje
	* de actualizacion exitosa.
	*/
    public function editarRolesProcess()
    {
   
		if(isset($_POST[TN_ROL_NOMBRE]) && isset($_POST[TN_ROL_DESCRIPCION]) && isset($_POST[TN_USU_CLIENTE_ID]) && isset($_POST[TN_EST_ROL]))
		{
			if(strlen($_POST[TN_ROL_NOMBRE]) > 250 || strlen($_POST[TN_ROL_DESCRIPCION]) > 250) 
			{
				$_SESSION[DATA][MSG] = MSN_LIM_CHAR;

		        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

		        return redirect(FUN_ROL_EDI_ROL.'?id='.$_POST['id']);

			}else{
				//revisar por que no funciona
				$response = $this->gstRoles->editarProcess($_POST);
				if($response){
					
	    			$_SESSION[DATA][MSG] = SUCC_ROL;

	     			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
	    			
				}else{

					$_SESSION[DATA][MSG] = ERR_EDI_ROL;

	     			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
				}
	    	return redirect(FUN_ROL_LST_ROL);
			}

		}else{
			
			$_SESSION[DATA][MSG] = ERR_EDI_ROL;

			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

	    	return redirect(FUN_ROL_LST_ROL);

			}

    }

        /*
    *Autor: Samuel Beltran
    *Modulo: Roles
    *Versiòn: 1.0
    *Entrada:
    *Salida: Devuelve la vista con la lista de roles con 
    * su nuevo cambio de estado.
    *Descripciòn: Funcion para hacer un cambio de estado de "Activo" a 
    * "Inactivo" y viceversa.
    */
    public function cambiarEstado()
    {
    	$this->gstRoles->changeEstado($_GET['id'],TN_EST_ROL);

    	$_SESSION[DATA][MSG] 	= SUCC_EST_ROL;

        $_SESSION[DATA][TYPE] 	= NOTIFICACION_SUCCESS;

    	return redirect(FUN_ROL_LST_ROL);
    }
}
