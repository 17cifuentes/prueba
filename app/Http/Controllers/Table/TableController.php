<?php

namespace App\Http\Controllers\Table;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Gst\GstTable;
use App\Http\Clases\Table;
use App\Http\Clases\FuncionesGenerales;
class TableController extends Controller
{
	private $gstTable;
    /* Constructor de la clase
    * @category Funcion
    * @package Table
    * @version 1.0
    * @author Cristian Cifuentes <ccifuentes@nexura.com>
    */
	public function __construct(GstTable $gstTable){
        $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
		session_start();
		$this->gstTable = $gstTable;
	}
    /* Función para generar tabla desde la base de datos con componente LIST
    * @category Funcion
    * @package Table
    * @version 1.0
    * @author Cristian Cifuentes <ccifuentes@nexura.com>
    */
    public function generateTable($id){
    	$table = $this->gstTable->getTable($id);
    	$_SESSION[CLIENTE_TITLE] = $table->lista_nombre;
		$_SESSION[DATA][BREAD] = FuncionesGenerales::getBreadByJson($table->rastro_miga);
    	$colTable = $this->gstTable->getColTable($id);
    	$colOpcTable = $this->gstTable->getColOpcTable($id);
    	$path =  $table->namespace.$table->controllador;
    	$archivoJs = $table->archivo_js;
        $gst = new $path;
    	$method = $table->method;
    	$dataT = $gst->$method();
    	$pathEntidad = "App\\".$table->entidad;
        $entidad = new $pathEntidad;
        $excluedes = json_decode($table->methodJoin,true)[0];
        $dataTable = FuncionesGenerales::listaOrderDataJoin($entidad,$dataT,$excluedes["excludes"]);
        $table = new Table;
        $columnas = $this->gstTable->setColum($colTable);
    	$table->setColum($table,$columnas);
        $opciones = $this->gstTable->setOpc($colOpcTable);
        $orderArray = array_values($dataTable);
        $table->setItems($table,$orderArray);
        $table = $table->dps($table);
        return view(PATH_LIST,[TABLE=>$table,SFT_LIB_MOD=>[$archivoJs],'opc'=>json_encode($opciones)]);
    }
}
