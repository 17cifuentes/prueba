<?php

namespace App\Http\Controllers\Reportes;
use App\Http\Controllers\Controller;
use App\Http\Clases\FuncionesDB;
use App\Http\Gst\GstPermisos;
use App\Http\Gst\GstReportes;
use App\Http\Gst\GstGeneral;
use App\TipoGrafico;
use Auth;



//use App\Http\Clases\FuncionesGenerales;
//use Illuminate\Http\Request;
//use URL;


class ReportesController extends Controller
{
	private $gstgGen;
    private $gstPermisos;
    private $gstReportes;
	public function __construct(GstReportes $gstReportes,GstPermisos $gstPermisos,GstGeneral $gstgGen){
        $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
		session_start();
		$this->gstgGen = $gstgGen;
        $this->gstPermisos = $gstPermisos;
        $this->gstReportes = $gstReportes;
	}

/**
 * Conrtrolador de reportes, funcion que pintar formulario de reportes 
 * @category modulo reportes
 * @package SGD
 * @version 1.0
 * @author Cristian Cifuentes
 */
    public function crearReporte(){
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_REPORTES=>'/Menu/29',CREAR_REPORTE=>''];
        $modulos = $this->gstPermisos->getAllRolesMod(Auth::user()->rol_id);
        $opcReporte = $this->gstReportes->getOpcionesReportes();
        $operaciones = $this->gstReportes->getAllOperaciones();
        $graficos = FuncionesDB::consultarRegistrosTotales(new TipoGrafico);
        return view(ROUTE_CREATE_REPORTE,['modulos'=>$modulos,'opcReporte'=>$opcReporte,'operaciones'=>$operaciones,'graficos'=>$graficos,SFT_LIB_MOD=>[SFT_LIB_MOD_REPORTES]]);
    }
/**
 * Conrtrolador de reportes, funcion process para registrar reportes en el sistema 
 * @category modulo reportes
 * @package SGD
 * @version 1.0
 * @author Cristian Cifuentes
 */    
    public function generarReporte(){

        if($_POST['tipo'] != ""){
            if($_POST['tipo'] == 'Reporte'){
                $reporte = $this->gstReportes->getReporte($_POST);
                if($reporte){

                    $datos = $_POST;
                    $table = view(ROUTE_TABLE_REPORTE,[TABLE=>$reporte])->render();
                    $modulos = $this->gstPermisos->getAllRolesMod(Auth::user()->rol_id);
                    $opcReporte = $this->gstReportes->getOpcionesReportes();
                    $operaciones = $this->gstReportes->getAllOperaciones();
                    $graficos = FuncionesDB::consultarRegistrosTotales(new TipoGrafico);
                    return view(ROUTE_CREATE_REPORTE,['modulos'=>$modulos,'opcReporte'=>$opcReporte,'datos'=>$datos,'table'=>$table,'operaciones'=>$operaciones,'graficos'=>$graficos,SFT_LIB=>[SFT_DATATABLE,SFT_GRAFICOS],SFT_LIB_MOD=>[SFT_LIB_MOD_REPORTES]]);    
                }else{
                    $_SESSION[DATA][MSG] = MSG_NO_GENERAR_REPORTE;
                    $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                }
                
            }else if($_POST['tipo'] == 'Grafico'){
                $reporte = $this->gstReportes->getReporte($_POST);
                if(!$reporte){
                    $_SESSION[DATA][MSG] = MSG_NO_GENERAR_REPORTE;
                    $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                }else{
                    $_POST[TITULO_GRAFICO] = 'Reportes';
                    $_POST[CONTENEDOR_GRAFICO] = 'contenedor';
                    $_POST[DATA_GRAFICO] = $reporte['data'];
                    $_POST[VALOR_GRAFICO] = $reporte['valor'];
                    $modulos = $this->gstPermisos->getAllRolesMod(Auth::user()->rol_id);
                    $opcReporte = $this->gstReportes->getOpcionesReportes();
                    $operaciones = $this->gstReportes->getAllOperaciones();
                    $graficos = FuncionesDB::consultarRegistrosTotales(new TipoGrafico);
                    return view(ROUTE_CREATE_REPORTE,['modulos'=>$modulos,'opcReporte'=>$opcReporte,'datos'=>$_POST,'operaciones'=>$operaciones,'graficos'=>$graficos,SFT_LIB=>[SFT_DATATABLE,SFT_GRAFICOS],SFT_LIB_MOD=>[SFT_LIB_MOD_REPORTES]]); 
                }
                   
            }    
        }else{
            $_SESSION[DATA][MSG] = MSG_SELECCION_REPORTE;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        }
        return redirect(ROUTE_GENERATE_REPORTE);
        
        
    }
    public function getReporteMod(){
        $opciones = $this->gstReportes->getReportesModUsu($_GET['id'],Auth::user()->id,Auth::user()->cliente_id);
        return $opciones[0]->columnas;
    }

}
