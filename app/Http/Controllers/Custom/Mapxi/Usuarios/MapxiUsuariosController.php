<?php

namespace App\Http\Controllers\Custom\Mapxi\Usuarios;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Cliente\ClientesController;
use App\Http\Gst\Custom\Mapxi\Usuario\GstMapxiUsuarios;
use App\Http\Clases\Table;
use Auth;
use URL;
/*
*Autor: Cristian Cifuentes
*Modulo: Permisos
*Versiòn: 1.0
*Descripciòn: Controllador de permisos dentro del sistema
*/
class MapxiUsuariosController extends Controller
{
	private $gstMapxiUsuarios;
	public function __construct(GstMapxiUsuarios $gstMapxiUsuarios)
  	{
      session_start();
    	$this->gstMapxiUsuarios = $gstMapxiUsuarios;
  	}
   	public function lstUsuario()
   	{
   		$_SESSION['data'][CLIENTE_TITLE] = MAPXI_LST_USUS_APP;
      $_SESSION['data'][BREAD] = [INICIO=>PATH_HOME,MAPXI_MENU_USUS_APP=>URL::previous(),MAPXI_LST_USUS_APP=>""];  
      $tableUsu = new Table;
      $tableUsu->setColum($tableUsu,
        [
          MAPXI_PERSONA_NOMBRES=>MAPXI_TN_PERSONA_NOMBRES,
          MAPXI_PERSONA_CORREO=>MAPXI_TN_PERSONA_CORREO,
          MAPXI_PERSONA_CEL=>MAPXI_TN_PERSONA_CEL,
          MAPXI_USU_CODPROMOCIONAL=>MAPXI_TN_USU_CODPROMOCIONAL,
				  MAPXI_USU_CREATE=>MAPXI_TN_USU_CREATE,
				  MAPXI_ESTADOS_NOMBRE=>MAPXI_TN_ESTADOS_NOMBRE,
				  "Cambiar estado-"=>[
					   "url"=>"Mapxi/Usuarios/cambiarestado",
					   "dataItem"=>MAPXI_TN_PERSONA_ID,
					   "texto"=>"<i class='material-icons'>settings</i>",
					   "atributos"=>"class='btn bg-amber waves-effect'"
				  ],
				  "Eliminar-"=>[
					"url"=>"Mapxi/Usuarios/cambiarestado",
					"dataItem"=>MAPXI_TN_PERSONA_ID,
					"texto"=>"<i class='material-icons'>warning</i>",
					"atributos"=>"class='btn bg-red waves-effect'"
				]
            ]
        );
        $usersApp = $this->gstMapxiUsuarios->getUserApp();
        $tableUsu->setItems($tableUsu,$usersApp["respuesta"]);
        $tableUsu = $tableUsu->dps($tableUsu);
        return view(PATH_MAPXI_VIEW_USERAPP,[TABLE=>$tableUsu]);
   	}
}
