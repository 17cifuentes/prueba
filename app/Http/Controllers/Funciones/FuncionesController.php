<?php

namespace App\Http\Controllers\Funciones;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Table;
use App\User;
use Auth;
use URL;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controllers\Cliente\ClientesController;
use App\Http\Gst\GstFunciones;
use App\Http\Gst\GstModulos;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstRoles;
/*
*Autor: Cristian Cifuentes
*Modulo: Modulos
*Versiòn: 1.0
*Descripciòn: Controllador Funciones del sistema.
*/
class FuncionesController extends Controller
{
    private $gstFunciones;
    private $gstModulos;
    private $gstCliente;
    private $gstRoles;
    public function __construct(GstRoles $gstRoles,GstCliente $gstCliente,GstFunciones $gstFunciones, GstModulos $gstModulos){
        $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
        session_start();
        $this->gstFunciones = $gstFunciones;
        $this->gstModulos = $gstModulos;
        $this->gstCliente = $gstCliente;
        $this->gstRoles = $gstRoles;
    }

    public function listarFunciones(){

        $_SESSION[DATA][CLIENTE_TITLE] = 'Listar funciones';
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú funciones'=>'/Menu/12','Listar funciones'=>"#"];
        $estado_funcion=" ";
        $tableFun = new Table;
        $tableFun->setColum($tableFun,
            [
                'id' => 'id',
                'Nombre'=>'fun_menu_nombre',
                'Descripción'=>'fun_descripcion',
                'Ruta'=>'fun_ruta',
                'Fecha'=>'created_at',
                'Estado'=>'estado_funcion'
            ]
        );
         $opciones = 
            [
                'Editar'=> 'Funciones/editarFunciones',
                'val-Cambiar estado'=> 'Funciones/cambiarEstadoFuncion',
                'val- Eliminar'=>'Funciones/eliminarFuncion'
                //'Funciones Activas'=> 'Funciones/listarFunciones',
                //'Funciones Inactivas'=> 'Funciones/listarFunciones'
            ];
            if($estado_funcion == " ")
            {
                $datos = $this->gstFunciones->getAllFunciones()->toArray();
            }else
            {
                $datos = $this->gstFunciones->funcionEstado($estado_funcion);
            }

        $tableFun->setItems($tableFun,$datos);
        $tableFun = $tableFun->dps($tableFun);
        return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableFun,'opc'=>json_encode($opciones)]);
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para enviar a la vista de crear funciones
    */  
    public function crearFunciones()
    {
    
    $_SESSION[DATA][CLIENTE_TITLE] = 'Crear Funciones'; 
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menù Funcionalidades'=>'/Menu/12','Crear Funciones'=>""];
    //Nombre/VALOR A DAR/ 
        $clientes = $this->gstCliente->getAllClientes()->toArray();
        
        $modulos= $this->gstModulos->getModulos();

        $roles = $this->gstRoles->getAllRoles()->toArray();
        

         return view ('core.Funciones.crearFunciones',compact('modulos','clientes','roles'));
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para crear funciones.
    */
    public function crearFuncion()
    {
        if(strlen($_POST['fun_nombre']) > 250 || strlen($_POST['fun_menu_nombre']) > 255 || strlen($_POST['fun_constante']) > 255 || strlen($_POST['fun_descripcion']) > 255)
        {
            $_SESSION[DATA][MSG] = ERROR_CANT_CARACTERES_FUN;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(FUN_CREAR_FUN);
       }else{
        
        $roles = $_POST['roles'];

        $roles = json_encode($roles);

        unset($_POST['roles']);
        
        unset($_POST['_token']);

        $_POST['roles'] = $roles;
        
        $_SESSION[DATA][MSG] = 'Registro de funcion exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstFunciones->InsertFuncion($_POST);
        return $this->listarFunciones();
        }
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada: id: de la funcion
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para cambiar estado de las funciones
    */  
    
    public function cambiarEstadoFuncion(){

        $_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstFunciones->CambiarEstado($_GET['id'],'estado_funcion');
        return redirect(FUN_LST_FUN);
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada:id identificador de usuario
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para enviar a la vista de editar funciones
    */

    public function editarFunciones(){

        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menù Funcionalidades'=>'/Menu/12','Listar Funciones'=>'listarFunciones','Editar Funciones'=>'#'];

        $modulos= $this->gstModulos->getModulos();
        $funcion= $this->gstFunciones->getFuncion($_GET['id']);

        return view ('core.Funciones.editarFunciones',compact('modulos','funcion'));

    }
    public function eliminarFuncion()
    {
        $this->gstFunciones->eliminarFuncion($_GET);
        return $this->listarFunciones();
    }   

    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada: $_POST[] todos los datos enviados desde la vista de editar
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para editar o actualizar en la base de datos
    */

    public function editarFuncionProcess()
    {
        if(strlen($_POST['fun_nombre']) > 250 || strlen($_POST['fun_menu_nombre']) > 255 || strlen($_POST['fun_constante']) > 255 || strlen($_POST['fun_descripcion']) > 255)
        {
            $_SESSION[DATA][MSG] = ERROR_CANT_CARACTERES_FUN;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(FUN_LST_FUN);
       }else{

        $id=$_POST['id'];
        unset($_POST['_token']);
        unset($_POST['id']);
        $_SESSION[DATA][MSG] = 'Editado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstFunciones->EditarFuncion($id,$_POST);
        return $this->listarFunciones();
        }
    }
}





















