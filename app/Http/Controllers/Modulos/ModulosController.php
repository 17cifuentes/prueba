<?php

namespace App\Http\Controllers\Modulos;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Cliente\ClientesController;
use Illuminate\Http\Request;
use App\Http\Clases\Table;
use App\Http\Clases\Formulario;
use App\Http\Gst\GstModulos;
use App\Http\Gst\GstRoles;
use Auth;
use Config;
use App\Modulos;
use URL;
/*
*Autor: Cristian Cifuentes
*Modulo: Modulos
*Versiòn: 1.0
*Descripciòn: Controllador de modulos del sistema.
*/
class ModulosController extends Controller
{
  private $gstModulos;
  private $gstRoles;
  /*
  *Autor: Cristian Cifuentes
  *Modulo: Modulos
  *Versiòn: 1.0
  *Entrada:GstModulos = clase de gestion de modulos
  *Salida:
  *Descripciòn: Constructor de modulos para inyeccion de dependencias.
  */

  public function __construct(GstModulos $gstModulos, GstRoles $gstRoles)
  {
    $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
    session_start();
    $this->gstRoles = $gstRoles;
    $this->gstModulos = $gstModulos;
  }
  /*
  *Autor: Cristian Cifuentes
  *Modulo: Modulos
  *Versiòn: 1.0
  *Entrada:
  *Salida:Vista de lista de todos lso modulos del sistema
  *Descripciòn: Funcion que lista todos los modulos del sistema
  */
	 public function lstModulos()
  {
    $tableMod = new Table;
    $tableMod->setColum($tableMod,
      [
        'id'=>ID,
        MODULOS_NOMBRE=>TN_MODULOS_NOMBRE,
        MODULOS_DESCRICION=>TN_MODULOS_DESCRIPCION,
        MODULOS_ICONO=>TN_MODULOS_ICONO,
        'Core'=>TN_MODULOS_SFT,
        'Estado' => 'estado_modulo'

      ]
    );
    $opciones = 
    [
        'Editar'=>'Modulos/editarModulo',
        'val-'.'Cambiar Estado'=>'Modulos/cambiarEstado'
    ];

    $tableMod->setItems($tableMod,$this->gstModulos->getModulos()->toArray());
    $tableMod = $tableMod->dps($tableMod);
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_MODULOS=>'/Menu/1',LST_MODULOS=>""]; 
    $_SESSION[DATA][CLIENTE_TITLE] = LST_MODULOS;
    return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableMod,'opc'=>json_encode($opciones)]);
  }

  public function createModulo(){
/*    $_SESSION[DATA][CLIENTE_TITLE] = TITLE_MODULOS_CREAR;
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_MODULOS=>URL::previous(),TITLE_MODULOS_CREAR=>""]; 
    $form = new Formulario;    
    
    $fiel1 = $form->setFielset(COL_LG_12,TITLE_MODULOS_CREAR);
    $fiel1 = $form->setInput($fiel1,"",INPUT_TEXT,COL_LG_4,MODULOS_NOMBRE,TN_MODULOS_NOMBRE);
    $fiel1 = $form->setInput($fiel1,"",INPUT_TEXT,COL_LG_4,MODULOS_DESCRICION,TN_MODULOS_DESCRIPCION);
    $fiel1 = $form->setInput($fiel1,"",INPUT_TEXT,COL_LG_4,MODULOS_ICONO,TN_MODULOS_ICONO);
    $modSft = [
      [ID=>SI,NOMBRE=>SI],
      [ID=>NO,NOMBRE=>NO]
    ];
    $modSft[RELACION] = [ID,NOMBRE];
    $fiel1 = $form->setInput($fiel1,"",INPUT_SELECT,COL_LG_4,TITLE_MODULOS_SISTEMA,TN_TITLE_MODULOS_SISTEMA,'',$modSft);

    $form->addField($form,$fiel1);
    
    $form->col = COL_LG_12;
    $form->action = 'createModuloProcess';
    $form->method = GET;

    $form = $form->render($form);*/
    $_SESSION[DATA][CLIENTE_TITLE] = TITLE_MODULOS_CREAR;
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_MODULOS=>'/Menu/1',TITLE_MODULOS_CREAR=>""]; 
    return view('core.Modulos.crearModulos');
  }
    
    /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: 
    *Salida: vista de formalario
    *Descripciòn: Esta funcion muestra un formulario
    *con datos en sus campos, y permite al usuario
    * actualizar los campos.
    */
    public function editarModulo()
    {
        if(!isset($_GET['id']))
        {
          $_SESSION[DATA][MSG] = 'Módulo no encontrado';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Modulos/lstModulos');

        }
        if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
          $_SESSION[DATA][MSG] = 'El Módulo no es número';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Modulos/lstModulos');
        }
        $lsModulo = $this->gstModulos->getModulo($_GET);
        if(count($lsModulo) == 0)
        {
          $_SESSION[DATA][MSG] = 'Módulo no encontrado';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Modulos/lstModulos');
        }else{
           
            $_SESSION[DATA][CLIENTE_TITLE] = 'Editar Modulo';
            $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_MODULOS=>'/Menu/1','Lista de modulos'=>'/Modulos/lstModulos', 'Editar modulo'=>''
          ];
            return view('core.Modulos.editarModulos',compact('lsModulo'));
      }
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: 
    *Salida: Muestra la vista del listar despues de actualizar los datos
    *Descripciòn: Esta funcion actualiza los datos de la tabla "tn_sft_modulos",
    * finalmente devuelve al usuario a la vista de la lista, y muestra un mensaje
    * de actualizacion exitosa.
    */
    public function editarModuloProcess()
    {
      unset($_POST['_token']);
      
        if(isset($_POST['mod_nombre']) && isset($_POST['mod_descripcion']) && isset($_POST['mod_icono']))
        {
              if(strlen($_POST['mod_nombre']) > 250 || strlen($_POST['mod_descripcion']) > 250) 
              {
                $_SESSION[DATA][MSG] = 'Ha excedido el limite de caracteres. Limite 250';
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Modulos/editarModulo?id='.$_POST['id']);

              }else{
                $id = $_POST['id'];
                $response = $this->gstModulos->processEditModulos($id ,$_POST);
                if($response){
                  $_SESSION[DATA][MSG] = 'El modulo se ha editado exitosamente';
                  $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
                }else{
                  $_SESSION[DATA][MSG] = 'El modulo no se ha editado';
                  $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                }
                 return redirect('/Modulos/lstModulos');
             }
        }else{
          $_SESSION[DATA][MSG] = 'El modulo no se ha editado';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('/Modulos/lstModulos');
        }
    }

     /*
    *Autor: Samuel Beltran
    *Modulo: Roles
    *Versiòn: 1.0
    *Entrada:
    *Salida: Muestra el menu de creacion de modulo despues de que 
    * un moduLo sea creado.
    *Descripciòn: Funcion que evalua si el nombre del rol a crear 
    * ya existe en la tabla,inserta datos en la tabla "tn_sft_roles"
    * y finalmente retorna a la vista de menu.
    */
    public function createModuloProcess()
    {
      unset($_POST['_token']);
      
      if(isset($_POST['mod_nombre']) && isset($_POST['mod_descripcion']) && isset($_POST['mod_icono']) && isset($_POST['mod_sft']))
      {
          if(!isset($_POST))
          {
            $_SESSION[DATA][MSG] = 'El modulo no se ha podido crear';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Modulos/createModulo');
           }
       if(strlen($_POST['mod_nombre']) > 250 || strlen($_POST['mod_descripcion']) > 250) 
          {
            $_SESSION[DATA][MSG] = 'Ha excedido el limite de caracteres. Limite 250';
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Modulos/createModulo');

          }else {
              $_POST['estado_modulo'] = 'Activo';

              $response = $this->gstModulos->crearModulo($_POST);
              if($response)
              {
                $_SESSION[DATA][MSG] = 'El modulo se ha creado exitosamente';
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;  
              }else{
                $_SESSION[DATA][MSG] = 'El modulo no se ha podido crear';
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
              }
            
              return redirect('/Modulos/lstModulos'); 
        }
      }else{
         $_SESSION[DATA][MSG] = 'No se puede crear el Módulo';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('Modulos/createModulo');
      }
     }

   /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada:
    *Salida: Devuelve la vista con la lista de modulos con 
    * su nuevo cambio de estado.
    *Descripciòn: Funcion para hacer un cambio de estado de "Activo" a 
    * "Inactivo" y viceversa.
    */
    public function cambiarEstado()
    {
      $this->gstModulos->cambEstado($_GET['id'],'estado_modulo');
      $_SESSION[DATA][MSG] = CAMBIO_ESTADO_SUCCESS;
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect('/Modulos/lstModulos');
    }
}
