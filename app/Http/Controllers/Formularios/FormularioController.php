<?php

namespace App\Http\Controllers\Formularios;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Table;
use App\User;
use Auth;
use URL;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controllers\Cliente\ClientesController;
use App\Http\Gst\GstFormularios;
use App\Http\Gst\GstGeneral;
/*
*Autor: Cristian Cifuentes
*Modulo: Modulos
*Versiòn: 1.0
*Descripciòn: Controllador para generar formularios dinamicos desde bd.
*/
class FormularioController extends Controller
{
	private $gstForm;
    private $gstGeneral;
	public function __construct(GstGeneral $gstGeneral,GstFormularios $gstForm){
        $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => []]);
		session_start();
		$this->gstForm = $gstForm;
        $this->gstGeneral = $gstGeneral;
	}
    public function generateForm($id){ 
    	$data = $this->gstForm->getForm($id);
        return view('core.modelForm',["form"=>$data["form"],"campos"=>$data["campos"]]);
    }
    public function FormSave($id,Request $request){
        $form = $this->gstForm->getFormByAtribbute(ID,$request['idForm']);
        $this->gstGeneral->validateData($request);
    	$model = "App\\".$form[0]->entidad;
        if(count($_FILES) > 0){
            foreach ($_FILES as $key => $value) {
                $idFile = $this->gstGeneral->saveFile($value,1,$form[0]->modulo,$form[0]->id_form,'A',Auth::user()->cliente_id);
                $src = $idFile->path;
                FuncionesGenerales::cargarFile($value,$src,'vacio.png');
                $files[$key] = $idFile->id;
                $keys[] = $key;
            }
        }
        if(isset($files)){
            $excep = array_merge(['_token','idForm'],$keys);
            $dataParcial = $request->except($excep);
            $data = array_merge($dataParcial,$files);
        }else{
            $data = $request->except(['_token','idForm']);
        }
        $id = $this->gstForm->saveForm(new $model,$data);
        $_SESSION[DATA][MSG] = 'Se registro correctamente';
    	$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    	return redirect(URL::previous());
    }
    public function FormUpdate($id,Request $request){
         $form = $this->gstForm->getFormByAtribbute(ID,$request['idForm']);
        $this->gstGeneral->validateData($request);
        $model = "App\\".$form[0]->entidad;
        if(count($_FILES) > 0){
            foreach ($_FILES as $key => $value) {
                $idFile = $this->gstGeneral->saveFile($value,1,$form[0]->modulo,$form[0]->id_form,'A',Auth::user()->cliente_id);
                $src = $idFile->path;
                FuncionesGenerales::cargarFile($value,$src,'vacio.png');
                $files[$key] = $idFile->id;
                $keys[] = $key;
            }
        }
        if(isset($files)){
            $excep = array_merge(['_token','idForm'],$keys);
            $dataParcial = $request->except($excep);
            $data = array_merge($dataParcial,$files);
        }else{
            $data = $request->except(['_token','idForm']);
        }
        $model = "App\\".$form[0]->entidad;
        $this->gstForm->updateForm(new $model,$data);
        $_SESSION[DATA][MSG] = 'Se edito correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect(URL::previous());
    }
    public function generateFormEdit($columna,$entidad,$formId){
		$data = $this->gstForm->getForm($formId);
		$model = "App\\".$entidad;
		$dataForm = $this->gstForm->getDataByAtribbute($model,$columna,$_GET['id']);
		return view('core.modelForm',["form"=>$data["form"],"campos"=>$data["campos"],"dataForm"=>$dataForm]);
    }
}





















