<?php

namespace App\Http\Controllers\General;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CambioEstado;
use App\Http\Gst\GstGeneral;
use App\Http\Clases\FuncionesGenerales;
use URL;
class GeneralController extends Controller
{
	private $gstgGen;
	public function __construct(GstGeneral $gstgGen){
		$this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => ['getMunicipioById']]);
        session_start();
		$this->gstgGen = $gstgGen;
	}
    /**
     * Función general para cambiar estado (se usa basicamente con el componente list.)
     * @category GeneralController
     * @package Modulo General
     * @version 1.0
     * @author Cristian Cifuentes.
     */
    public function cambiarEstado(){
    	$cambio =$this->gstgGen->getCambioEstado($_GET['id_estado']);
    	$entidad = new $cambio[0]->namespace;
    	$res = FuncionesGenerales::cambiarEstadoTable($entidad,$_GET['id'],$cambio[0]->columnas_tabla);
    	if($res == 1){
            $_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }else{
            $_SESSION[DATA][MSG] = 'Fallo al cambiar estado, intentelo de nuevo';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        }
		$_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    	return redirect( URL::previous());
    }

    /**
     * Función que hace una consulta por id_parent y la retorna en formato Json
     * @category GeneralController
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
    public function getMunicipioById()
    {
        $municipios = FuncionesGenerales::getWorldbyParent($_GET['id']);
        return json_encode($municipios);
    }
}
