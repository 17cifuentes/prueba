<?php
namespace App\Http\Clases;

use Illuminate\Http\Request;
use Auth;
use View;
/*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de formulario
    *Versiòn: 1.0
    *Descripciòn: Funciòn para crear un listado de datos en una tabla
    *
    */
class Table
{
	public $colum;
    public $columOpt;
	public $items;
	public $class;
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de Table
    *Versiòn: 1.0
    *Entradas: $table = Instancia de la clase Table, $colum = Columnas que debe llevar la table - array de donde el indice es el nommbre que se muestra y el valor es el name que se va desplegar
    *Salida: Instancia de la table cargada de columnas
    *Descripciòn: Funciòn para dar las columnas de una tabla
    *
    */
    public static function setColum($table,$colum)
    {
		$table->colum = $colum;
    	return $table;
    }
        /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de Table
    *Versiòn: 1.0
    *Entradas: $table = Instancia de la clase Table, $colum = Columnas que debe llevar la table - array de donde el indice es el nommbre que se muestra y el valor es el name que se va desplegar
    *Salida: Instancia de la table cargada de columnas
    *Descripciòn: Funciòn para dar las columnas de una tabla
    *
    */
    public static function setClass($table,$class)
    {
        $table->class = $class;
        return $table;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de Table
    *Versiòn: 1.0
    *Entradas: $table = instancia de la clase Table debe estar cargafas las columnas, $items = Arreglo de datos para agregar a la tabla
    *Salida: Instancia de la tabla cargada de intems
    *Descripciòn: Funciòn para agregar los intems a una tabla
    *
    */
    public static function setItems($table,$items)
    {
    	$c = count($table->colum);
    	$f = count($items);

        for($x = 0;$x < $f;$x++)
    	{
            foreach ($table->colum as $key => $value) {
    		  	$p = strstr($key, '-',true);
                if($p == "")
                {
                    $table->items[$key][] = $items[$x][$value]; 
                }else
                {
                    if($p == "html"){
                        if(gettype($value) == "string"){
                            $value = explode(",", $value);
                        }
                        $n = strstr($key, '-',false);
                        $nombre = str_replace('-',"", $n);
                        $valorNew = str_replace("-".$value[1],$items[$x][$value[1]],$value[0]);
                        $table->items[$nombre][] = $valorNew;         
                    }
                    
                }
			}
    	}
    	return $table;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de Table
    *Versiòn: 1.0
    *Entradas: $table = Instancia de la clase cargada de items
    *Salida: string de la tabla
    *Descripciòn: Funciòn para desplegar la tabla.
    *
    */
    public static function dps($table)
    {

		return View(PATH_VIEW_CLASS_TABLE,[TABLE=>(array)$table]);
    }
}
