<?php

namespace App\Http\Clases;
use Illuminate\Http\Request;
use App\Cliente;
use DB;
use Session;
use Redirect;
use Auth;
/*
*Autor: Cristian Cifuentes
*Modulo: Clase de Funciones BD
*Versiòn: 1.0
*Descripciòn: Clase para consultar, editar modificar o hacer movimientod en bases de datos
*/
class FuncionesDB
{
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $modelo = modelo de entidad, $data = Array con (indice)posicion igual al nombre de la columna a registrar y el valor correspondiente
    *Salida: instancia de clase setiada
    *Descripciòn: Funciòn registrar en la base de datos
    *
    */
    public static function registrar($modelo,$data){
        return FuncionesDB::verificador($modelo::insert($data));
    } 
    public static function registrarGetRegistro($modelo,$data){
        try {
            $modelo::insert($data);
            //FuncionesDB::verificador($modelo::insert($data));
        } catch (\Exception $e) {
            try {
                $data['cliente_id'] = Auth::user()->cliente_id;
                FuncionesDB::verificador($modelo::insert($data));
            } catch (\Exception $e) {
                return false;
            }
        }
        return $modelo::orderBy('id','DESC')->get()->first();

    } 
    
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $data = Boolean
    *Salida: Boolean
    *Descripciòn: Funciòn para validar procesos de consultas a la BD
    *
    */
    public  static function verificador($data){
        if($data == true or $data == 1)
            {
                Session::flash(NOTIFICACION_CORRECTO,NOTIFICACION_PROCESO_CORRECTO);  
                return 1;
            }else
            {
                Session::flash(NOTIFICACION_INCORRECTO,NOTIFICACION_PROCESO_INCORRECTO);  
                return 0;
            }
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $modelo = Modelo de entidad, $columna = columna de referencia, $valor = valor de busqueda
    *Salida: Colecciòn de consulta
    *Descripciòn: Funciòn realizar consulta simple de un dato en una entidad.
    *
    */
    public static function consultarRegistro($modelo,$columna,$valor)
    {
        $data = $modelo::select("*")->where($columna,"=",$valor)->get();
    	return $data;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $table = nombre de entidad, $columna = columna de referencia, $valor = valor de busqueda, $tabla = nombre de la table en BD,
    *Salida: Colecciòn de consulta
    *Descripciòn: Funciòn realizar consulta simple de un dato en una entidad.
    *
    */
    public static function VerificacionRegistro($table,$columna,$valor,$tabla,$valores)
    {
        $dato = FuncionesDB::consultarRegistro($table,$columna,$valor);
        if(!isset($dato[0]->id))
        {
            $tabla = FuncionesDB::ConocerTablaRegistrar($tabla,$valores);
            if($tabla->save())
            {
                Session::flash(NOTIFICACION_CORRECTO,NOTIFICACION_PROCESO_CORRECTO);  
                return 1;
            }else
            {
                Session::flash(NOTIFICACION_INCORRECTO,NOTIFICACION_PROCESO_INCORRECTO);  
                return 2;
            }
        }else
        {
                Session::flash(NOTIFICACION_INCORRECTO,NOTIFICACION_PROCESO_YA_EXISTE_REGISTRO);  
                return 3;
        }
    }  
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $modelo = nombre de entidad.
    *Salida: Colecciòn de consulta
    *Descripciòn: Funciòn que retorna todos los datos de una entidad
    *
    */  
    public static function consultarRegistrosTotales($modelo)
    {
        return $modelo::all();
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: 
    *Salida: 
    *Descripciòn:
    *
    */     
    public static function ConocerTablaRegistrar($valor,$valores)
    {
        switch ($valor) {
            case 'cliente':
                $tabla = new Cliente($valores);
                break;
            case 'proyecto':
                $tabla = new Proyecto;
                break;
            case 'usuario':
                $tabla = new User;
                break;                                
        }
        return $tabla;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $tabla = nombre de tabla en la bd, $columna = columna de valor maximo
    *Salida: string
    *Descripciòn: Funciòn que retorna el valor maximo de una columna
    *
    */     
    public static function consultarRegistroMaximo($tabla,$columna)
    {
    	$sql = DB::table($tabla)->Max($columna);
    	return $sql;
    }
    
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $tabla = nombre de la table ne BD, $columna = nombre de columna de referencia, $id = valor de busqueda, $data = array de donde el indice es el nombre de la columna y el valor es el registro a realizar.
    *Salida: Boolean 
    *Descripciòn: Funciòn para editar cualqueir tabla teniendo un identiiacdor como referencia
    *
    */ 
    public static function Editar($tabla,$columna,$id,$data){
        unset($data[TOKEN]);
         $respon = DB::table($tabla)
            ->where($columna,$id)
            ->update($data); 
          
         return FuncionesDB::verificador($respon);
    }    
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $data = colletion de dato unico, $name_estado = nombre de la columna estado, $accion = aaccion que se va realizar sobre el estado, $tabla = nombre de la tabla en BD, $id_name = id dentro de $data donde se debe actualizar el estado
    *Salida: 
    *Descripciòn:FUNCION PARA CAMBIAR EL ESTADADO DE UN REGISTRO DE UNA TABLA, DEVUELVE ALERTA AL LA VISTA SI EL ELEMENTO YA ESTA ACTIVO O NO
    *
    */     
    public static function cambiarestado($data,$name_estado,$accion,$tabla,$id_name){
        if($data[0]->$name_estado == $accion)
        {
            Session::flash(NOTIFICACION_INCORRECTO,NOTIFICACION_PROCESO_YA_REALIZADO.$accion);
        }else
        {
            DB::table($tabla)
            ->where($id_name, $data[0]->$id_name)
            ->update([$name_estado => $accion]);
            Session::flash(NOTIFICACION_CORRECTO,NOTIFICACION_PROCESO_CORRECTO .$accion);
        }        
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Funciones DB
    *Versiòn: 1.0
    *Entradas: $tabla = nombre de la tabla en la BD, $id_table = identifiacdor de referencia, $id = valor del registro a eliminar
    *Salida: 
    *Descripciòn:Funciòn para eliminar un registro de la BD
    *
    */     
    public static function eliminar($tabla,$id_table,$id){
            return DB::table($tabla)
            ->where($id_table, $id)
            ->delete();
                
    }
    
}
