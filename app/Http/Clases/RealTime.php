<?php

namespace App\Http\Clases;
use Illuminate\Http\Request;
use App\Cliente;
use DB;
use Session;
use Redirect;
use Pusher\Pusher;
/*
*Autor: Cristian Cifuentes
*Modulo: Clase de Funciones BD
*Versiòn: 1.0
*Descripciòn: Clase para consultar, editar modificar o hacer movimientod en bases de datos
*/
class RealTime
{
    /**
     * Metodo para configurar servidor pusher. funciones pusher terminadas en PS
     * @category RealTime
     * @package Componente
     * @version 1.0
     * @author Critian cifuentes
     */
    public static function confRealTimePusher(){
        $options = array(
            'cluster' => 'us2',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '909751c4db02622d014b',
            'ad98c0d8613f11795d0a',
            '761543',
            $options
        );
        return $pusher;
    }   
    /**
     * Metodo para conectarse a un canal y a un evento especifico
     * @category RealTime
     * @package Componente
     * @version 1.0
     * @author Critian cifuentes
     */
    public static function connectCanalPS($mensaje,$canal,$evento){
        $pusher = RealTime::confRealTimePusher();
        $data['message'] = $mensaje;
        $pusher->trigger($canal,$evento, $data);
    }
}
