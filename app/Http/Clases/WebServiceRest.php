<?php

namespace App\Http\Clases;

use Illuminate\Http\Request;
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase WebServiceRest
    *Versiòn: 1.0
    *Descripciòn: Funciòn gestionar servicios tipo rest
    *
    */
class WebServiceRest
{
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase WebServiceRes
    *Versiòn: 1.0
    *Entradas: $url = Url del servicio
    *Salida: Matriz de respuesta del servicio
    *Descripciòn: Funciòn para conusmir un servicio rest
    *
    */
    public static function getServicio($url)
    {
		$ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);
    	return $response;
    }
}
