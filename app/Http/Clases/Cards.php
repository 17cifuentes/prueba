<?php

namespace App\Http\Clases;

use Illuminate\Http\Request;
use View;
/*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Cards
    *Versiòn: 1.0
    *Descripciòn: Clase para generar cards HTML dinamicos
    */
class Cards
{
	public $col;
    public $url;
    public $color;
	public $title;
	public $subtitle;
    public $ico;
    public $cards;
    
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Cards
    *Versiòn: 1.0
    *Entradas: $cards = instancia de clase, data elementos de un cards
    *Salida: instancia de clase setiada
    *Descripciòn: Funciòn setiar datos de una cards
    *
    */
    public static function setCard($cards,$data)
    {
        $card = new Cards;
        (!isset($data[URL])) ? $card->url = JS_VOID : $card->url = $data[URL];
        (!isset($data[COL])) ? $card->col = COL_LG_4.COL_MD_4.COL_SM_4.COL_XS_6 : $card->col = $data[COL];
        (!isset($data[COLOR])) ? $card->color = SFT_COLOR : $card->color = $data[COLOR];
        (!isset($data[ICO])) ? $card->ico = SFT_DEFAULT_ICO : $card->ico = $data[ICO];
        $card->title = $data[TITLE];
        $card->subtitle = $data[SUBTITLE];
        $cards->cards[] = $card;
        return $cards;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase Cards
    *Versiòn: 1.0
    *Entradas: $card = instancia de clase Cards
    *Salida: html de cards 
    *Descripciòn: Funciòn desplegar html de un cards
    *
    */
    public static function dps($card){
        $html = "";
        foreach ($card->cards as $key => $card) {
            $dps =  View(PATH_INFOCARD,[COL=>$card->col,TITLE=>$card->title,SUBTITLE=>$card->subtitle,COLOR=>$card->color,ICO=>$card->ico,URL=>$card->url]);        
            $html .= $dps->render();
        }
        return $html;
    }
    
}
