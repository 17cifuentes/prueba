<?php

namespace App\Http\Clases;

use Illuminate\Http\Request;
use View;
/*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de formulario
    *Versiòn: 1.0
    *Descripciòn: Clase para generar formularios dinamicos
    */
class Formulario
{
	public $col;
    public $idForm;
    public $action;
    public $method;
	public $fieldSet;
	public $atributes;
    public $formulario;
    public $form = "";
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de formulario
    *Versiòn: 1.0
    *Entradas: $col = Columnas que ocupara el fielset, $nombre = Nombre del fielset se vizualiza en *la vista
    *Salida: Arreglo con ambas posiciones.
    *Descripciòn: Funciòn para crear un fielSet de un formulario
    *
    */
    public static function setFielset($col,$nombre,$idfiel = NULL,$add = null)
    {
		$fieldSet[COL]= $col;
        $fieldSet[NOMBRE] = $nombre; 
        $fieldSet[ADD] = $add; 
        $fieldSet[ID] = $idfiel; 
        return $fieldSet;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de formulario
    *Versiòn: 1.0
    *Entradas: $fielset = arreglo de fielset, $type= tipo de campo a añadir, $col = columnas que ocupa el input, $label = texto del input, $name = name del inpit para el submit, $data
    *Salida: Arreglo con ambas posiciones.
    *Descripciòn: Funciòn para añadir un campo a un fielset
    *
    */
    public static function setInput($fieldSet,$value,$type,$col,$label,$name,$subtype = null,$data = null,$atributes = null)
    {
        if($_SESSION['data']["conf"][0]->estructura_admin == 2){
            $col = 'col-lg-12';
        }else{
            $col = 'col-lg-6';
        }
        
        if($subtype == null){
                $subtype = $type;
        }
        if(in_array($type,[INPUT_TEXT,INPUT_NUMBER,CHECKBOX,INPUT_RADIO,INPUT_DATE,INPUT_FILE,INPUT_EMAIL,INPUT_PASSWORD,INPUT_COLOR,INPUT_HIDDEN]))
        {
           $fieldSet[CAMPO][] =  View(PATH_FORM_INPUTGENERAL,[TYPE=>$type,COL=>$col,LABEL=>$label,NAME=>$name,INPUT_VALUE=>$value,SUBTYPE=>$subtype,ATRIBUTES=>$atributes]);
           return $fieldSet;
        }else if($type == INPUT_TEXTAREA)
        {

        }else if($type == INPUT_SELECT)
        {
            $fieldSet[CAMPO][] =  View(PATH_FORM_SELECT,[COL=>$col,LABEL=>$label,NAME=>$name,DATA=>$data]);
            return $fieldSet;
        }
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de formulario
    *Versiòn: 1.0
    *Entradas: $fiel = arreglo fiel a ingresar, $form = instancia de la clase Formulario
    *Salida: Instancia de formulario cargada con un nuevo fielset.
    *Descripciòn: Funciòn para agregar un fielset a un formulario
    *
    */
    public function addField($form,$fiel)
    {
        return $form->fieldSet[] = $fiel;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Clase de formulario
    *Versiòn: 1.0
    *Entradas: $form = Instancia de clase Formulario cargada con almenos un filedset.
    *Salida: String del formulario.
    *Descripciòn: Funciòn para desplegar formulari
    *
    */
    public function render($form)
    {
        $form->formulario =  View(PATH_FORMULARIO,[FORM=>(array)$form])->render();
        $y = 0;
        foreach ($form->fieldSet as $key) {
            if(isset($key[ADD]) and $key[ADD] == true){
               $form->form .=  View(PATH_FIELDSET,[DATA=>$key])->render().FIELDSET_FIN;
               
            }else{
                $fieldSet[FIELDSET][] =  View(PATH_FIELDSET,[DATA=>$key])->render();
                $c = count($key[CAMPO]);
                $campo ="";
                for ($x = 0;$x < $c;$x++) {
                    $campo .= $key[CAMPO][$x]->render();
                }
                $form->form .= $fieldSet[FIELDSET][$y].$campo.FIELDSET_FIN;
                $y++;
            }
            
        }
        $form->formulario .= $form->form.FORMULARIO_FIN;
        return $form->formulario;
    }
    
}
