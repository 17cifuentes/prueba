<?php

namespace App\Http\Clases;
use App\Ciudad;
use App\Depto;
use Illuminate\Http\Request;
use Session;
use Mail;
use Auth;
use DB;
use App\Productos;
use App\ActividadUsuario;
use App\Cliente;
use App\Modulos;
use App\Roles;
use App\ReportesJoin;
use App\World;


use Dompdf\Dompdf;

class FuncionesGenerales
{
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Funciones Generales
    *Versiòn: 1.0
    *Entrada:$entidad, $datos(consultados con where),$exclude = array de relacione s que no se ejecutan
    *Salida:
    *Descripciòn: Cargar todas las relaciones de una entidad en un arreglo de indeice valor que esten conectadas auna misma tabla la definici{on quedaria nombreRelacion-columna (ideal para el componente table)
    */
    public static function listaOrderDataJoinRelMultiple($entidad,$data,$exclude){
        $all = get_class_methods($entidad); 
        $colum = $entidad->field;
        foreach ($all as $key => $value) {
            if(substr($value,0,3) == "sft"){
                $method[] = $all[$key];
            }
        }
        if(isset($method)){
            foreach ($exclude as $key => $value) {
            $response =array_search($value ,$method);
            if ($response) {
                unset($method[$response]);
            }
        }
        $componente = $data;
        foreach ($componente as $keys => $value){
            foreach ($method as $key => $meto) {
                $con = $value->$meto->fields;
                foreach ($con as $k => $v) {
                    if($v == 'id' ){

                    }else{
                        $data[$keys][$meto."-".$v] = $value->$meto->$v;   
                    }
                }
            }
        }
        return $data->toArray();
    }else{
        return $data->toArray();
    }
        
    }

    /*
    *Autor: Cristian Cifuentes
    *Modulo: Funciones Generales
    *Versiòn: 1.0
    *Entrada:$entidad, $datos(consultados con where),$exclude = array de relacione s que no se ejecutan
    *Salida:
    *Descripciòn: Cargar todas las relaciones de una entidad en un arreglo de indeice valor (ideal para el componente table)
    */
    public static function listaOrderDataJoin($entidad,$data,$exclude){
        $all = get_class_methods($entidad); 
        $colum = $entidad->field;
        foreach ($all as $key => $value) {
            if(substr($value,0,3) == "sft"){
                $method[] = $all[$key];
            }
        }
        if(isset($method)){
            foreach ($exclude as $key => $value) {
            $response =array_search($value ,$method);
            if($response) {
                unset($method[$response]);
            }
        }
        $componente = $data;
        foreach ($componente as $keys => $value){
            foreach ($method as $key => $meto) {
                $con = $value->$meto->fields;
                foreach ($con as $k => $v) {
                    if($v == 'id' ){

                    }else{
                        $data[$keys][$v] = $value->$meto->$v;   
                    }
                }
            }
        }
        return $data->toArray();
    }else{
        return $data->toArray();
    }
        
    }

    public static function listaOrderDataJoinBD($entidad,$exclude){
        $all = get_class_methods($entidad); 
        $colum = $entidad->field;
        foreach ($all as $key => $value) {
            if(substr($value,0,3) == "sft"){
                $method[] = $all[$key];
            }
        }
        if(isset($method)){
            foreach ($exclude as $key => $value) {
            $response =array_search($value ,$method);
            if ($response) {
                unset($method[$response]);
            }
        }
        $join = "";
        foreach ($method as $key => $meto) {
            $jo = ReportesJoin::where("nombre",$meto)->get();
            if(count($jo) > 0){
                $join .= $jo[0]->join." ";
            }
        }
        return $join;
    }else{
        return "";
    }
        
    }     
    public function getArchivo($view,$extencion,$prueba){
        header("Content-Type:   application/vnd.".$extencion."; charset=utf-8");
        header("Content-Disposition: attachment; filename=".$prueba.".xsl"); 
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        echo $view;
    }
    public static function getPdf($view){
       $dompdf = new Dompdf();
       $dompdf->loadHtml($view);
       $dompdf->setPaper('A4', 'landscape');
       $dompdf->render();
       $dompdf->stream();
    }
    public static function getBreadByJson($data){
        $rastro = json_decode($data);
        foreach ($rastro as $key => $value) {
            foreach ($value as $k => $v) {
                $arrayRastro[$k] = $v;
            }
        }
        return $arrayRastro;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Funciones
    *Versiòn: 1.0
    *Entrada:$idModulo = Id del modulo
    *Salida:
    *Descripciòn: Carga todas las funciones de un modulo.
    */
    public static function getByAtribbute($model,$atribute,$value)
    {
        return $model::where($atribute,$value)->get();
    }    
    //funcion para creear codigos aleatorios
    public static function generarCodigo($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }   
    // FUNCION PARA CARGAR LA CIUDAD EN UN SELECT SEGUN DEPARTAMENTO SELECCINADO
    public function cargarciudad(){
        extract($_POST);
        $ciudades = Ciudad::where("depto_id",$data["depto_id"])->get();
        $select = $this->cargarSelect($ciudades,"ciu_id","ciu_nombre");
        echo $select;
    }
    // FUNCION PARA CARGAR UN SELECT DE DEPARTAMETOS
    public function cargselectdepto(){

        extract($_POST);
        $deptos = Depto::all();
        $select = $this->cargarSelect($deptos,"depto_id","depto_nombre");
        echo $select;
    }    
    // FUNCION PARA REGISTRAR Y CARGAR SELECT CON NUEVO REGISTRO DE DEPARTAMENTO
    public function registrardepto(){
        extract($_POST);
        $dato = FuncionesDBController::registrar(new Depto,$data);
        $deptos = Depto::all();
        $select = $this->cargarSelect($deptos,"depto_id","depto_nombre");
        echo $select;
    }  
    // FUNCION PARA REGISTRAR Y CARGAR SELECT CON NUEVO REGISTRO DE CIUDAD
    public function registrarciu(){
        extract($_POST);
        $dato = FuncionesDBController::registrar(new Ciudad,$data);
        $ciudades = Ciudad::where("depto_id",$data["depto_id"])->get();
        $select = $this->cargarSelect($ciudades,"ciu_id","ciu_nombre");
        echo $select;
    }   
    // FUNCION PARA MANDAR CORREOS SEGUN PLANTILLA Y DATA ---pendiente
    public static function correo($plantilla,$data,$para,$mensaje,$asunto,$copia){
        Mail::send($plantilla,['data'=>$data],function ($message) use ($copia,$asunto,$para){
            $message->subject($asunto);
            if($copia != ""){
                $message->cc($copia);    
            }
            $message->to($para);
        });  
    } 
    // FUNCION PARA EVALUAR SI UN DATO LE PERTENECE AL USUARIO ACTUAL
    public static function datosrol($data){
        if(empty($data[0])){
            return "No";
        }else{
            return "Si";
        }
    }  
    // FUNCION PARA CARGAR ARCHIVOS A LA BASE DE DATOS
    public static function cargararchivo($file,$path,$archivodefecto){
        if($file[NAME] == null){
            $src = $path.$archivodefecto;
        }else{
            $nom = $file[NAME];
            $dir_temp = $file["tmp_name"];
            $src = $path.$nom;
            move_uploaded_file($dir_temp, $src);
        }
        return $src;
    }
    // FUNCION PARA CARGAR ARCHIVOS A LA BASE DE DATOS
    public static function cargarFile($file,$path,$archivodefecto){
        if($file[NAME] == null){
            $src = $path.$archivodefecto;
        }else{
            $nom = $file[NAME];
            $dir_temp = $file["tmp_name"];
            move_uploaded_file($dir_temp,$path);
        }
        return $path;
    }
    public function cargararchivos($file,$path,$archivodefecto){
        if($file[NAME] == null){
            $src = $path.$archivodefecto;
        }else{
            $nom = $file[NAME];
            $dir_temp = $file["tmp_name"];
            $src = $path.$nom;
            move_uploaded_file($dir_temp, $src);
        }
        return $src;
    }
    // FUNCION PARA SUBIR IMAGEN CON AJAX Y MOSTRARLA A TIEMPO REAL
    public function subirArchivoAjax(){
        dd($_POST);

    }  
             
    //FUNCION PARA CAMBIAR UN ESTADO
    public static function cambiarEstadoTable($model,$id,$columna)
    {
        $est = $model::find($id);
        $estado = $est[$columna];


            
        if($estado == "Activo")
        {
            $estado = "Inactivo";

        }else{
            $estado = "Activo";
        }
        $est = [$columna=>$estado];
        return $model::where('id',$id)->update($est);
    }  

     /**
     * Metodo para eliminar.
     * @category FuncionesGenerales
     * @package Modulo Cliente
     * @version 1.0
     * @author Samuel Beltran
     */
    public static function deleteByElement($model,$column,$id)
    {
        $model::where($column,$id)->delete();
    }   
 
     /**
     * Función que busca un pais por su id
     * @category FuncionesGenerales
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
    public static function getWorldbyId($id)
    {
        return World::where('id',$id)->get();
    }   

     /**
     * Función que busca un departamento por el id de su respectivo pais
     * @category FuncionesGenerales
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
    public static function getWorldbyParent($id)
    {  
        return World::where('id_parent',$id)->get();
    } 
     /**
     * Función que trae todos los items que coincidan con
     * el valor que el cliente requiere.
     * @category FuncionesGenerales
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
    public static function searchItem($entidades,$item)
    {
        foreach (FILTRAR_ENTIDAD as $filEntidad => $val) 
        {
            if (in_array($filEntidad, $entidades)) 
            {
                foreach ($val as $columnas => $value) 
                {
                    $entidadReal = "App\\".$filEntidad;
                    $entidadRealSi = new $entidadReal;

                    $response = $entidadRealSi::where($value,'like','%'.$item.'%')->get();
                    if(count($response)>0){
                        $storeSearch[] = $response;
                    }
                }   
            }           
        }
        if(!isset($storeSearch))
        {   
            return false;
        }else{
            return $storeSearch[0];
        }

    }    
}
