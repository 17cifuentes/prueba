<?php

namespace App\Http\Gst;
use App\DatosRoles;
use App\Campos;

//use App\Http\Clases\FuncionesGenerales;
//use Illuminate\Http\Request;

class GstCampos
{


	public function getAllCampos()
	{
		return Campos::all();
	}
	public function camposJoin($idRol)
	{
		return DatosRoles::where(TN_DET_MOD_ROL_ID,$idRol)->join('tn_sft_campos','tn_sft_campos.id','tn_sft_datos_roles.id_campo')->get();
	}
	public function getCamposByAtributte($columna,$valor)
	{
		return Campos::where($columna,$valor)->get();
	}
}
?>