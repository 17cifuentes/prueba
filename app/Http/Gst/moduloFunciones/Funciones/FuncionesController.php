<?php

namespace App\Http\Controllers\Funciones;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Table;
use App\User;
use Auth;
use URL;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controllers\Cliente\ClientesController;
use App\Http\Gst\GstFunciones;
use App\Http\Gst\GstModulos;
/*
*Autor: Cristian Cifuentes
*Modulo: Modulos
*Versiòn: 1.0
*Descripciòn: Controllador Funciones del sistema.
*/
class FuncionesController extends Controller
{
    private $gstFunciones;
    private $gstModulos;
    public function __construct(GstFunciones $gstFunciones, GstModulos $gstModulos){
        session_start();
        $this->gstFunciones = $gstFunciones;
        $this->gstModulos = $gstModulos;
    }

    public function listarFunciones(){
        $_SESSION[DATA][CLIENTE_TITLE] = 'Listar funciones';
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú funciones'=>'Menu/12','Listar funciones'=>"#"];  
        $tableFun = new Table;
        $tableFun->setColum($tableFun,
            [
                'id' => 'id',
                'Nombre'=>'fun_menu_nombre',
                'Descripción'=>'fun_descripcion',
                'Ruta'=>'fun_ruta',
                'Fecha'=>'created_at',
                'Estado'=>'estado_funcion'
            ]
        );
         $opciones = 
            [
                'Editar'=> 'Funciones/editarFunciones',
                'Cambiar Estado'=> 'Funciones/cambiarEstadoFuncion'
            ];
        $datos = $this->gstFunciones->getAllFunciones()->toArray();
        
        $tableFun->setItems($tableFun,$datos);
        $tableFun = $tableFun->dps($tableFun);
        return view(PATH_LIST,[TABLE=>$tableFun,'opc'=>json_encode($opciones)]);
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para enviar a la vista de crear funciones
    */  
    public function crearFunciones()
    {
    
    $_SESSION[DATA][CLIENTE_TITLE] = 'Crear Funciones'; 
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menù Funcionalidades'=>URL::previous(),'Crear Funciones'=>"#"];
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú funciones'=>'Menu/12','Crear Funciones'=>"#"];
    //Nombre/VALOR A DAR/ 
        $modulos= $this->gstModulos->getModulos();
         return view ('core.Funciones.crearFunciones',compact('modulos'));
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada:
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para crear funciones.
    */
    public function crearFuncion(){
        unset($_POST['_token']);
        $_SESSION[DATA][MSG] = 'Registro de funcion exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstFunciones->InsertFuncion($_POST);
        return $this->listarFunciones(); // Crear funcion para ir a MENU
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada: id: de la funcion
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para cambiar estado de las funciones
    */  
    
    public function cambiarEstadoFuncion(){

        $_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstFunciones->CambiarEstado($_GET['id'],'estado_funcion');
        return redirect('Funciones/listarFunciones');
    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada:id identificador de usuario
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para enviar a la vista de editar funciones
    */

    public function editarFunciones(){

    $_SESSION[DATA][CLIENTE_TITLE] = 'Editar Funciones';
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú funciones'=>'Menu/12','Listar funciones'=>'listarFunciones','Editar Funciones'=>"#"];


        $modulos= $this->gstModulos->getModulos();
        $funcion= $this->gstFunciones->getFuncion($_GET['id']);

        return view ('core.Funciones.editarFunciones',compact('modulos','funcion'));

    }
    /*
    *Autor: Javier R
    *Modulo: Funciones
    *Entrada: $_POST[] todos los datos enviados desde la vista de editar
    *Salida:
    *Versiòn: 1.0
    *Descripciòn: Funcion para editar o actualizar en la base de datos
    */
    public function editarFuncionProcess(){

        $id=$_POST['id'];
        unset($_POST['_token']);
        unset($_POST['id']);
        $_SESSION[DATA][MSG] = 'Editado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstFunciones->EditarFuncion($id,$_POST);
        return $this->listarFunciones();
    }

}





















