<?php

namespace App\Http\Gst\Custom\Mapxi\Usuario;

use Illuminate\Http\Request;
use App\Http\Clases\WebServiceRest;
/*
*Autor: Cristian Cifuentes
*Modulo: Cliente
*Versiòn: 1.0
*Descripciòn: Clase de gestion para el modulo de cliente.
*/
class GstMapxiUsuarios
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$idUsuario = Id del cliente
	*Salida:
	*Descripciòn: Metodo para cargar configuraciones del cliente
	*/
    public static function getUserApp()
    {
    	return WebServiceRest::getServicio("https://webservice.softheory.com/mapxi/lstUsus");
    }
}
