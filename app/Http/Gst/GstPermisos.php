<?php

namespace App\Http\Gst;
use App\FuncionesRoles;
use App\ClientesMod;
use App\ModulosRol;
use App\Modulos;
use Auth;

//use Illuminate\Http\Request;
//use App\UsuarioModulos;


    /*
    *Autor: Cristian Cifuentes
    *Modulo: Modulos
    *Versiòn: 1.0
    *Descripciòn: Clase de gestion de Modulos
    */
class GstPermisos
{
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada:
    *Salida:
    *Descripciòn: Devuelve el modulo
    */
    public static function getPermisosFunciones($idModulo)
    {
        return FuncionesRoles::where(TN_ROLES_ROL_ID,$idModulo)->join(TN_FUNCIONES,TN_DET_FUNCIONES_ROL.'.'.TN_FUNCIONES_ID,TN_FUNCIONES.'.'.ID)->get();
    }
    
    /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de un registro la tabla "tn_sft_detalle_modulo_rol"
    *Salida: Array con registros de una tabla
    *Descripciòn: Devuelve el modulo 
    */
    public function getIdModRoles($id,$idModulo)
    {
        return ModulosRol::where(TN_DET_MOD_ROL_ID,$id)->where(TN_MOD_ID_MOD,$idModulo)->get();
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: funciones detalle rol
    *Versiòn: 1.0
    *Entrada: $post = coleccion de datos que se van a insertar
    *Salida: 
    *Descripciòn: Funcion que descarta los datos: "_token", "id_cliente"
    * e inserta las columnas "id_modulo", "id_rol" en la tabla
    * "tn_sft_detalle_modulo_rol",
    */
  public function crearDetModClientes($post)
    {
        unset($post[TOKEN]);
        unset($post[TN_CLIENTE_ID]);

        try {
            return ModulosRol::insert($post);   
        
        } catch (\Exception $e) {
            error_log("Error: ".$e->getMessage());
            return false;
        }
        
          
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de un registro la tabla "tn_sft_detalle_modulo_rol"
    *Salida: Array con registros de una tabla
    *Descripciòn: Devuelve el modulo 
    */
    public function getAllModRoles($id)
    {
        return ModulosRol::where('id_modulo',$id)->get();
    }
    public function getAllRolesMod($id)
    {
        return ModulosRol::where('id_rol',$id)->get();
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $idModulo = id de la tabla "tn_sft_modulos".
    *Salida: Array con registros de una tabla
    *Descripciòn: Devuelve registros: modulo rol id, 
    * nombre del modulo y nombre del rol, de la tabla
    * "tn_sft_detalle_modulo_rol".
    */
    public function getsDetalleModuloRolJoin($idModulo)
    {
        return ModulosRol::where(TN_SFT_MODULOS.'.id',$idModulo)
        ->join(TN_SFT_MODULOS,TN_SFT_MODULOS.'.id',TN_SFT_DETALLE_MODULO_ROL.'.id_modulo')
        ->join(TN_SFT_ROLES,TN_SFT_ROLES.'.id',TN_SFT_DETALLE_MODULO_ROL.'.id_rol')
        ->select(TN_SFT_DETALLE_MODULO_ROL.'.id as id','mod_nombre','rol_nombre')->get();
    }
    public function getsDetalleRolesModulosJoin($idRol)
    {
        return ModulosRol::where('tn_sft_roles.id',$idRol)
        ->join(TN_SFT_MODULOS,TN_SFT_MODULOS.'.id',TN_SFT_DETALLE_MODULO_ROL.'.id_modulo')
        ->join(TN_SFT_ROLES,TN_SFT_ROLES.'.id',TN_SFT_DETALLE_MODULO_ROL.'.id_rol')
        ->select(TN_SFT_DETALLE_MODULO_ROL.'.id as id','mod_nombre','rol_nombre')->get();
    }

       /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de la tabla "tn_sft_detalle_modulo_rol"
    *Salida: Array con registros de una tabla
    *Descripciòn: Devuelve registros: modulo rol id, 
    * nombre del modulo y nombre del rol, de la tabla
    * "tn_sft_detalle_modulo_rol".
    */
    public function getModuloDetalleID($id)
    {
        return ModulosRol::where(TN_SFT_DETALLE_MODULO_ROL.'.id',$id)
        ->join(TN_SFT_MODULOS,TN_SFT_MODULOS.'.id',TN_SFT_DETALLE_MODULO_ROL.'.id_modulo')
        ->join(TN_SFT_ROLES,TN_SFT_ROLES.'.id',TN_SFT_DETALLE_MODULO_ROL.'.id_rol')
        ->select(TN_SFT_MODULOS.'.id as id')->get()->toArray();
    }

       /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de la tabla "tn_sft_detalle_modulo_rol"
    *Salida:
    *Descripciòn: esta funcion elimina el registro de una tabla 
    * segun su id.
    */
    public function deletePermiso($id)
    {
        ModulosRol::where(TN_SFT_DETALLE_MODULO_ROL.'.id',$id)->delete();
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de la tabla "tn_sft_detalle_modulo_rol"
    *Salida: Array con registros de una tabla
    *Descripciòn: Devuelve registros de la tabla fun "tn_sft_detalle_modulo_rol";
    */
    public function getAllFunRoles($id)
    {
        return FuncionesRoles::where('funcion_id',$id)->get();
    }

    /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de la tabla "tn_sft_funciones"
    *Salida: retorno de datos
    *Descripciòn: La funcion retorna de datos de las columnas: "id", nombre del rol
    * nombre del menu de las tablas: "tn_sft_detalle_rol_funciones", "tn_sft_roles"
    * y "tn_sft_funciones".
    */
    public function getDetalleRolFunciones($id)
    {
        return FuncionesRoles::where(TN_SFT_FUNCIONES.'.id',$id)
        ->join(TN_SFT_FUNCIONES,TN_SFT_FUNCIONES.'.id',TN_SFT_DET_ROL_FUN.'.funcion_id')
        ->join(TN_SFT_ROLES,TN_SFT_ROLES.'.id',TN_SFT_DET_ROL_FUN.'.rol_id')
        ->select(TN_SFT_DET_ROL_FUN.'.id as id','rol_nombre','fun_menu_nombre')->get()->toArray();
    }

       /*
    *Autor: Samuel Beltran
    *Modulo: Modulos
    *Versiòn: 1.0
    *Entrada: $id = id de la tabla "tn_sft_detalle_rol_funciones"
    *Salida: Retorno del id de la tabla "tn_sft_funciones"
    *Descripciòn: esta funcion elimina el registro de una tabla 
    * segun su id.
    */
    public function getDetaRolFunId($id)
    {
        return FuncionesRoles::where(TN_SFT_DET_ROL_FUN.'.id',$id)
        ->join(TN_SFT_FUNCIONES,TN_SFT_FUNCIONES.'.id',TN_SFT_DET_ROL_FUN.'.funcion_id')
        ->select(TN_SFT_FUNCIONES.'.id')->get();
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: funciones detalle rol
    *Versiòn: 1.0
    *Entrada: $id = id de la tabla "tn_sft_detalle_rol_funciones"
    *Salida: 
    *Descripciòn: esta funcion elimina el registro de una tabla 
    * segun su id.
    */
    public function deletePermisoFun($id)
    {
        FuncionesRoles::where(TN_SFT_DET_ROL_FUN.'.id',$id)->delete();
    }

      /*
    *Autor: Samuel Beltran
    *Modulo: funciones detalle rol
    *Versiòn: 1.0
    *Entrada: $idCliente = tiene el dato id de cliente.
              $idModulo  = tiene el dato del id de modulos
    *Salida: consulta los datos de la tabla y retorna a la vista
    *Descripciòn: Funcion que obtine los datos por id de cliente y id.
    */
    public function getIdModClientes($idCliente,$idModulo)
    {
        return ClientesMod::where('id_cliente',$idCliente)->where('id_modulo',$idModulo)->get();
    }

        /*
    *Autor: Samuel Beltran
    *Modulo: funciones detalle rol
    *Versiòn: 1.0
    *Entrada: $post = coleccion de datos que se van a insertar
    *Salida: 
    *Descripciòn: Funcion que descarta los datos: "_token"
    * e inserta las columnas "id_cliente", "id_modulo" en la tabla
    * "tn_sft_det_mod_clientes`",
    */
  public function crearPModulosClientes($post)
    {
        unset($post[TOKEN]);
        try {
          return ClientesMod::insert($post);  
        } catch (\Exception $e) {
            error_log("Error: ".$e->getMessage());
            return false;
        }

    }


    /*
    *Autor: Samuel Beltran
    *Modulo: funciones detalle rol
    *Versiòn: 1.0
    *Entrada: 
    *Salida: 
    *Descripciòn: consulta del id de un cliente el la tabla
    * tn_sft_det_mod_clientes.
    */
    public function queryPermisos($idModulo)
    {
        return ClientesMod::where(TN_CLIENTE_ID,Auth::user()->cliente_id)->where(TN_MOD_ID_MOD,$idModulo)->get();
    }
}

