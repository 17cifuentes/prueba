<?php

namespace App\Http\Gst;

use App\Http\Clases\FuncionesGenerales;
use App\DatosValRoles;
use App\Roles;

//use Illuminate\Http\Request;
//use App\Http\Gst\GstCliente;
//use App\FuncionesRoles;



	/*
	*Autor: Cristian Cifuentes
	*Modulo: Roles
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para los Roles
	*/
class GstRoles
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getRole($id)
    {
    	return Roles::find($id);
    }
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getAllRoles($cliente=false)
    {
    	if ($cliente!=false) {

    		return Roles::where('cliente_id',$cliente)->get();	

    	}else{
    		
    		return Roles::all();
    	}
    	
    }
    public function saveDatosRol($data)
    {
		return DatosValRoles::insert($data);
    }
    public function gatDatosRol($idRol,$idUsu)
    {
    	return DatosValRoles::where(TN_USUARIO_ID,$idUsu)->join("tn_sft_campos","tn_sft_campos.id","id_campo")->get();
    }
    public function updateDataRol($idUsu,$idCampo,$valor){

    	return DatosValRoles::where(TN_USUARIO_ID,$idUsu)->where('id_campo',$idCampo)->update(['valor'=>$valor]);
    }
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getRolesCliente($idCliente)
    {
    	return Roles::where(TN_ID_CLIENTE,$idCliente)->get();
    }


    	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function findRole($nombre)
    {
    	return Roles::where(TN_ROL_NOMBRE,'=',$nombre)->get();
    }


	/*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Descarta el '_token' Insereta un 
	*nuevo registro en la tabla "tn_sft_roles".
	*/
	public function insertRol()
	{
		unset($_POST['_token']);
		Roles::insert($_POST);
	}

	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getRolesClienteJoin()
    {
    	return Roles::select('tn_sft_roles.id as id','cliente_nombre','rol_nombre','rol_descripcion','estado_roles')->join('tn_sft_cliente','tn_sft_cliente.id','cliente_id')->get();
    }

    /*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada: $post = Datos a insertar
	*Salida:
	*Descripciòn: La funcion descarta el "_token" y actualiza los datos
	* de un registro ya creado anteriormente.
	*/
    public function editarProcess($post)
    {
    	//revisar
		unset($_POST['_token']);
		try{
			return Roles::where('id',$_POST['id'])->update($_POST);
		}catch(\exception $a){
			error_log("Error".$a->getMessage());
			return false;
		}    	
    }

        /*
	*Autor: Samuel Beltran
	*Modulo: Roles
	*Versiòn: 1.0
	*Entrada:$id = Id del cliente ,
	*        $columna = nombre de la columna estado 
	*                   de la tabla cliente
	*Descripciòn: Metodo para cambiar el estado de 
	* un cliente de "Activo" a "Inactivo" y viceversa.
	*/
    public function changeEstado($id,$columna)
    {
    	FuncionesGenerales::cambiarEstadoTable(new Roles,$id,$columna);
    }

    public function getDatosByAtributte($columna,$valor)
    {
    	return Roles::where($columna,$valor)->get();
    }
    public function registroExterno($idCliente)
    {
    	return Roles::where(TN_USU_CLIENTE_ID,$idCliente)->where('registro_externo','Activo')->get();
    }

     /**
	 * Función que trae todos los registros de un rol
	 * con su respectivo id.
	 * @category GstRoles
	 * @package Modulo Roles
	 * @version 1.0
	 * @author Samuel Beltran
	 */
    public function getRolesByID($id)
    {
    	return Roles::where('id',$id)->get();
    }
}
