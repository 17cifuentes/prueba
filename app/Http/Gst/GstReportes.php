<?php
namespace App\Http\Gst;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Gst\GstModulos;
use App\ReportesOperaciones;
use App\ReportesCliente;
use App\ReportMain;
use App\Http\Clases\Table;
use App\ReportesColumnas;
use App\ReporteOpciones;
use Auth;
use DB;

//use App\Http\Clases\FuncionesDB;
//use Illuminate\Http\Request;


/*
*Autor: Cristian Cifuentes
*Modulo: Cliente
*Versiòn: 1.0
*Descripciòn: Clase de gestion para el modulo de reportes.
*/
class GstReportes
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$idUsuario = Id del cliente
	*Salida:
	*Descripciòn: Metodo para cargar configuraciones del cliente
	*/
    public static function getReportsDash()
    {
         $reports = ReportesCliente::where("id_cliente",Auth::user()->cliente_id)->get();
         $reportes = [];
         foreach ($reports as $key => $value) {
            $reportes[$key]['titulo'] = $value->sftReporte->nombre;
            $gst = new $value->sftReporte->pathGst;
            $method = $value->sftReporte->funcion;
            $data = $gst->$method();
            $reportes[$key]['data'] = $data;
            $reportes[$key]['tamano'] = $value->tamano;
            $reportes[$key]['tipo'] = $value->tipo;
         }
         return $reportes;
    }
    /*
    *Autor: Cristian Cifuentes
    *Modulo: Cliente
    *Versiòn: 1.0
    *Entrada:$idUsuario = Id del cliente
    *Salida:
    *Descripciòn: Metodo para cargar configuraciones del cliente
    */
    public function getOpcionesReportes()
    {
        return ReporteOpciones::all();
    }
/**
 * Clase de gestion para el modulo de reportes.
 * @category Reportes 
 * @package SGD
 * @version 1.0
 * @author Cristian Cifuentes
 */
    public function getAllOperaciones(){
    	return ReportesOperaciones::all();
    }
    
    public function getReporte($data){
    	$gstModulo = new GstModulos;
    	$modulo = $gstModulo->getModulo($data['modulo']);
    	$enti = 'App\\'.$modulo[0]->entidad;
    	if(class_exists($enti)){
    		$entidad = new $enti;
    	}else{
    		return false;
    	}
    	$col = $this->getReportesModUsu($modulo[0]->id,Auth::user()->id,Auth::user()->cliente_id);
    	$columnas = json_decode($col[0]->columnas,true);
    	$join = FuncionesGenerales::listaOrderDataJoinBD($entidad,[]);
		$query = $this->getQuery($data,$modulo,$join);
		
		try{
            $dat = (array)DB::select($query);
            foreach ($dat as $key => $value) {
                $datos[] = (array)$value;
            }
        }catch(\Exception $e){
            error_log($e->getMessage());
        }
        if($_POST['operacion'] == "count group"){
			$columnas = ['Total'=>'total','Valor'=>$_POST['item']];
		}
		
		if(!isset($datos)){
			return false;
		}else{
            if($data['tipo'] == 'Grafico' && $_POST['operacion'] == "count group"){
                foreach ($datos as $key => $value) {
                    $arr['data'][] = $value['name'];
                    $arr['valor'][] = (float)$value['total'];
                }
                return $arr;
            }else if($data['tipo'] == 'Reporte'){
                $table = new Table;
                $table->setColum($table,$columnas);
                $table->setItems($table,$datos);
                $table->setClass($table,'sftDataTable');
                $table = $table->dps($table);
                return $table;      
            }else{
                return false;
            }
			
		}
    	
    	
    }
    public function getQuery($data,$modulo,$join = ""){

    	if(Auth::user()->id != 1 and Auth::user()->cleinte_id){
    		$cliente = " where ".$modulo[0]->tabla_primaria.".cliente_id = ".Auth::user()->cliente_id." ";
    	}else{
    		$cliente = "";
    	}
    	if($data['opc'] == 'all'){
    		return "SELECT * FROM ".$modulo[0]->tabla_primaria." ".$join." ".$cliente;
    	}elseif($data['opc'] == 'where'){
    		if(Auth::user()->id != 1){
    			$cliente = " where ".$modulo[0]->tabla_primaria.".cliente_id = ".Auth::user()->cliente_id." ";
    		}else{
    			$cliente = "";
    		}
    		if($data['operacion'] == "like"){
                if(Auth::user()->id != 1){
                    $cliente = " and ".$modulo[0]->tabla_primaria.".cliente_id = ".Auth::user()->cliente_id." ";
                }else{
                    $cliente = "";
                }
    			$data['valor'] = "%".$data['valor']."%";
    			return "SELECT * FROM ".$modulo[0]->tabla_primaria." ".$join." ".$data['opc']." ".$data['item']." ".$data['operacion']." '".$data['valor']."'".$cliente;
    		}
    		if($data['operacion'] == "GROUP BY"){
    			return "SELECT * FROM ".$modulo[0]->tabla_primaria." ".$join." ".$data['opc']." ".$data['item']." ".$data['operacion']." '".$data['valor']."'".$cliente;
    		}
    		if($data['operacion'] == "count group"){
    			if(isset($data['valor']) and $data['valor'] != ""){
					if(Auth::user()->id != 1){
    					$cliente = " where ".$modulo[0]->tabla_primaria.".cliente_id = ".Auth::user()->cliente_id." ";
    				}else{
    					$cliente = "";
    				}
					$where = "where ".$modulo[0]->tabla_primaria.".id = ".$data['valor']." ".$cliente;
    			}else{
    				$where = $cliente;
    			}
    			return "SELECT COUNT(".$modulo[0]->tabla_primaria.".id) as total, ".$modulo[0]->tabla_primaria.".".$data['item']."  FROM ".$modulo[0]->tabla_primaria." ".$join." ".$where." GROUP BY ".$modulo[0]->tabla_primaria.".".$data["item"]. " ";
    		}
    		$cliente = " and ".$modulo[0]->tabla_primaria.".cliente_id = ".Auth::user()->cliente_id." ";
    		return "SELECT * FROM ".$modulo[0]->tabla_primaria." ".$join." ".$data['opc']." ".$modulo[0]->tabla_primaria.".".$data['item']." ".$data['operacion']." '".$data['valor']."'".$cliente;

    	}
    }
    public function getReportesModUsu($modId,$usuId,$clienteId){
    	$res = ReportesColumnas::where('modulo_id',$modId)->where('id_usu',$usuId)->get();
    	if(count($res) == 0){
    		$res = ReportesColumnas::where('modulo_id',$modId)->where('cliente_id',$clienteId)->get();
    		if(count($res) > 0){
    			return ReportesColumnas::where('modulo_id',$modId)->where('cliente_id',$clienteId)->get();
    		}else{
    			return ReportesColumnas::where('modulo_id',$modId)->get();
    		}
    	}else{
    		return $res;
    	}
    }

}
