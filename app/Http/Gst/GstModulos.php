<?php

namespace App\Http\Gst;

use Illuminate\Http\Request;
use App\Modulos;
use App\UsuarioModulos;
use App\ModulosRol;
use App\Http\Clases\FuncionesGenerales;
use App\ClientesMod;
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion de Modulos
	*/
class GstModulos
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Devuelve el modulo
	*/
    public function getModulo($idModulo)
    {
    	return Modulos::where(ID,$idModulo)->get();
    }
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los modulos del sistema.
	*/
    public function getModulos()
    {
    	return Modulos::all();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Entrada:$idCliente = Id del cliente
	*Salida:
	*Descripciòn: Carga los modulos de un cliente.
	*/
    public static function getModulosCliente($idrol)
    {
    	 return ModulosRol::join(TN_MODULOS,TN_DET_MOD_ROL.'.'.TN_MOD_ID_MOD,TN_MODULOS.'.'.ID)->where(TN_DET_MOD_ROL.'.'.TN_DET_MOD_ROL_ID,$idrol)->get();
    }

    /*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Devuelve el modulo
	*/
    public function getModuloDetalle($id)
    {
    	return ClientesMod::where(TN_CLIENTE_ID,$id)->join(TN_MODULOS,TN_MODULOS.'.'.ID,TN_MODULOS_CLIENTE.'.'.TN_MODULOS_ID)->get();
    }

    /*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Devuelve atributos del modulo
	*/
    public function getDatosByAtribute($model,$columna,$valor)
    {
    	return $model::where($columna,$valor)->get();
    }

   /*
	*Autor: Cristian Cifuentes
	*Modulo: Modulos
	*Versiòn: 1.0
	*Entrada:$idCliente = Id del cliente
	*Salida:
	*Descripciòn: Carga los modulos de un cliente.
	*/

	public function crearModulo($id)
	{
		try {
			return Modulos::insert($id);
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
    		return false;
		}
		
	}

    /*
	*Autor: Samuel Beltran
	*Modulo: Modulos 
	*Versiòn: 1.0
	*Entrada:$id = Id de la tabla para su cambio de estado, 
	* $columna = nombre de la columna estado, segun este redactada en
	la tabla.
	*Salida:
	*Descripciòn: cambia el estado de "Activo" a "inactivo".
	*/
    public function cambEstado($id,$columna)
    {
    	FuncionesGenerales::cambiarEstadoTable(new Modulos,$id,$columna);
    }

    /*
	*Autor: Samuel Beltran
	*Modulo: Modulos 
	*Versiòn: 1.0
	*Entrada:$idCliente = Id del cliente
	*Salida:
	*Descripciòn: Carga los modulos de un cliente.
	*/
    public function processEditModulos($id,$post)
    {    	
    	try{
    		return Modulos::where(ID,$id)->update($post);	
    	}catch(\Exception $e){
    		error_log("Error: ".$e->getMessage());
    		return false;
    	}
    	
    }
}
