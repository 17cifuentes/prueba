<?php

namespace App\Http\Gst;

use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use App\SFTConf;
use App\Cliente;


//use Illuminate\Http\Request;
//use Auth;


/*
*Autor: Cristian Cifuentes
*Modulo: Cliente
*Versiòn: 1.0
*Descripciòn: Clase de gestion para el modulo de cliente.
*/
class GstCliente
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$idUsuario = Id del cliente
	*Salida:
	*Descripciòn: Metodo para cargar configuraciones del cliente
	*/
    public static function getConfCliente($idUsuario)
    {
        return SFTConf::where(TN_CONF_ID_CLIENTE,$idUsuario)->join(TN_CLIENTE,TN_CONF.".".TN_CONF_ID_CLIENTE,TN_CLIENTE.".".ID)->get();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:
	*Salida: Colecciòn de todos los clientes.
	*Descripciòn: Metodo para cargar configuraciones del cliente
	*/
    public static function getAllClientes()
    {
    	return Cliente::all();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:
	*Salida: Colecciòn de todos los clientes.
	*Descripciòn: Metodo para cargar configuraciones del cliente
	*/
    public static function saveCliente($data)
    {
    	return FuncionesDB::registrar(new Cliente,$data);

    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$idUsuario = Id del cliente
	*Salida: colletion del cliente
	*Descripciòn: Metodo para obtener cliente
	*/
    public static function getCliente($idCliente)
    {
    	return Cliente::where(ID,$idCliente)->get();
    }

         /*
	*Autor: Samuel Beltran
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$id = Id del cliente ,
	*        $post = todos los values 
	*                 enviados por post
	*Descripciòn: Metodo para actualizar
	*			  datos del cliente
	*/
    public static function processEditCliente($id,$post)
    {
    	
		$colNames = [TN_NOMBRE_CLIENTE=>$post[TN_NOMBRE_CLIENTE],
					TN_CLIENTE_DESCRIPCION=>$post[TN_CLIENTE_DESCRIPCION],
					TN_CLIENTE_DIRECCION=>$post[TN_CLIENTE_DIRECCION],
					TN_CLIENTE_TELEFONO=>$post[TN_CLIENTE_TELEFONO],
					TN_CLIENTE_CORREO=>$post[TN_CLIENTE_CORREO]];
		Cliente::where(CLIENTE_ID,$id)->update($colNames);
	}

		 /*
	*Autor: Samuel Beltran
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$id = Id del cliente ,
	*        $columna = nombre de la columna estado 
	*                   de la tabla cliente
	*Descripciòn: Metodo para cambiar el estado de 
	* un cliente de "Activo" a "Inactivo" y viceversa.
	*/
    public static function estCliente($id,$columna)
    {
    	
    	try{
    		
    	 	FuncionesGenerales::cambiarEstadoTable(new Cliente,$id,$columna);

            return true;

        }catch(Exception $e){

            error_log("Error: ".$e->getMessage());

            return false;
        }
    	
    }

}
