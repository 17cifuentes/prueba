<?php

namespace App\Http\Gst;
use App\CambioEstado;
use App\Estado;
use App\SftFiles;
use App\Http\Clases\FuncionesGenerales;
use App\Iconos;
use App\Personal;
use App\Empresa;
use App\Plantilla;
/*
*Autor: Cristian Cifuentes
*Modulo: Cliente
*Versiòn: 1.0
*Descripciòn: Clase de gestion para el modulo de cliente.
*/
class GstGeneral
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Cliente
	*Versiòn: 1.0
	*Entrada:$idUsuario = Id del cliente
	*Salida:
	*Descripciòn: Metodo para cargar configuraciones del cliente
	*/
    public static function getCambioEstado($id)
    {
        return CambioEstado::where('tn_sft_cambiar_estado.id',$id)->join("tn_sft_columnas_tabla","tn_sft_columnas_tabla.id","tn_sft_cambiar_estado.columna_id")->get();
    }
    public function validateData($request){
    	foreach (VALIDATE_DATA as $key => $value) 
    	{
    		foreach ($request->all() as $k => $v) 
    		{
    			if($k == $key)
    			{
    				$validations[$key] =$value;
    			}
    		}
    	}
        if(!isset($validations) || count($validations) == 0)
        {
            return true;
        }else
        {
            $validatedData = $request->validate($validations);    
        }
    	
    } 
    /**
    * Función consulta todos los registros de la tabla de estados
    * @category General
    * @package Modulo General
    * @version 1.0
    * @author Samuel Beltran
    */ 
    public function getAllEstados()
    {
        return Estado::All();
    }
    /**
    * Función que guarda un archivo en sftFiles
    * @category General
    * @package Modulo General
    * @version 1.0
    * @author Samuel Beltran
    */  
    public function saveFile($file,$idRel,$modulo,$opcion,$estado)
    {
        $cod = FuncionesGenerales::generarCodigo(5);
        $archivo  = $this->getSftFiles();
        $archivo->modulo = $modulo;
        $archivo->opcion = $opcion;
        $archivo->idRel = $idRel;
        $archivo->path = 'media/'.$cod.$file['name'];
        $archivo->nombre = $file['name'];
        $archivo->tipoDoc = $file['type'];
        $archivo->size = $file['size'];
        $archivo->fExpedicion = date("Y/m/d");
        $archivo->estado = $estado;
        $archivo->seguro = 'N';
        $archivo->save();
        return $archivo;
    }
    /**
    * Función para instanciar entidad SftFIles
    * @category General
    * @package Modulo General
    * @version 1.0
    * @author Samuel Beltran
    */  
    public function getSftFiles()
    {
        return new SftFiles;
    }
    /* Función consulta todos los registros de la tabla de estados donde se le especifica que razon a que razon desea ingresar ejem (PMI-Estados)
    * @category General
    * @package Modulo General
    * @version 1.0
    * @author Javier R
    */ 
    public function getAllEstadosRazon($razon)
    {
        return Estado::where('razon',$razon)->get();
    }

    public static function getIconos()
    {
        return Iconos::all();
    }

    /**
    * Función que consulta todos los registros de la tabla personal
    * @category General
    * @package Personal
    * @version 1.0
    * @author Samuel Beltran
    */ 
    public function getAllPersonal()
    {
        return Personal::all();
        
    }

    /**
    * Función que consulta todos los registros de la tabla Empresa
    * @category General
    * @package Empresa
    * @version 1.0
    * @author Samuel Beltran
    */ 
    public function getAllEmpresa()
    {
        return Empresa::all();
    }
        /**
     * Función que hace una consulta por id_parent y la retorna en formato Json
     * @category GeneralController
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
    public function getMunicipioById()
    {
        $municipios = FuncionesGenerales::getWorldbyParent($_GET['id']);
        return $municipios;
    }

    /**
     * Función para actualizar datos de un registro de la tabla plantilla.
     * @category GeneralController
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
    public function updatePlantilla($id,$comision,$otros,$arl,$contrato)
    {
        try {
            
            return Plantilla::where('id_plantilla', $id)->update(['comisiones'=>$comision,'otros'=>$otros,'arl_categoria'=>$arl,'contrato'=>$contrato]);
              
        } catch (\Exception $e) {
            error_log("Error: ".$e->getMessage());
            return false;
        }
    }
}
