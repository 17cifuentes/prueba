<?php

namespace App\Http\Gst;


use App\Http\Clases\FuncionesDB;
use App\User;
use Auth;
use DB;
//use Illuminate\Http\Request;
//use App\UsuarioModulos;




	/*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para Usuarios.
	*/
class GstUsuarios
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga los usuarios que tiene un cliente.
	*/
    public static function getReportUserByRol()
    {
    	$data = User::join('tn_sft_roles','tn_sft_roles.id','users.rol_id')->groupBy('tn_sft_roles.rol_nombre')->select('rol_nombre', DB::raw('count(*) as total'))->get();
    	foreach ($data as $key => $value) {
    		$datos['titulos'][] = $value->rol_nombre;
    		$datos['valores'][] = (int)$value->total;
    	}
    	return $datos;
    }
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga los usuarios que tiene un cliente.
	*/
    public static function getUserClient()
    {
        return User::select("users.img","users.id as id","name","email","rol_nombre","departamento_id","municipio_id")->join("tn_sft_roles","tn_sft_roles.id","rol_id")->where("users.".TN_USU_CLIENTE_ID,Auth::user()->cliente_id)->get();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: Array(tabla users)
	*Salida: Colecciòn del usuario registrado.
	*Descripciòn: Metodo para registrar un usuario de un cleinte
	*/
    public static function saveUsuario($data)
    {
    	return FuncionesDB::registrarGetRegistro(new User,$data);
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: 
	*Salida: Colecciòn de usuarios registrados.
	*Descripciòn: Metodo para obtener todos los usuarios
	*/
    public static function getAllUsers()
    {
    	return User::select("users.img","users.id as id","name","email","rol_nombre","departamento_id","municipio_id")->join("tn_sft_roles","tn_sft_roles.id","rol_id")->get();
    }
     /*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: 
	*Salida: Colecciòn de usuarios registrados.
	*Descripciòn: Metodo para obtener todos los usuarios
	*/
    public static function getAllUsersJoins()
    {
    	return User::where(TN_CONF_ID_CLIENTE,Auth::user()->cliente_id);
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: Array de datos del usuario 
	*Salida: 
	*Descripciòn: Metodo para obtener todos los usuarios
	*/
    public static function actualizarUsuario($data,$id)
    {
    	try {
			return User::where(ID,$id)->update($data);
			
    	} catch (\Exception $e) {
    		error_log($e);
    		return false;
    	}
    	
    }
    /*
	*Autor: Javier R
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: id Del usuario
	*Salida: 
	*Descripciòn: metodo para eliminar Usuario
	*/
    public static function eliminarUsuario($id)
    {
    	try {
    		return User::where(ID,$id)->delete();	
    	} catch (\Exception $e) {
    		error_log($e);
    		return false;
    	}
    	
	}
	    /*
	*Autor: Javier R
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: id Del usuario
	*Salida: 
	*Descripciòn: metodo para eliminar Usuario
	*/
    public static function getUser($id)
    {
    	return	User::find($id);
	}
	    /*
	*Autor: Javier R
	*Modulo: Usuarios
	*Versiòn: 1.0
	*Entrada: id Del usuario
	*Salida: 
	*Descripciòn: metodo para eliminar Usuario
	*/
    public static function getUserByAttribute($column,$val)
    {
    	try{

     		return User::where($column,$val)->get();
     		
     	}catch(\Exception $registrar){

     		error_log("Error: ".$registrar->getMessage());

     		return false;
     	}
	}
	/**
     * Función de Process para Registrar el usuario externo en la opción del login : se creo con el id del cliente y el id de rol para registrar el usuario en el sistema
     * @ctegory usuariosController y GstUsuario
     * @package Modulo Usuarios
     * @version 1.0
     * @author Jasareth Obando
     */	
	public static function getRegisterExterno($post)
	{
		unset($post['id']);
		try{
     		return User::insert($post); 
     	}catch(\Exception $registrar){
     		error_log("Error: ".$registrar->getMessage());
     		return false;
     	}
		 
	}
	/*
     * funcion para consultar el correo para verificar si existe 
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
     */
	public function consultaEmail($email)
	{
		return User::where('email',$email)->get();
	}
	/*
     * funcion para cambiar la contraseña del usuario 
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
     */

	public function cambioPassword($id,$pass)
	{
		try{
			return User::where(ID,$id)->update(['password'=>$pass]);
		}catch(\Exception $e){
			error_log("Error: ".$e->getMessage());
	 		return false;	
		}
	}
	public function validarDatos($id,$nombre,$email,$password)
	{
		if(is_numeric($id) == false )
		{
			return false;
		}
		if(strlen($nombre) > 250 or strlen($email) > 250)
		{
			
			return false;
		}
		if(isset($password) && $password != "" or $password != null)
		{
			if(strlen($password) > 250)
				return false;
		}
		$consultar = User::where('id',$id)->get();
		if($consultar == "" or $consultar ==null)
		{
			return false;
		}
		return true;
	}
	
	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Retorna clientes identificados con un mismo id 
	*/
	public function getUsers($cliente){

		return User::where('cliente_id',$cliente)->get();

		
	}
}
