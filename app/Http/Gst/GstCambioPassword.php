<?php

 namespace App\Http\Gst;
 //use Illuminate\Http\Request;
 Use App\Cambiopassword;

class GstCambioPassword
{
	/*
     * Funcion encargada para recibir la nueva contraseña y su confirmacion de contraseña, sera el encargado de modificar la contraseña y borrar el codigo que se le envio al cliente 
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword 
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
    */
	public function insertarCodigo($data)
	{
		return Cambiopassword::insert($data);
	}
	/*
     * Funcion encargada para consultar el codigo que le llega y revisar si existe en la base de datos
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword 
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
    */

	public function consultaCodigo($codigo)
	{
		return Cambiopassword::where('codigo',$codigo)->get();
	}
	/*
     * Funcion encargada para eliminar el codigo segun id del usuario
     * @category UsuariosController, GstUsuario, Cambio Password, Y GstCambioPassword 
     * @package Modulo Usuarios
     * @version 1.0
     * @author Javier Rojas
    */
	public function deleteCod($id)
	{
		try {
			return Cambiopassword::where('id_usuario',$id)->delete();
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
	 		return false;	
		}
		
	}
}

?>