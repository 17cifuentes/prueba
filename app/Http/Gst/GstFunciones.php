<?php

namespace App\Http\Gst;
use App\Http\Clases\FuncionesGenerales;
use App\FuncionesRoles;
use App\Funciones;

//use Illuminate\Http\Request;
//use Auth;


	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion de funciones.
	*/
class GstFunciones
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:$idModulo = Id del modulo
	*Salida:
	*Descripciòn: Carga todas las funciones de un modulo.
	*/
    public function getFuncionesMod($idModulo)
    {
    	return Funciones::where(TN_FORENGKEY_ID,$idModulo)->where('estado_funcion','Activo')->get();
    }
    /*
    *Autor: Samuel Beltran
    *Modulo: Funciones
    *Versiòn: 1.0
    *Entrada:$idRol = Id de la tabla "tn_sft_roles",
    * $idFun = id de la tabla "tn_sft_funciones"
    *Salida: Retorno de los registros 
    *Descripciòn: funcion que retorna los registros de las tablas segun los 
    * id's ingresados.
    */
    public function getRolFuncionsIds($idRol,$idFun)
    {
    	return FuncionesRoles::where('rol_id',$idRol)->where('funcion_id',$idFun)->get();
    }

    public function insertRolFunciones($post)
    {
    	unset($post['_token']);
    	unset($post['id_cliente']);
    	unset($post['id_modulo']);
    	try {
    		return FuncionesRoles::insert($post);
    	} catch (\Exception $e) {
    		error_log("Error: ".$e->getMessage());
            return false;
    	}
    	
    }
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:$idModulo = Id del modulo
	*Salida:
	*Descripciòn: Carga todas las funciones de un modulo.
	*/
	public function getAllFunciones(){
		return Funciones::all();
	}
	/*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada: $data = informacion que se va a insertar
	*Salida:
	*Descripciòn: Inserta una nueva funcion para el sistema.
	*/
	public function InsertFuncion($data)
	{
		Funciones::insert($data);
	}
	/*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada: $data = informacion que se va a insertar
	*Salida:
	*Descripciòn: Inserta una nueva funcion para el sistema.
	*/
	public function CambiarEstado($id,$columna){

	FuncionesGenerales::cambiarEstadoTable(new Funciones,$id,$columna);	
	}
	/*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada: $id = identificador de la funcion
	*Salida:
	*Descripciòn: toma los datos de la funcion segun id y los envia a al controlador
	*/
	public function getFuncion($id)
	{
		return Funciones::find($id);
	}
	/*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada: $id = identificador de la funcion
	*Salida:
	*Descripciòn: toma los datos de la funcion segun id y edita
	*/
	public function EditarFuncion($id,$data)
	{
		Funciones::where('id',$id)->update($data);
	}

	/*public function funcionEstado($estado_funcion)
	{
		Funciones::where('estado_funcion',$estado_funcion)->get();
	}
	*/
	public function eliminarFuncion($id)
	{	
		Funciones::where('id',$id)->delete();
	}

	/**
	* Función edita el valor de una columna especifica
	* de una tabla,pormedio de su id,y nombre de la columna.
	* @category GstFunciones
	* @package Modulo Funciones
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function updateColumnValue($id,$data,$column)
	{
		try {
    		unset($_POST["_token"]);
			return Funciones::where('id',$id)->update([$column => $data]);	
		} catch (Exception $e) {
			error_log("Error: ".$e->getMessage());
            return false;
		}
		
	}

	/**
	* Función consulta un registro de la tabla
	* por su id.
	* @category GstFunciones
	* @package Modulo Funciones
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function getFuncionById($id)
	{
		return Funciones::where('id',$id)->get();		
	}
}
