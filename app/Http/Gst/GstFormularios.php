<?php

namespace App\Http\Gst;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use App\Formularios;
use App\FormCampos;


//use Illuminate\Http\Request;
//use App\SFTConf;
//use Auth;

	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion de funciones.
	*/
class GstFormularios
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:$idModulo = Id del modulo
	*Salida:
	*Descripciòn: Carga todas las funciones de un modulo.
	*/
    public function getFormByAtribbute($atribute,$value)
    {
    	return FuncionesGenerales::getByAtribbute(new Formularios,$atribute,$value);     
    }
    	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:$idModulo = Id del modulo
	*Salida:
	*Descripciòn: Carga todas las funciones de un modulo.
	*/
    public function getDataByAtribbute($model,$atribute,$value)
    {
    	return FuncionesGenerales::getByAtribbute($model,$atribute,$value);     
    }
    public function getCamposJoin($id)
    {
    	return FormCampos::where("id_form",$id)->join('tn_sft_campos','tn_sft_campos.id','tn_sft_detalle_form_campos.id_campo')->get();     
    }
    public function saveForm($model,$data)
    {
    	return FuncionesDB::registrarGetRegistro($model,$data);     
    }
    public function updateForm($model,$data)
    {   
        $id = $data['id'];unset($data['id']);
        return $model::where('id',$id)->update($data);
    }
    public function getForm($id)
    {
    	$data['form'] = $this->getFormByAtribbute(ID,$id);
        $data['campos'] = json_encode($this->getCamposJoin($id));
        $_SESSION[DATA][CLIENTE_TITLE] = $data['form'][0]->nombre_form;
        $_SESSION[DATA][BREAD] = FuncionesGenerales::getBreadByJson($data['form'][0]->rastro_miga);
        return $data;
    }    
    
    
}
