<?php

namespace App\Http\Gst;
use App\ColumnLista;
use App\OpcListas;
use App\Listas;


//use Illuminate\Http\Request;
//use App\Http\Clases\FuncionesGenerales;


	/*
	*Autor: Cristian Cifuentes
	*Modulo: Roles
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para los Roles
	*/
class GstTable
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getTable($id)
    {
    	return Listas::find($id);
    }
    public function getColTable($id){
    	return ColumnLista::where("id_lista",$id)->join("tn_sft_columnas_tabla","tn_sft_columnas_tabla.id","tn_sft_column_lista.id_column")->get();
    }
    public function getColOpcTable($id){
    	return OpcListas::where("id_lista",$id)->get();	
    }
    public function setColum($data){
    	foreach ($data as $key => $value) {
    		$arrColumnas[$value->columna_usuario] = $value->columnas_tabla;
    	}
    	return $arrColumnas;
    }
    public function setOpc($data){
    	foreach ($data as $key => $value) {
    		$arrOpcs[$value->accion] = $value->evento;
    	}
    	return $arrOpcs;
    }
    

}
