<?php 
namespace App\Http\Modulos\ShopCart\Gst;
use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\RemisionesProductos;
use Auth;

class GstRemisiones
{
	public function registrarRemision($data)
	{
		try {
	    	return FuncionesDB::registrarGetRegistro(new RemisionesProductos,$data);
	    } catch (\Exception $e) {
	      error_log("Error: ".$e->getMessage());
	      	return false;
	    }
	}

	public function changeStateRemission($idDomi,$message)
	{
		try {
			RemisionesProductos::where('id_domi',$idDomi)->update(['estado_remision'=>$message]);
			return FuncionesDB::consultarRegistro(new RemisionesProductos,'id_domi',$idDomi);
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
	      	return false;	
		}
	}
}
