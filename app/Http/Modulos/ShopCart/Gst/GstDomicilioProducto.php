<?php 
namespace App\Http\Modulos\ShopCart\Gst;
use Illuminate\Http\Request;
use App\DomicilioProducto;
use Auth;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;

class GstDomicilioProducto
{

	public function registrarDomicilioProducto($data)
	{
		try {
			return FuncionesDB::registrarGetRegistro(new DomicilioProducto,$data);	
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
	      	return false;	
		}
		
	}	
	public function changeStateAddressProduct($idRemision,$message)
	{
		try {
			return DomicilioProducto::where('remision_producto',$idRemision)->update(['estado_producto'=>$message]);	
		} catch (\Exception $e) {
				error_log("Error: ".$e->getMessage());
	      	return false;	
		}
		
	}
	public function getDomicilioProducts($id_domi)
	{
		return DomicilioProducto::where('id_domi',$id_domi)
		->get();
	}
	public function getProducto($id_domi){
		$producto = DomicilioProducto::findOrFail($id_domi);
		

		return $producto['id_pro'];
	}
	public function domicilioProductos($id_domi)
	{
		$data =  DomicilioProducto::where('id_domi',$id_domi)
		->get();
		return FuncionesGenerales::listaOrderDataJoin(new DomicilioProducto,$data,["sft_domicilio"]);
	}
	
	public function cambiarEstadoDomicilioProducto($id,$data)
	{
		try {
			return DomicilioProducto::where('id',$id)->update(['estado_producto'=>$data]);	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
	
	public function consultProducIdDomi($idProdDomi)
	{
		return DomicilioProducto::where('id',$idProdDomi)->get();
	}
	public function consultProducDomicilio($idProdDomi)
	{
		return DomicilioProducto::where('id',$idProdDomi)->get()[0];
	}
	
	public function updateDomicilioProducto($id,$data)
	{
		try {
			return DomicilioProducto::where('id',$id)->update($data);	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
	/*
	Javier R (Programador1)
		Funcion para cambiar al ultimo estado del domicilio "Entregado"
		para que ya no puedan cancelar un producto despues de que ya entre entregado
	*/
	public function cambiarEstadoFinalDomicilioProducto($id_domi,$estado)
	{
		try {
			return DomicilioProducto::where('id_domi',$id_domi)->update(['estado_producto'=>$estado]);	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
	
	public function getProductDomiEstado($id_domi,$estado)
	{
		return DomicilioProducto::where('id_domi',$id_domi)->where('estado_producto',$estado)->get();
	}
	public function consultarProductoDomicilio($id_producto)
	{
		return DomicilioProducto::where('id_pro',$id_producto)->get();
	}
	public function validarDatosNuevoProducto($datos)
	{
		if($datos['idPro'] == null or $datos['idPro'] == "" || $datos['cantidadProd'] == null or $datos['cantidadProd'] == "" || $datos['idDomicilio'] == null or $datos['idDomicilio'] == "")
		{
			return false;
		}else{
			return true;
		}
	}
	public function validarDatosNuevoProductoNumeros($datos)
	{
		if(is_numeric($datos['idPro']) == false or is_numeric($datos['cantidadProd']) == false or is_numeric($datos['idDomicilio']) == false )
		{
			return false;
		}else{
			return true;
		}

	}
	public function updateDomicilioProductoIdDOmi($idDOmi,$data)
	{
		try {
			return DomicilioProducto::where('id_domi',$idDOmi)->update($data);	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
}
