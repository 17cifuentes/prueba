<?php 
namespace App\Http\Modulos\ShopCart\Gst;
use Illuminate\Http\Request;
use App\EstadosDomicilioCliente;
use Auth;

class GstEstadosDomicilioCliente
{
	public function ConsultaOrden($cliente)
	{
		return EstadosDomicilioCliente::where('id_cliente',$cliente)->get();
	}
	public function insertNewstate($data)
	{
		try {
			return EstadosDomicilioCliente::insert($data);
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
			return false;
		}
	}
	public function ConsultaOrdenCliente($cliente)
	{
	  return EstadosDomicilioCliente::join("sft_estados_domicilio","sft_estados_domicilio.id","sft_estados_domi_clientes.id_estado")
	  ->where('id_cliente',$cliente)
	  ->get();
	}

	public function consultarEstadoDomicilioClienteID($id)
	{
		return EstadosDomicilioCliente::
		select("sft_estados_domicilio.nombre_estado","sft_estados_domi_clientes.id as id","sft_estados_domi_clientes.orden")
		->join("sft_estados_domicilio","sft_estados_domicilio.id","sft_estados_domi_clientes.id_estado")
		->where('id_estado',$id)
		->get();
	}
	public function consultarEstadoDomicilioClienteOrden($id)
	{
		return EstadosDomicilioCliente::select("orden")
		->where('id_cliente',$id)
		->get();		
	}	
	public function editarEstadosDomicilioCliente($id_estado,$data,$estado_final)
	{
		try {
			return EstadosDomicilioCliente::where('id_estado',$id_estado)->update(['orden'=>$data,'estado_final'=>$estado_final]);	
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
			return false;
		}
	}

	public function updateDomicilioCliente($id_estado,$orden)
	{
		try {
			return EstadosDomicilioCliente::where('id_estado',$id_estado)->update(['orden'=>$orden]);	
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
			return false;
		}
	}
	public function eliminarEstadosDomicilioCliente($id)
	{
		try {
			return EstadosDomicilioCliente::where('id_estado',$id)->delete();
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
			return false;
		}
	}

	public function estadosDomicilioCliente($id_cliente)
	{
		return EstadosDomicilioCliente::where('id_cliente',$id_cliente)
		->where('orden','1')
		->get();
	}
	public function consultarEstadoActualDomicilio($id_estado)
	{
		return EstadosDomicilioCliente::select('orden')->where('id_estado',$id_estado)->where('estado_final','N')->get();
	}
	public function siguienteOrden($orden,$cliente)
	{
		try {
			return EstadosDomicilioCliente::select('id_estado')->where('orden',$orden)->where('estado_final','N')->where('id_cliente',$cliente)->get();	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}

	public function updateEstadobefore($idcliente,$orden,$data)
	{
		unset($data['id_cliente']);
		unset($data['id_estado']);
		unset($data['orden']);
		return EstadosDomicilioCliente::where('id_cliente',$idcliente)
		->where('orden',$orden)->update($data);
	}
	public function consultarEstadoCancellDOmi($cliente_id)
	{
		return EstadosDomicilioCliente::join("sft_estados_domicilio","sft_estados_domicilio.id","sft_estados_domi_clientes.id_estado")
		->where('id_cliente',$cliente_id)
		->where('estado_final','S')
		->get();
		
	  
	}

	
}