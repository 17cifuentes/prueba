<?php 
namespace App\Http\Modulos\ShopCart\Gst;
use Illuminate\Http\Request;
use App\EstadosDomicilio;
use App\Http\Clases\FuncionesDB;
use Auth;

class GstEstadosDomicilio
{
	public function insertarEstadoDomicilio($data)
	{
		try {
			return FuncionesDB::registrarGetRegistro(new EstadosDomicilio,$data);	
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
			return false;
		}
		
	}
	public function editarEstadosDomicilio($id,$data)
	{
		try{
				return EstadosDomicilio::where('id',$id)->update(['nombre_estado'=>$data]);
			} catch (\Exception $e) {
				error_log("Error: ".$e->getMessage());
				return false;
		}
	}
	public function eliminarEstadosDomicilio($id)
	{
		try {
			return EstadosDomicilio::where('id',$id)->delete();
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
			return false;
		}
		
	}
	
}