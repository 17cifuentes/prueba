<?php 
namespace App\Http\Modulos\ShopCart\Gst;
use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Domicilio;
use Auth;

class GstDomicilio
{
	public function regitrarDomicilio($data)
	{
	    try {
	      	return FuncionesDB::registrarGetRegistro(new Domicilio,$data);
	    } catch (\Exception $e) {
	      error_log("Error: ".$e->getMessage());
	      	return false;	
	    }

	}
	public function answerTransaction($referenceCode,$message,$lapTransactionState)
	{
		try {
			Domicilio::where('referencia',$referenceCode)->update(['domi_estado'=>$message,'transaccionState'=>$lapTransactionState]);
			return FuncionesDB::consultarRegistro(new Domicilio,'referencia',$referenceCode);	
		} catch (\Exception $e) {
			error_log("Error: ".$e->getMessage());
	      	return false;	
		}
		
	}
	public function estadoDomicilio($id_domicilio)
	{
		return Domicilio::where('id',$id_domicilio)->get();
	}
	public function cambiarEstadoDomicilio($id,$data)
	{
		try {
			return Domicilio::where('id',$id)->update(['domi_estado'=>$data]);	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
	public function validarDatosCancell($datos)
	{
		if($datos['idProducDomi'] != null or $datos['idProducDomi'] != ""){

			$validaNum = is_numeric($datos['idProducDomi']);

			if($validaNum == true){

				if($datos['motivo'] != null or $datos['motivo'] != "" ){

					return true;

				}else{
					return false;	
				}
				
			}else{
				return false;	
			}
			
		}else{

			return false;
		}
	}

	public function validateCaractCancell($datos)
	{
		if(strlen($datos['motivo'])>250)
		{
			return false;
		}else{
			return true;
		}
	}

	public function updateDomicilio($id,$data)
	{
		try {
			return Domicilio::where('id',$id)->update($data);	
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
	
}
