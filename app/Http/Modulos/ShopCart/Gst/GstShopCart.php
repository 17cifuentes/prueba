<?php
namespace App\Http\Modulos\ShopCart\Gst;
use Illuminate\Http\Request;
use App\Categorias;
use App\Domicilio;
use App\Roles;
use App\Menu;
use App\SubMenu;
use App\Apartados;
use App\Estructuras;
use App\ConfigPagos;
use App\Modulos;
use App\MotivosCliente;
use Auth;
use App\Http\Clases\FuncionesGenerales;

  /*
  *Autor: Cristian Cifuentes
  *Modulo: Roles
  *Versiòn: 1.0
  *Descripciòn: Clase de gestion para los Roles
  */
class GstShopCart
{
  /*
  *Autor: Cristian Cifuentes
  *Modulo: Funciones
  *Versiòn: 1.0
  *Entrada:
  *Salida:
  *Descripciòn: Carga todos los Roles del sistema.
  */
    public function getMenuCliente($id)
    {
      return Menu::where(TN_CONF_ID_CLIENTE,$id)->get();
    }
    public function  getSubMenuCliente($id){
      return SubMenu::where(TN_CONF_ID_CLIENTE,$id)->get();
    }
  public function  getConfigPago($id  =null){
    if($id == null){
      $id = Auth::user()->cliente_id;
    }
    return ConfigPagos::where(TN_CONF_ID_CLIENTE,$id)->get();
    }
    public function ConfigPagoSave($data){
      return ConfigPagos::where(TN_CONF_ID_CLIENTE,Auth::user()->cliente_id)->update($data);
    }
    public function genReferenceCode(){
      $code1 = FuncionesGenerales::generarCodigo(2);
        $code2 = FuncionesGenerales::generarCodigo(4);
        if(isset(Auth::user()->id)){
          $code3 = Auth::user()->id;
        }else{
          $code3 = FuncionesGenerales::generarCodigo(2);
        }
        return $codeReference = $code1.$code3.$code2; 
    }
    public function validateCaract($data)
    {
      if(isset($data['cliente_id']))
      {
        $num = is_numeric($data['cliente_id']);
          if($num == false)
          {
            return false;
          }
          else{
            if(strlen($data['merchantId']) > 200 or strlen($data['accountId'])>200 or strlen($data['description'])>500 or strlen($data['currency'])>200  or strlen($data['buyerEmail'])>200 or strlen($data['responseUrl'])>200 or strlen($data['api_key'])>200)
            {
              return false;
            }else{
              return true;
          }  
        }
        
      }else{
        if(strlen($data['merchantId']) > 200 or strlen($data['accountId'])>200 or strlen($data['description'])>500 or strlen($data['currency'])>200  or strlen($data['buyerEmail'])>200 or  strlen($data['api_key'])>200)
          {
            return false;
          }else{
            return true;
          }
        }
    }

    public function validateDataConfig($data)
    {
      if(isset($data['cliente_id']))
      {
        if($data['cliente_id'] != "" or $data['merchantId'] != "" or $data['accountId'] != "" or $data['description'] != "" or $data['currency'] != "" or $data['buyerEmail'] != "" or $data['api_key'] != "" )
          {
            return true;
          }else{
            return false;
        }
      }else{
        if($data['merchantId'] != "" or $data['accountId'] != "" or $data['description'] != "" or $data['currency'] != "" or $data['buyerEmail'] != "" or $data['api_key'] != "")
          {
            return true;
          }else{
            return false;
          }
        }

    }

    public function newConfigPagoSave($data)
    {
      try {
        return ConfigPagos::insert($data);
      } catch (\Exception $e) {
        error_log($e);
        return false;
      }
    }
    
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: 
      *Salida: 
      *Descripciòn: Funcion para insertar datos de el Menu 
    */

    public function insertMenu($data)
    {
      try{
          $r = Menu::insert($data);
        return $r;
      }catch(\Exception $e) {
          error_log("Error: ".$e->getMessage());
          return false;
      }
        
    }
   /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: 
      *Salida: 
      *Descripciòn: Funcion para traer todos los datos del Listar Menu 
    */
    public function getAllMenu()
    {
        return Menu::select('sft_mod_shopcarrt_menu.id as id','nombre','cliente_nombre','estado_menu')
        ->join('tn_sft_cliente','tn_sft_cliente.id','sft_mod_shopcarrt_menu.cliente_id')
        ->where('cliente_id',Auth::User()->cliente_id)
        ->get();
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: 
      *Salida: 
      *Descripciòn: Funcion para traer todos los datos del Listar SubMenu 
    */
     public function getAllSubMenu()
    {
        return SubMenu::select('sft_mod_shopcarrt_sub_menu.id as id','sft_mod_shopcarrt_sub_menu.nombre as nombre','sft_mod_shopcarrt_menu.nombre as menu_nombre','cliente_nombre','estado_submenu')
        ->join('tn_sft_cliente','tn_sft_cliente.id','sft_mod_shopcarrt_sub_menu.cliente_id')
        ->join('sft_mod_shopcarrt_menu','sft_mod_shopcarrt_menu.id','sft_mod_shopcarrt_sub_menu.menu_id')->get();
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para traer todos los datos del id menu
    */
    public function getMenuid($id)
    {
        return Menu::find($id);   
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para actualizar-modificar los datos de un Menu
    */
     public function updateMenu($id,$data)
     {
      try {
        
        $r = Menu::where('id',$id)->update($data);
      return $r;

      }catch(\Exception $e) 
        {
        error_log("Error: ".$e->getMessage());
        return false;
        }
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para cambiar estados del Menu(Activo-Inactivo)
    */
     public function CambiarEstado($id,$columna)
     {
        try {
            $r =FuncionesGenerales::cambiarEstadoTable(new Menu,$id,$columna);
          return $r;
        } catch (\Exception $e) {
          error_log("Error: ".$e->getMessage());
          return false;
        }
      }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador para eliminar
      *Salida: 
      *Descripciòn: Funcion para eliminar el Menu segun id
    */
     public function eliminarMenu($id)
     {
      try {

        $s =SubMenu::where('menu_id',$id)->delete();
        $r = Menu::where('id',$id)->delete();
        $r++;
        return $r;
      } catch (\Exception $e) {
        error_log("Error: ".$e->getMessage());
          return false;
      }
        
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: id: identificador para eliminar
      *Salida: 
      *Descripciòn: Funcion para eliminar los datos del submenu segun id
    */
      public function eliminarSubMenu($id)
     {
      try {
        $r =SubMenu::where('id',$id)->delete();
        return $r;
      } catch (\Exception $e) {
       error_log("Error: ".$e->getMessage());
          return false; 
      }
        
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: id: identificador llamar datos por AJAX
      *Salida: 
      *Descripciòn: Funcion para traer datos al crear 
    */
     public function consultaMenu($id)
     {
        return Menu::select('nombre','id')->where('cliente_id',$id)->get();
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: 
      *Salida: 
      *Descripciòn: Funcion para insertar datos de el SubMenu 
    */
     public function insertSubMenu($data)
     {
      try {
        
        $r = SubMenu::insert($data);
      return $r;

      }catch(\Exception $e) 
        {
        error_log("Error: ".$e->getMessage());
        return false;
        }
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para traer todos los datos del id SubMenu
    */
    
    public function getAllSubMenuid($id)
    {
        return SubMenu::find($id);
    }
    
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para cambiar estados del SubMenu(Activo-Inactivo)
    */
    public function cambiarEstadoSub($id,$columna)
     {
      try {
            $r =FuncionesGenerales::cambiarEstadoTable(new SubMenu,$id,$columna);
          return $r;
        } catch (\Exception $e) {
          error_log("Error: ".$e->getMessage());
          return false;
        }
        
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: id: identificador del menu
      *Salida: 
      *Descripciòn: Funcion para traer el id y nombre del menu segun su id
    */
     public function consultaMenuid($id)
     {
        return Menu::select('nombre','id')->where('id',$id)->get();
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para actualizar-modificar los datos de un SubMenu
    */

     public function updatesubMenu($data)
     {
        try {

          $r = SubMenu::where('id',$data['id'])->update($data);
          return $r;
        } catch (\Exception $e) {
          error_log("Error: ".$e->getMessage());
          return false;
        }
        
      }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Domicilio)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para traer todos los domicilios en un listar
    */
    public function getAllDomicilios($id_cliente)
    {
      //return Domicilio::select('sft_domicilio.id as id_domi','referencia','domi_forma_pago','domi_valor','nombre_estado','tipo_transaccion','users.name','sft_domicilio.created_at as fecha_inicio','cancela_motivo')
      //->join('sft_estados_domi_clientes','sft_estados_domi_clientes.id_estado','sft_domicilio.domi_estado')
      //->join('sft_estados_domicilio','sft_estados_domicilio.id','sft_estados_domi_clientes.id_estado')
      //->join('users','users.id','sft_domicilio.domi_usuario_id')
      //->where('sft_domicilio.cliente_id',$id_cliente)
      //->get();
      $data = Domicilio::where('sft_domicilio.cliente_id',$id_cliente)
      ->where('domi_estado','<>','Pendiente Pago')->get();
      return FuncionesGenerales::listaOrderDataJoin(new Domicilio,$data,[]);
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para tarer Informacion del Domicilio segun su id
    */
    public function getDataDomiId($id)
    {
      return  Domicilio::where('sft_domicilio.id',$id)
      ->join('users','users.id','sft_domicilio.domi_usuario_id')
      ->get();
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar
      *Salida: 
      *Descripciòn: Funcion para tarer Informacion del Domicilio
    */
     public function estadoDomicilio($id)
    {
        return Domicilios::where('id_domi',$id)->get();
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenu)
      *Versiòn: 1.0
      *Entrada: id: identificador para editar estado: del domicilio
      *Salida: 
      *Descripciòn: Funcion para cambiar el estado del domicilio
    */
    public function cambiosEstadoDomicilio($id,$estado)
    {
       $est = $this->estadoDomicilio($id);
       $estadoP=$est[0]['domi_estado'];
       if($estado == $estadoP )
       {
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return ('El producto ya se ha sido '.$estado);
       }else
       {
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
          Domicilios::where('id_domi',$id)->update(['domi_estado'=>$estado]);
          return 'El domicilio ha sido '.$estado;
       }
     }
     
    public function getModuloAtributte($atribute,$value)
     {
        return FuncionesGenerales::getByAtribbute(New Modulos,$atribute,$value);
     }

    /* public function registerApartadosProcess($data)
     {
        return Apartados::insert($data);
         $apartados = Apartados::all();
         //dd($apartados);
     }*/
     public function estructuras()
     {
      return Apartados::select('tn_sft_estructuras.id as id' ,'nombre')
      ->join('tn_sft_estructuras','tn_sft_estructuras.id','tn_sft_conf_tienda.estructura_id')
      ->get();
     }
     
     public function getAllApartados()
    {
        return Apartados::select('tn_sft_conf_tienda.id as id','apartado','valor_apartado','estructura_id','cliente_id')
        ->join('tn_sft_cliente','tn_sft_cliente.id','tn_sft_conf_tienda.cliente_id')
        ->join('tn_sft_estructuras','tn_sft_estructuras.id','tn_sft_conf_tienda.estructura_id')
        ->get();
    }

    public function findApartados($id)
    {
        return Apartados::find($id);
    }

     public function getEditarApartados($data)
    {
      return Apartados::where("id",$data["id"])->update($data);
    }

    public function comprobarEstadoCliente($id_estado)
    {
      return Domicilio::where('domi_estado',$id_estado)->get();
    }

    public function motivosCancelProd($cliente,$Modulo)
    {
      return MotivosCliente::where('cliente_id',$cliente)->where('modulo',$Modulo)->get();
    }
}
