<?php
namespace App\Http\Modulos\ShopCart\Controllers;
use URL;
use Auth;
use App\Productos;
use App\Http\Clases\Table;
use App\Http\Clases\Cards;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\CaracteristicasProducto;
use App\Http\Controllers\Controller;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Modulos\ShopCart\Gst\GstShopCart;
use App\Http\Gst\GstUsuarios;
use App\Http\Modulos\Productos\Gst\GstProductos;
use App\Http\Modulos\Productos\Gst\GstCategorias;
use App\Http\Gst\GstCliente;
use App\Http\Modulos\ShopCart\Gst\GstEstadosDomicilio;
use App\Http\Modulos\ShopCart\Gst\GstEstadosDomicilioCliente;
use App\Http\Modulos\ShopCart\Gst\GstDomicilio;
use App\Http\Modulos\ShopCart\Gst\GstDomicilioProducto;
use App\Http\Modulos\ShopCart\Gst\GstRemisiones;
use App\Http\Controllers\Cliente\ClientesController;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Modulos\Promociones\Gst\GstPromociones;
use Session;

class ShopCartController extends Controller
{
  private $gstProductos;
  private $gstCategorias;
  private $gstShopCart;
  private $gstCliente;
  private $gstDomicilio;
  private $gstDomicilioProducto;
  private $GstRemisiones;
  private $gstEstadosDomicilio;
  private $gstEstadosDomicilioCliente;
  private $gstPromociones;
  private $gstUsuarios;

  public function __construct(GstShopCart $gstShopCart,GstProductos $gstProductos,GstCategorias $gstCategorias, GstCliente $gstCliente, GstDomicilio $gstDomicilio, GstDomicilioProducto $gstDomicilioProducto, GstRemisiones $gstRemisiones,GstEstadosDomicilio $gstEstadosDomicilio, GstEstadosDomicilioCliente $gstEstadosDomicilioCliente,GstPromociones $gstPromociones, GstUsuarios $gstUsuarios) 
  {

   $this->middleware(MIN_AUTH, [MIDDLEWARE_EXCEPT => ['tienda','registerProductsDatabases','mostrarProductoDetalle','addProductToCart','removeProToCart','emptyCart','buscadorCategorias','crearEstadoDomicilioCliente','ordenarEstadosDomicilio','eliminarEstadoDomicilioCliente','compararOrdenDomicilio','buscarItemNavBar']]);

   session_start();
   $this->gstUsuarios = $gstUsuarios;
   $this->gstProductos = $gstProductos;
   $this->gstCategorias = $gstCategorias;
   $this->gstShopCart = $gstShopCart;
   $this->gstCliente = $gstCliente;
   $this->gstDomicilio = $gstDomicilio;
   $this->gstDomicilioProducto = $gstDomicilioProducto;
   $this->gstRemisiones = $gstRemisiones;
   $this->gstEstadosDomicilio = $gstEstadosDomicilio;
   $this->gstEstadosDomicilioCliente = $gstEstadosDomicilioCliente;
   $this->gstPromociones = $gstPromociones;
 }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion Vista Para La Gestion del Menu de la tienda
   */
    public function gstShopCartMenu()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Gestion Menú Tienda';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda'=>"#"];

      return view('Modulos.ShopCart.gestionMenu');
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion llevar a la vista del crear Menu
   */
    public function crearMenuTienda()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Crear Menú Tienda';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda'=>'gstShopCartMenu',"Crear Menú"=>"#"];
      return view('Modulos.ShopCart.crearMenuTienda');
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para crear Menú a la tienda
   */
    public function crearMenuProcess()
    {
      if(isset($_POST['nombre']) && isset($_POST['estado_menu']))
      {
        if(strlen($_POST['nombre'])> 250)
        {
          $_SESSION[DATA][MSG] = 'Demasiados Caracteres';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/crearMenuTienda');
        }
        unset($_POST['_token']);
        $_POST['cliente_id'] =Auth::user()->cliente_id;
        $response = $this->gstShopCart->insertMenu($_POST);
        if($response)
        {
          $_SESSION[DATA][MSG] = 'Registro de Menú exitoso';
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
          return redirect('carritoCompras/listarMenu');  
        }else
        $_SESSION[DATA][MSG] = 'Error Al Registrar Menú';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/crearMenuTienda');
      }else
      { 
        $_SESSION[DATA][MSG] = 'Error faltan datos';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/crearMenuTienda');
      }


    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion listar todos los Menú de la tienda
   */
    public function listarMenu()
    {
        //ClientesController::configuration();
      $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Menú';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda '=>'gstShopCartMenu','Listar Menú'=>""]; 
      $tableMenu = new Table;
      $tableMenu->setColum($tableMenu,
        [
          'id' => 'id',
          'Nombre Menú'=>'nombre',
          'Cliente'=>'cliente_nombre',
          'Estado Menú'=> 'estado_menu'
        ]
      );
      $opciones = 
      [
        'Editar'=> 'carritoCompras/editarMenuTienda',
        'val- Eliminar' => 'carritoCompras/eliminarMenuid',
        'val- cambiar estado'=> 'carritoCompras/cambiarEstadoMenu'

      ];

      $datos = $this->gstShopCart->getAllMenu()->toArray();

      $tableMenu->setItems($tableMenu,$datos);
      $tableMenu = $tableMenu->dps($tableMenu);

      return view(PATH_LIST,[TABLE=>$tableMenu,'opc'=>json_encode($opciones)]);
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador del menu a modificar
      *Salida: 
      *Descripciòn: Funcion llevar a la vista del editar del Menú
   */
    public function editarMenuTienda()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Editar Menú';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda'=>'gstShopCartMenu','Listar Menú'=>'listarMenu','Editar Menú'=>"#"];

      $menu = $this->gstShopCart->getMenuid($_GET);
      $clientes = $this->gstCliente->getAllClientes();

      return view('Modulos.ShopCart.editarMenuTienda',compact('menu','clientes'));
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador del menu a modificar
      *Salida: 
      *Descripciòn: Funcion para modificar el menú seleccionado
   */
    public function editarMenuProcess()
    {

      if(isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['cliente_id']) && isset($_POST['estado_menu']))
      {
        if (filter_var($_POST['id'], FILTER_VALIDATE_INT))
        {
          if(strlen($_POST['nombre'])> 250)
          {
            $_SESSION[DATA][MSG] = 'Demasiados Caracteres';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('carritoCompras/listarMenu');
          }
          $consulta = $this->gstShopCart->getMenuid($_POST['id']);
          if($consulta != null)
          {
            unset($_POST['_token']);
            $id=$_POST['id'];
            unset($_POST['id']);
            $response =$this->gstShopCart->updateMenu($id,$_POST);
            if($response)
            {
              $_SESSION[DATA][MSG] = 'Actualizacion exitosa';
              $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
              return redirect('carritoCompras/listarMenu');
            }else
            $_SESSION[DATA][MSG] = 'Error al actualizar datos';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('carritoCompras/listarMenu');

          }else
          {
            $_SESSION[DATA][MSG] = 'Error el identificador no existe en la base de datos';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('carritoCompras/listarMenu');
          }
        }else
        {
          $_SESSION[DATA][MSG] = "Error el identificador no es un numero";
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/listarMenu');
        }

      }else
      $_SESSION[DATA][MSG] = 'Error faltan datos';
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('carritoCompras/listarMenu');


    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu)
      *Versiòn: 1.0
      *Entrada: id: identificador del menu a cambiar de estado
      *Salida: 
      *Descripciòn: Funcion para cambiar el estado del Meú
   */

    public function cambiarEstadoMenu()
    {

      $res = $this->gstShopCart->CambiarEstado($_GET['id'],'estado_menu');
      if($res)
      { 
        $_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('carritoCompras/listarMenu');
      }else 
      $_SESSION[DATA][MSG] = 'Error al cambiar estado';
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('carritoCompras/listarMenu');
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: id: identificador del menu a modificar
      *Salida: 
      *Descripciòn: Funcion llevar a la vista del gestionar del SubMenú
   */
     public function eliminarMenuid()
     {
      $res = $this->gstShopCart->eliminarMenu($_GET);
      if($res)
      { 
        $_SESSION[DATA][MSG] = 'Se elimino exitosamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('carritoCompras/listarMenu');
      }else 
      $_SESSION[DATA][MSG] = 'Error al eliminar';
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('carritoCompras/listarMenu');
    }

    public function gestionSubMenu()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Gestion SubMenú Tienda';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda'=>'gstShopCartMenu','Gestion SubMenú Tienda'=>"#"];
      return view('Modulos.ShopCart.gestionSubMenu');
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion llevar a la vista del crear SubMenú
    */

     public function crearSubMenu()
     {

      $_SESSION[DATA][CLIENTE_TITLE] = 'Crear SubMenú Tienda';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda'=>
      'gstShopCartMenu','Gestion SubMenú Tienda'=>'gestionSubMenu',"Crear SubMenú"=>"#"];
      $cliente = $this->gstCliente->getCliente(Auth::user()->cliente_id);
      return view('Modulos.ShopCart.crearSubMenu',compact('cliente'));  
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: $id: cliente
      *Salida: 
      *Descripciòn: Funcion para consultas del menú (Ajax) mediante el cliente id
    */

     //Funcion Para AJAX
     public function consultaMenu()
     {
      $menu = $this->gstShopCart->consultaMenu($_GET['id']);
      return json_encode($menu);
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para crear un SubMenú
    */

     public function crearSubMenuProcess()
     {
      if(isset($_POST['cliente_id']) && isset($_POST['menu_id']) && isset($_POST['nombre']) && isset($_POST['menu_id']) && isset($_POST['estado_submenu']))
      {
        if(strlen($_POST['nombre'])> 250)
        {
          $_SESSION[DATA][MSG] = 'Error Demasiados Caracteres';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/crearSubMenu'); 
        }
        unset($_POST['_token']);
        $response = $this->gstShopCart->insertSubMenu($_POST);
        if($response)
        {
          $_SESSION[DATA][MSG] = 'Registro exitoso';
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
          return redirect('carritoCompras/listarSubMenu');
        }else
        {
          $_SESSION[DATA][MSG] = 'Error Al Registrar';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/crearSubMenu');
        }
      }else
      {
        $_SESSION[DATA][MSG] = 'Error faltan datos';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/crearSubMenu');
      }

    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion listar todos los SubMenú 
    */

     public function listarSubMenu()
     {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Listar SubMenú';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda '=>'gstShopCartMenu','Gestion SubMenú Tienda'=>'gestionSubMenu','Listar SubMenú'=>"#"];

      $tablesubMenu = new Table;
      $tablesubMenu->setColum($tablesubMenu,
        [
          'id' => 'id',
          'Nombre SubMenú'=>'nombre',
          'Menú'=>'menu_nombre',
          'cliente'=>'cliente_nombre',
          'Estado SubMenú'=> 'estado_submenu'
        ]
      );
      $opciones = 
      [
        'Editar'=> 'carritoCompras/editarsubMenuTienda',
        'val- Cambiar estado'=> 'carritoCompras/cambiarEstadoSubMenu',
        'val- Eliminar SubMenú'=> 'carritoCompras/eliminarSubMenuid'
      ];
      $datos = $this->gstShopCart->getAllSubMenu()->toArray();

      $tablesubMenu->setItems($tablesubMenu,$datos);
      $tablesubMenu = $tablesubMenu->dps($tablesubMenu);

      return view(PATH_LIST,[TABLE=>$tablesubMenu,'opc'=>json_encode($opciones)]);

    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: id del submenú a modificar
      *Salida: 
      *Descripciòn: Funcion para cambiar estados de los SubMenú 
    */

    public function cambiarEstadoSubMenu()
    {

      $res = $this->gstShopCart->cambiarEstadoSub($_GET['id'],'estado_subMenu');
      if($res)
      { 
        $_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('carritoCompras/listarSubMenu');
      }else 
      $_SESSION[DATA][MSG] = 'Error al cambiar estado';
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('carritoCompras/listarSubMenu');
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: $id del submenu a modificar
      *Salida: 
      *Descripciòn: Funcion para llevar a la vusta del editar los SubMenú 
    */

    public function editarsubMenuTienda()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Editar SubMenú';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Menú Tienda '=>'gstShopCartMenu','Gestion SubMenú Tienda'=>'gestionSubMenu','Listar SubMenú'=>'listarSubMenu','Editar SubMenú'=>"#"];

      $datos = $this->gstShopCart->getAllSubMenuid($_GET);
      $menu= $this->gstShopCart->consultaMenuid($datos[0]['menu_id']);
      $cliente = $this->gstCliente->getCliente($datos[0][TN_CONF_ID_CLIENTE]);
      return view('Modulos.ShopCart.editarSubMenu',compact('cliente','datos','menu'));
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para editar los SubMenú 
    */

    public function editarSubMenuProcess()
    {
      if(isset($_POST['nombre']) && isset($_POST['estado_subMenu']) && isset($_POST['id']) )
      {
        if (filter_var($_POST['id'], FILTER_VALIDATE_INT))
        {
          $consulta = $this->gstShopCart->getAllSubMenuid($_POST['id']);
          if($consulta != null)
          {
            if(strlen($_POST['nombre'])> 250)
            {
              $_SESSION[DATA][MSG] = 'Error Demasiados Caracteres';
              $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
              return redirect('carritoCompras/listarSubMenu');
            }
            unset($_POST['_token']);
            $response = $this->gstShopCart->updatesubMenu($_POST);

            if ($response) {
              $_SESSION[DATA][MSG] = 'Actualizacion exitosa';
              $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;    
              return redirect('carritoCompras/listarSubMenu'); 
            }else{
              $_SESSION[DATA][MSG] = 'Error no se pudo actualizar';
              $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;    
              return redirect('carritoCompras/listarSubMenu'); 
            }
          }else
          {
            $_SESSION[DATA][MSG] = "Error el identificador no existe en la base de datos";
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('carritoCompras/listarMenu');   
          } 
        }else
        {
          $_SESSION[DATA][MSG] = "Error el identificador no es un numero";
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/listarMenu');
        } 
      }else
      {
        $_SESSION[DATA][MSG] = 'Error Faltan Datos';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/listarSubMenu');
      }

    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Menu-SubMenú)
      *Versiòn: 1.0
      *Entrada: id: identificador para eliminar
      *Salida: 
      *Descripciòn: Funcion para eliminar los SubMenú 
    */

    public function eliminarSubMenuid()
    {
      $respu = $this->gstShopCart->eliminarSubMenu($_GET);
      if ($respu) 
      {
        $_SESSION[DATA][MSG] = 'Se elimino exitosamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('carritoCompras/listarSubMenu');
      }else
      $_SESSION[DATA][MSG] = 'Error al eliminar';
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('carritoCompras/listarSubMenu');
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Domicilios)
      *Versiòn: 1.0
      *Entrada: 
      *Salida: 
      *Descripciòn: Funcion para redireccionar a la vista del Gestion De Domicilios
    */
     public function gestionDomicilios()
     {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Gestion Domicilios';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios '=>"#"];
      return view('Modulos.ShopCart.gestionDomicilios');
    }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Domicilios)
      *Versiòn: 1.0
      *Entrada: 
      *Salida: 
      *Descripciòn: Funcion para listar todos los Domicilios 
    */
     public function listarDomicilios()
     {

      $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Domicilios';

      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios '=>'gestionDomicilios','Listar Domicilios'=>"#"];
      $tableDomicilio = new Table;
      $tableDomicilio->setColum($tableDomicilio,
        [
          'id'=> 'id',
          'Domicilio' =>'id',
          'Usuario'=> 'name',
          'Estado domicilio'=>'nombre_estado',
          'Referencia'=> 'referencia',
          'Forma de pago'=> 'domi_forma_pago',
          'Valor domicilio'=> 'domi_valor',
          'Tipo transacion'=> 'tipo_transaccion',
          'Fecha '=> 'created_at',
          'Cancela motivo'=> 'cancela_motivo'
        ],
        $opciones = 
        [
          'Productos domicilio' => 'carritoCompras/listProdDomicilio',
          'fun- Informacion Usuario'=>'datosUsuario',
          'val- Cambiar estado' => 'carritoCompras/cambiarEstadoDomicilio',
          'fun- Cancelar domicilio' => 'cancelarDomicilio'
        ]
      );
      $datos = $this->gstShopCart->getAllDomicilios(Auth::user()->cliente_id);
      $tableDomicilio->setItems($tableDomicilio,$datos);
      $tableDomicilio = $tableDomicilio->dps($tableDomicilio);
      return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA],TABLE=>$tableDomicilio,'opc'=> json_encode($opciones)]);

    }
    public function listProdDomicilio()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Productos Domicilio';

      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios '=>'gestionDomicilios','Listar Domicilios'=>'listarDomicilios','Productos Domicilios'=>"#"];
      $tableDomicilio = new Table;
      $tableDomicilio->setColum($tableDomicilio,
        [
          'id'=> 'id',
          'Domicilio' =>'id_domi',
          'Remision producto'=>'remision_producto',
          'Nombre producto'=> 'pro_descripcion',
          'Cantidad'=> 'cantidad_producto',
          'Precio unitario'=> 'precio_unitario',
          'Subtotal Producto'=> 'subtotal_producto',
          'Producto cancelado'=> 'cancela_motivo',
        ],
        $opciones = 
        [
          'fun- Cancelar producto'=> 'cancelarProducto',
          'fun- Detalle producto'=> 'detalleProducto'
        ]
      );
      $datos = $this->gstDomicilioProducto->domicilioProductos($_GET['id']);
      $domicilio = $this->gstShopCart->getDataDomiId($_GET['id'])->toArray();
      $tableDomicilio->setItems($tableDomicilio,$datos);
      $tableDomicilio = $tableDomicilio->dps($tableDomicilio);
      $estadoCancelado= $this->consultarEstadoCancelar(Auth::user()->cliente_id);
      $configurationCliente = $this->gstEstadosDomicilioCliente->estadosDomicilioCliente(Auth::user()->cliente_id);
      $estado = $configurationCliente[0]['id_estado'];

      return view('Modulos.ShopCart.listarDomicilioProductos',['estado'=>$estado,'estadoCancelado'=>$estadoCancelado,'domicilio'=>$domicilio,SFT_LIB=>[SFT_DATATABLE],SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA],TABLE=>$tableDomicilio, 'opc'=> json_encode($opciones)]);
    }

     
    public function cambiarEstadoDomicilio()
     {
      if(!isset($_GET['id'])){

          $_SESSION[DATA][MSG] = 'No existe el identificador';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/listarDomicilios');
      }
      $respuesta = $this->validarDomicilioCancelado();
      if($respuesta == true)
      {
          $_SESSION[DATA][MSG] = 'Error el domicilio se encuentra cancelado';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/listarDomicilios');
      }

        $estadosCliente =$this->gstEstadosDomicilioCliente->consultarEstadoDomicilioClienteOrden(Auth::user()->cliente_id);
       $cantidadEstadosCliente = count($estadosCliente);
       
       $estadoDomicilio= $this->gstDomicilio->estadoDomicilio($_GET['id']);
       
       $consultarDato = is_numeric($estadoDomicilio[0]['domi_estado']);
       if($consultarDato == false)
       {
          $estado = $this->gstEstadosDomicilioCliente->estadosDomicilioCliente(Auth::user()->cliente_id);
          $respuesta = $this->gstDomicilio->cambiarEstadoDomicilio($_GET['id'],$estado[0]['id_estado']);
            if($respuesta){
                $_SESSION[DATA][MSG] = 'Se cambio el estado satisfactoriamente';
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }else{
                $_SESSION[DATA][MSG] = 'Error al cambiar estado del domicilio, revise los datos o contacte al Administrador';
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }  
        }else{
          $data =$this->gstEstadosDomicilioCliente->consultarEstadoActualDomicilio($estadoDomicilio[0]['domi_estado']);

          $nextOrden = $data[0]['orden']+1;

          if( $nextOrden <= $cantidadEstadosCliente )
          {
            $estado = $this->gstEstadosDomicilioCliente->siguienteOrden($nextOrden,Auth::user()->cliente_id);  
            $existeSiguienteEstado = count($estado);
              
              $respuesta = $this->validarUltimoEstado($_GET['id'],$nextOrden);
              if($respuesta != false )
              {
                $this->cambiarUltimoEstadoDomicilioProducto($_GET['id']);  
              }
            
          }else{
            $_SESSION[DATA][MSG] = 'Error no hay mas estados a seguir';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('carritoCompras/listarDomicilios');
          }

          if($existeSiguienteEstado > 0)
          {
            $respuesta = $this->gstDomicilio->cambiarEstadoDomicilio($_GET['id'],$estado[0]['id_estado']);
          }else{
            $_SESSION[DATA][MSG] = 'Error no hay mas estados a seguir';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('carritoCompras/listarDomicilios');
          }
          
        }
        if($respuesta){
          $_SESSION[DATA][MSG] = 'Se cambio el estado satisfactoriamente';
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }else{
          $_SESSION[DATA][MSG] = 'Error al cambiar estado del domicilio, revise los datos o contacte al Administrador';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        }
        return redirect('carritoCompras/listarDomicilios');
     }
     /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para consultar el domicilio si se encuentra en su ultimo estado para cambiar el estado del producto a entregado
  */
     public function validarUltimoEstado($id_domicilio,$ordenActual)
     {
        $orden = $this->gstEstadosDomicilioCliente->consultarEstadoDomicilioClienteOrden(Auth::user()->cliente_id);
        $cantidadEstados = count($orden);
        if($cantidadEstados == $ordenActual )
        {
          return true;
        }
        return false;
     } 
     public function cambiarUltimoEstadoDomicilioProducto($id_domi)
     {
      $estado_final = "Entregado";
        $this->gstDomicilioProducto->cambiarEstadoFinalDomicilioProducto($id_domi,$estado_final);
     }

  /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para consultar el domicilio si se encuentra cancelado
  */
    public function validarDomicilioCancelado()
    {
      $consultarEstadoActualDomicilio = $this->gstDomicilio->estadoDomicilio($_GET['id']);
      $estadoCancelado = $this->consultarEstadoCancelar();

        if($consultarEstadoActualDomicilio[0]['domi_estado'] == $estadoCancelado[0]['id_estado'] )
        {
          return true;
        }
      return false;
    }   
     
    public function addProductToCart(){
      $this->getAtributesPagos();
      $producto = $this->gstProductos->getProductoById($_GET[SHOPCART_ID_PRODUCTO])->toArray(); 
      
      for ($i=1; $i < count($_SESSION[SHOPCART_PRODUCTOS_NAV]); $i++) { 

        if($_SESSION[SHOPCART_PRODUCTOS_NAV][$i][0]['id'] == $_GET[SHOPCART_ID_PRODUCTO]){

          $_SESSION[SHOPCART_PRODUCTOS_NAV][$i]['pro_cantidad_cart'] += 1;

        }

        $ids[] = $_SESSION[SHOPCART_PRODUCTOS_NAV][$i][0]['id'];

      }
      
      if(!isset($ids) or !in_array($_GET[SHOPCART_ID_PRODUCTO],$ids)){

        $producto['pro_cantidad_cart'] = 1;

        $_SESSION[SHOPCART_PRODUCTOS_NAV][] = $producto;

        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

        $_SESSION[DATA][MSG] = 'Producto agregado correctamente';

      }else{

        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

        $_SESSION[DATA][MSG] = 'El producto ya se encuentra en el carrito';

      }

      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] = $this->calcularTotalShopCart();

      return json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]);
    }
    public function getAtributesPagos(){
      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode'] = $this->gstShopCart->genReferenceCode();
      if(isset($_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'])){
        $total = str_replace(",", "",$_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] );
      }else{
        $total = 0;    
      }

      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['signature'] = md5($_SESSION[SHOPCART_CONFIGPAGOS][0]->api_key.'~'.$_SESSION[SHOPCART_CONFIGPAGOS][0]->merchantId.'~'.$_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode'] .'~'.(string)$total.'~'.$_SESSION[SHOPCART_CONFIGPAGOS][0]->currency); 
    }
    public function calcularTotalShopCart($producto = null){
      $total = 0;
      if(!isset($_SESSION[SHOPCART_PRODUCTOS_NAV])){
        $total = 0;
      }else{
        if(isset($_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode']) and isset($_SESSION[SHOPCART_PRODUCTOS_NAV][0]['signature'])){
          $referenceCode = $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode'];
          $signature = $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['signature'];

        }
        unset($_SESSION[SHOPCART_PRODUCTOS_NAV][0]);
        if(count($_SESSION[SHOPCART_PRODUCTOS_NAV]) > 0){
          foreach($_SESSION[SHOPCART_PRODUCTOS_NAV] as $pro){
            $total += $pro[0]['pro_precio_venta']*$pro['pro_cantidad_cart'];
          }
        }else{
          $total = 0;
        }

        $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode'] = $referenceCode;
        $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['signature'] = $signature;    


      }
      if($producto == null){
        $producto[0]['pro_precio_venta'] = 0;
      }
      return number_format($total+$producto[0]['pro_precio_venta']);
    }
    public function removeProToCart(){
        //dd($_SESSION[SHOPCART_PRODUCTOS_NAV]);
      foreach ($_SESSION[SHOPCART_PRODUCTOS_NAV] as $key => $value) {
        if($key != 0){
          if($value[0]['id'] == $_GET[SHOPCART_ID_PRODUCTO]){
            unset($_SESSION[SHOPCART_PRODUCTOS_NAV][$key]);
            $this->getAtributesPagos();
          }
        }
      }
      if($_SESSION[SHOPCART_PRODUCTOS_NAV] != null){
        $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] = $this->calcularTotalShopCart(); 
      }
      return json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]);
    }
    
    public function emptyCart(){
      $_SESSION[SHOPCART_PRODUCTOS_NAV] = [];
      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode'] =000;
      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] ="0,0";
      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['signature'] ="0";
      return json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]);
    }
    public function tienda(){  

      if (!isset($_GET['id']) || $_GET['id']== null)  
      {
        return redirect ('/');
      }
      
      $allClientes = $this->gstCliente->getCliente($_GET['id']);
      if (count($allClientes) == 0)
      {

        return redirect ('/');
      }
      $_SESSION[DATA] = ClientesController::configuration($_GET[ID]);
      $_SESSION[CATEGORIES] = $this->gstCategorias->getCategoriasPadres($_GET[ID]);    
      $_SESSION[MENU_SHOPCART] = $this->gstShopCart->getMenuCliente($_GET[ID]);
      $_SESSION[SUBMENU_SHOPCART] = $this->gstShopCart->getSubMenuCliente($_GET[ID]);
      $_SESSION[SHOPCART_PRODUCTOS] = $this->gstProductos->getProductosShopCart($_GET[ID]);
      $_SESSION[SHOPCART_CONFIGPAGOS] = $this->gstShopCart->getConfigPago($_GET[ID]);
      $_SESSION[SHOPCART_PROMOCION_DEL_MES] = $this->gstPromociones->getPromocionMesActual($_GET[ID]);

      $validate = count($_SESSION[SHOPCART_CONFIGPAGOS]);
      if($validate == 0)
      {
        return redirect ('/ingresar/'.$_GET[ID].'/9');
      }
      $this->getAtributesPagos(); 

      if(!isset($_SESSION[SHOPCART_PRODUCTOS_NAV]) or count($_SESSION[SHOPCART_PRODUCTOS_NAV]) == 0){
        $_SESSION[SHOPCART_PRODUCTOS_NAV] = [];
      }

      else
      {
        $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] = $this->calcularTotalShopCart();
      }

      $atrFavorito = $this->gstShopCart->getModuloAtributte('mod_nombre','Favoritos');

      if (count($atrFavorito) > 0)
      {
        $evaluacion = PermisosController::checkPermisosModulos($atrFavorito[0]->id);


        if($evaluacion)
        {
          $favoritos = new \App\Favoritos;
          $gstFav = new \App\Http\Modulos\Favoritos\Gst\GstFavoritos;
          $proFavoritos = $gstFav->getFavoritoUsuarioEmisor(Auth::user()->id,$_GET['id']);

        }else{
          $proFavoritos = null;
        }

      }

      $atrInventario = $this->gstShopCart->getModuloAtributte('mod_nombre','Inventario');

      if (count($atrInventario) > 0)
      {
        $permisoInventario = PermisosController::checkPermisosModulos($atrInventario[0]->id);
        if($permisoInventario)
        {
          $gstInv = new \App\Http\Modulos\Inventario\Gst\GstInventario;
          $inventario = $gstInv->getInventarioByAtributo('cliente_id',Auth::user()->cliente_id);
        }else{
          $inventario = null;
        }
      }

      $promociones = $this->gstShopCart->getModuloAtributte('mod_nombre','Promociones');

      if(count($promociones) > 0)
      {
        $permisoPromociones = PermisosController::checkPermisosModulos($promociones[0]->id);

        if($permisoPromociones)
        { 

          $allPromociones = $this->gstPromociones->getPromocionesJoin();
          $hoy = date('Y-m-d');
        }else{
          $hoy = null;
          $allPromociones = null;
        }
      }

      if(isset($_GET["msg"]))
      {
       if($_GET["msg"] == "1")
       {
         Session::flash(NOTIFICACION_SUCCESS, 'Producto agregado a favoritos exitosamente');
       }if($_GET["msg"] == "2")
       {
        Session::flash(NOTIFICACION_ERROR, 'El producto ya no esta en tus favoritos');
      }if($_GET["msg"] == "3")
      {
       Session::flash(NOTIFICACION_SUCCESS, 'Registro de domicilio exitoso');
     }if($_GET["msg"] == "4")
     {
      Session::flash(NOTIFICACION_ERROR, 'Error, las referencias del domicilio no coiciden');
    }if($_GET["msg"] == "5")
    {
      Session::flash(NOTIFICACION_ERROR, 'No hay un producto que corresponda con esta categoría');
    }if($_GET["msg"] == "6")
    {
      Session::flash(NOTIFICACION_ERROR, 'No hay un producto que corresponda con este item');
    }
    $_SESSION['data']['shorUrl'] = true;
  }
  return view($_SESSION['data']["conf"][0]->layoutsUsu.".index",[ID=>$_GET[ID],"permisoFavorito"=>$evaluacion,"proFavoritos"=>$proFavoritos,"permisoInventario"=>$permisoInventario,"inventario"=>$inventario,"permisoPromociones"=>$permisoPromociones,"allPromociones"=>$allPromociones,"hoy"=>$hoy,SFT_LIB_MOD=>['Productos',SFT_LIB_MOD_TIENDA]]);           
}

public function carrito()
{
  $_SESSION[DATA] = ClientesController::configuration($_GET[ID]);
  return view($_SESSION['data']["conf"][0]->layoutsUsu.".carritoCompras",[SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);   
}
public function descripcionProductos()
{
 $allProductos = $this->gstProductos->getProductoByAtributo('id',$_GET['id']);
 $_SESSION[DATA] = ClientesController::configuration($_GET['idCliente']);
 return view($_SESSION['data']["conf"][0]->layoutsUsu.'.descripcionProductos',[ID=>$_GET['id'],"allProductos"=>$allProductos,SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);   
}

public function contacto()
{
  $_SESSION[DATA] = ClientesController::configuration($_GET[ID]);
  return view($_SESSION['data']["conf"][0]->layoutsUsu.".contact",[SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);   
}

    /*public function crearApartados()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Registro Apartados Tienda';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Registro apartados Tienda'=>"#"];
      $estructura = $this->gstShopCart->estructuras();
      //dd($estructura);
      return view('Modulos.ShopCart.crearApartadosTienda', compact('estructura'));
    }

    public function crearApartadosProcess()
    {
      unset($_POST['_token']);

      $_POST['id'] =Auth::user()->id;
      if($_FILES['apartado']['name'] != ''){
              $_POST['apartado'] = FuncionesGenerales::cargararchivo($_FILES['apartado'],'Modulos/ShopCart/img','Imagen_no_disponible.svg.png');
          }else{
                  unset($_POST['apartado']);
                }
      if($_FILES['valor_apartado']['name'] != ''){
                $_POST['valor_apartado'] = FuncionesGenerales::cargararchivo($_FILES['valor_apartado'],'Modulos/ShopCart/img','Imagen_no_disponible.svg.png');
          }else{ 
                  unset($_POST['valor_apartado']);
                }
      $_POST['cliente_id'] =Auth::user()->cliente_id;
      //dd($_POST);
      $apartados = $this->gstShopCart->registerApartadosProcess($_POST);
      $_SESSION[DATA][MSG] = 'Su Apartado se registro correctamente';
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return view('Modulos.ShopCart.crearApartadosTienda',compact('apartados'));
    }*/

    public function listarApartados()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Lista apartados';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Lista apartados'=>"#"]; 
      $tableMenu = new Table;
      $tableMenu->setColum($tableMenu,
        [
          'id' => 'id',
          'Apartado'=>'apartado',
          'Valor apartado'=>'valor_apartado',
          'Estructura_id'=> 'estructura_id',
          'Cliente_id'=> 'cliente_id'

        ]
      );
      $opciones = 
      [
        'val- Editar' => 'carritoCompras/editarApartados',
        'Gestionar' =>    'carritoCompras/gestionarApartados'

      ];

      $datos = $this->gstShopCart->getAllApartados()->toArray();
      $tableMenu->setItems($tableMenu,$datos);
      $tableMenu = $tableMenu->dps($tableMenu);
      return view(PATH_LIST,[TABLE=>$tableMenu,'opc'=>json_encode($opciones)]);

    }

    /*public function eliminarApartados()
    {
      $_SESSION[DATA][MSG] = 'Apartado eliminado correctamente';
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      $this->gstShopCart->deleteApartados($_GET['id']);
      return redirect('carritoCompras/listarApartados');

    }*/
    public function editarApartados()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Editar Apartados Tienda';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Lista apartados'=>'/carritoCompras/listarApartados','Editar apartados'=>"#"];
      $apartados = $this->gstShopCart->findApartados($_GET['id']);

      return view('Modulos.ShopCart.editarApartadosTienda',compact('apartados'));

    }
    public function editarApartadosProcess()
    {
      unset($_POST['_token']);

      if($_FILES['apartado']['name'] != ''){
        $_POST['apartado'] = FuncionesGenerales::cargararchivo($_FILES['apartado'],'Modulos/ShopCart/img','Imagen_no_disponible.svg.png');
      }else{
        unset($_POST['apartado']);
      }
      if($_FILES['valor_apartado']['name'] != ''){
        $_POST['valor_apartado'] = FuncionesGenerales::cargararchivo($_FILES['valor_apartado'],'Modulos/ShopCart/img','Imagen_no_disponible.svg.png');
      }else{ 
        unset($_POST['valor_apartado']);

        $_POST['id'] =Auth::user()->id;
        $_SESSION[DATA][MSG] = 'Su Apartado se actualizo correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

        return redirect('carritoCompras/listarApartados');
      }

    }

    public function newCantProCart(){
      $cont = 0;
      foreach ($_SESSION[SHOPCART_PRODUCTOS_NAV] as $key => $pro) {
        if($key != 0){
          if($pro[0]['id'] == $_GET[SHOPCART_ID_PRODUCTO]){
            $_SESSION[SHOPCART_PRODUCTOS_NAV][$key]['pro_cantidad_cart'] = $_GET['cantPro'];
          }
          $cont++;
        }

      }
      $this->getAtributesPagos();
      $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'] = $this->calcularTotalShopCart();
      return json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]);
    }
    public function configPagos(){

      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú tienda'=>'/Menu/27','Configurar pagos'=>''];
      $_SESSION[DATA][CLIENTE_TITLE] = 'Configuación pagos por paYu';
      $pago =  $this->gstShopCart->getConfigPago();
      $respuesta = count($pago);
      if($respuesta> 0 )
      {
        return view('Modulos.ShopCart.formConfigPago',compact('pago'));  
      }
      
      $urlRespuesta =$_SERVER['HTTP_HOST']."/carritoCompras/responseUrl/";
      return view('Modulos.ShopCart.formConfigPago1',compact('urlRespuesta'));
      /*$urlRespuesta =$_SERVER['REMOTE_ADDR']."/carritoCompras/responseUrl/";
      return view('Modulos.ShopCart.formConfigPago1',compact('urlRespuesta'));*/
    }

    public function newConfigPagoProccess(){

      unset($_POST['_token']);
      $_POST['cliente_id'] = Auth::user()->cliente_id;
      $respuestaValida = $this->gstShopCart->validateDataConfig($_POST);
      $respuestaValida2 = $this->gstShopCart->validateCaract($_POST);
      if($respuestaValida == false or $respuestaValida2 == false )

      {
        $_SESSION[DATA][MSG] = 'Error revise los datos y reintente de nuevo';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return $this->configPagos();
      }

      $respuesta = $this->gstShopCart->newConfigPagoSave($_POST);
      if($respuesta)
      {
        $_SESSION[DATA][MSG] = 'Se registro correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return $this->configPagos();
      }else{
        $_SESSION[DATA][MSG] = 'Error revise los datos y reintente de nuevo';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return $this->configPagos();
      }

    }

    public function configPagoProccess(){
      unset($_POST['_token']);

      $respuestaValida = $this->gstShopCart->validateDataConfig($_POST);
      $respuestaValida2 = $this->gstShopCart->validateCaract($_POST);
      if($respuestaValida == false or $respuestaValida2 == false )
      {
        $_SESSION[DATA][MSG] = 'Error revise los datos y reintente de nuevo';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return $this->configPagos();
      }

      $respuesta = $this->gstShopCart->ConfigPagoSave($_POST);
      if($respuesta)
      {
        $_SESSION[DATA][MSG] = 'Se edito correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return $this->configPagos();
      }else{
        $_SESSION[DATA][MSG] = 'Error revise los datos y reintente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return $this->configPagos();
      }


    }

    public function mostrarProductoDetalle()
    {
      
      $_SESSION[MENU_SHOPCART] = $this->gstShopCart->getMenuCliente($_GET['idCliente']);

      $_SESSION[SUBMENU_SHOPCART] = $this->gstShopCart->getSubMenuCliente($_GET['idCliente']);

      $_SESSION[SHOPCART_PRODUCTOS] = $this->gstProductos->getProductosShopCart($_GET['idCliente']);

      $_SESSION[SHOPCART_CONFIGPAGOS] = $this->gstShopCart->getConfigPago($_GET['idCliente']);
      if(!isset($_SESSION[SHOPCART_PRODUCTOS_NAV])){
        $_SESSION[SHOPCART_PRODUCTOS_NAV]=$this->gstProductos->getProductosShopCart($_GET['idCliente']);
      $this->emptyCart();
      }
      
      
      $allProductos = $this->gstProductos->getProductoByAtributo('id',$_GET['id']);
      $allProductos[0]['pro_precio_venta'] = number_format($allProductos[0]['pro_precio_venta']);

      $allPromocionesPro = $this->gstProductos->getPromocionProducto($_GET['id']);
      if (!count($allPromocionesPro) > 0) 
      {
        $promociones = null;
      }else
      {
        $promociones = $allPromocionesPro;
      }
      $_SESSION[DATA] = ClientesController::configuration($_GET['idCliente']);
      return view($_SESSION['data']["conf"][0]->layoutsUsu.'.productoDetalle',[ID=>$_GET['id'],"allProductos"=>$allProductos,"promociones"=>$promociones,SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);
    }




    public function validarProductos()
    {

      $totalProductos = count($_SESSION['productos_cart']);
      if($totalProductos >1)
      {
        return json_decode(true);
      }else{
        return json_decode(false);
      }

    }

    public function registerProductsDatabases($tipo_transaccion)
    {
      $total= $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart'];
      $total=str_replace(',','',$_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart']);
      $formPago = $tipo_transaccion;
      if($formPago == "Contraentrega")
      {
        $configurationCliente = $this->gstEstadosDomicilioCliente->estadosDomicilioCliente(Auth::user()->cliente_id);
        $estado = $configurationCliente[0]['id_estado'];
        $estado2 = "Aprobado";
        
      }else{
        $estado = "Pendiente Pago";
        $estado2 = "Aprobado";
      }

      $cliente_id =Auth::user()->cliente_id;
      $id=Auth::user()->id;
      $domicilio=[];
      $domicilio['domi_usuario_id'] = $id;
      $domicilio['domi_estado'] = $estado;
      $domicilio['referencia'] = $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['referenceCode'];
      $domicilio['domi_forma_pago'] = $formPago;
      $domicilio['domi_valor'] = $total;
      $domicilio['transaccionState'] = $formPago;
      $domicilio['tipo_transaccion'] = $formPago;
      $domicilio['cliente_id'] =  $cliente_id;
      $domicilio = $this->gstDomicilio->regitrarDomicilio($domicilio);
      $cantPro = count($_SESSION[SHOPCART_PRODUCTOS_NAV]);
      $productos =[];
      for($i=1; $i<$cantPro; $i++)
      {
        $productos[] =$_SESSION[SHOPCART_PRODUCTOS_NAV][$i];
      }
      $remision2="";
      $usuarioProductoid="";
      foreach ($productos as $producto)  
      {
         $cantidad_producto = $producto['pro_cantidad_cart'];
         $productoPrecio = $producto[0]['pro_precio_venta'];
         $cliente_id =  $producto[0]['cliente_id'];
         $id_prod = $producto[0]['id'];
         $remisiones=[];

           if($cliente_id != $usuarioProductoid)
           {
              //$usuarioProducto[] = $cliente_id;
              $usuarioProductoid=$cliente_id;
              $remisiones['id_domi']= $domicilio['id'];
              $remisiones['id_cliente_producto']=$cliente_id;
              $remisiones['estado_remision']= $estado2;
              $remision = $this->gstRemisiones->registrarRemision($remisiones);
            }

          $Subtotal= $productoPrecio * $cantidad_producto;
          $insertProduct['id_domi'] = $domicilio['id'];
          $insertProduct['id_pro'] = $id_prod;
          $insertProduct['remision_producto'] = $remision['id'];
                    //$insertProduct['especificacion_producto'] = 
          $insertProduct['cantidad_producto'] = $cantidad_producto;
          $insertProduct['precio_unitario'] = $productoPrecio;
          $insertProduct['subtotal_producto'] = $Subtotal;
          $insertProduct['estado_producto'] = $estado2;  

          $respuesta = $this->gstDomicilioProducto->registrarDomicilioProducto($insertProduct); 
      }

        
      if($respuesta != false)
      {
        $_SESSION[DATA][MSG] = 'Su pedido se registro correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        FuncionesGenerales::correo('Modulos.ShopCart.correos.exitoContraentrega',$domicilio,Auth::user()->email,'',Auth::user()->sftCliente->cliente_nombre.' - Domicilio contraentrega','');
        $this->emptyCart();
        return json_encode($domicilio);
      }else{
        $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return false;
      }
    }

    public function responseUrl($reference)
    {
        //dd(Auth::user()->cliente_id);
      extract($_GET);
      $cliente = Auth::user()->cliente_id;
      $referenceCode = $_GET['referenceCode'];
      $message = $_GET['message'];
      $lapTransactionState= $_GET['lapTransactionState'];

      if($referenceCode == $reference )
      {
        if($message == "APPROVED")
        {
          $configurationCliente = $this->gstEstadosDomicilioCliente->estadosDomicilioCliente(Auth::user()->cliente_id);
          
          $message = $configurationCliente[0]['id_estado'];
          $message2 = "Aprobado";
        }
        $dataInfo= $this->gstDomicilio->answerTransaction($referenceCode,$message,$lapTransactionState);
        $domicilio = $dataInfo[0]['id'];
        $remision = $this->gstRemisiones->changeStateRemission($domicilio,$message2);
        $idRemision =$remision[0]['id'];
        $response= $this->gstDomicilioProducto->changeStateAddressProduct($idRemision,$message2);
        if($response)
        {
          return redirect('carritoCompras/tienda?msg=3&id='.$cliente);
          /*return redirect('carritoCompras/tienda'.'?id='.$cliente.'&msg='.'3');*/
        }
      }
      return redirect('carritoCompras/tienda?msg=4&id='.$cliente);
      /*return redirect('carritoCompras/tienda'.'?id='.$cliente.'&msg='.'4');*/
    }

     /**
     * Función que trae todos los productos que 
     * coincidan con una categoria
     * @category FuncionesGenerales
     * @package Modulo usuario
     * @version 1.0
     * @author Samuel Beltran
     */
     public function buscadorCategorias()
     {

      $_SESSION[SHOPCART_PRODUCTOS] = FuncionesGenerales::searchItem(['Productos'],$_GET['idCat']);
       
      if($_SESSION[SHOPCART_PRODUCTOS])
      {
        $atrFavorito = $this->gstShopCart->getModuloAtributte('mod_nombre','Favoritos');
        if (count($atrFavorito) > 0)
        {
          $evaluacion = PermisosController::checkPermisosModulos($atrFavorito[0]->id);
          if($evaluacion)
          {
            $favoritos = new \App\Favoritos;
            $gstFav = new \App\Http\Modulos\Favoritos\Gst\GstFavoritos;
            $proFavoritos = $gstFav->getFavoritoUsuarioEmisor(Auth::user()->id,$_GET['id']);

          }else
          {
            $proFavoritos = null;
          }
        }

        $atrInventario = $this->gstShopCart->getModuloAtributte('mod_nombre','Inventario');
        if (count($atrInventario) > 0)
        {
          $permisoInventario = PermisosController::checkPermisosModulos($atrInventario[0]->id);
          if($permisoInventario)
          {
            $gstInv = new \App\Http\Modulos\Inventario\Gst\GstInventario;
            $inventario = $gstInv->getInventarioByAtributo('cliente_id',Auth::user()->cliente_id);
          }else
          {
            $inventario = null;
          }
        }

        $promociones = $this->gstShopCart->getModuloAtributte('mod_nombre','Promociones');

        if(count($promociones) > 0)
        {
          $permisoPromociones = PermisosController::checkPermisosModulos($promociones[0]->id);  
          if($permisoPromociones)
          { 
            $promocionesClass = new \App\Promociones;
            $gstPromociones = new \App\Http\Modulos\Promociones\Gst\GstPromociones;
            $allPromociones = $gstPromociones->getPromocionesJoin();
            $hoy = date('Y-m-d');
          }else{
            $hoy = null;
            $allPromociones = null;
          }
        }
        $_SESSION[DATA] = ClientesController::configuration($_GET['id']);

        return view($_SESSION['data']["conf"][0]->layoutsUsu.".busquedaProductos",[ID=>$_GET['id'],"permisoFavorito"=>$evaluacion,"proFavoritos"=>$proFavoritos,"permisoInventario"=>$permisoInventario,"inventario"=>$inventario,"permisoPromociones"=>$permisoPromociones,"allPromociones"=>$allPromociones,"hoy"=>$hoy,SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);

      }
      return redirect('carritoCompras/tienda?msg=5&id='.$_GET['id']);
    }
    
    public function crearEstadoDomicilioCliente()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Crear Estados Domicilio';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios'=>'gestionDomicilios','Crear Estado Domicilio'=>"#"];
      $clientes = $this->gstCliente->getAllClientes();
      return view('Modulos.ShopCart.crearEstadoDomicilioCliente',compact('clientes'));
    }

    public function crearEstadoDomicilioClienteProcess()
    {
      if(!isset($_POST['cliente_id']) or !isset($_POST['nombre_estado']))
      {
        $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos enviados';
       $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
       return redirect('carritoCompras/crearEstadoDomicilioCliente');  
      }
      unset($_POST['_token']);
      $cliente_id = $_POST['cliente_id'];
      unset($_POST['cliente_id']);
      if(strlen($_POST['nombre_estado']>38))
      {
       $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise la cantidad de Caracteres enviados';
       $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
       return redirect('carritoCompras/crearEstadoDomicilioCliente'); 
     }
     $id_estado= $this->gstEstadosDomicilio->insertarEstadoDomicilio($_POST);

     $estadoDomicilioCliente = [];
     $estadoDomicilioCliente['id_cliente']= $cliente_id;
     $estadoDomicilioCliente['id_estado']=$id_estado->id;
     $consulta =$this->gstEstadosDomicilioCliente->ConsultaOrden($estadoDomicilioCliente['id_cliente']);
     $cantidad = count($consulta);
     if($cantidad >0)
     {
      $estadoDomicilioCliente['estado_final'] = "N";
      $estadoDomicilioCliente['orden'] = $cantidad;
      $this->gstEstadosDomicilioCliente->updateEstadobefore($estadoDomicilioCliente['id_cliente'],$estadoDomicilioCliente['orden'],$estadoDomicilioCliente);

      $estadoDomicilioCliente['orden'] = $cantidad+1;
      $estadoDomicilioCliente['estado_final'] = "S";
    }else{

      $estadoDomicilioCliente['orden'] = 1;
      $estadoDomicilioCliente['estado_final'] = "S";
    } 
    
    $respuesta = $this->gstEstadosDomicilioCliente->insertNewstate($estadoDomicilioCliente);
    if($respuesta)
    {
      $_SESSION[DATA][MSG] = 'Registro exitoso';
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect('carritoCompras/listarEstadosCliente');  
    }else
    {
      $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect('carritoCompras/crearEstadoDomicilioCliente');  
    }
  }

    public function listarEstadosCliente()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Estados Domicilio';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios'=>'gestionDomicilios','Listar Estado Domicilio'=>"#"];

      $tableEstados = new Table;
      $tableEstados->setColum($tableEstados,
        [
          'id' => 'id',
          'Nombre estado'=>'nombre_estado',
          'Orden'=>'orden'
        ]
      );
      $opciones = 
      [
        'Editar'=> 'carritoCompras/editarEstadoDomicilioCliente',
        'val- Eliminar' => 'carritoCompras/eliminarEstadoDomicilioCliente'
      ];

      $datos = $this->gstEstadosDomicilioCliente->ConsultaOrdenCliente(Auth::user()->cliente_id)->toArray();
      $tableEstados->setItems($tableEstados,$datos);
      $tableEstados = $tableEstados->dps($tableEstados);

      return view(PATH_LIST,[TABLE=>$tableEstados,'opc'=>json_encode($opciones)]);
    }

    public function editarEstadoDomicilioCliente()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Editar Estado Domicilio Cliente';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios'=>'gestionDomicilios','Listar Estados Domicilio Cliente'=>'listarEstadosCliente','Editar EstadoDomicilio Cliente'=>"#"];
      if(!isset($_GET['id']))
      {
        $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/listarEstadosCliente');
      }

      $estado = $this->gstEstadosDomicilioCliente
      ->consultarEstadoDomicilioClienteID($_GET['id']);
      $orden =  $this->gstEstadosDomicilioCliente
      ->consultarEstadoDomicilioClienteOrden(Auth::user()->cliente_id);
      return view('Modulos.ShopCart.editarEstadoDomicilioCliente',compact('estado','orden'));      
    }

    public function editarEstadoDomicilioClienteProcess()
    {
        unset($_POST['_token']);
        if(!isset($_POST['id_estado']) or !isset($_POST['nombre_estado']) or !isset($_POST['orden']))
        {
          $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
          return redirect('carritoCompras/listarEstadosCliente');
        }
        if(strlen($_POST['nombre_estado']>38))
        {
         $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise la cantidad de Caracteres enviados';
         $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
         return redirect('carritoCompras/listarEstadosCliente'); 
       }
       $respuesta = $this->gstEstadosDomicilio->editarEstadosDomicilio($_POST['id_estado'],$_POST['nombre_estado']);
      
       $respuesta2 =$this->gstEstadosDomicilioCliente->updateDomicilioCliente($_POST['id_estado'],$_POST['orden']);

       if($respuesta == false or $respuesta2 == false )
       {
        $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/listarEstadosCliente'); 
      }else
      {
        $_SESSION[DATA][MSG] = 'Se edito correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('carritoCompras/listarEstadosCliente');    
      }

    }

    public function ordenarEstadosDomicilio()
    {
      $_SESSION[DATA][CLIENTE_TITLE] = 'Ordenar Estados Domicilio';
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Tienda'=>'/Menu/27','Gestion Domicilios'=>'gestionDomicilios','Ordenar Estados Domicilio'=>"#"];
      $estadosDomicilio= $this->gstEstadosDomicilioCliente->ConsultaOrdenCliente(Auth::user()->cliente_id);

      $cantidadEstados = count($estadosDomicilio);
      $contador =[];
      $x=1;
      for($i=0; $i<$cantidadEstados; $i++)
      {
        $contador[] = $x;
        $x++;
      }
      $estadoCancelarDomi= $this->gstEstadosDomicilioCliente->consultarEstadoCancellDOmi(Auth::user()->cliente_id)->toArray();

      $validarEstados = count($estadosDomicilio);
      if($validarEstados>0)
      {
        return view('Modulos.ShopCart.ordenarEstadosDomicilio',compact('estadosDomicilio','contador','estadoCancelarDomi'));  
      }else{
        $_SESSION[DATA][MSG] = 'No existen estados en el momento';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/gestionDomicilios');
      }
      
    }

    public function compararOrdenDomicilio()
    {
      $noValor = false;
      for ($i=1; $i < count($_POST['orden'])+1; $i++) { 
        if(!in_array($i,$_POST['orden'])){
          $noValor = true;

        }
      }
      return $noValor;
    }

    public function editarOrdenEstadosDomicilioProcess()
    {
      if(!isset($_POST['id_estado']) or !isset($_POST['orden']))
      {
        $_SESSION[DATA][MSG] = 'Error no tiene estados para organizar';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/ordenarEstadosDomicilio');      
      }

      $x= count($_POST['id_estado']);
      $y= count($_POST['orden']);

      if($x == 0 or $y ==0)
      {
        $_SESSION[DATA][MSG] = 'Error no tiene estados para organizar';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/ordenarEstadosDomicilio');      
      }
      $orden = $_POST['orden'];
      $orden1 = $_POST['orden'];
      asort($orden);
      
      $reponse = $this->compararOrdenDomicilio();     
      if($reponse){
        Session::flash("error","El oreden enviado no es correcto");
        return redirect('carritoCompras/ordenarEstadosDomicilio');
      }
      if($x == $y)
      {
        for($i=0; $i<$x; $i++)
        {
          
          if($orden[$i] != $x )
          {
            $estado_final = "N";
            $respuesta = $this->gstEstadosDomicilioCliente->editarEstadosDomicilioCliente($_POST['id_estado'][$i],$orden[$i],$estado_final);
          }else{
            $estado_final = "S";
            $respuesta = $this->gstEstadosDomicilioCliente->editarEstadosDomicilioCliente($_POST['id_estado'][$i],$orden[$i],$estado_final);
          }
          
        } 
        if ($respuesta)
        {
          $_SESSION[DATA][MSG] = 'Se organizaron los valores satisfactoriamente';
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }
        else{
          $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        }
      }

      return redirect('carritoCompras/ordenarEstadosDomicilio');
    }
    public function comprobarEstadoDomicilio()
    {
      $consulta = $this->gstShopCart->comprobarEstadoCliente($_GET['id']);
      $respuesta = count($consulta);
      if($respuesta>0)
      {
        return true;
      }else{
        return false;
      }

    }

    public function eliminarEstadoDomicilioCliente()
    {

      $respuestaConsulta = $this->comprobarEstadoDomicilio();
      if($respuestaConsulta == true)
      {
        $_SESSION[DATA][MSG] = 'Ocurrio un error, el estado se encuentra en uso';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/listarEstadosCliente'); 
      }

      $respuesta= $this->gstEstadosDomicilioCliente->eliminarEstadosDomicilioCliente($_GET['id']);
      $respuesta2= $this->gstEstadosDomicilio->eliminarEstadosDomicilio($_GET['id']);

      $this->reorganizarEstados();

      if($respuesta == false or $respuesta2 == false )
      {
        $_SESSION[DATA][MSG] = 'Ocurrio un error por favor revise los datos o contacte al Administrador';
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect('carritoCompras/listarEstadosCliente'); 
      }else
      {
        $_SESSION[DATA][MSG] = 'Se elimino correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('carritoCompras/ordenarEstadosDomicilio');    
      }
    }
    public function reorganizarEstados()
    {
        $estados = $this->gstEstadosDomicilioCliente->ConsultaOrdenCliente(Auth::user()->cliente_id);
        $cantEstados = count($estados);
        
        $idEstado = [];
        for($y=0; $y<$cantEstados; $y++)
        {
          $idEstado[$y] = $estados[$y]['id_estado'];
        }
        $orden=1;
        
        for($i=0; $i<$cantEstados; $i++)
        {
          if($cantEstados == $orden )
          {
            $estado_final = "S";
            $this->gstEstadosDomicilioCliente->editarEstadosDomicilioCliente($idEstado[$i],$orden,$estado_final);
          }else{
            $estado_final = "N";
            $this->gstEstadosDomicilioCliente->editarEstadosDomicilioCliente($idEstado[$i],$orden,$estado_final);
            $orden++;
          }
        }

    }

public function buscarItemNavBar()
{
  $_SESSION[SHOPCART_PRODUCTOS] = FuncionesGenerales::searchItem(['Productos'],$_GET['subMeNombre']);

  if($_SESSION[SHOPCART_PRODUCTOS])
  {
    $atrFavorito = $this->gstShopCart->getModuloAtributte('mod_nombre','Favoritos');
    if (count($atrFavorito) > 0)
    {
      $evaluacion = PermisosController::checkPermisosModulos($atrFavorito[0]->id);
      if($evaluacion)
      {
        $favoritos = new \App\Favoritos;
        $gstFav = new \App\Http\Modulos\Favoritos\Gst\GstFavoritos;
        $proFavoritos = $gstFav->getFavoritoUsuarioEmisor(Auth::user()->id,$_GET['id']);

      }else
      {
        $proFavoritos = null;
      }
    }

    $atrInventario = $this->gstShopCart->getModuloAtributte('mod_nombre','Inventario');
    if (count($atrInventario) > 0)
    {
      $permisoInventario = PermisosController::checkPermisosModulos($atrInventario[0]->id);
      if($permisoInventario)
      {
        $gstInv = new \App\Http\Modulos\Inventario\Gst\GstInventario;
        $inventario = $gstInv->getInventarioByAtributo('cliente_id',Auth::user()->cliente_id);
      }else
      {
        $inventario = null;
      }
    }

    $promociones = $this->gstShopCart->getModuloAtributte('mod_nombre','Promociones');

    if(count($promociones) > 0)
    {
      $permisoPromociones = PermisosController::checkPermisosModulos($promociones[0]->id);  
      if($permisoPromociones)
      { 
        $promocionesClass = new \App\Promociones;
        $gstPromociones = new \App\Http\Modulos\Promociones\Gst\GstPromociones;
        $allPromociones = $gstPromociones->getPromocionesJoin();
        $hoy = date('Y-m-d');
      }else{
        $hoy = null;
        $allPromociones = null;
      }
    }
    $_SESSION[DATA] = ClientesController::configuration($_GET['id']);

    return view($_SESSION['data']["conf"][0]->layoutsUsu.".busquedaProductos",[ID=>$_GET['id'],"permisoFavorito"=>$evaluacion,"proFavoritos"=>$proFavoritos,"permisoInventario"=>$permisoInventario,"inventario"=>$inventario,"permisoPromociones"=>$permisoPromociones,"allPromociones"=>$allPromociones,"hoy"=>$hoy,SFT_LIB_MOD=>[SFT_LIB_MOD_TIENDA]]);
  }
  else
  {
    return redirect('carritoCompras/tienda?msg=6&id='.$_GET['id']);
  }

}
    public function domiciliomotivosCancell()
    {
      $cliente = Auth::user()->cliente_id;
      $estadosCancelar= $this->gstShopCart->motivosCancelProd($cliente,'Tienda');
      return json_encode($estadosCancelar);
    }
  
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Domicilios)
      *Versiòn: 1.0
      *Entrada: id: identificador para cambio estado
      *Salida: 
      *Descripciòn: Funcion para Cambiar el estado del domicilio  Cancelado
    */
    public function cancelarProductoDomicilio()
    {
      
      $resultado = $this->gstDomicilio->validarDatosCancell($_GET);
      $resultado2 = $this->gstDomicilio->validateCaractCancell($_GET);
      if($resultado != true or $resultado2 != true )
      {
        return json_encode(false);
      }
      $respuestaPro = $this->gstDomicilioProducto->consultProducDomicilio($_GET['idProducDomi']);

      $respuesta = $respuestaPro->estado_producto;
      if(  $respuesta == "Cancelado" or $respuesta == "Entregado" )
      {
         return json_encode(false);
      }else{
          
          $datosDomicilio = $this->consultarDomicilio($respuestaPro->id_domi);
          $datos = [];
          $datos['domi_valor'] = $datosDomicilio[0]['domi_valor']-$respuestaPro['subtotal_producto'];
          $respuesta = $this->updateDomicilio($respuestaPro->id_domi,$datos);
          if($respuesta == true){
            $data = [];
            $data['cancela_motivo']= $_GET['motivo'];
            $data['estado_producto']= 'Cancelado';
            $respuestaDomi = $this->gstDomicilioProducto->updateDomicilioProducto($_GET['idProducDomi'],$data);
          }else{
            return json_encode(false) ;
          }
          if(isset($respuestaDomi))
          {
            return json_encode(true);
          }else{
            return json_encode(false);
          }
      }  
    }
    /*
    *Autor: Javier R
    *Modulo: Tienda (ShopCart-Domicilios)
    *Versiòn: 1.0
    *Entrada: id: identificador para cambio estado
    *Salida: 
    *Descripciòn: Funcion para validar los productos del domicilio si estan todos 
    en Cancelado cancela el producto
    */

    public function validarProductosDomicilio($id_domi)
    {
        $respuesta = $this->gstDomicilioProducto->getDomicilioProducts($id_domi);
        $cantidadProductos = count($respuesta);
        
        $respuesta2 = $this->gstDomicilioProducto->getProductDomiEstado($id_domi,"Cancelado");
       $cantidadProductosCancelados = count($respuesta2);
        
        if($cantidadProductosCancelados == $cantidadProductos )
          {
            $estadoCancelar = $this->consultarEstadoCancelar();
            $data = [];
            $data['cancela_motivo'] = "Se cancelaron todos los productos del domicilio";
            $data['domi_estado'] = $estadoCancelar[0]['id_estado'];
            $this->gstDomicilio->updateDomicilio($id_domi,$data);
          } 
    }
    public function consultarEstadoCancelar()
    {
      $estadoCancelar = $this->gstEstadosDomicilioCliente->consultarEstadoCancellDOmi(Auth::user()->cliente_id);
      return $estadoCancelar;
    }
  /*
  public function consultarProductoId()
  {

    $producto = $this->gstDomicilioProducto->consultProducIdDomi($_GET['idProducDomi']);
    $product = [];
    $product['totalProducto'] = $producto[0]['precio_unitario']*$producto[0]['cantidad_producto'];
    $product['id_domi'] = $producto[0]['id_domi'];
    $product['estado_producto'] = $producto[0]['estado_producto'];
    return $product;
  }
  */
    public function consultarDomicilio($id_domi)
    {
        $domicilio = $this->gstDomicilio->estadoDomicilio($id_domi);
        return $domicilio;
    }
    /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart-Domicilios)
      *Versiòn: 1.0
      *Entrada: id: identificador
      *Salida: 
      *Descripciòn: Funcion para traer todos los productos del cliente que esten activos 
    */
    public function productsCliente()
    {
      $domicilio= $this->consultarDomicilio($_GET['idDomicilio']);
      $products=$this->gstProductos->getProductsCliente($domicilio[0]['cliente_id']);
      return json_encode($products);
    }

    public function updateDomicilio($id_domi,$dato)
    {
      $respuesta= $this->gstDomicilio->updateDomicilio($id_domi,$dato);
      
      if($respuesta)
      {
          return true;
      }else{
          return false;
        }
    }

    public function consultarProductoAjax()
    {
      $producto = $this->gstDomicilioProducto->consultProducIdDomi($_GET['idProducDomi']);
      $this->validarProductosDomicilio($producto[0]['id_domi']); 
      
    }
  /*
      *Autor: Javier R
      *Modulo: Tienda (ShopCart)
      *Versiòn: 1.0
      *Entrada:
      *Salida: 
      *Descripciòn: Funcion para agregar nuevo producto al domicilio con sus respectivas validaciones
      que existan los datos que no esten vacios y que sean numeros

   */
    public function agregarNuevoProductoDomicilio()
    {
      
      if(!isset($_GET['idPro']) or !isset($_GET['cantidadProd']) or !isset($_GET['idDomicilio']))
      {
        return json_encode(false);
      }
      $validarDatosVacios = $this->gstDomicilioProducto->validarDatosNuevoProducto($_GET);
      if($validarDatosVacios == false)
      {
        return json_encode($validarDatosVacios);
      }
      $validarDatosNumericos = $this->gstDomicilioProducto->validarDatosNuevoProductoNumeros($_GET);
      if($validarDatosNumericos == false)
      {
        return json_encode($validarDatosNumericos); 
      }
      $addProductoDomicilio = [];
      $productos = $this->gstProductos->getProductoById($_GET['idPro']);
      $domicilio = $this->gstDomicilio->estadoDomicilio($_GET['idDomicilio']);
      $domicioProducto= $this->gstDomicilioProducto->getDomicilioProducts($_GET['idDomicilio']);
      $datosDomicilio = $this->consultarProductoDomicilioProducto($_GET['idPro']);
      
      $respuesta = count($datosDomicilio);
      if($respuesta)
      {
        $updateDomicilioProducto = [];
        $idDomicilioProducto = $datosDomicilio[0]['id'];
        $updateDomicilioProducto = $this->agregarNuevaCantidad($productos,$datosDomicilio);
        $respuesta = $this->gstDomicilioProducto->updateDomicilioProducto($idDomicilioProducto,$updateDomicilioProducto);
        $subtotal_producto = $updateDomicilioProducto['subtotal_producto'];
      }else{
        
        $addProductoDomicilio = $this->agregarNuevoProducto($productos,$domicioProducto);
        $respuesta = $this->gstDomicilioProducto->registrarDomicilioProducto($addProductoDomicilio);
        $subtotal_producto = $addProductoDomicilio['subtotal_producto'];
      }

      if($respuesta)
      {
        $addPrecioProdcDomi= [];
        $addPrecioProdcDomi['domi_valor']= $domicilio[0]['domi_valor'] + $subtotal_producto;
        $respuesta = $this->gstDomicilio->updateDomicilio($_GET['idDomicilio'],$addPrecioProdcDomi);
        return json_encode($respuesta);  
      }else{
        return json_encode($respuesta);  
      }
    }
    /*
        *Autor: Javier R
        *Modulo: Tienda (ShopCart)
        *Versiòn: 1.0
        *Entrada:
        *Salida: 
        *Descripciòn: Funcion para agregar nueva cantidad, se usa como funcion para no alargan tanto el codigo de
        Agregar nuevo producto al domicilio con esta validaciones lo que se busca es que si el producto existe en el domicilio solo lo modifique agregandole la nueva cantidad el valor
        
     */
    public function agregarNuevaCantidad($productos,$datosDomicilio)
    {
        $updateDomicilioProducto = [];
        $Subtotal = $productos[0]['pro_precio_venta']*$_GET['cantidadProd'];
        $nuevoSubtotal = $Subtotal + $datosDomicilio[0]['subtotal_producto'];
        $updateDomicilioProducto['subtotal_producto']= $nuevoSubtotal;
        $updateDomicilioProducto['cantidad_producto'] = $datosDomicilio[0]['cantidad_producto'] + $_GET['cantidadProd'];
        return $updateDomicilioProducto;
    }
    /*
        *Autor: Javier R
        *Modulo: Tienda (ShopCart)
        *Versiòn: 1.0
        *Entrada:
        *Salida: 
        *Descripciòn: Funcion para agregar nuevo producto al domicilio se agrego esta funcion para minimizar las 
        lineas de codigo de agregar nuevo producto a el domicilio esta funcion lo que busca es que si no existe el producto en el domicilio lo va a gregar y retirnarlo en un arreglo 
        
     */
    public function agregarNuevoProducto($productos,$domicioProducto)
    {
        $addProductoDomicilio['precio_unitario'] = $productos[0]['pro_precio_venta'];
        $addProductoDomicilio['subtotal_producto'] = $productos[0]['pro_precio_venta']*$_GET['cantidadProd'];
        $addProductoDomicilio['cantidad_producto'] = $_GET['cantidadProd'];
        $addProductoDomicilio['id_pro'] = $_GET['idPro'];
        $addProductoDomicilio['id_domi'] = $_GET['idDomicilio'];
        $addProductoDomicilio['estado_producto'] = "Aprobado";
        $addProductoDomicilio['remision_producto'] = $domicioProducto[0]['remision_producto'];
        $addProductoDomicilio['cancela_motivo'] = "";
        return $addProductoDomicilio;
    }
    /*
        *Autor: Javier R
        *Modulo: Tienda (ShopCart)
        *Versiòn: 1.0
        *Entrada:
        *Salida: 
        *Descripciòn: Funcion para consultar el producto del domicilio
    */

    public function consultarProductoDomicilioProducto($idPro)
    {
       return $this->gstDomicilioProducto->consultarProductoDomicilio($idPro);
    }
    
    public function cancelarDomicilio()
    {
      if(!isset($_GET['idDomicilio']) or !isset($_GET['cancela_motivo']) )
      {
        return json_encode(false);
      }
      
      $respuesta = $this->validarEstadoDomicilioCancelado($_GET['idDomicilio']);
      if($respuesta == true)
      {
        return json_encode(false);
      }
      $idDomi = $_GET['idDomicilio'];
      unset($_GET['idDomicilio']);
      $estadoCancelar = $this->consultarEstadoCancelar();
      $_GET['domi_estado'] = $estadoCancelar[0]['id_estado'];
      
      $respuesta = $this->gstDomicilio->updateDomicilio($idDomi,$_GET);
      if($respuesta){
        $updateProductDomi= [];
        $updateProductDomi['estado_producto'] = "Cancelado";
        $updateProductDomi['cancela_motivo'] = $_GET['cancela_motivo'];
        $respuesta = $this->gstDomicilioProducto->updateDomicilioProductoIdDOmi($idDomi,$updateProductDomi);
        if($respuesta == false)
        {
          return json_encode($respuesta);   
        }else{
          return json_encode(true);
        }
        
      }else{
        return false;
      }
      

    
    }
    /*
        *Autor: Javier R
        *Modulo: Tienda (ShopCart)
        *Versiòn: 1.0
        *Entrada:
        *Salida: 
        *Descripciòn: Funcion para consultar el domicilio y validar 
        si esta cancelado o no dependiendo el estado de cancelar del 
        cliente.
    */
    public function validarEstadoDomicilioCancelado($idDomi)
    {
      $domicilio = $this->consultarDomicilio($idDomi); 
      $cancelar = $this->consultarEstadoCancelar();

        if($cancelar[0]['id_estado'] == $domicilio[0]['domi_estado'] )
        {
          return true;
        }
      return false;
    } 

    public function ConsultUserForOrder()
    {
      $dataDomi = $this->consultarDomicilio($_GET['idDomicilio']);
      $user = $this->gstUsuarios->getUser($dataDomi[0]['domi_usuario_id']);
      return json_encode($user);
    }

    /*
      *Autor: Cristian Campo
      *Modulo: Tienda (ShopCart)
      *Versiòn: 1.0
      *Entrada:id_domi(shopcart.js->detalleProducto)
      *Salida: 
      *Descripciòn: Consulta el id de un producto a través del id_domi
    */
    public function detalleProducto(){

      $cliente = Auth::user()->cliente_id;

      $producto = $this->gstDomicilioProducto->getProducto($_GET['id']);

      $data = array($producto, $cliente);

      return json_encode($data);
      
    }

    public function politicaPrivacidad()
    {
      return view('Modulos.ShopCart.layouts.JuniorHome.politicaPrivacidad');
    }
}
