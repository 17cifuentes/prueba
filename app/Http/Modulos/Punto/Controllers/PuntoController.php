<?php

namespace App\Http\Modulos\Punto\Controllers;

use App\Http\Modulos\Punto\Gst\GstPunto;
use App\Http\Modulos\Productos\Gst\GstProductos;
use App\Http\Controllers\Controller;
use App\Http\Gst\GstGeneral;
use Illuminate\Http\Request;
use App\Http\Clases\Table;
use Auth;

class PuntoController extends Controller{
	
	private $gstGeneral;
	private $gstPunto;
	private $gstProducto;


	public function __construct(GstGeneral $gstGeneral,GstPunto $gstPunto,GstProductos $gstProducto){

		$this->middleware(MIN_AUTH);
		session_start();

		$this->gstPunto = $gstPunto;
		$this->gstGeneral = $gstGeneral;
		$this->gstProducto = $gstProducto;
	
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Punto
	*Versiòn: 1.0
	*Descripciòn: Funcion retorna la vista para crear punto.
	*/

	public function crearPunto(){

		$cliente = Auth::user()->cliente_id;

		$estado = $this->gstPunto->consultarEstados();

		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PUNTO=>'/Menu/38',CREAR_PUNTO=>''];

		$data = array('cliente','estado');

		return view(PATH_VIEW_CREAR_PUNTO,compact($data));
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion realizar el proceso de creacion en la BBDD.
	*/
	public function createProccess(Request $request){

		$this->gstGeneral->validateData($request);

		$response=$this->gstPunto->createPunto($_POST);
		
		if ($response) {

			$_SESSION[DATA][MSG] = MSG_CRE_PRO_SUC;

			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (PATH_VIEW_LISTAR_PUNTO);	
		}else{

			$_SESSION[DATA][MSG] = MSG_CRE_PRO_FAIL;

			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (PATH_VIEW_LISTAR_PUNTO);
		}


	}


	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion para listar los puntos creados.
	*/
	public function lstPuntos(){

		$cliente=Auth::user()->cliente_id;
		
		$data = $this->gstPunto->getAllPuntoJoin($cliente);


		
		$tableInv = new Table;

		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PUNTO=>'/Menu/38',MSG_LISTA_PUNTOS=>''];

		$tableInv->setColum($tableInv,
			[
				ID=>TN_ID,
				'Nombre'=>TN_PUNTO_NOMBRE,
				'Tipo'=>TN_PUNTO_TIPO,
				'Descripción'=>TN_PUNTO_DESCRIPCION,
				'Estado'=>TN_NAME_ESTADO

			]
		);

		$opciones = 
		[
			
			'Editar'=> PATH_EDITAR_PUNTO,
			'val-Eliminar'=> PATH_ELIMINAR_PUNTO,
			'val-Cambiar estado'=>PATH_CAMBIAR_ESTADO,
			'fun- Vender'=>'modalPunto'
			
		];

		
		
		$tableInv->setItems($tableInv,$data);
		
		$tableInv = $tableInv->dps($tableInv);

		return view(PATH_LIST,[SFT_LIB=>[SFT_SELECT2],[SFT_DATATABLE],SFT_LIB_MOD=>[SFT_LIB_MOD_PUNTO],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
		
	}


	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion retorna la vista para editar punto.
	*/
	public function editarPunto(){

		if (isset($_GET[TN_ID])){

			$punto=$this->gstPunto->getDataPunto($_GET[TN_ID]);

			$estado = $this->gstPunto->consultarEstados();

			$cliente=Auth::user()->cliente_id;

			$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PUNTO=>'/Menu/38',MSG_LISTA_PUNTOS=>PATH_VIEW_LISTAR,EDITAR_PUNTO=>''];

			return view(PATH_VIEW_EDITAR_PUNTO,compact('punto','cliente','estado'));	
		}	

	}


	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion realiza el proceso de actualizacion en la BBDD.
	*/
	public function editarProccess(Request $request){

		$this->gstGeneral->validateData($request);

		$response = $this->gstPunto->updatePunto('id',$_POST[TN_ID],$_POST);

		if ($response) {

			$_SESSION[DATA][MSG] = MSG_EDICION_CORRECTA;

			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (PATH_VIEW_LISTAR_PUNTO);	

		}else{

			$_SESSION[DATA][MSG] = MSG_EDICION_ERROR;

			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (PATH_VIEW_LISTAR_PUNTO);
		}


	}


	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion que elimina un punto.
	*/
	public function eliminarPunto(){
		
		if (isset($_GET[TN_ID])){

			$response = $this->gstPunto->eliminarPunto($_GET[TN_ID]);
			
			if ($response) {

				$_SESSION[DATA][MSG] = MSG_BORRADO_EXITOSO;

				$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

				return redirect (PATH_VIEW_LISTAR_PUNTO);
				
			}else{

				$_SESSION[DATA][MSG] = MSG_BORRADO_ERROR;
				$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

				return redirect (PATH_VIEW_LISTAR_PUNTO);
			}	
		}
	}


	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion que cambia el estado de un punto.
	*/
	public function cambiarEstado(){

		$response = $this->gstPunto->cambiarEstado($_GET[TN_ID]);

		if ($response) {

			$_SESSION[DATA][MSG] = SUCC_EST_ROL;

			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (PATH_VIEW_LISTAR_PUNTO);
			
		}else{

			$_SESSION[DATA][MSG] = ERR_EST_ROL;
			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (PATH_VIEW_LISTAR_PUNTO);
		}	
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Punto
	*Versiòn: 1.0
	*Descripciòn: Funcion datos de un punto de venta en especifico.
	*/
	public function punto(){

		return json_encode($this->gstPunto->getDataPunto($_GET['id']));
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Punto
	*Versiòn: 1.0
	*Descripciòn: Funcion datos de un producto en especifico.
	*/
	public function producto(){

		return json_encode($this->gstProducto->consultaProducto($_GET['id']));
	}
}