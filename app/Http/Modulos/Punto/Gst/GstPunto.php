<?php 

namespace App\Http\Modulos\Punto\Gst;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use App\Punto;
use App\Estado;


class GstPunto{

	public function createPunto($post){

		unset($post['_token']);

		try{

            return FuncionesDB::registrar(new Punto,$post);

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }

	}

    public function consultarEstados(){

        return Estado::select('id','nombre_estado')
        ->where("modulo","Puntos")
        ->get()
        ->toArray();
    }

	public function getAllPuntos($idCliente){

        try{

            return Punto::where("cliente_id",$idCliente)->get();

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    public function getAllPuntoJoin($idCliente){

        $data = $this->getAllPuntos($idCliente);
        
        if ($data) {

            try{
                                    
                return FuncionesGenerales::listaOrderDataJoin(new Punto,$data,[]);

            }catch(Exception $e){

                error_log("Error : ".$e->getMessage());

                return false;
            }

        }else{

            return redirect ('Punto/lstPuntos');
        }
    }




    public function eliminarPunto($id){

    	
        try{

            return FuncionesDB::eliminar('tn_sft_mod_pos_punto','id',$id);

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    public function getDataPunto($id){
    	try{

            return Punto::findOrFail($id);

        }catch(Exception $e){

            error_log("Error : " . $getMessage());

            return false;
        }
    }

    public function updatePunto($columna,$id,$data){

         try{

            return FuncionesDB::Editar('tn_sft_mod_pos_punto',$columna,$id,$data);
            
        }catch(\Exception $e) {

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    public static function cambiarEstado($id)
    {
        $est = Punto::find($id);

        $estado = $est['estado'];

        if($estado == "10")
        {
            $estado = "9";

        }else{
            $estado = "10";
        }
        $est = ['estado'=>$estado];
        
        return Punto::where('id',$id)->update($est);
    }
}