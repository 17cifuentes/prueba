<?php

namespace App\Http\Modulos\Promociones\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Promociones;
use App\Estado;
use App\Paquetes;


	/*
	*Autor: Jasareth Obando
	*Modulo: PROMOCIONES
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para las promociones
	*/
class GstPromociones
{
/*Autor:jasareth obando
*/
    public function getRegisterPromociones($data)
    {
        try
      {
        return Promociones::insert($data);
      }
      catch(\Exception $promocion){
        error_log("Error: ".$promocion->getMessage());

        return false;
      }
    }
    public function getPromocionesJoin(){
        return Promociones::all();
    }
    public function getListPromociones(){
        $dat = $this->getPromocionesJoin();
        $pro = new Promociones;
        
         return FuncionesGenerales::listaOrderDataJoin(new Promociones,$dat,[]);
        
    }
    public function deletePromo($id)
    {
    	Promociones::where('id',$id)->delete();
    }

    public function getPromocionesId($id)
    {
        return Promociones::where('id_producto',$id);
    }

	public function getPromocionMesActual($cliId = NULL)
    {
        return Promociones::where('cliente_id',$cliId)->whereMonth('inicio','=', date('m'))->get();
    }

    /**
    * Función que consulta los registros por su columna
    * y valor, de la tabla estado
    * @category GstPromociones
    * @package Modulo Promociones
    * @version 1.0
    * @author Samuel Beltran
    */
    public function getEstados($atribute,$value)
    {
        return FuncionesGenerales::getByAtribbute(new Estado,$atribute,$value);
    }

    /**
    * Función que inserta nuevos datos en las tabla de 
    * paquetes
    * @category GstPromociones
    * @package Modulo Promociones
    * @version 1.0
    * @author Samuel Beltran
    */
    public function createPaquete($post)
    {
        try {
            unset($post[TOKEN]);
            return Paquetes::insert($post);

        } catch (\Exception $e) {
            error_log("Error: ".$e->getMessage());
            return false;            
        }
    }

    /**
    * Función que consulta los registros de la tabla
    * de paquetes con el id del cliente
    * @category GstPromociones
    * @package Modulo Promociones
    * @version 1.0
    * @author Samuel Beltran
    */
    public function getPaquetes($cliId)
    {
        return Paquetes::where('cliente_id',$cliId)->get();
    }

    public function getPaquetesEstadoJoin($cliId)
    { 
        $all = $this->getPaquetes($cliId);
        return FuncionesGenerales::listaOrderDataJoinRelMultiple(new Paquetes,$all,[]);
    }

    /**
    * Función que consulta los registros de la tabla
    * de Estado por su id
    * @category GstPromociones
    * @package Modulo Promociones
    * @version 1.0
    * @author Samuel Beltran
    */
    public function getEstadoById($id)
    {
        return Estado::where(ID,$id)->get();
    }

    /**
    * Función que elimina un registro de la vase de datos
    * @category PromocionesController
    * @package Modulo Promociones
    * @version 1.0
    * @author Samuel Beltran
    */
    public function deletePaquete($id)
    {
        Paquetes::where(ID,$id)->delete();
    }

    public function validateOperacion($optionPaq,$simbolos,$value)
    {
        switch ($simbolos) 
        {
            case '>':
                if ($optionPaq > $value) {
                return $response = true;
                
                break;
                }
            case '<':
                if ($optionPaq < $value) {
                return $response = true;
                
                break;
                }
            case '>=':
                if ($optionPaq >= $value) {
                return $response = true;
            
                break;
            }
            case '<=':
                if ($optionPaq <= $value) {
                return  $response = true;
            
                break;
            }
            
        }
    }
    public function verifyPromo($tipoPromo,$valPromo,$canTotal,$canPagar)
    {
        switch($tipoPromo)
        {
            case "%":
            $porcentaje = $canTotal * $valPromo;
            $porcentaje = $porcentaje / 100;
            return $canTotal - $porcentaje;
            break;

            case "Menos":
            return  $canPagar - $valPromo;                
            break;

            case "Mas":
            return  $canTotal  + $valPromo;
            break;

        } 
    }

}
