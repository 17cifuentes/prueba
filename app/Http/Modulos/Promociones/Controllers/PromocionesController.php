<?php
namespace App\Http\Modulos\Promociones\Controllers;
use App\Http\Modulos\Promociones\Gst\GstPromociones;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Gst\GstGeneral;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Promociones;
use App\Http\Clases\FuncionesGenerales;



/*
*Autor: Jasareth Obando
*Modulo: Promociones
*Versiòn: 1.0
*Descripciòn: Controllador de Promociones dentro del sistema
*/
class PromocionesController extends Controller
{
    private $gstPromociones;
    private $gstGeneral;
	function __construct(GstPromociones $gstPromociones, GstGeneral $gstGeneral){
        session_start();
        $this->middleware(MIN_AUTH);
        $this->gstPromociones = $gstPromociones;
        $this->gstGeneral = $gstGeneral;
	}
	/*
*Autor: Jasareth Obando
*Modulo: Promociones
*Versiòn: 1.0
*Descripciòn: funcion para crear Promociones dentro del sistema
*/
   public function crearPromociones()
    {
        if(!isset($_GET['id'])) 
        {
            $_SESSION[DATA][MSG] = MSG_NO_PROMO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }
            if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }  
            $consulta = $this->gstPromociones->getPromocionesId($_GET);
            if ($consulta == null)
        {
            $_SESSION[DATA][MSG] = NO_PROMO_BD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }
        else
        {   
            $_SESSION[DATA][CLIENTE_TITLE] = 'Registrar Promociones';
            $_SESSION[DATA][BREAD] = 
            [
                INICIO=>PATH_HOME,
                BREAD_MENU_PROMOCIONES=>"/Menu/30",
                'Registrar Promociones'=>RUTA_CREAR_PROMO,
            ];
            $producto = $_GET['id'];
            return view('Modulos.Promociones.crearPromociones',[SFT_LIB=>[SFT_DATEPICKER]],compact('producto'));
              

        }
	}
 /*
*Autor: Jasareth Obando
*Modulo: Promociones
*Versiòn: 1.0
*Descripciòn: funcion para Registrar Promociones dentro del sistema
*/
	 public function crearPromoProcess(Request $request){

        $this->gstGeneral->validateData($request);
        if(!isset($_GET['id_producto'])) 
        {   
            $_SESSION[DATA][MSG] = MSG_NO_PROMO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }
            if(strlen($_GET['mensaje']) > 200)
        {
            $_SESSION[DATA][MSG] = CANT_CARACTERES;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }
            if(!filter_var($_GET['id_producto'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }
        $_GET['cliente_id'] = Auth::user()->cliente_id;
            $consulta = $this->gstPromociones->getPromocionesId($_GET['id_producto']);
            if ($consulta == null)
        {
            $_SESSION[DATA][MSG] = NO_PROMO_BD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(LST_PRODUCTOS);
        }
        else
        {
            $promo = $this->gstPromociones->getRegisterPromociones($_GET);
            
            if($promo)
            {
                $_SESSION[DATA][MSG] = REGISTER_CORRECTLY;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }else{
                $_SESSION[DATA][MSG] = REGISTER_INCORRECTO;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            }
            
            return redirect('/Promociones/listarPromociones');
        }
	}

    public function listarPromociones(){
        $_SESSION[DATA][CLIENTE_TITLE] = 'listar Promociones';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            BREAD_MENU_PROMOCIONES=>"/Menu/30",
            'listar Promociones'=>"/Promociones/listarPromociones"
        ];
        $tableInv = new Table;
        
        $tableInv->setColum($tableInv,
            [   
                ID=>'id',
                'Nombre producto'=>NAME_PRODUCT,
                'Inicio'=>INICIO_PROMO,
                'Fin'=>FIN,
                'Mensaje'=>MENSAJE_PROMO
            ]
        );
        
        $opciones = 
            [
                'val-Eliminar'=> RUTA_DELETE_PROMO,

            ];

        $datos = $this->gstPromociones->getListPromociones();
        $tableInv->setItems($tableInv,$datos);
        $tableInv = $tableInv->dps($tableInv);
        return view(PATH_LIST,[TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
    } 

 public function eliminarPromocion()
    {
        $_SESSION[DATA][MSG] = ELIMINADO_CORRECTO;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstPromociones->deletePromo($_GET['id']);
        return redirect(LST_PROMO);
    }

     /**
     * Función que muestra un formalario para la creacion de un
     * paquete de promocion
     * @category PromocionesController
     * @package Modulo Promociones
     * @version 1.0
     * @author Samuel Beltran
     */
    public function createPaquete(Request $request)
    {
        $this->gstGeneral->validateData($request);
        $_SESSION[DATA][BREAD] = 
        [
        INICIO=>PATH_HOME,
        BREAD_MENU_PROMOCIONES=>"/Menu/30",
        BREAD_CREAR_PAQUETE=>"",
        ]; 
       $opciones = $this->gstPromociones->getEstados('razon','pro-opciones');
       $operacion = $this->gstPromociones->getEstados('razon','pro-operacion');
       $tipoPromocion = $this->gstPromociones->getEstados('razon','pro-tipo-promocion');
        return view('Modulos.Promociones.createPaquete',['opciones'=>$opciones,'operacion'=>$operacion,'tipoPromocion'=>$tipoPromocion]);
    }
	
     /**
     * Función que crea un
     * paquete de promocion
     * @category PromocionesController
     * @package Modulo Promociones
     * @version 1.0
     * @author Samuel Beltran
     */
    public function crearPaqueteProcess()
    {
        $_POST[TN_CONF_ID_CLIENTE] = Auth::user()->cliente_id;
        $response = $this->gstPromociones->createPaquete($_POST);
        if($response)
        {
            $_SESSION[DATA][MSG] = NOTIFICATION_PAQUETE_CREADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            return redirect(RUTA_LST_PAQUETES);
        }else
        {
            $_SESSION[DATA][MSG] = NOTIFICATION_PAQUETE_NO_CREADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_CREATE_PAQUETES);
        }   
    }

     /**
     * Función que lista los paquetes que se han creado
     * @category PromocionesController
     * @package Modulo Promociones
     * @version 1.0
     * @author Samuel Beltran
     */
    public function listPaquetes()
    {
        $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LISTAR_PAQUETES;
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            BREAD_MENU_PROMOCIONES=>"/Menu/30",
            TITLE_LISTAR_PAQUETES=>""
        ];
        $tableInv = new Table;
        
        $tableInv->setColum($tableInv,
            [   
                ID=>ID,
                MODULOS_NOMBRE=>NOMBRE,
                OPCION=>'sftEstadoOpcion-nombre_estado',
                OPERACION=>'sftEstadoOperacion-nombre_estado',
                VALOR_OPERACION=>'valor_operacion',
                TIPO_PROMOCION=>'sftEstadoPromocion-nombre_estado',
                VALOR_PROMOCION=>'valor_promocion',
                
            ]
        );
        
        $opciones = 
            [
                'val-Eliminar'=> 'Promociones/eliminarPaquete',

            ];

        $data = $this->gstPromociones->getPaquetesEstadoJoin(Auth::user()->cliente_id);
        
        $tableInv->setItems($tableInv,$data);
        $tableInv = $tableInv->dps($tableInv);
        return view(PATH_LIST,[TABLE=>$tableInv,TN_OPC=>json_encode($opciones)]);
    }

     /**
     * Función que elimina un paquete de la base de datos
     * @category PromocionesController
     * @package Modulo Promociones
     * @version 1.0
     * @author Samuel Beltran
     */
    public function eliminarPaquete()
    {
        $this->gstPromociones->deletePaquete($_GET[ID]);
        $_SESSION[DATA][MSG] = NOTIFICATION_PAQUETE_ELIMINADO;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect(RUTA_LST_PAQUETES);
    }

    public function calculatePromocion()
    {
        $allPaquetesQuery = $this->gstPromociones->getPaquetesEstadoJoin(Auth::user()->cliente_id);
        $total = intval(str_replace(",", "", $_SESSION[SHOPCART_PRODUCTOS_NAV][0]['totalCart']));
        $cantidad = 0;
        $totalPago = 0;

        for($i = 1; $i < count($_SESSION[SHOPCART_PRODUCTOS_NAV]); $i++)
        {
            $cantidad += $_SESSION[SHOPCART_PRODUCTOS_NAV][$i]['pro_cantidad_cart'];
        }
 
            foreach($allPaquetesQuery as $paq => $value)
            {
                $simbolos[]= $value['sftEstadoOperacion-valor']; 
                if ($value['sftEstadoOpcion-nombre_estado'] == "Cantidad producto")
                {
                    $optionPaq = $cantidad;
                }elseif ($value['sftEstadoOpcion-nombre_estado'] == "Total") {
                    $optionPaq = $total;
                }else
                {
                    $optionPaq = '';
                }

                $response = $this->gstPromociones->validateOperacion($optionPaq,$simbolos[$paq],$value['valor_operacion']);
                if($response)
                {

                   $totalPago = $this->gstPromociones->verifyPromo($value['sftEstadoPromocion-nombre_estado'],$value['valor_promocion'],$total,$totalPago);
                   $valorPago[] = $totalPago;

                }else{
                    dd('does not acomplish any requirement');
                }
                
            } 

       return json_encode($_SESSION[SHOPCART_PRODUCTOS_NAV]);

    }
}   