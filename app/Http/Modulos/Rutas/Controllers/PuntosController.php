<?php
namespace App\Http\Modulos\Rutas\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\Http\Clases\Cards;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Puntos;
use App\Http\Modulos\Rutas\Gst\GstPuntos;

class PuntosController extends Controller
{
	private $gstPuntos;
	
	function __construct(GstPuntos $gstPuntos){
        session_start(); 
        $this->middleware(MIN_AUTH);
	    $this->gstPuntos = $gstPuntos;
	}
/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: Controllador de Puntos dentro del sistema
*/

/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion para crear puntos dentro del sistema 
*/

public function crearPunto(){

    	$_SESSION[DATA][CLIENTE_TITLE] = 'Registrar Punto';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,	
            'Menu Rutas'=>"/Menu/20",
            'Registrar Punto'=>"/Puntos/crearPuntos",
        ];
       return view ('Modulos.Rutas.crearPuntos');
	}
/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion  crearprocess para crear puntos dentro del sistema 
*/
        
public function crearPuntoProcess()
    {
    	$Puntos = $this->gstPuntos->registerPuntoProcess($_GET);
        $_SESSION[DATA][MSG] = 'Su punto se registro correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
		return view('Modulos.Rutas.crearPuntos',compact('Puntos'));
         
    }
/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion para listar puntos dentro del sistema 
*/

 public function listarPunto(){

        $_SESSION[DATA][CLIENTE_TITLE] = ' Listar Puntos';
        $_SESSION[DATA][BREAD] = 
            [
                INICIO=>PATH_HOME,
                'Menu Rutas'=>"/Menu/20",
                'listar Puntos'=>"/Rutas/listarPunto"
            ]; 
        $tableInv = new Table;
        $tableInv->setColum($tableInv,
            [
                'id'=>'punto_id',
                'Nombre del Punto'=>'nombre_punto',
                'Direccion del punto'=>'direccion_punto',
                'Estrato del punto'=>'estracto_punto',
                'Entidad Responsable'=>'entidad_responsable',
                'Estado'=>'estado_punto'
            ]
        );
        $opciones = 
            [
               'val-Cambiar estado'=> 'Rutas/cambiarEstadoPuntos',
               'Editar'=> 'Rutas/editarPunto',
               'val-Eliminar'=> 'Rutas/eliminarPuntos'
            ];
  
        $data = $this->gstPuntos->getAllPunto($_GET);
        $tableInv->setItems($tableInv,$data);
        $tableInv = $tableInv->dps($tableInv);
    
        return view(PATH_LIST,[TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
    } 
/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion para cambiar estado de  puntos dentro del sistema 
*/


public function cambiarEstadoPuntos()
    {

    $Puntos2= $this->gstPuntos->getPuntos($_GET['id']);
    
    
    if($Puntos2[0]->estado_punto == 'Activo')
    {
       $Puntos = 'Inactivo';
    }else if($Puntos2[0]->estado_punto == 'Inactivo')
    {
       $Puntos = 'Activo';
    }
    $this->gstPuntos->actualizarPuntos($_GET['id'],['estado_punto'=>$Puntos]);
    return $this->listarPunto();
    }
 /*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion para editar puntos dentro del sistema 
*/


 public function editarPunto(){ 
     $_SESSION[DATA][CLIENTE_TITLE] = ' Editar Punto';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Rutas'=>"/Menu/20",
            'Listar Puntos'=>"/Rutas/listarPunto",
            'Editar Puntos'=>"/Rutas/editarPunto"
        ]; 
        $Puntos = $this->gstPuntos->getPuntos($_GET['id']);
        return view('Modulos.Rutas.editarPunto',compact('Puntos'));
    }
/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion editarprocess para editar puntos dentro del sistema 
*/


     public function editarPuntoProcess(){
        $this->gstPuntos->getEditarPunto($_GET['punto_id'],$_GET);
        $_SESSION[DATA][MSG] = 'Su Punto se Actualizo correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return $this->listarPunto();
    }
/*Autor: Jasareth Obando
*Modulo: Puntos
*Versiòn: 1.0
*Descripciòn: funcion eliminarPuntos para elimar puntos dentro del sistema 
*/
    public function eliminarPuntos()
    {
        $_SESSION[DATA][MSG] = 'Se elimino correctamente el punto';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstPuntos->deletePuntos($_GET['id']);
        return redirect('Rutas/listarPunto');
    }

}