<?php
namespace App\Http\Modulos\Rutas\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\Http\Clases\Cards;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Rutas;
use App\Http\Modulos\Rutas\Gst\GstRutas;
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: Controllador de Rutas dentro del sistema
*/
class RutasController extends Controller
{
	private $gstRutas;
	
	function __construct(GstRutas $gstRutas){
        session_start(); 
	    $this->gstRutas = $gstRutas;
	}

/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion para crear rutas dentro del sistema 
*/

	public function crearRutas(){

    	$_SESSION[DATA][CLIENTE_TITLE] = 'Registrar Rutas';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Rutas'=>"/Menu/20",
            'Registrar Rutas'=>"/Rutas/crearRutas",
        ];
       return view ('Modulos.Rutas.crearRutas');
	}
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion crearprocess para crear rutas dentro del sistema 
*/
 
 	public function crearRutasProcess()
    {
    	$_GET['cliente_id'] = Auth::user()->cliente_id;
 		$Rutas = $this->gstRutas->registerRutasProcess($_GET);
        $_SESSION[DATA][MSG] = 'Su ruta se registro correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
		return view('Modulos.Rutas.crearRutas',compact('Rutas'));
         
    }
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion para listar rutas dentro del sistema 
*/


    public function listarRutas(){

         $_SESSION[DATA][CLIENTE_TITLE] = ' Listar Rutas';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Rutas'=>"/Menu/20",
            'Listar Rutas'=>"/Rutas/listarRutas"
        ]; 
        $tableInv = new Table;
        $tableInv->setColum($tableInv,
            [
                ID=>'id',
                'Nombre Ruta'=>'nombre_ruta',
                'Tipo de ruta'=>'tipo_ruta',
                'Estado de Ruta'=>'estado_ruta'
            ]
        );
        $opciones = 
            [
               'val-Cambiar estado'=> 'Rutas/cambiarEstadoRutas',
               'Editar'=> 'Rutas/editarRutas',
               'val-Eliminar'=> 'Rutas/eliminarRutas'
            ];
  
        $data = $this->gstRutas->getRutasJoin(Auth::user()->cliente_id)->toArray();
        $tableInv->setItems($tableInv,$data);
        $tableInv = $tableInv->dps($tableInv);
    
        return view(PATH_LIST,[TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
    }
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion para cambiar esstado de  rutas dentro del sistema 
*/
    public function cambiarEstadoRutas()
    {

    $Rutas2= $this->gstRutas->getRutas($_GET['id']);
    
    $Rutas=$Rutas2['estado_ruta'];
    if ($Rutas=='Activo')
    {
       $Rutas = 'Inactivo';
    }else if($Rutas=='Inactivo')
    {
       $Rutas = 'Activo';
    }
    $this->gstRutas->actualizarRutas($_GET['id'],['estado_ruta'=>$Rutas]);
    return $this->listarRutas();
    }
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion para editar estado de  rutas dentro del sistema 
*/

    public function editarRutas(){ 
     $_SESSION[DATA][CLIENTE_TITLE] = ' Editar Rutas';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Rutas'=>"/Menu/20",
            'Listar Rutas'=>"/Rutas/listarRutas",
            'Editar Rutas'=>"/Rutas/editarRutas"
        ]; 
        $Rutas = Rutas::find($_GET['id']);
        return view('Modulos.Rutas.editarRutas',compact('Rutas'));

    }
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion editarRutasProcess para editar estado de  rutas dentro del sistema 
*/

    public function editarRutasProcess(){
        
        $id=$_GET['id'];
        unset($_GET['id']);
        $this->gstRutas->getEditarRutas($id,$_GET);
        $_SESSION[DATA][MSG] = 'Su ruta se edito correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return $this->listarRutas();
    }
/*Autor: Jasareth Obando
*Modulo: RUTAS
*Versiòn: 1.0
*Descripciòn: funcion eliminarProveedores para eliminar rutas dentro del sistema 
*/
    public function eliminarRutas()
    {
        $_SESSION[DATA][MSG] = 'Se elimino correctamente la ruta';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->GstRutas->deleteRutas($_GET['id']);
        return redirect('Rutas/listarRutas');
    }

}