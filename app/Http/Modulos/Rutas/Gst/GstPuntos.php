<?php

namespace App\Http\Modulos\Rutas\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Puntos;
/*
	*Autor: Jasareth obando 
	*Modulo: Rutas
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para las rutas
	*/
class GstPuntos
{
/*
	*Autor: Jasareth obando 
	*Modulo: Rutas
	*Versiòn: 1.0
	*Descripciòn: funcion gestion para crear rutas en el sistema
	*/
public function getAllPunto(){

		return $Punto = Puntos::all();
	}

public  function registerPuntoProcess($Punto){

		Puntos::insert($Punto);
		$Puntos = Puntos::all();
	    return $Puntos;
	}

public static function getPuntos($id)
    {
    	return Puntos::where("punto_id",$id)->get();	
	}

public static function actualizarPuntos($id,$data)
    {
    	return Puntos::where('punto_id',$id)->update($data);
    }

public function getEditarPunto($id,$data){
     	
        Puntos::where("punto_id",$id)->update($data);
    }
    public function deletePuntos($id)
    {
    	Puntos::where('id',$id)->delete();
    }
}