<?php

namespace App\Http\Modulos\Rutas\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Rutas;

class GstRutas
{
	public function getAllRutas(){

		return $Rutas = Rutas::all();
	}

	public  function registerRutasProcess($ruta){

		Rutas::insert($ruta);
		$Rutas = Rutas::all();
	    return $Rutas;
	}
	
 	public function getAllProveedores()
 	{
    	return Rutas::all();
    }

    public function getRutasJoin($id){

      return Rutas::where('tn_sft_mod_rutas.cliente_id',$id)->get();
    }
    public static function getRutas($id)
    {
     	return Rutas::find($id);	
    }
    public static function actualizarRutas($id,$data)
    {
    	return Rutas::where('id',$id)->update($data);
    }
    public function getEditarRutas($id,$data){
     	
        Rutas::where("id",$id)->update($data);
    }
     public function deleteRutas($id)
    {
        Rutas::where('id',$id)->delete();
    }

}
