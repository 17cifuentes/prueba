<?php
namespace App\Http\Modulos\Favoritos\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\http\Modulos\Favoritos\Gst\GstFavoritos;
use App\Http\Clases\Table;
use Auth;
use URL;

class favoritoController extends Controller
{
    
  private $gstFavoritos;

  public function __construct(GstFavoritos $gstFavoritos)
  {
    session_start();
    $this->middleware(MIN_AUTH);
    $this->gstFavoritos  = $gstFavoritos;
  }


  public function lstFavoritos()
  {
   
    $tableFav = new Table;
      $tableFav->setColum($tableFav,
        [
            'Producto'=>'pro_nombre',
            'Cantidad'=> 'cantidad'

        ]);
      $opciones = [
    
      ];
      
      $tableFav->setItems($tableFav,$this->gstFavoritos->getFavoritosJoin()->toArray());
      $tableFav = $tableFav->dps($tableFav);
      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Favoritos'=>'/Menu/20','Cliente Favoritos'=>'']; 
      $_SESSION[DATA][CLIENTE_TITLE] = 'Lista de favoritos del cliente';

      return view (PATH_LIST,[TABLE=>$tableFav,'opc'=>json_encode($opciones)]);
  }

  public function addToFavoritos()
  {
    $productos = $this->gstFavoritos->getProductosById($_GET);
    if ($_GET['opc'] == "undefined" )
    {
      $this->gstFavoritos->insertFavoritos($productos[0]);
      return redirect('carritoCompras/tienda?msg=1&id='.Auth::user()->cliente_id);
    }else{
      $this->gstFavoritos->borrarFavorito($productos[0]->id);
      return redirect('carritoCompras/tienda?msg=2&id='.Auth::user()->cliente_id);
    } 
  }

  public function lstMisFavoritos()
  {
    $tableFav = new Table;
      $tableFav->setColum($tableFav,
        [
            'Producto'=>'pro_nombre',
            'Descripción'=> 'pro_descripcion',
            'Precio de venta'=> 'pro_precio_venta',
            'Nombre del cliente'=> 'cliente_nombre'

        ]);
      $opciones = [
    
      ];
      
      $tableFav->setItems($tableFav,$this->gstFavoritos->getFavoritosUsuariosJoin()->toArray());
      $tableFav = $tableFav->dps($tableFav);

      $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú Favoritos'=>'/Menu/20','Cliente Favoritos'=>'']; 
      $_SESSION[DATA][CLIENTE_TITLE] = 'Lista de favoritos del cliente';
      return view (PATH_LIST,[TABLE=>$tableFav,'opc'=>json_encode($opciones)]);
  }
}


