<?php
namespace App\http\Modulos\Favoritos\Gst;
use Illuminate\Http\Request;
use App\Favoritos;
use App\Productos;
use App\Http\Clases\Table;
use Auth;
use DB;
use App\Http\Clases\FuncionesGenerales;

class GstFavoritos
{
	public function getFavoritos()
	{
		return Favoritos::all();
	}

	public function getFavoritosJoin()
	{
		return Favoritos::where('tn_favoritos.usuario_resector', Auth::user()->cliente_id)
		->join('tn_sft_productos_servicios','tn_sft_productos_servicios.id','tn_favoritos.id_rel')
		->groupBy('tn_sft_productos_servicios.pro_nombre')
        ->select('pro_nombre',DB::raw('count(tn_favoritos.id) as cantidad'))
		->get();
		
	}


	public function insertFavoritos($data)
	{	
		$check = Favoritos::insert([
			['id_rel'=>$data->id, 'entidad'=>'ProductosServicios', 'usuario_emisor'=>Auth::user()->id,'usuario_resector'=>$data->cliente_id]
		]);
	}

	public function getProductosById($id)
	{
		return Productos::where('id',$id)->get();
	}

	public function getFavorito($id)
	{
		return Favoritos::find($id);
	}

	public function getFavoritoUsuarioEmisor($usuEmisor,$clienteId)
	{
		return Favoritos::where('entidad','ProductosServicios')->where('usuario_emisor',$usuEmisor)->where('usuario_resector',$clienteId)->get();
	}

	public function borrarFavorito($id)
	{
		FuncionesGenerales::deleteByElement(new Favoritos,'id_rel',$id);
	}
  public function getFavoritosUsuariosJoin()
  {
    return Favoritos::where('tn_favoritos.usuario_emisor', Auth::user()->id)
    ->join('tn_sft_productos_servicios','tn_sft_productos_servicios.id','tn_favoritos.id_rel')->join('tn_sft_cliente','tn_sft_cliente.id','tn_sft_productos_servicios.cliente_id')->select('pro_nombre','pro_descripcion','pro_precio_venta','cliente_nombre')
    ->get();
    
  }
}

?>

