<?php

namespace App\Http\Modulos\Reservas\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\ReservasEventos;
use App\Productos;
use Auth;
/*
	*Autor: Jasareth obando 
	*Modulo: Rutas
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para las rutas
	*/
class GstReservas
{
/*
	*Autor: Jasareth obando 
	*Modulo: Rutas
	*Versiòn: 1.0
	*Descripciòn: funcion gestion para crear rutas en el sistema
	*/
	public function getAllreservas(){
			return $reservas = ReservasEventos::join('tn_sft_cliente','tn_sft_cliente.id','cliente_id')
			->select("tn_sft_cliente.cliente_nombre","tn_sft_reservas_events.*")
			->get();
	}
	public function newEvento(){
			return new ReservasEventos;
	}

	public  function saveEvent($reserva){
		$evento = $this->newEvento();
		$evento->title = $reserva['nombre'];
		$evento->fecha_inicio = $reserva['fechaInicio'];
		$evento->fecha_fin = $reserva['fechaFin'];
		$evento->hora_inicio = $reserva['horaInicio'];
		$evento->hora_fin = $reserva['horaFin'];
		$evento->className = $reserva['tipoEvento'];
		$evento->cliente_id = Auth::user()->cliente_id;
		$evento->id_producto_servicio = $reserva['id_producto_servicio']; 
		$evento->save();
		/*try{
			$evento->save();
		} catch (\Exception $e) {
			error_log($e->getMessage());
			return false;
		}*/
	    return $evento;
	}
	public function findEvento($id){
		return ReservasEventos::find($id);
	}
		public  function updateEvent($id,$reserva){
		$evento = $this->findEvento($id);
		$evento->title = $reserva['nombre'];
		$evento->fecha_inicio = $reserva['fechaInicio'];
		$evento->fecha_fin = $reserva['fechaFin'];
		$evento->hora_inicio = $reserva['horaInicio'];
		$evento->hora_fin = $reserva['horaFin'];
		$evento->className = $reserva['tipoEvento'];
		$evento->cliente_id = Auth::user()->cliente_id;
		$evento->id_producto_servicio = $reserva['id_producto_servicio']; 
		$evento->save();
		/*try{
			$evento->save();
		} catch (\Exception $e) {
			error_log($e->getMessage());
			return false;
		}*/
	    return $evento;
	}
	public function deleteEvent($id){
		$evento = $this->findEvento($id);
		return $evento->delete();
	}
	public function getAllreservasCliente($id){
		return $reservas = ReservasEventos::join('tn_sft_cliente','tn_sft_cliente.id','cliente_id')
			->select("tn_sft_cliente.cliente_nombre","tn_sft_reservas_events.*")
			->where("tn_sft_reservas_events.cliente_id",$id)
			->get();
	}



	public function getReservasProductosJoin($id)
	{
		$all = $this->getAllreservasCliente($id);
		return FuncionesGenerales::listaOrderDataJoin(new ReservasEventos,$all,[]);
	}

}