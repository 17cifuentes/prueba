<?php
namespace App\Http\Modulos\Reservas\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\Http\Clases\Cards;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Http\Gst\GstGeneral;
use App\Http\Modulos\Reservas\Gst\GstReservas;
use App\Http\Modulos\Productos\Gst\GstProductos;
class ReservasController extends Controller
{
    private $gstGeneral;
    private $gstReservasEventos;
    private $gstProductos;
	function __construct(GstReservas $gstReservas,GstGeneral $gstGeneral,GstProductos $gstProductos){
        session_start(); 
        $this->middleware(MIN_AUTH);
        $this->gstGeneral = $gstGeneral;
        $this->gstReservas = $gstReservas;
        $this->gstProductos = $gstProductos;
	}
    /*Autor: Jasareth Obando
    *Modulo: Puntos
    *Versiòn: 1.0
    *Descripciòn: funcion para crear puntos dentro del sistema 
    */

    public function calendario(){

        $_SESSION[DATA][CLIENTE_TITLE] = 'Registrar evento';
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menu reservas'=>'/Menu/32',$_SESSION[DATA][CLIENTE_TITLE]=>"#"];

        $eventos = $this->gstReservas->getAllreservasCliente(Auth::user()->cliente_id);
        $productos = $this->gstProductos->getProductoByAtributo('cliente_id', Auth::user()->cliente_id);
        return view ('Modulos.Reservas.calendario',[SFT_LIB=>[SFT_DATEPICKER,SFT_FULLCALENDAR],"eventos"=>$eventos,"productos"=>$productos]);
	}

    public function crearEventoProccess(Request $request)
    {

        $this->gstGeneral->validateData($request);
        if(isset($_GET['id'])){
            $reponse = $this->gstReservas->updateEvent($_GET['id'],$request->all());
            if($reponse){
                $_SESSION[DATA][MSG] = RESERVA_MSG_EVENT_UPDATE_GOOD;            
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }else{
                $_SESSION[DATA][MSG] = RESERVA_MSG_EVENT_UPDATE_BAD;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }    
        }else{
            $reponse = $this->gstReservas->saveEvent($request->all());    
            if($reponse){
                $_SESSION[DATA][MSG] = RESERVA_MSG_EVENT_GOOD;            
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }else{
                $_SESSION[DATA][MSG] = RESERVA_MSG_EVENT_BAD;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }
        }
        return redirect("reservas/calendario");
    }
    public function eliminarEvento(){
        $reponse = $this->gstReservas->deleteEvent($_GET['id']);
        if($reponse){
            $_SESSION[DATA][MSG] = RESERVA_MSG_DELETE_GOOD;            
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }else{
            $_SESSION[DATA][MSG] = RESERVA_MSG_DELETE_BAD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }
        return redirect("reservas/calendario");   
    }
    public function listarEventos(){
        $_SESSION[DATA][CLIENTE_TITLE] = 'Lista de eventos';
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menu reservas'=>'/Menu/32',$_SESSION[DATA][CLIENTE_TITLE]=>"#"];  
        $tableReserva = new Table;
        $tableReserva->setColum($tableReserva,
            [
                ID=>'id',
                'Titulo'=>'title',
                'Fencha incio'=>'fecha_inicio',
                'Hora inicio'=>'hora_inicio',
                'Fecha fin'=>'hora_fin',
                'Hora fin'=>'hora_fin',
                'Tipo evento'=>'className',
                'Producto o Servicio'=>'pro_nombre'
            ]
        );
        $opciones = 
            [
               'fun-Gestionar'=> 'showDataEvent'
            ];

        $data =  $this->gstReservas->getReservasProductosJoin(Auth::user()->cliente_id);
        $tableReserva->setItems($tableReserva,$data);
        $tableReserva = $tableReserva->dps($tableReserva);
        return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE,SFT_FULLCALENDAR,SFT_DATEPICKER],SFT_LIB_MOD=>[SFT_LIB_MOD_RESERVAS],TABLE=>$tableReserva,'opc'=>json_encode($opciones)]);
    }
    public function getEvent(){
        $evento = $this->gstReservas->findEvento($_GET['id']);
        $productos = $this->gstProductos->getProductoByAtributo("cliente_id",Auth::user()->cliente_id);
        $evento["productos"] = $productos;
        return json_encode($evento);
    }
}