<?php

namespace App\Http\Modulos\Crm\Gst;
use App\HistorialContacto;
use Carbon\Carbon;
use DB;
//use App\Http\Clases\FuncionesGenerales;
//use Illuminate\Http\Request;


class GstHistorialContacto
{

	public function insertarHistorial($post)
	{
		try {
			return HistorialContacto::insert($post);
		} catch (\Exception $e) {
			error_log($e);
		}
	}

	public function consultarHistorial($contacto,$cliente)
	{
		return HistorialContacto::select('sft_mod_historial_contacto.id as id',TN_CONTACTO_NOMBRE,TN_CONTACTO_APELLIDO,'telefono','direccion','correo','estado_contacto','resumen','sft_mod_historial_contacto.created_at as fecha','tiempo_de_contacto','name')
		->where('id_contacto',$contacto)
		->where('sft_mod_historial_contacto.cliente_id',$cliente)
		->join('users','users.id','sft_mod_historial_contacto.responsable')
		->get();
	}	

	/*Autor: Cristian Campo
    *Modulo: CRM
    *Versiòn: 1.0
    *Descripciòn: Devuelve la cantidad de llamadas por periodo de días (Actual,6,15 y 30 días)
    */
	public function historialCalls($days=false){
		
		$date = Carbon::now();

		if($days!=true){
			
			$date = Carbon::now()->format('Y-m-d');

			$total = HistorialContacto::whereDate('created_at', '=', $date)->get()->toArray();

		}elseif($days==6) {

			$date = $date->subday(6)->toDateString();	
			
		}elseif ($days==15) {

			$date = $date->subday(15)->toDateString();
			
		}elseif ($days==30) {
			
			$date= $date->subday(30)->toDateString();

		}else{

			dd('Error');
		}

		$total = HistorialContacto::whereDate('created_at', '>=', $date)->get()->toArray();

		$total = count($total);

		return $total;

	}

	/*Autor: Cristian Campo
    *Modulo: CRM
    *Versiòn: 1.0
    *Descripciòn: Devuelve la cantidad de llamadas por contacto (exam: Juan ->2)
    */
	public function llamadaPorContactos(){
		return  DB::table(TN_HISTORIAL_CONTACTO)
		->select(TN_ID_CONTACTO,TN_CONTACTO_NOMBRE,TN_CONTACTO_APELLIDO, DB::raw('count(*) as total'))
		->groupBy(TN_ID_CONTACTO,TN_CONTACTO_NOMBRE,TN_CONTACTO_APELLIDO)
		->get()
		->toArray();
		

	}

	/*Autor: Cristian Campo
    *Modulo: CRM
    *Versiòn: 1.0
    *Descripciòn: Devuelve la cantidad de tiempo de las 
     llamadas por contacto en formato de hora
    */
	public function totalLlamasDeContactos(){
		return DB::table(TN_HISTORIAL_CONTACTO)
		->select(DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC('.TN_TIEMPO_DE_CONTACTO.'))) suma'))
		->get()
		->toArray();
		
	}

	/*Autor: Cristian Campo
    *Modulo: CRM
    *Versiòn: 1.0
    *Descripciòn: Devuelve la cantidad de tiempo de las 
     llamadas de todos los contacto en formato de hora
    */
	public function totalLlamadasPorContactos(){
		return  DB::table(TN_HISTORIAL_CONTACTO)
		->select(TN_ID_CONTACTO,TN_CONTACTO_NOMBRE,TN_CONTACTO_APELLIDO,DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC('.TN_TIEMPO_DE_CONTACTO.'))) suma'))
		->groupBy(TN_ID_CONTACTO,TN_CONTACTO_NOMBRE,TN_CONTACTO_APELLIDO)
		->get()
		->toArray();

	}

	/*Autor: Cristian Campo
    *Modulo: CRM
    *Versiòn: 1.0
    *Descripciòn: Devuelve la cantidad de contactos
     por estado (exam: 2 ->Pendiente)
    */
	public function totalContactosPorEstado(){
		return  DB::table(TN_HISTORIAL_CONTACTO)
		->select(TN_CONTACTO_ESTADO,DB::raw('count(nombre) as contactos'))
		->groupBy(TN_CONTACTO_ESTADO)
		->get()
		->toArray();

	}
}   

