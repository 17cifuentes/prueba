<?php

namespace App\Http\Modulos\Crm\Gst;

use App\Http\Clases\FuncionesGenerales;
use App\Contactos;

//use Illuminate\Http\Request;


class GstContactos
{
   /**
   * Función que quita el token, y registra 
   * el contacto en la base de datos
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
	public function insertToContactos($post)
	{
		unset($post[TOKEN]);
      try {
         return Contactos::insert($post);
      } catch (\Exception $e) {
         error_log("Error".$e->getMessage());
         return false;
      }
		
	}

   /**
   * Función que consulta y retorna todos los
   * contactos de la base de datos
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
	public function getAllContacts()
	{
		return Contactos::all();
	}

   /**
   * Función que cambia el estado del contacto
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
	public function changeState($id)
	{
		FuncionesGenerales::cambiarEstadoTable(new Contactos,$id,TN_CONTACTO_ESTADO);
	}

   /**
   * Función que elimina el contacto de la base de datos
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
	public function deleteContact($id)
	{
		FuncionesGenerales::deleteByElement(new Contactos,ID,$id);
	}

   /**
   * Función que trae todos los datos de un contacto usando su id
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
   public function getContactById($id)
   {
      return Contactos::where(ID,$id)->get();
   }

   /**
   * Función que actualiza todos los datos de un contacto usando su id
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
   public function editContactProcess($id,$data)
   {
      unset($data[TOKEN]);
      try {
         return Contactos::where(ID,$id)->update($data);
      } catch (\Exception $e) {
         error_log("Error: ".$e->getMessage());
         return false;
      }
      
   }


   public function validarDatosCaracteres($post)
   {
      if(strlen($post[NOMBRE])>250 or strlen($post[TN_CONTACTO_APELLIDO])>250 or strlen($post[TN_CLIENTE_DIRECCION])>250 or strlen($post[TN_CLIENTE_CORREO])>250 or strlen($post[TN_CONTACTO_RESUMEN])>250 or strlen($post[TN_CONTACTO_ESTADO])>250)
      {
         return false;
      }
      else{
         return true;
      }
   }
   public function validarDatosVacios($post)
   {
      if($post[NOMBRE] == "" or $post[NOMBRE] ==null or $post[TN_CONTACTO_APELLIDO] == "" or $post[TN_CONTACTO_APELLIDO] ==null or $post[TN_CLIENTE_DIRECCION] == "" or $post[TN_CLIENTE_DIRECCION] ==null or $post[TN_CLIENTE_TELEFONO] == "" or $post[TN_CLIENTE_TELEFONO] ==null or $post[TN_CLIENTE_CORREO] == "" or $post[TN_CLIENTE_CORREO] ==null or $post[TN_CONTACTO_RESUMEN] == "" or $post[TN_CONTACTO_RESUMEN] ==null or$post[TN_CONTACTO_ESTADO] == "" or $post[TN_CONTACTO_ESTADO] ==null)
      {
         return false;
      }else{
         return true;
      }
   }
   public function contactarContacto($id,$data)
   {
      unset($data[TITLE_TIEMPO_CONTACTO_ACTUAL]);
     try {
         return Contactos::where(ID,$id)->update($data);  
     } catch (\Exception $e) {
        error_log($e);
        return false;
     }
      
   }
   
   /**
   * Función que busca un registro segun una columna y valor especificos
   * @category GstContactos
   * @package Modulo Crm
   * @version 1.0
   * @author Samuel Beltran
   */
   public function getContactByValCol($col,$val)
   {
      return Contactos::where($col,$val)->get();
   }
}