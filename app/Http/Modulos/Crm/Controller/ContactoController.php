<?php

namespace App\Http\Modulos\Crm\Controller;

use App\Http\Modulos\Crm\Gst\GstHistorialContacto;
use App\Http\Modulos\Crm\Gst\GstContactos;
use App\Http\Controllers\Controller;
use App\Http\Gst\GstGeneral;
use Illuminate\Http\Request;
use App\Http\Clases\Table;
use Auth;

//use App\Http\Clases\FuncionesGenerales;
//use Carbon\Carbon;
//use URL;


class ContactoController extends Controller
{
  private $gstHistorialContacto;
  private $gstContactos;
  private $gstGeneral;
  public function __construct(GstContactos $gstContactos,GstGeneral $gstGeneral, GstHistorialContacto $gstHistorialContacto)
  {
    session_start();
    $this->middleware(MIN_AUTH);
    $this->gstContactos  = $gstContactos;
    $this->gstGeneral = $gstGeneral;
    $this->gstHistorialContacto = $gstHistorialContacto;
  }

 /**
 * Función que muestra un formulario para
 * crear un contacto
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function createContacto()
  {
    $_SESSION[DATA][CLIENTE_TITLE] = CREAR_CONTACTO;
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,TITLE_MENUCMR=>'/Menu/34',CREAR_CONTACTO=>'']; 
    return view(PATH_CREATE_CONTACTO);
  }

 /**
 * Función que inserta la informacion del contacto
 * en la base de datos
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function createContactProcess(Request $request)
  {
    $_POST[TN_CONF_ID_CLIENTE] = Auth::user()->cliente_id;
    $_POST[TN_RESPONSABLE_CONTACTO] = Auth::user()->id;
    $this->gstGeneral->validateData($request);
    $response = $this->gstContactos->insertToContactos($_POST);
    if (!$response) {
      $_SESSION[DATA][MSG] = MSG_NO_CREADO_CONTACTO;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
    }
    $_SESSION[DATA][MSG] = MSG_CLIENTE_CREATE_EXITOSAMENTE;
    $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    return redirect(PATH_LIST_CONTACTOS);
  }

 /**
 * Función que lista todos los contactos almacenados
 * en la base de datos
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function listContactos()
  {
    $table = new Table;
    $table->setColum($table,
      [
        CLIENTE_ID=>TN_ID_CLIENTE,
        CLIENTE_NOMBRE=>NOMBRE,
        CLIENTE_APELLIDO=>TN_CONTACTO_APELLIDO,
        CLIENTE_TELEFONO=>TN_CLIENTE_TELEFONO,
        CLIENTE_DIRECCION=>TN_CLIENTE_DIRECCION,
        CLIENTE_CORREO=>TN_CLIENTE_CORREO,
        TIEMPO_DE_CONTACTO=>TN_TIEMPO_DE_CONTACTO,
        CLIENTE_TITLE_ESTADO=>TN_CONTACTO_ESTADO
      ]);
    $opciones = 
      [
        EDITAR=> CRM_URL_EDITAR_CONTACTO,
        TITLE_CONTACTAR=>CRM_URL_CONTACTAR_CONTACTO,
        TITLE_HISTORIAL_CONTACTO =>CRM_URL_HISTORIAL_CONTACTO,
        //'val-'.CAMBIAR_ESTADO=>'Callcenter/cambiarEstado',
        'val-'.TITLE_ELIMINAR=>CRM_URL_DELETE_CONTACTO
      ];

    $table->setItems($table,$this->gstContactos->getContactByValCol(TN_CONF_ID_CLIENTE,Auth::user()->cliente_id)->toArray());
    $table = $table->dps($table);
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,TITLE_MENUCMR=>'/Menu/34',TITLE_LIST_CONTACTOS=>''];
    $_SESSION[DATA][OPC_FUNCIONES] = null;
    $_SESSION[DATA][CLIENTE_TITLE] = TITLE_LIST_CONTACTOS;
    return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$table,TN_OPC=>json_encode($opciones)]);
}

 /**
 * Función que cambia el estado de un contacto
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function cambiarEstado()
  {   
    $chEstado= $this->gstContactos->changeState($_GET[ID]);
    $_SESSION[DATA][MSG] = CAMBIO_ESTADO_SUCCESS;
    $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    return redirect(PATH_LIST_CONTACTOS);
  }


 /**
 * Función que edita un contacto
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function editarContacto(Request $request)
  {
    if (!isset($_GET[ID])) {
      $_SESSION[DATA][MSG] = MSG_ERROR_PRO_ID_NUll;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_LIST_CONTACTOS); 
    }
    if (!filter_var($_GET[ID],FILTER_VALIDATE_INT)) {
        $_SESSION[DATA][MSG] = MSG_ERROR_PRO_INT_NULL;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect(PATH_LIST_CONTACTOS);        
      }
      $allConct = $this->gstContactos->getContactById($_GET[ID]);
      if (count($allConct) == 0 ) {
        $_SESSION[DATA][MSG] = MSG_CONTACTO_NO_ENCONTRADO;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect(PATH_LIST_CONTACTOS);
      }
        $this->gstGeneral->validateData($request);
        $_SESSION[DATA][CLIENTE_TITLE] = CREAR_CONTACTO;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,TITLE_MENUCMR=>'/Menu/34',TITLE_LIST_CONTACTOS=>'/Crm/listContactos',EDITAR_CONTACTO=>'']; 
        $contactByID = $this->gstContactos->getContactById($_GET);
        return view(PATH_EDITAR_CONTACTO,['contactByID'=>$contactByID]);
  }

 /**
 * Función que edita un contacto de la base de
 * de datos.
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function editarContactoProcess(Request $request)
  {
    $this->gstGeneral->validateData($request);
    $response = $this->gstContactos->editContactProcess($_POST[ID],$_POST);
    if ($response) {
      $_SESSION[DATA][MSG] = UPDATED_SUCCESS;
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect(PATH_LIST_CONTACTOS);
    }else{
      $_SESSION[DATA][MSG] = UPDATED_ERROR;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_LIST_CONTACTOS);
    }
    
  }
 /**
 * Función que borra un contacto de la base de
 * de datos.
 * @category ContactoController
 * @package Modulo Callcenter
 * @version 1.0
 * @author Samuel Beltran
 */
  public function borrarContacto()
  {
    $this->gstContactos->deleteContact($_GET[ID]);
    $_SESSION[DATA][MSG] = DELETED_SUCCESS;
    $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    return redirect(PATH_LIST_CONTACTOS);
  }
  public function contactarContacto()
  {
    $_SESSION[DATA][CLIENTE_TITLE] = TITLE_CONTACTAR_CONTACTO;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,TITLE_MENUCMR=>'/Menu/34',TITLE_LIST_CONTACTOS=>'/Crm/listContactos',TITLE_CONTACTAR_CONTACTO=>'']; 
    $contacto= $this->gstContactos->getContactById($_GET[ID]);
    return view(PATH_CONTACTAR_CONTACTO,[SFT_LIB_MOD=>[SFT_LIB_MOD_CRM],'contacto'=>$contacto]);

  }

  public function parteHora( $hora )
  {    
    $horaSplit = explode(":", $hora);
        
    if( count($horaSplit) < 3 )
    {
      $horaSplit[2] = 0;
    }
      return $horaSplit;
  }

  public function SumaHoras( $time1, $time2 )
  {
      list($hour1, $min1, $sec1) =$this->parteHora($time1);
      list($hour2, $min2, $sec2) =$this->parteHora($time2);

      return date('H:i:s', mktime( $hour1 + $hour2, $min1 + $min2, $sec1 + $sec2));
  }


  public function contactarContactoProcess()
  {
    unset($_POST[TOKEN]);
    if(!isset($_POST[ID]) or !isset($_POST[NOMBRE]) or !isset($_POST[TN_CONTACTO_APELLIDO]) or !isset($_POST[TN_CLIENTE_DIRECCION]) or !isset($_POST[TN_CLIENTE_TELEFONO]) or !isset($_POST[TN_CLIENTE_CORREO]) or !isset($_POST[TN_CONTACTO_RESUMEN]) or !isset($_POST[TN_CONTACTO_ESTADO]))
    {
      $_SESSION[DATA][MSG] = MSG_ERROR;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_LIST_CONTACTOS);
    }
      $validar1 =$this->gstContactos->validarDatosCaracteres($_POST);
      $validar2 =$this->gstContactos->validarDatosVacios($_POST);

    if($validar1 == false or $validar2 == false )
    {
      $_SESSION[DATA][MSG] = MSG_ERROR;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_LIST_CONTACTOS);
    }
    
    if($_POST[TN_TIEMPO_DE_CONTACTO] == " " or null)
    {
      unset($_POST[TN_TIEMPO_DE_CONTACTO]);
    }

     $datos = $this->gstContactos->getContactById($_POST[ID]);
     if($datos[0][TN_TIEMPO_DE_CONTACTO] == "" or null)
     {
        $_POST[TN_TIEMPO_DE_CONTACTO] = $_POST[TN_TIEMPO_DE_CONTACTO];
        $_POST[TITLE_TIEMPO_CONTACTO_ACTUAL] =$_POST[TN_TIEMPO_DE_CONTACTO];

     }else{
      
      $hora = explode(":",$_POST[TN_TIEMPO_DE_CONTACTO]);
      if($hora[0] <10){
          $hora[0] = "0".$hora[0];
      }if($hora[1] <10){
          $hora[1] = "0".$hora[1];
      }if($hora[2] <10){
          $hora[2] = "0".$hora[2];
      }
        $time1 = $hora[0].":".$hora[1].":".$hora[2]; // Tiempo que trae el contador
        $time2 = $datos[0][TN_TIEMPO_DE_CONTACTO]; // TIempo que trae la base de datos
        
        $data = $this->SumaHoras($time1, $time2); // trae el total de el tiempo
        $_POST[TITLE_TIEMPO_CONTACTO_ACTUAL] = $time1;
        $_POST[TN_TIEMPO_DE_CONTACTO] = $data; //datos que se van a modificar

     }  
    $respuesta = $this->gstContactos->contactarContacto($_POST[ID],$_POST);
    if($respuesta != false)
    {
      unset($_POST[TN_TIEMPO_DE_CONTACTO]);
      $_POST[TN_TIEMPO_DE_CONTACTO] = $_POST[TITLE_TIEMPO_CONTACTO_ACTUAL];
      unset($_POST[TITLE_TIEMPO_CONTACTO_ACTUAL]);
      $_POST[TN_ID_CONTACTO] = $_POST[ID];
      unset($_POST[ID]);
      $_POST[TN_CONF_ID_CLIENTE]= Auth::user()->cliente_id;
      $_POST[TN_RESPONSABLE_CONTACTO]= Auth::user()->id;
      $respuesta = $this->gstHistorialContacto->insertarHistorial($_POST);
      if($respuesta != false){
        $_SESSION[DATA][MSG] = UPDATED_SUCCESS;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect(PATH_LIST_CONTACTOS);  
    }
      $_SESSION[DATA][MSG] = MSG_ERROR;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect(PATH_LIST_CONTACTOS);
      }else{
      $_SESSION[DATA][MSG] = MSG_ERROR;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(PATH_LIST_CONTACTOS);
    }
  }

  public function historialContacto()
  {
    if(!isset($_GET[ID]) or $_GET[ID] == "" or $_GET[ID] == null)
    {
      $_SESSION[DATA][MSG] = MSG_ERROR;
      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      return redirect(CRM_URL_LISTAR_HISTORIAL);
    }
    $_SESSION[DATA][CLIENTE_TITLE] = TITLE_HISTORIAL_CONTACTO;
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,TITLE_MENUCMR=>'/Menu/34',TITLE_LIST_CONTACTOS=>'listContactos',TITLE_HISTORIAL_CONTACTO=>''];



    $table = new Table;
    $table->setColum($table,
      [
        CLIENTE_ID=>ID,
        CLIENTE_NOMBRE=>NOMBRE,
        CLIENTE_APELLIDO=>TN_CONTACTO_APELLIDO,
        CLIENTE_TELEFONO=>TN_CLIENTE_TELEFONO,
        CLIENTE_DIRECCION=>TN_CLIENTE_DIRECCION,
        CLIENTE_CORREO=>TN_CLIENTE_CORREO,
        TITLE_RESUMEN_LLAMADA=> TN_CONTACTO_RESUMEN,
        CLIENTE_TITLE_ESTADO => TN_CONTACTO_ESTADO,
        TITLE_FECHA_LLAMADA=>TN_FECHA,
        TITLE_DURACION_CONTACTO =>TN_TIEMPO_DE_CONTACTO,
        TITLE_RESPONSABLE_CONTACTO=> NAME
      ]);
    $opciones = 
      [
        
      ];

  $table->setItems($table,$this->gstHistorialContacto->consultarHistorial($_GET[ID],Auth::user()->cliente_id)->toArray());
    $table = $table->dps($table);
    
    return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$table,TN_OPC=>json_encode($opciones)]);
  }


  /*Autor: Cristian Campo
    *Modulo: CRM
    *Versiòn: 1.0
    *Descripciòn: Retorna las estadisticas basicas ,como : tiempo de llamadas en general y por contacto,cantidad de llamadas por contacto, estado de llamadas y numero de llamadas por cantidad de fechas a los contactos en el modulo crm
    */
  public function estadisticas(){
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,TITLE_MENUCMR=>'/Menu/34',TITLE_ESTADISTICAS=>''];


    $allToday=$this->gstHistorialContacto->historialCalls();

    $oneWeeks=$this->gstHistorialContacto->historialCalls(6);

    $twoWeeks=$this->gstHistorialContacto->historialCalls(15);

    $fourWeeks=$this->gstHistorialContacto->historialCalls(30);

    $call_for_con=$this->gstHistorialContacto->llamadaPorContactos();

    $tot_calls=$this->gstHistorialContacto->totalLlamasDeContactos();

    $tot_calls_for_con=$this->gstHistorialContacto->totalLlamadasPorContactos();

    $tot_con_for_est=$this->gstHistorialContacto->totalContactosPorEstado();

    $response = array('allToday','oneWeeks','twoWeeks','fourWeeks','call_for_con','tot_calls','tot_calls_for_con','tot_con_for_est');

     return view(PATH_ESTADISTICAS_CONTACTO,compact($response),
                [SFT_LIB=>[SFT_DATATABLE,SFT_GRAFICOS],SFT_LIB_MOD=>[SFT_LIB_MOD_CRM]]);
  }
}
