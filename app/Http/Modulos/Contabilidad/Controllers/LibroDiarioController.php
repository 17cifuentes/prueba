<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\libroDiario;
class LibroDiarioController extends Controller
{
    public static function elimnarmovimiento($id){
        $movi = libroDiario::where("id_diarios",$id)->get();
        $caja = Empresa::where('id', 1)->get();
        if($movi[0]->tipo == "Entrada"){
            $cajanueva = floatval($caja[0]->caja) - floatval($movi[0]->valor_operar);
        }else{
            $cajanueva = floatval($caja[0]->caja) + floatval($movi[0]->valor_operar);
        }
        $caja = Empresa::where('id', 1)->update(["caja"=>$cajanueva]);
        libroDiario::where("id_diarios",$id)->delete();
        
        
    }

    public static function movimientoLibro($tipo,$concepto,$valorop){
        //consultamos datos de la empresa para la caja
        //dd($valorop);
        $empresa = Empresa::all();
        if($tipo == "Entrada"){
            $cajanueva = floatval($empresa[0]->caja) + floatval($valorop);
            $opc = 2;
        }else if($tipo == "Salida"){
            //if($valorop > floatval($empresa[0]->caja)){
                $opc = 1;
            //}else{
                $cajanueva = floatval($empresa[0]->caja)- floatval($valorop);
                $opc = 2;
            //}
        }
        //if($opc == 2){
        $libro = new libroDiario;
        $libro->tipo = $tipo;
        $libro->concepto = $concepto;
        $libro->valor_anterior = $empresa[0]->caja;
        $libro->valor_operar = $valorop;
        $libro->valor_nuevo = $cajanueva;
        $libro->save();
        $caja = Empresa::where('id', 1)->update(["caja"=>$cajanueva]);
            return "ok";
        //}else{
            return "Dinero insuficiente en caja";
        //}
        
    }
}