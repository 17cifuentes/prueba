<?php
namespace App\Http\Modulos\Contabilidad\Controllers;
use App\Http\Modulos\Contabilidad\Gst\GstContabilidad;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Gst\GstGeneral;
use App\Http\Clases\Table;
use Auth;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controller\LibroDiarioController;

class ContabilidadController extends Controller
{

	private $gstContabilidad;
	private $gstGeneral;

    public function __construct(GstContabilidad $gstContabilidad,GstGeneral $gstGeneral){
    	session_start();
    	$this->gstContabilidad = $gstContabilidad;
    	$this->gstGeneral = $gstGeneral;
		
    }

	/**
	* Función que muestra un formulario para
	* crear una cuenta
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
    public function createCuenta()
    {
	    $_SESSION[DATA][CLIENTE_TITLE] = CREAR_CONTACTO;
    	$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CONTABILIDAD=>'/Menu/35',TITLE_CREAR_CUENTA=>'']; 
    	$tipoEstados = $this->gstContabilidad->getAllEstados();
    	return view('Modulos.Contabilidad.createContabilidad',[TN_TIPO_ESTADOS=>$tipoEstados]);
    }

	/**
	* Función que registra una neva cuenta
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
    public function createCuentaProcess(Request $request)
    {
    	$this->gstGeneral->validateData($request);
    	$_POST[TN_CONF_ID_CLIENTE] = Auth::user()->cliente_id;
    	$response = $this->gstContabilidad->createRegisCuenta($_POST);

    	if ($response) {
    		
    		$_SESSION[DATA][MSG] = MSG_CUENTA_SUCCESS;
    		$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    		return redirect(PATH_LISTA_CUENTA);
    	}else
    	{
    		$_SESSION[DATA][MSG] = MSG_CUENTA_ERROR;
    		$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
    		return redirect(PATH_LISTA_CUENTA);
    	}
    }


	/**
	* Función que lista las cuentas de la tabla contabilidad
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
    public function listCuenta()
    {
    	$table = new Table;
	    $table->setColum($table,
	      [
	      	ID=>ID,	
	        CLIENTE_NOMBRE => NOMBRE,
	        CLIENTE_DESCRIPCION => TN_DESCRIPCION,
	        CODIGO => TN_CODIGO,
	        CLIENTE_TITLE_ESTADO => TN_NOMBRE_ESTADO
	      ]);
	    $opciones = 
	      [
	        EDITAR=> PATH_URL_EDITAR_CUENTA,
	        'val-'.TITLE_ELIMINAR=>PATH_URL_DELETE_CUENTA
	      ];
	    $table->setItems($table,$this->gstContabilidad->getCuentaJoin(Auth::user()->cliente_id,TN_CONF_ID_CLIENTE));
	    $table = $table->dps($table);
	    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CONTABILIDAD=>'/Menu/35',PATH_LIST_CUENTAS=>''];
	    $_SESSION[DATA][OPC_FUNCIONES] = null;
	    return view(PATH_LIST,[TABLE=>$table,TN_OPC=>json_encode($opciones)]);	
    }

	/**
	* Función muestra un registro y da la posibilidad
	* de modificar sus campos
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
    public function editCuenta()
    {	
    	if(!isset($_GET[ID]))
    	{
    		$_SESSION[DATA][MSG] = MSG_ERROR_PRO_ID_NUll;
      		$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
      		return redirect(PATH_LISTA_CUENTA);
    	}
    	if (!filter_var($_GET[ID],FILTER_VALIDATE_INT)) 
    	{
    		$_SESSION[DATA][MSG] = MSG_ERROR_PRO_INT_NULL;
        	$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        	return redirect(PATH_LISTA_CUENTA);
    	}
    	$allCuentas = $this->gstContabilidad->getCuentasByiId($_GET[ID],ID);
    	
    	if(!count($allCuentas) > 0)
    	{
    		$_SESSION[DATA][MSG] = MSG_CUENTA_NO_ENCONTRADA;
        	$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        	return redirect(PATH_LISTA_CUENTA); 	
    	}
    	$tipoEstados = $this->gstContabilidad->getAllEstados();
    	$tipoEstadosJoin = $this->gstContabilidad->getCuentaJoin($_GET[ID],ID);
	    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CONTABILIDAD=>'/Menu/35',PATH_LIST_CUENTAS=>PATH_LISTA_CUENTA,PATH_EDITAR_CUENTA=>''];
    	return view('Modulos.Contabilidad.editCuenta',[TN_TIPO_ESTADOS=>$tipoEstados,'tipoEstadosJoin'=>$tipoEstadosJoin]);
    }

	/**
	* Función que reistra los nuevos cambios hechos en 
	* en el registro
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
    public function editarCuentaProcess(Request $request)
    {
    	$this->gstGeneral->validateData($request);
    	$response = $this->gstContabilidad->updateCuenta($_POST[ID],$_POST);
    	if ($response) {
    		$_SESSION[DATA][MSG] = MSG_CUENTA_UPDATE_SUCCESS;
        	$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    		return redirect(PATH_LISTA_CUENTA); 
    	}else
    	{
    		$_SESSION[DATA][MSG] = MSG_CUENTA_UPDATE_ERROR;
        	$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
    		return redirect(PATH_LISTA_CUENTA); 
    	}
    }

	/**
	* Función elimina un registro
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
    public function eliminarCuenta()
    {
    	$this->gstContabilidad->deleteCuenta($_GET[ID]);
    	$_SESSION[DATA][MSG] = DELETED_CUENTA_SUCCESS;
    	$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
    	return redirect(PATH_LISTA_CUENTA);
    }
        /**
    * Función que lista la nomina de un usuario
    * @category ContabilidadController
    * @package Modulo Contabilidad
    * @version 1.0
    * @author Samuel Beltran
    */
    public function listarNomina()
    {

        $usuarios = $this->gstContabilidad->getUsers();
        $personal = $this->gstContabilidad->getPersonal();
        $info = $this->gstContabilidad->getAllEmpresa();
        $ver= "Activo";
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CONTABILIDAD=>'/Menu/35','Listar nómina'=>''];
        return view("Modulos.Contabilidad.listar",["usuarios" => $usuarios,"personal" => $personal,"info" => $info,SFT_LIB_MOD => [SFT_LIB_MOD_CONTABILIDAD]]);   
    }

    /**
    * Lista la nomina quincenal
    * @category ContabilidadController
    * @package Modulo Contabilidad
    * @version 1.0
    * @author Samuel Beltran
    */
    public function reservado()
    {
        $usuarios = $this->gstContabilidad->getUsers();
        $personal = $this->gstContabilidad->getPersonal();
        $info = $this->gstContabilidad->getAllEmpresa();
        $ver= "Activo";
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_CONTABILIDAD=>'/Menu/35','Listar nómina'=>'/Contabilidad/listarNomina','Nómina quincenal'=>''];
        return view("Modulos.Contabilidad.reservado",["usuarios" => $usuarios,"personal" => $personal,"info" => $info,SFT_LIB_MOD => [SFT_LIB_MOD_CONTABILIDAD]]);

    }   

    /**
    * Funcion que actualiza la informacion de la tabla plantilla
    * @category ContabilidadController
    * @package Modulo Contabilidad
    * @version 1.0
    * @author Samuel Beltran
    */
    public function updateCuota()
    {
        $id = $_GET['Id'];
        $comision = $_GET['Comisioon'];
        $otros = $_GET['Otros'];
        $arl = $_GET['ARL'];
        $contrato = $_GET['contrato'];         
        $response = $this->gstContabilidad->updatePlantilla($id,$comision,$otros,$arl,$contrato);
        if ($response)
        {
            return $response;    
        }
    }

    public function pdf()
    { 
        
        
           $allEmpresas = $this->gstContabilidad->getAllEmpresa()[0];
           
           //$días= date($FechaFinal)-date($FechaInicio);
           $date1 = date_create($_POST['FechaInicio']);//date_create("2018-01-21");
           
           $date2 = date_create($_POST['FechaFinal']);//date_create("2018-02-02");
           $dias = date_diff($date1,$date2);
           $dias= $dias->format("%a");
           //if($días  < 0 ) $días= 0;
           

           $letras = "DIEZ";//
           $info = ['allEmpresas'=>$allEmpresas,'dias'=>$dias,'letras'=>$letras,'FechaInicio'=>$_POST['FechaInicio'],'Nombre'=>$_POST['Nombre'],'Sueldo'=>$_POST["Sueldo"],'FechaFinal'=>$_POST["FechaFinal"],'Cargo'=>$_POST["Cargo"],'Identidad'=>$_POST["Identidad"],'Contrato'=>$_POST["Contrato"],'Pensión'=>$_POST["Pensión"],'Salud'=>$_POST["Salud"],'Auxilio'=>$_POST["Auxilio"],'Comisiones'=>$_POST["Comisiones"],'Otros_Pagos'=>$_POST["Otros_Pagos"]];

           

           return view('Modulos/Contabilidad/pdfhtml',["info"=>$info]);

           //return funcionesGenerales::getPDF(view('Modulos.Contabilidad.pdfhtml'),compact('info'));


           /*return PDF::loadView('Finanzas.pdfhtml',["dias"=>$dias,"letras"=>$letras,"info"=>$info,$_GET['FechaInicio'],$_GET['Nombre'],$_GET["Sueldo"],$_GET["FechaFinal"],$_GET["Cargo"],$_GET["Identidad"],$_GET["Contrato"],$_GET["Pensión"],$_GET["Salud"],$_GET["Auxilio"],$_GET["Comisiones"],$_GET["Otros_Pagos"]])->stream('Desprendible.pdf'); 
            }catch(\Exception $e){
            Session::flash("incorrecto", " 
            ¿Accediste correctamente al proceso?
            ");
            return redirect("Modulos.Contabilidad/listar");*/
    } 


    public function pdf_todos()
    { 

           extract($_POST);
           $info = $this->gstContabilidad->getAllEmpresa()->toArray();
           
           //$días= date($FechaFinal)-date($FechaInicio);
           $date1 = date_create($_POST['FechaInicio']);//date_create("2018-01-21");
           
           $date2 = date_create($_POST['FechaFinal']);//date_create("2018-02-02");
           $dias = date_diff($date1,$date2);
           $dias= $dias->format("%a");
           //if($días  < 0 ) $días= 0;
           

           $letras = "DIEZ";//
               
               $arrayNombre = array();
               $arraySueldo = array();
               $arrayIdentidad = array();
               $arrayCargo = array();
               $arrayContrato = array();
               $arrayPensión = array();
               $arraySalud = array();
               $arrayAuxilio = array();
               $arrayComisiones = array();
               $arrayOtros_Pagos = array();
               
                $usuarios = $this->gstContabilidad->getUsers();

                
                $total = 0; 
                $valorop = 0;
                foreach($usuarios as $elemento)
                {
                    if($elemento->user_estado == "Activo" && $elemento->name != "admin" && $elemento->rol != "Cliente" )
                    {

                   $arrayNombre[] = $elemento->nombre;
                   $arraySueldo[] = $elemento->valor_a_pagar;
                   $arrayCargo[] = $elemento->cargo;
                   $arrayIdentidad[] = $elemento->identidad;

                   if($elemento->contrato == 0) $arrayContrato[] = "Prestación de Servicios";
                   else $arrayContrato[] = "Labor Contratada";
                   $arrayPensión[] = round(($elemento->valor_a_pagar+$elemento->comisiones+$elemento->otros)*(0.16-$info[0]['pensioon']/100));
                   
                   $arraySalud[] = round(($elemento->valor_a_pagar+$elemento->comisiones+$elemento->otros)*(0.125-$info[0]['salud']/100));

                   if($elemento->contrato == 0) $arrayAuxilio[] = 0;
                   else $arrayAuxilio[] = $info[0]['auxilio'];
                   $arrayComisiones[] = $elemento->comisiones;
                   $arrayOtros_Pagos[] = $elemento->otros;
                    }
                   $total = $total +  $elemento->valor_a_pagar - (round(($elemento->valor_a_pagar+$elemento->comisiones+$elemento->otros)*(0.16-$info[0]['pensioon']/100))) - (round(($elemento->valor_a_pagar+$elemento->comisiones+$elemento->otros)*(0.125-$info[0]['salud']/100))) + (($info[0]['auxilio']*$dias)/30) + $elemento->otros;
                   
                 //dd(number_format($total,0));  
                }
                
               if($opc == "2"){
                   $respuesta = LibroDiarioController::movimientoLibro("Salida","pago no mina del ".$FechaInicio." al ". $FechaFinal,$total);
                    if($respuesta == "ok"){
                        Session::flash("correcto"," Se ah pagado la nomina del ".$FechaInicio." al ".$FechaFinal);
                    }else{
                        Session::flash("incorrecto",$respuesta." para cancelar la nomina del ".$FechaInicio." al ".$FechaFinal);  
                        
                    }
               
                   
               }else{
                   //dd("no pagar");
               }
                          
                //dd($arrayNombre);
               //return PDF::loadView('Finanzas.desprendibles',compact("días","letras","info","FechaInicio","arrayNombre","arraySueldo","FechaFinal","arrayCargo","arrayContrato","arrayPensión","arraySalud","arrayAuxilio","arrayComisiones","arrayOtros_Pagos"))->stream('Desprendible.pdf'); 
               $pdf = \App::make('dompdf.wrapper');
               //PDF::loadView('Finanzas.desprendibles',compact("días","letras","info","FechaInicio","arrayNombre","arraySueldo","FechaFinal","arrayCargo","arrayContrato","arrayPensión","arraySalud","arrayAuxilio","arrayComisiones","arrayOtros_Pagos"))->save('/path//my_stored_file.pdf');
        $pdf->loadView('Finanzas.desprendibles',compact("días","letras","info","FechaInicio","arrayNombre","arrayIdentidad","arraySueldo","FechaFinal","arrayCargo","arrayContrato","arrayPensión","arraySalud","arrayAuxilio","arrayComisiones","arrayOtros_Pagos"));
        //$pdf->save(asset('/images/pdf.pdf'));
        return $pdf->stream();

          } 
}
