<?php

namespace App\Http\Modulos\Contabilidad\Gst;
use App\Http\Clases\FuncionesGenerales;
use App\Contabilidad;
use App\Http\Gst\GstGeneral;
use App\Http\Gst\GstUsuarios;

//use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
//use Auth;
//use URL;


class GstContabilidad
{
    /**
    * Función consulta todos los registros de la tabla de estados
    * @category ContabilidadController
    * @package Modulo Contabilidad
    * @version 1.0
    * @author Samuel Beltran
    */  
	public function getAllEstados()
	{
		$gstGeneral = new GstGeneral;
		return $gstGeneral->getAllEstados();
	}

	/**
	* Función consulta todos los registros de la tabla de contabilidad
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function getAllCuentas(){
		return Contabilidad::all();
	}

	/**
	* Función consulta todos los registros de la tabla de contabilidad
	* usando el id del cliente
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function getCuentasByiId($id,$column){
		return Contabilidad::where($column,$id)->get();
	}

	/**
	* Función que inserta un registro en la tabla contabilidad
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
   	public function createRegisCuenta($id)
   	{
   		try {
   			unset($id[TOKEN]);
			return Contabilidad::insert($id);	
   		} catch (\Exception $e) {
   			error_log("Error: ".$e->getMessage());
    		return false;
   		}	
   	}

	/**
	* Función que consulta todos los registros de la tabla de contabilidad
	* junto con la columna nombre_estado de la tabla estado
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function getCuentaJoin($id,$column)
	{
		$cuentas = $this->getCuentasByiId($id,$column);
			return FuncionesGenerales::listaOrderDataJoin(new Contabilidad,$cuentas,[]);
	}

	/**
	* Función edita un registro
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function updateCuenta($id,$data)
	{
	   	try {
	   		unset($data[TOKEN]);
	   		return Contabilidad::where(ID,$id)->update($data);
	   	} catch (\Exception $e) {
	   		error_log("Error: ".$e->getMessage());
	    		return false;	
	   	}
	}

	/**
	* Función elimina un registro de la tabla
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function deleteCuenta($id)
	{
		FuncionesGenerales::deleteByElement(new Contabilidad,'id',$id);
	}

	/**
	* Función que consulta todos los datos de la tabla users
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function getUsers()
	{
		$gstUsuarios = new GstUsuarios;
		return $gstUsuarios->getUserPersonalJoin();
	}

	/**
	* Función que consulta todos los datos de la tabla Personal
	* @category ContabilidadController
	* @package Modulo Contabilidad
	* @version 1.0
	* @author Samuel Beltran
	*/
	public function getPersonal()
	{
		return $this->gstGeneral->getAllPersonal();
	}

    /**
    * Función que consulta todos los registros de la tabla Empresa
    * @category General
    * @package Empresa
    * @version 1.0
    * @author Samuel Beltran
    */ 
	public function getAllEmpresa()
	{
		return $this->gstGeneral->getAllEmpresa();
	}

    /**
    * Funcion que actualiza la informacion de la tabla plantilla
    * @category ContabilidadController
    * @package Modulo Contabilidad
    * @version 1.0
    * @author Samuel Beltran
    */
	public function updatePlantilla($id,$comision,$otros,$arl,$contrato)
	{ 
        return $this->gstGeneral->updatePlantilla($id,$comision,$otros,$arl,$contrato);
	}
}
