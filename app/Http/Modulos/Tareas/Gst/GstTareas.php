<?php

namespace App\Http\Modulos\Tareas\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Tareas;

/*
	*Autor: Jasareth obando 
	*Modulo: Tareas
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para las Tareas
	*/
class GstTareas
{
/*
*Autor: Jasareth obando 
*Modulo: Tareas
*Versiòn: 1.0
*Descripciòn: funcion  de gestion para crear Tareas
*/
	public function getAllTareas(){

		return $Tareas = Tareas::all();
	}
	public  function registerTareasProcess($tareas){

		return Tareas::insert($tareas);
	    
	}
	public static function getTareas($id)
    {
     	return Tareas::find($id);	
    }
    public static function actualizarTareas($id,$data)
    {
    	return Tareas::where('id',$id)->update($data);
    }
    public function getEditarTareas($data){
     	
        Tareas::where("id")->update($data);
    }

     public static function deleteTareas($id){
     	Tareas::where('id',$id)->delete();
     }

}