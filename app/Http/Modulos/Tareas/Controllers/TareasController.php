<?php
namespace App\Http\Modulos\Tareas\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\Http\Clases\Cards;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\Funciones;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Tareas;
use App\Http\Modulos\Tareas\Gst\GstTareas;
/*
*Autor: Jasareth Obando
*Modulo: Tareas
*Versiòn: 1.0
*Descripciòn: Controllador de Tareas dentro del sistema
*/
class TareasController extends Controller
{
	private $gstTareas;

	function __construct(GstTareas $gstTareas){
        session_start();
	    $this->gstTareas = $gstTareas;
    }

    /*Autor: Jasareth Obando
    *Modulo: Proveedores
    *Versiòn: 1.0
    *Descripciòn: funcion para crear Proveedores dentro del sistema
    */
    public function crearTareas(){

    $_SESSION[DATA][CLIENTE_TITLE] = 'Registrar Tareas';
    $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Tareas'=>"/Menu/29",
            'Registrar Tareas'=>"#",
        ];
        return view ('Modulos.Tareas.crearTareas');
    }
/*Autor: Jasareth Obando
*Modulo: Tareas
*Versiòn: 1.0
*Descripciòn: funcion crearprocess para Registrar Tareas dentro del sistema 
*/
    public function crearTareasProcess()
    {
        $Tareas = $this->gstTareas->registerTareasProcess($_GET);
        $_SESSION[DATA][MSG] = 'Su Tarea se Asigno correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return view('Modulos.Tareas.crearTareas',compact('Tareas'));
    }

    public function listarTareas(){

         $_SESSION[DATA][CLIENTE_TITLE] = ' Listar Tareas';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Tareas'=>"/Menu/29",
            'Listar Tareas'=>"/Tareas/listarTareas"
        ]; 
        $tableInv = new Table;
        $tableInv->setColum($tableInv,
            [
                ID=>'id',
                'Nombre'=>'nombre',
                'Descripcion'=>'descripcion',
                'Estado'=>'estado'
            ]
        );
        $opciones = 
            [
               'val- Cambiar estado'=> 'Tareas/cambiarEstadoTareas',
               'Editar'=> 'Tareas/editarTareas',
               'val- Eliminar'=> 'Tareas/eliminarTareas'
            ];
        $data = $this->gstTareas->getAllTareas()->toArray();
        $tableInv->setItems($tableInv,$data);
        $tableInv = $tableInv->dps($tableInv);
    
        return view(PATH_LIST,[TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
    }



      public function cambiarEstadoTareas()
    {
    $Tareas2= $this->gstTareas->getTareas($_GET['id']);
    
    $Tareas=$Tareas2['estado'];
    if ($Tareas=='Activo')
    {
       $Tareas = 'Inactivo';
    }else if($Tareas=='Inactivo')
    {
       $Tareas = 'Activo';
    }
    $this->gstTareas->actualizarTareas($_GET['id'],['estado'=>$Tareas]);
    return $this->listarTareas();
        }

/*Autor: Jasareth Obando
*Modulo: TAREAS
*Versiòn: 1.0
*Descripciòn: funcion para editar Tareas dentro del sistema 
*/
    public function editarTareas(){ 
     $_SESSION[DATA][CLIENTE_TITLE] = ' Editar Tareas';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Tareas'=>"/Menu/29",
            'Listar Tareas'=>"/Tareas/listarTareas",
            'Editar Tareas'=>"/Tareas/editarTareas"
        ]; 
        $Tareas = Tareas::find($_GET['id']);
        return view('Modulos.Tareas.editarTareas',compact('Tareas'));
    }
/*Autor: Jasareth Obando
*Modulo: TAREAS
*Versiòn: 1.0
*Descripciòn: funcion editarTareasProcess para editar Tareas dentro del sistema 
*/
    public function editarTareasProcess(){
        $_GET['id'];
        $this->gstTareas->getEditarTareas($_GET);
        $_SESSION[DATA][MSG] = 'Su Tarea se edito correctamente';
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        return redirect('Tareas/listarTarea');
    }
/*Autor: Jasareth Obando
*Modulo: TAREAS
*Versiòn: 1.0
*Descripciòn: funcion eliminarTareas para eliminar Tareas dentro del sistema 
*/
    public function eliminarTareas(){
//dd($_GET);
    $_SESSION[DATA][MSG] = 'Su Tarea se Elimino correctamente';
    $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;  
    $this->gstTareas->deleteTareas($_GET['id']);
    return redirect('Tareas/listarTareas');
    }

}