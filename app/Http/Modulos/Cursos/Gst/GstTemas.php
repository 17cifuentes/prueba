<?php
namespace App\http\Modulos\Cursos\Gst;
use Illuminate\Http\Request;
use App\Temas;
use App\Http\Clases\Table;
use Auth;
use DB;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;

class GstTemas
{
	/*
	*Autor: Jasareth obando
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion que Registra los temas en el sistema.
	*/
public function postRegisterTema($post){

		unset($post['_token']);

        try{

            return FuncionesDB::registrar(new Temas,$post);

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

     public function getAllTemas($idCliente){

        try{

            return Temas::where("cliente_id",$idCliente)->get();

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    public function getAllTemasJoin($idCliente){

        $data = $this->getAllTemas($idCliente);
        
        if ($data) {

            try{
                                    
                return FuncionesGenerales::listaOrderDataJoin(new Temas,$data,[]);

            }catch(Exception $e){

                error_log("Error : ".$e->getMessage());

                return false;
            }

        }else{

            return redirect ('/Cursos/listarTemas');
        }
    }
    public function getDataTema($id,$cliente=false){

        try{
            if ($cliente!=false) {
                return Temas::where('id',$id)
                ->where('cliente',$cliente)
                ->get()
                ->toArray();    
            }else{
                
                 return Temas::findOrFail($id);
            }

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    /*
    *Autor: Jasareth obando 
    *Modulo: Temas
    *Versiòn: 1.0
    *Descripciòn: Actualiza los datos de un tema registrado en especifíco.
    */
    public function updateDataTema($columna,$id,$data)
    {
        try{

            return FuncionesDB::Editar(TABLA_TEMAS,$columna,$id,$data);
            
        }catch(\Exception $a) {
            error_log("Error : ".$a->getMessage());
            return false;
        }
    }
    /*
    *Autor: Jasareth obando
    *Modulo: Temas
    *Versiòn: 1.0
    *Descripciòn: funcion para eliminar un registro de temas por medio del id.
    */
    public function deleteProjectTemas($id)
    {
        try{

            return FuncionesDB::eliminar(TABLA_TEMAS,'id',$id);

        }catch(Exception $i){

            error_log("Error : ".$i->getMessage());

            return false;
        }
    }
    /*
    *Autor: Jasareth obando
    *Modulo: Temas
    *Versiòn: 1.0
    *Descripciòn: funcion para cambiar estado de un registro de temas.
    */
     public function updateEstadoTema($id,$columna)
    {
        FuncionesGenerales::cambiarEstadoTable(new Temas,$id,$columna);
    }

}

?>