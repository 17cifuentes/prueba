<?php
namespace App\http\Modulos\Cursos\Gst;
use Illuminate\Http\Request;
//Modificar aqui //use App\Temas;
use App\Http\Clases\Table;
use App\Cursos;
use App\CategoriasCurso;
use Auth;
use DB;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;

class GstCursos
{
	public function registrarCurso($post)
	{
		unset($post['_token']);

        try{

            return FuncionesDB::registrarGetRegistro(new Cursos,$post);

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }
    public function registrarCategoriasCurso($categorias)
    {
    	try{

            return FuncionesDB::registrarGetRegistro(new CategoriasCurso,$categorias);

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }
    public function getAllCursos()
    {
    	$cliente = Auth::user()->cliente_id;
    	$data = Cursos::where('cliente_id',$cliente)->get();
    	$info = FuncionesGenerales::listaOrderDataJoin(new Cursos,$data,[]);
    	return $info;
    }
    public function categoriasCursoId($id)
    {
    	$data = CategoriasCurso::where('id_curso',$id)->get();
    	$categorias = FuncionesGenerales::listaOrderDataJoin(new CategoriasCurso,$data,[]);
    	return $categorias;

    }
    	
}

