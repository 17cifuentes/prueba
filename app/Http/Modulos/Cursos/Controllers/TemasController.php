<?php
namespace App\Http\Modulos\Cursos\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Modulos\Cursos\Gst\GstTemas;
use App\Http\Gst\GstGeneral;
use App\Http\Gst\GstUsuarios;
use App\Http\Clases\Table;
use App\Temas ;
use App\Http\Clases\FuncionesGenerales;
use Auth;
use URL;

class TemasController extends Controller
{
  private $gstUsuarios;
  private $gstTemas;
  private $gstGeneral;

  public function __construct(GstTemas $gstTemas,GstUsuarios $gstUsuarios,GstGeneral $gstGeneral)
  {
    session_start();
    $this->middleware(MIN_AUTH);
    $this->gstUsuarios = $gstUsuarios;
    $this->gstTemas  = $gstTemas;
    $this->gstGeneral = $gstGeneral;

  }

  /*
	*Autor: Jasareth obando
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion retorna la vista para crear Temas.
	*/
	public function crearTemas()
	{

	$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_TEMAS=>'/Menu/40',CREAR_TEMAS=>''];
	$cliente=Auth::user()->cliente_id;

	return view(PATH_TEMAS_CREAR_TEMAS,compact('cliente'));

	}

	/*
	*Autor: Jasareth obando
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion que realiza y valida la creacion de un tema en el sistema.
	*/

	public function crearTemaProccess(Request $request)
	{
		if(strlen($_POST['nombre_tema']) > 255 || strlen($_POST['descripcion_tema']) > 255)
		{
            $_SESSION[DATA][MSG] = ERROR_CANT_CARACTERES_TEMAS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(FUN_LST_TEMA);
       }else{

		$this->gstGeneral->validateData($request);

		$response=$this->gstTemas->postRegisterTema($_POST);
		if ($response) {

			$_SESSION[DATA][MSG] = MSG_CRE_TEM;

			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (FUN_LST_TEMA);	
		}else{

			$_SESSION[DATA][MSG] = MSG_CRE_TEM_FAIL;

			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (FUN_LST_TEMA);
		}
		}
	}
		/*
	*Autor: Jasareth obando
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion que lista los temas.
	*/
	public function ListarTemas(){
		
		$cliente=Auth::user()->cliente_id;

		$tableInv = new Table;
		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_TEMAS=>'/Menu/40',LISTAR_TEMAS=>''];
		$data = $this->gstTemas->getAllTemasJoin($cliente);
		$tableInv->setColum($tableInv,
			[
				ID=>'id',
				NOMBRE_DEL_TEMA=>BD_NOMBRE,
				DESCRIPCIÓN=>BD_DESCRIPCION,
				CLIENTE=>BD_CLIENTE,
				ESTADO=>BD_ESTADO

			]
		);
		$opciones = 
		[
			BTN_EDITAR=> RUTA_EDITAR,
			ELIMINAR=>RUTA_ELIMINAR,
			CAMBIAR_ESTADO_TEMA=>RUTA_ESTADO
		];
		
		$tableInv->setItems($tableInv,$data);
		
		$tableInv = $tableInv->dps($tableInv);

		return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
	}
	/*
	*Autor: Jasareth obando
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion para editar temas del sistema.
	*/
	public function editarTema()
	{
		if (isset($_GET['id'])){

			$temas=$this->gstTemas->getDataTema($_GET['id']);
			$cliente=Auth::user()->cliente_id;
			$usuario=$this->gstUsuarios->getUsers($cliente);

			$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PMI=>'/Menu/40',LISTAR_TEMAS=>"ListarTemas",EDITAR_TEMA=>''];

			return view(PATH_TEMAS_EDITAR_TEMA,compact('temas','cliente','usuario'));	
		}	
	}
	/*
	*Autor: Jasareth
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion que valida y edita los temas registrados en el sistema.
	*/
	public function editarTemaProccess(Request $request)
	{
		if(strlen($_POST['nombre_tema']) > 255 || strlen($_POST['descripcion_tema']) > 255)
		{
            $_SESSION[DATA][MSG] = ERROR_CANT_CARACTERES_TEMAS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(FUN_LST_TEMA);
       }else{

		$this->gstGeneral->validateData($request);
		$response = $this->gstTemas->updateDataTema('id',$_POST['id'],$_POST);

		if ($response) 
		{
			$_SESSION[DATA][MSG] = MSG_UPDATE_TEM;
			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (FUN_LST_TEMA);	
		}else{

			$_SESSION[DATA][MSG] = MSG_ERROR_UPDATE_TEM;
			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (FUN_LST_TEMA);
		}
		}
	}
	/*
	*Autor: Jasareth
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion que elimina los temas registrados en el sistema.
	*/
	public function eliminarTema()
	{
		$_SESSION[DATA][MSG] = MSG_DELETE_CORRECTAMENTE;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstTemas->deleteProjectTemas($_GET['id']);
        return redirect(FUN_LST_TEMA);
	}
	/*
	*Autor: Jasareth
	*Modulo: Temas
	*Versiòn: 1.0
	*Descripciòn: Funcion que cambia el estado de los temas registrados en el sistema.
	*/
	public function cambiarEstadoTema()
    {
      $this->gstTemas->updateEstadoTema($_GET['id'],'estado_tema');
      $_SESSION[DATA][MSG] = CAMBIO_ESTADO_SUCCESS;
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect(FUN_LST_TEMA);
    }

}