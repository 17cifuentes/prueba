<?php
namespace App\Http\Modulos\Cursos\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Gst\GstGeneral;
use App\Http\Gst\GstUsuarios;
use App\Http\Clases\Table;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Modulos\Cursos\Gst\GstCursos;
use App\Http\Modulos\Productos\Gst\GstCategorias;
use Session;
use Auth;
use URL;

class CursosController extends Controller
{
	private $gstCampos;
	private $gstUsuarios;
	private $gstGeneral;
	private $gstCursos;	
	private $gstCategorias;


	public function __construct(GstCursos $gstCursos,GstUsuarios $gstUsuarios,GstGeneral $gstGeneral,GstCategorias $gstCategorias)
	{
	 	session_start();
	    $this->middleware(MIN_AUTH);
	    $this->gstUsuarios = $gstUsuarios;
	    $this->gstCursos  = $gstCursos;
	    $this->gstGeneral = $gstGeneral;
	    $this->gstCategorias = $gstCategorias;
	}

	public function crearCurso()
	{
		$usuarios = $this->getUsuarioDirector();
		$categorias = $this->gstCategorias->getAllCategorias()->toArray();
		return view(CREAR_CURSO,['usuarios'=>$usuarios,'categorias'=>$categorias,'libMod'=>['Cursos'],SFT_LIB=>[SFT_SELECT2],SFT_LIB_MOD=>[SFT_LIB_MOD_CURSOS]]);
	}
	/*
		Funcion para traer todos los usuarios del cliente
		JR
	*/

	public function getUsuarioDirector()
	{
		$usuarios = $this->gstUsuarios->getUserClient();
		return $usuarios;
	}
	/*	
		Funcion para crear el curso (agregar en Basese de datos)
		JR
	*/
	public function crearCursoProccess(Request $request)
	{
		$this->gstGeneral->validateData($request);
		unset($_POST['_token']);
		$_POST['cliente_id'] = Auth::user()->cliente_id;
		$_POST['img_curso']= FuncionesGenerales::cargararchivo($_FILES['file-upload'],PATH_CURSO_IMG,CURSO_IMG_DEFAULT);
		$numcategorias =count($_POST['id_categoria']);
		$categorias = [];
		for($i=0; $i<$numcategorias; $i++)
		{	
			$categorias[$i] = $_POST['id_categoria'][$i];
		}
		unset($_POST['id_categoria']);
		$registraProducto = $this->gstCursos->registrarCurso($_POST);
		for($i=0; $i<$numcategorias; $i++)
		{	
			$registrarCategoria = [];
			$registrarCategoria['id_curso'] = $registraProducto->id;
			$registrarCategoria['id_categoria'] = $categorias[$i];
			$respuesta = $this->gstCursos->registrarCategoriasCurso($registrarCategoria);
		}
		if(isset($respuesta) && $respuesta != false)
		{
			Session::flash(NOTIFICACION_SUCCESS, 'Se creo el nuevo curso satisfactoriamente');
		}else{
			Session::flash(NOTIFICACION_ERROR, 'Ocurrio un error');
		}
		return redirect('Cursos/listarCursos');
	}
	/*
		Funcion para Listar los cursos del cliente
		JR
	*/
	public function listarCursos()
	{

      $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Cursos';
      
      $tableMenu = new Table;
      $tableMenu->setColum($tableMenu,
        [
          'id' => 'id',
          'Nombre curso'=>'nombre_curso',
          'Encargado'=>'name',
          'html-Foto'=>['<img src="/-img_curso" width="100em">','img_curso']
        ]
      );
      $opciones = 
      [
        'Editar'=> '/Cursos/editarCurso',
        'fun- Categorias curso'=>'categoriasCurso'
      ];

      $datos = $this->gstCursos->getAllCursos();
      
      $tableMenu->setItems($tableMenu,$datos);
      $tableMenu = $tableMenu->dps($tableMenu);

      return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],SFT_LIB_MOD=>[SFT_LIB_MOD_CURSOS],TABLE=>$tableMenu,'opc'=>json_encode($opciones)]);
	}
	/*
		Funcion para traer todas las categorias del curso para un JS y una modal "categoriasCurso()"
		JR
	*/
	public function categoriasCursoId()
	{
		//dd($_GET['curso_id']);
		$resultado = $this->gstCursos->categoriasCursoId($_GET['curso_id']);
		//dd($resultado);
		return json_encode($resultado);
	}
	public function editarCurso()
	{
	
	 return view(CREAR_CURSO,['usuarios'=>$usuarios,'categorias'=>$categorias,'libMod'=>['Cursos'],SFT_LIB=>[SFT_SELECT2],SFT_LIB_MOD=>[SFT_LIB_MOD_CURSOS]]);
	}



}