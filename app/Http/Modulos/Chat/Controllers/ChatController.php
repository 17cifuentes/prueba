<?php

namespace App\Http\Modulos\Chat\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Gst\GstCliente;
use App\Http\Clases\Table;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\RealTime;
use App\Http\Controllers\Permisos\PermisosController;
use App\Cliente;
use App\Http\Modulos\Chat\Gst\GstChat;
use URL;
use Auth;
/*
    *Autor: Cristian Cifuentes
    *Modulo: Chat
    *Versiòn: 1.0
    *Descripciòn: Controlador para gestionar Chat en el sistema.
    */
class ChatController extends Controller
{
    private $gstChat;
    function __construct(GstChat $gstChat){
        session_start();
        $this->middleware(MIN_AUTH);
        $this->gstChat = $gstChat;
    }
    /**
     * Funcion para enviar mensaje a un canal
     * @category Chat
     * @package Modulo Chat
     * @version 1.0
     * @author Critian cifuentes
     */
    public function sendMsg()
    {
        $response = $this->gstChat->saveMsgUsu($_GET['id'],$_GET['msg']);
        if($response)
        {
            $data = $this->getMsgChat($_GET['id']);
        }
        RealTime::connectCanalPS($_GET['msg'],'canalUser'.$_GET['contacto'],'getMsg');
        return $data;
    }
    /**
     * Funcion para mostrar el panel del chat
     * @category Chat
     * @package Modulo Chat
     * @version 1.0
     * @author Critian cifuentes
     */
    public function panelChat(){
        $contacts = $this->gstChat->getContactsUser(Auth::user()->id);
        return view("Modulos.Chat.panelChat",[
            'contacts' =>$contacts,
            SFT_LIB_MOD=>[SFT_LIB_MOD_CHAT]]
        );
    }
    /**
     * Funcion para inicializar conversación o mostrar conversación
     * @category Chat
     * @package Modulo Chat
     * @version 1.0
     * @author Critian cifuentes
     */
    public function getMsgChat($id = false){
        if(!$id)
        {
            $id = $_GET['id'];
        }
        $chat = $this->gstChat->getMsgChat($id);
        $data['idContacto'] = $chat[0]->contacto_id;
        $data['imgUser'] = $chat[0]->user->img;
        $data['imgContacto'] = $chat[0]->contacto->img;
        $data['msg'] = $chat[0]->msg;
        return json_encode($data);
    }
    /**
     * Funcion buscar conversaciones y nuevos chats, mensajes, etc
     * @category Chat
     * @package Modulo Chat
     * @version 1.0
     * @author Critian cifuentes
     */
    public function searchChat(){
        $hoy = date("Y-m-d h:m:s");
        
        $chat = $this->gstChat->getContactsUserUpdate(Auth::user()->id,$hoy);
        $data['idContacto'] = $chat[0]->contacto_id;
        $data['imgUser'] = $chat[0]->user->img;
        $data['imgContacto'] = $chat[0]->contacto->img;
        $data['msg'] = $chat[0]->msg;
        return json_encode($data);
    }    
}
