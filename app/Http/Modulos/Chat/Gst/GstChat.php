<?php
namespace App\http\Modulos\Chat\Gst;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\ChatContactos;
use App\Http\Clases\FuncionesGenerales;

class GstChat
{
	public function getFavoritos()
	{
		return Favoritos::all();
	}
	public function getContactsUser($id)
	{
		return ChatContactos::where("id_usu",$id)->Orwhere("contacto_id",$id)->get();
	}
	public function getContactsUserUpdate($id,$time)
	{
		return ChatContactos::where("id_usu",$id)->Orwhere("contacto_id",$id)->get();
	}
	public function getMsgChat($id)
	{
		return ChatContactos::where("id",$id)->get();
	}
	public function findMsgChat($id)
	{
		return ChatContactos::find($id);
	}
	public function saveMsgUsu($id,$msg)
	{
		$chatContacto =  $this->findMsgChat($id);
		$chat =  json_decode($chatContacto->msg);
		$chat[0]->usu[] = $msg;
		$chat[0]->timeUsu[] = date('Y-m-d h:m:s');
		$chatContacto->msg = json_encode($chat);
		try{
			if(!$chatContacto->save()){
				return false;
			}
			return $chatContacto;
		} catch (\Exception $e) {
			error_log($e->getMessage());
			return false;
		}
		
	}
	
}

?>

