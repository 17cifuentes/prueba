<?php

namespace App\Http\Modulos\Productos\Gst;

use Illuminate\Http\Request;
use App\Categorias;
use App\Roles;
use Auth;
use App\Http\Gst\GstGeneral;
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Roles
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para los Roles
	*/
class GstCategorias
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getAllCategorias()
    {
    	return Categorias::all();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getAllByClient()
    {
    	return Categorias::where("cliente_id",Auth::user()->cliente_id)->get();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function deleteCat($id)
    {
    	return Categorias::where("id",$id)->delete();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todas las categorias de un cliente.
	*/
    public function getCategoriasByAtribute($atribute,$valor)
    {
    	return Categorias::where($atribute,$valor)->orderBy('orden')->get();
    }
     /*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todas las categorias de un cliente.
	*/
    public function getCategoriasPadres($id)
    {
    	return Categorias::where('cliente_id',$id)->where("padre",null)->orderBy('orden')->get();
    }
    public function InsertCat($data)
    {
    	try{
    		$r = Categorias::insert($data);
			return $r;
		}catch(\Exception $e) {
    		error_log("Error: ".$e->getMessage());
    		return false;
		}
    	
    }
    
    public function insertarOrden($id_categoria,$data){

		try {

			return Categorias::where('id',$id_categoria)->update(['orden'=>$data]);	

		} catch (\Exception $e) {

			error_log("Error: ".$e->getMessage());
			return false;
		}
	}

	public function getAllPadres(){
		return Categorias::all();
		
	}
	public function getAllPadres2(){
		return Categorias::where("cliente_id",Auth::user()->cliente_id)->get();
		
	}

	public function getMyFatherId($identificador){
		return Categorias::select('padre')
		->where('id',$identificador)
		->get()
		->toArray();
		
	}

	public function getMyFatherName($identificador){
		return Categorias::select('cat_nombre')
		->where('id',$identificador)
		->get()
		->toArray();
		
	}
	public function getHijos($identificador){
		return Categorias::select("tn_sft_categoria_productos.*","tn_sft_files.path","sft_iconos.formato")
		->where('padre',$identificador)
		->join("tn_sft_files","tn_sft_files.id","tn_sft_categoria_productos.img")
		->join("sft_iconos","sft_iconos.id","tn_sft_categoria_productos.icono")
		->get();
	}
	public function getIconos()
	{
		return GstGeneral::getIconos();
	}


}
