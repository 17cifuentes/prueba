<?php

namespace App\Http\Modulos\Productos\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Modulos;
use App\Categorias;
use App\Productos;
use App\CaracteristicasProducto;
use App\Promociones;

	/*
	*Autor: Cristian Cifuentes
	*Modulo: Roles
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para los Roles
	*/
class GstProductos
{
	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getAllProductos($cliente=false)
    {
        if ($cliente!=false) {

            return Productos::where('cliente_id',$cliente)->get()->toArray();

        }else{
            
            return Productos::all();    
        }
    	
    }
    	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getNewProducto()
    {
    	return new Productos;
    }
        	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Carga todos los Roles del sistema.
	*/
    public function getNewCarateristicaPro()
    {
    	return new CaracteristicasProducto;
    }
    public function getCaracteristicasProducto($id){
    	return CaracteristicasProducto::where('id_pro',$id)->get();
    }
    public function deleteCatPro($idPro){
    	return CaracteristicasProducto::where('id_pro',$idPro)->delete();
    }

    public function deleteCarPro($id){
    	return CaracteristicasProducto::where('id',$id)->delete();
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Consulta por id de producto.
	*/
    public function getProductoById($id)
    {
    	return FuncionesDB::consultarRegistro(new Productos,ID,$id);
    }
    /*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Consulta por id de producto.
	*/
    public function getProductoByAtributo($atributo, $valor)
    {

    	return Productos::where($atributo,$valor)->get();
    	
    }

	/*
	*Autor: Cristian Cifuentes
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Consulta por id de producto.
	*/
    public function getProductosShopCart($cliente)
    {
    	//return Productos::where(TN_PRODUCTOS_SERVICIOS.'.'.TN_USU_CLIENTE_ID,$cliente)->join(TN_CAT_PRO,TN_PRODUCTOS_SERVICIOS.'.'.TN_CAT_PRO_ID,'=',TN_CAT_PRO.'.'.ID)->get();
    	return Productos::where(TN_PRODUCTOS_SERVICIOS.'.'.TN_USU_CLIENTE_ID,$cliente)->where('en_venta','Activo')->get();
    }
    /*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Consulta por id de producto.
	*/
    public function consultaProducto($id)
    {
     return	Productos::find($id);
    }
    /*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Funcion Para Editar Producto
	*/

    public function updateProducto($id,$data)
    {
    	try{
    		$r = Productos::where('id',$id)->update($data);
			return $r;
		}catch(\Exception $e) {
    		error_log("Error: ".$e->getMessage());
    		return false;
		}
    }
    /*
	*Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada:
	*Salida:
	*Descripciòn: Funcion Para Eliminar Productos
	*/
    public function deleteProduct($id)
    {
    	try {
	    	CaracteristicasProducto::where('id_pro',$id)->delete();
	    	Productos::where('id',$id)->delete();
	    	return true;
    	} catch (\Exception $e) {
    		error_log("Error: ".$e->getMessage());
    		return false;
    	}
    	
    }
    /*
    *Autor: Javier R
	*Modulo: Funciones
	*Versiòn: 1.0
	*Entrada: $id=identificador del producto $columna del estado del producto
	*Salida: Modulo productos, id producto, columna producto
	*Descripciòn: Funcion Para Cambiar Estado Producto
	*/
    public function cambioEstado($id,$columna)
    {
    	try {
    		$r =FuncionesGenerales::cambiarEstadoTable(new Productos,$id,$columna);	
    		return $r;
    	} catch (\Exception $e) {
    		error_log("Error: ".$e->getMessage());
    		return false;
    	}
    	
    }

    public function getCategoriaJoin($id)
    {
	return Productos::select('img_producto','tn_sft_productos_servicios.id as id','pro_nombre','cat_nombre','pro_descripcion','pro_precio_venta','pro_precio_interno','estado_producto','tn_sft_productos_servicios.created_at')->join('tn_sft_categoria_productos','tn_sft_categoria_productos.id','cat_id')->where('tn_sft_categoria_productos.cliente_id',$id)->get();
    }

    public function getModuloAtributte($atribute,$value)
     {
        return FuncionesGenerales::getByAtribbute(New Modulos,$atribute,$value);
     }

     public function getPromocionProducto($idProducto)
     {
     	return Promociones::where('id_producto',$idProducto)->get();
     }
     
     // Funcion para traer todos los productos que esten activos en la tienda
     public function getProductsCliente($cliente)
    {
        return Productos::where('cliente_id',$cliente)
        ->where('en_venta','Activo')
        ->where('estado_producto','Activo')
        ->get();
    }
     
}
