<?php
namespace App\Http\Modulos\Productos\Controllers;
use App\Http\Controllers\Permisos\PermisosController;
use App\Http\Modulos\Productos\Gst\GstCategorias;
use App\Http\Modulos\Productos\Gst\GstProductos;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Controllers\Controller;
use App\Http\Clases\FuncionesDB;
use App\CaracteristicasProducto;
//use App\Http\Clases\Formulario;
//use Illuminate\Http\Request;
use App\Http\Gst\GstCampos;
use App\Http\Clases\Table;
//use App\Http\Clases\Cards;
//use App\Categorias;
use App\Productos;
use App\Modulos;
use Session;
use Auth;
use URL;
use Request;


class ProductosController extends Controller
{
    private $gstProductos;
    private $gstCategorias;
    private $gstCampos;
    public function __construct(GstProductos $gstProductos,GstCategorias $gstCategorias,GstCampos $gstCampos) 

    {
        $this->middleware(MIN_AUTH,[MIDDLEWARE_EXCEPT => ['identificarHijos']]);
        $this->gstProductos = $gstProductos;
        $this->gstCategorias = $gstCategorias;
        $this->gstCampos = $gstCampos;
        session_start();
    }
    public function borrarCategoria(){
        $res = $this->gstCategorias->deleteCat($_GET[ID]);
        if($res){
            Session::flash(NOTIFICACION_SUCCESS, 'Categoria modificada a favoritos exitosamente');        
        }else{
            Session::flash(NOTIFICACION_ERROR, 'Categoria  no modificada');
        }
        return redirect("/Table/2");

    }
    
    public function lstCategorias(){
        $tableMod = new Table;
        $tableMod->setColum($tableMod,
            [
                NOMBRE=>TN_CAT_PRO_NOMBRE,
                DESCRIPCION=>TN_CAT_PRO_DESCRIPCION,
                TIPO=>TN_CAT_PRO_TIPO
            ]);
        
        $tableMod->setItems($tableMod,$this->gstCategorias->getAllCategorias()->toArray());
        $tableMod = $tableMod->dps($tableMod);
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_MODULOS=>URL::previous(),LST_MODULOS=>""]; 
        $_SESSION[DATA][CLIENTE_TITLE] = MSG_CAT_PRO;
        $_SESSION[DATA][CAT_PRO_OPCFUNCION] = [MSG_REGISTRAR_CAT=>PATH_URL_CAT_PRO_CREATE]; 
        return view(PATH_LIST,[TABLE=>$tableMod]);
    }
    public function create(){
        $_SESSION[DATA][CLIENTE_TITLE] = MSG_CREAR_PRO;
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PRODUCTOS=>'/Menu/15',CREAR_PRODUCTOS=>""]; 
        $atrShopCart = $this->gstProductos->getModuloAtributte('mod_nombre','Tienda');
        
        if (count($atrShopCart) > 0)
        {
          $permisoShopCart = PermisosController::checkPermisosModulos($atrShopCart[0]->id);
          
          $data[TN_PRO_IMG] ="";
          $campos = $this->gstCampos->getCamposByAtributte('razon','Pro');
          $cats = $this->gstCategorias->getCategoriasByAtribute("cliente_id",Auth::user()->cliente_id);      
          return view('Modulos.Productos.crearProducto',[DATA =>'','campos'=>$campos,'cats'=>$cats,'permisoShopCart'=>$permisoShopCart,'libMod'=>['Productos'],SFT_LIB=>[SFT_SELECT2]]);
      }

        //return view(PATH_VIEW_IMG_DATA,compact(FORM,DATA,FORM_ID));
  }
  public function createProccess()
  {
    if(isset($_POST['pro_nombre']) && isset($_POST['pro_descripcion']) && isset($_POST['pro_precio_interno']) && isset($_POST['pro_precio_venta']) && isset($_POST['cat_id']))
    {
        if(strlen($_POST['pro_nombre']) > 200 or strlen($_POST['pro_descripcion']) > 200 or strlen($_POST['pro_precio_interno']) > 200 or strlen($_POST['pro_precio_venta']) > 200)
        {
            $_SESSION[DATA][MSG] = MSG_ERROR_PRO_CARACTERS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Productos/create');

        }else
        $valores = explode(",", $_POST['valorPro']);
        $carat = explode(",", $_POST['caractPro']);
        $_POST[TN_PRODUCTO_IMG]= FuncionesGenerales::cargararchivo($_FILES[CAT_PRO_FILE_UPLOAD],PATH_PRO_IMG,IMG_PRO_DEFAULT);
        unset($_POST['undefined']);unset($_POST[TOKEN]);unset($_POST['valorPro']);unset($_POST['caractPro']);
        $_POST[TN_CONF_ID_CLIENTE] = Auth::user()->cliente_id;
        $_POST[TN_USUARIO_ID] = Auth::user()->id;
        $_POST['estado_producto'] = 'Activo';
        if(!isset($_POST['en_venta']))
        {
            $_POST['en_venta'] = "Inactivo";
        }
        $producto = FuncionesDB::registrarGetRegistro(new Productos,$_POST);
        $cont = COUNT($carat);
        if($cont > 0){
            for ($i=0; $i < $cont; $i++) { 
                $data['campo_id'] = $carat[$i];
                $data['valor'] = $valores[$i];
                $data['id_pro'] = $producto->id;
                $response = FuncionesDB::registrar(new CaracteristicasProducto,$data);
            }
        }else{
            $response = 1;
        }
        if($response != 0 ){
            $_SESSION[DATA][MSG] = MSG_PRO_CORRECTO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;    
        }else{
            $_SESSION[DATA][MSG] = 'Fallo al registrar, intentelo denuevo';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;    
        }
        return redirect('Productos/lst');
    }else{             
       $_SESSION[DATA][MSG] = "Error faltan datos ";
       $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
       return redirect('Productos/create');
   }
}
public function bloquesProductos(){
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_MODULOS=>URL::previous(),LST_MODULOS=>""]; 
    $_SESSION[DATA][CLIENTE_TITLE] = SFT_MOD_PRODUCTOS;
    $_SESSION[DATA][CAT_PRO_OPCFUNCION] = [MSG_CATEGORIA_REGISTRAR=>PATH_URL_CAT_PRO_CREATE]; 
    $productos = $this->gstProductos->getProductoByAtributo('cliente_id',Auth::user()->cliente_id);
    $categorias= $this->gstCategorias->getCategoriasByAtribute('cliente_id',Auth::user()->cliente_id);
    return view(PATH_CAT_PRO_BLOQUE,compact(VAR_PRODUCTOS,'categorias'));
}
public function lstProductos(){
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PRODUCTOS=>'/Menu/15','Listar Productos'=>"#"];

    $tableMod = new Table;
    $tableMod->setColum($tableMod,
        [
            'id'=>'id',
            MSG_PRO_NOMBRE=>TN_PRO_NOMBRE,
            MSG_CATEGORIA=>'cat_nombre',
            DESCRIPCION=>TN_PRO_DESCRIPCION,
            MSG_VALOR_INTERNO=>TN_PRO_PRECIO_INTERNO,
            MSG_VALOR_VENTA=>TN_PRO_PRECIO_VENTA,
            FECHA=>CREATED_AT,
            'Estado Producto'=>'estado_producto',
            'html-Foto'=>['<img src="/-img_producto" width="100em">','img_producto']
        ]);
    $opciones = 
    [
        'val-Cambiar Estado'=> 'Productos/estadoProducto',
        'Editar'=> 'Productos/editarProductos',
        'val-Eliminar'=> 'Productos/eliminarProducto'
    ];
    $modulo = FuncionesGenerales::getByAtribbute(new Modulos,'mod_nombre','Promociones');
    if(count($modulo) > 0){
        $permiso = PermisosController::checkPermisosModulos($modulo[0]->id);
        if($permiso){
            $opciones['Promocionar'] = 'Promociones/crearPromociones';
        }
    }


    $tableMod->setItems($tableMod,$productos = $this->gstProductos->getCategoriaJoin(Auth::user()->cliente_id)->toArray());
    $tableMod = $tableMod->dps($tableMod);

    $_SESSION[DATA][CLIENTE_TITLE] = MSG_PRO_GESTION;
    $_SESSION[DATA][CAT_PRO_OPCFUNCION] = [MSG_PRO_REGISTRAR=>PATH_PRO_CREATE]; 

    return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableMod,'opc'=>json_encode($opciones)]);
}
     /*
    *Autor: Javier R
    *Modulo: Productos
    *Versiòn: 1.0
    *Entrada:
    *Salida:
    *Descripciòn: Cambiar estado del producto (Activo-Inactivo)
    */
     public function listCaracterisitcas(){


        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PRODUCTOS=>'/Menu/15','Listar Productos'=>"/Productos/lst","Características"=>""];
        $producto = $this->gstProductos->getProductoByAtributo('id',$_GET['id']);
        $tableMod = new Table;
        $tableMod->setColum($tableMod,
            [
                'id'=>'id',
                'Característica'=>'campo_nombre',
                'Valor'=>'valor'
            ]);
        $opciones = 
        [
            'val-Eliminar'=> 'Productos/eliminarCaracteristica'
        ];
        $data = $this->gstProductos->getCaracteristicasProducto($_GET['id']);
        $orderData = FuncionesGenerales::listaOrderDataJoin($this->gstProductos->getNewCarateristicaPro(),$data,[]);
        $tableMod->setItems($tableMod,$productos =$orderData);
        $tableMod = $tableMod->dps($tableMod);
        $_SESSION[DATA][CLIENTE_TITLE] = 'Caracteristicas: '.$producto[0]->pro_nombre;
        return view(PATH_LIST,[TABLE=>$tableMod,'opc'=>json_encode($opciones)]);
    }
    public function eliminarCaracteristica(){

        if (isset($_GET['id'])) {

            $pru=$this->gstProductos->deleteCarPro($_GET['id']);
            if($pru){

                $_SESSION[DATA][MSG] = MSG_ELI_CARAC;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
                
            }
        }
        return redirect(redirect()->getUrlGenerator()->previous());
    }
     /*
    *Autor: Javier R
    *Modulo: Productos
    *Versiòn: 1.0
    *Entrada:
    *Salida:
    *Descripciòn: Cambiar estado del producto (Activo-Inactivo)
    */
     public function estadoProducto()
     {
        $res =$this->gstProductos->cambioEstado($_GET['id'],'estado_producto');
        if($res)
        {
            $_SESSION[DATA][MSG] = 'Cambio de estado exitoso';
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            return redirect('Productos/lst');   
        }else
        {
            $_SESSION[DATA][MSG] = 'Error al Cambiar de estado';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Productos/lst');   
        }
        

    }
    /*
    *Autor: Javier R
    *Modulo: Productos
    *Versiòn: 1.0
    *Entrada:
    *Salida:
    *Descripciòn: Vista del editar Productos con su respectivos datos
    */
    public function editarProductos()
    {
        if(isset($_GET['id']))
        {
            if (filter_var($_GET['id'], FILTER_VALIDATE_INT))
            {
                $consulta = $this->gstProductos->consultaProducto($_GET['id']);
                if($consulta != null)
                {
                    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PRODUCTOS=>'/Menu/15','Listar Productos'=>'/Productos/lst','Editar Producto'=>"#"];
                    $producto = $this->gstProductos->consultaProducto($_GET['id']);
                    $data[TN_PRO_IMG] =$producto['img_producto'];
                    $categorias = $this->gstCategorias->getCategoriasByAtribute("cliente_id",Auth::user()->cliente_id);
                    $campos = $this->gstCampos->getCamposByAtributte('razon','Pro');
                    $datos = $this->gstProductos->getCaracteristicasProducto($_GET['id']);
                    $caracteristicasPro =  json_encode(FuncionesGenerales::listaOrderDataJoin($this->gstProductos->getNewCarateristicaPro(),$datos,[]));

                    $atrShopCart = $this->gstProductos->getModuloAtributte('mod_nombre','Tienda');

                    if (count($atrShopCart) > 0)
                    {
                      $permisoShopCart = PermisosController::checkPermisosModulos($atrShopCart[0]->id);
                  }
                  return view('Modulos.Productos.editarProducto',['data'=>$data,'campos'=>$campos,'caracteristicasPro'=>$caracteristicasPro,'producto'=>$producto,'categorias'=>$categorias,'permisoShopCart'=>$permisoShopCart,'libMod'=>['Productos'],SFT_LIB=>[SFT_SELECT2]]);       


              }else{
                $_SESSION[DATA][MSG] = MSG_ERROR_PRO_ID_NUll_DB;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Productos/lst');
            }
        }else{$_SESSION[DATA][MSG] = MSG_ERROR_PRO_INT_NULL;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Productos/lst');
        }
    }else{$_SESSION[DATA][MSG] = 'Error no existe un identificador';
    $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
    return redirect('Productos/lst');
}
}
    /*
    *Autor: Javier R
    *Modulo: Productos
    *Versiòn: 1.0
    *Entrada:
    *Salida:
    *Descripciòn Edita los productos 
    */

    public function editarProccess()
    {
        if(isset($_POST['pro_nombre']) && isset($_POST['pro_descripcion']) && isset($_POST['pro_precio_interno']) && isset($_POST['pro_precio_venta']) && isset($_POST['cat_id']))
        {
            if(isset($_POST['id']))
            {  
                if (filter_var($_POST['id'], FILTER_VALIDATE_INT))
                {
                    $consulta = $this->gstProductos->consultaProducto($_POST['id']);
                    if($consulta != null)
                    {
                        if(strlen($_POST['pro_nombre']) > 200 or strlen($_POST['pro_descripcion']) > 200 or strlen($_POST['pro_precio_interno']) > 200 or strlen($_POST['pro_precio_venta']) > 200)
                        {

                            $_SESSION[DATA][MSG] = MSG_ERROR_PRO_CARACTERS;
                            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                            return redirect('Productos/lst');

                        }else
                        unset($_POST['_token'],$_POST['nombre'],$_POST['valor']);
                        if($_FILES['file-upload']['name'] != ''){
                            $_POST['img_producto'] = FuncionesGenerales::cargararchivo($_FILES['file-upload'],PATH_PRO_IMG,'default.jpg');
                        }
                        if($_POST['caractPro'] == ""){
                            unset($_POST['caractPro']);unset($_POST['valorPro']);
                        }
                        if(!isset($_POST['caractPro'])){
                            $this->gstProductos->deleteCatPro($_POST['id']);
                            $cont = 0;
                        }else{
                            $stringCarat = $_POST['caractPro'];unset($_POST['caractPro']);
                            $stringValores = $_POST['valorPro'];unset($_POST['valorPro']);
                            $carat = explode(',', $stringCarat);
                            $valores = explode(',', $stringValores);
                            $cont = count($carat);
                        }
                        (!isset($_POST['en_venta'])) ? $_POST['en_venta'] = "Inactivo":"";

                        $response = $this->gstProductos->updateProducto($_POST['id'],$_POST);
                        if($cont > 0){
                            $this->gstProductos->deleteCatPro($_POST['id']);
                            for ($i=0; $i < $cont; $i++) { 
                                $data['campo_id'] = $carat[$i];
                                $data['valor'] = $valores[$i];
                                $data['id_pro'] = $_POST['id'];
                                $response = FuncionesDB::registrar(new CaracteristicasProducto,$data);
                            }
                        }else{
                            $response = 1;
                        }
                        if($response)
                        {
                            $_SESSION[DATA][MSG] = 'Se edito correctamente el producto';
                            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
                        }else
                        {
                            $_SESSION[DATA][MSG] = MSG_ERROR;
                            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                        }
                        return redirect('Productos/lst');
                    }else{
                        $_SESSION[DATA][MSG] = MSG_ERROR_PRO_ID_NUll_DB;
                        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                        return redirect('Productos/lst');
                    }
                }else{
                    $_SESSION[DATA][MSG] = MSG_ERROR_PRO_INT_NULL;
                    $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                    return redirect('Productos/lst');
                }
            }else{
                $_SESSION[DATA][MSG] = MSG_ERROR_PRO_ID_NUll;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
                return redirect('Productos/lst');
            }
        }else{
            $_SESSION[DATA][MSG] = "Error faltan datos ";
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Productos/lst');
        }             
    }
    /*
    *Autor: Javier R
    *Modulo: Productos
    *Versiòn: 1.0
    *Entrada:
    *Salida:
    *Descripciòn Elimina Productos de la BD
    */
    public function eliminarProducto()
    {
        $r = $this->gstProductos->deleteProduct($_GET['id']);
        if($r)
        {
            $_SESSION[DATA][MSG] = 'Se elimino correctamente el producto';
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            return redirect('Productos/lst');    
        }else{
            $_SESSION[DATA][MSG] = 'No se elimino el producto';
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Productos/lst');
        }            
    }
    

    public function crearCategoria()
    {
        $_SESSION[DATA][CLIENTE_TITLE] = 'Registrar categoría';
        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PRODUCTOS=>'/Menu/15','Registrar categoría'=>"#"];
        return view('Modulos.Productos.crearRegistrarCategoria');
    }
    public function crearCatProccess()
    {

        if(strlen($_POST['cat_nombre']) > 200 or strlen($_POST['cat_descripcion']) > 200 or strlen($_POST['cat_tipo']) > 200 )
        {

            $_SESSION[DATA][MSG] = MSG_ERROR_PRO_CARACTERS;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect('Productos/crearCategoria');
        }
        unset($_POST['_token']);
        $_POST['cliente_id'] =Auth::user()->cliente_id;
        $_POST['img']= FuncionesGenerales::cargararchivo($_FILES['img'],'Modulos/Categorias/img/',IMG_PRO_DEFAULT);
        $res = $this->gstCategorias->InsertCat($_POST);
        if($res)
        {
            $_SESSION[DATA][MSG] = 'Se registro la categoria';
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }else
        {

            $_SESSION[DATA][MSG] = MSG_ERROR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        }
        return redirect('Productos/crearCategoria');
    }

    public function organizarCategorias(){

        $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PRODUCTOS=>'/Menu/15',MSG_ORG_CAT=>""];
        $lista=$this->gstCategorias->getAllByClient()->toArray();
        $total=count($lista);
        $x=1;
        for($i=0; $i<$total; $i++){
            $contador[] = $x;
            $x++;
        }
        $response = array('lista','total','contador');
        //dd($total);

        return view(PATH_MOD_ORG_CAT,compact($response));
    }

    public function compararOrdenCategorias(){

        $noValor = false;

        for ($i=1; $i < count($_POST[VAR_ID_ORD])+1; $i++) { 

            if(!in_array($i,$_POST[VAR_ID_ORD])){

              $noValor = true;
            }
        }

      return $noValor;
  }

  public function editarOrdenCategoriaProcess(){

    if(!isset($_POST[VAR_ID_CAT]) or !isset($_POST[VAR_ID_ORD])){
        $_SESSION[DATA][MSG] = MSG_ERROR_ORG_CAT;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect(PATH_URL_ORG_CAT);      
    }

    $x= count($_POST[VAR_ID_CAT]);
    $y= count($_POST[VAR_ID_ORD]);

    if($x == 0 or $y ==0){
        $_SESSION[DATA][MSG] = MSG_ERROR_ORG_CAT;
        $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        return redirect(PATH_URL_ORG_CAT);      
    }

    $reponse = $this->compararOrdenCategorias();     
    if($reponse){
        Session::flash("error",MSG_ERROR_ORG_CAT);
        return redirect(PATH_URL_ORG_CAT);
    }
    if($x == $y){
        for($i=0; $i<$x; $i++){
          $respuesta = $this->gstCategorias->insertarOrden($_POST[VAR_ID_CAT][$i],$_POST[VAR_ID_ORD][$i]);
        } 
        if ($respuesta){
          $_SESSION[DATA][MSG] = MSG_SUCC_ORG_CAT;
          $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        }else{
          $_SESSION[DATA][MSG] = MSG_ERROR_ORG_CAT;
          $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
        }
    } 

    return redirect(PATH_URL_ORG_CAT);
    }

    public function listarPadres(){
        $padres = $this->gstCategorias->getAllPadres2();
        return json_encode($padres);
    }
    public function identificarPadre(){

        $myPadre = $this->gstCategorias->getMyFatherId($_GET['id']);
        $myPadre = $this->gstCategorias->getMyFatherName($myPadre);
        if($myPadre!=null){
            $name = "Padre : ". $myPadre[0]['cat_nombre'];
            return json_encode($name);
        }else{
            
            $myPadre="Esta categoria es Padre";
            return json_encode($myPadre);    
        }
        
    }
    public function identificarHijos(){
        $hijos = $this->gstCategorias->getHijos($_GET['id']);
        return json_encode($hijos);
    }

    public function getIconos()
    {
        $iconos = $this->gstCategorias->getIconos();
        return json_encode($iconos);
    }
    public function datosProductos(){

        $cliente = Auth::user()->cliente_id;

        $datos = $this->gstProductos->getAllProductos($cliente);

        return json_encode($datos);
    }
}
