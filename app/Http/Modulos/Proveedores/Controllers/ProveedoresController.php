<?php
namespace App\Http\Modulos\Proveedores\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\Http\Clases\Cards;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\Funciones;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Proveedores;
use App\Http\Modulos\Proveedores\Gst\GstProveedores;
/*
*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: Controllador de Proveedores dentro del sistema
*/
class ProveedoresController extends Controller
{
	private $gstProveedores;

	function __construct(GstProveedores $gstProveedores){
        session_start();

    $this->middleware(MIN_AUTH);
	$this->gstProveedores = $gstProveedores;
    }
    /*
*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: funcion para crear Proveedores dentro del sistema
*/

    public function crearProveedor(){

    	$_SESSION[DATA][CLIENTE_TITLE] = TN_TITLE_PROVEE;
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            'Menu Proveedores'=>"/Menu/19",
            'Registrar Proveedores'=> RUTA_CREAR_PROVEE,
        ];
        return view (RETURN_VIEW_PROVEE_CREAR);
    }
/*
*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: funcion crearProcess para registrar Proveedores dentro del sistema
*/
    public function crearProveedorProcess()
    {
        if(strlen($_POST['nombre_proveedor']) > 200){
            $_SESSION[DATA][MSG] = ERROR_CANT_CARACTERES_PROVEEDORES;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_CREAR_PROVEE);
       }else{

                unset($_POST['_token']);
                if($_FILES['camara_comercio']['name'] != ''){
                $_POST['camara_comercio'] = FuncionesGenerales::cargararchivo($_FILES['camara_comercio'],'Modulos/Proveedores/img','Imagen_no_disponible.svg.png');
                }else{
                    unset($_POST['camara_comercio']);
                }
                if($_FILES['rut']['name'] != ''){
                $_POST['rut'] = FuncionesGenerales::cargararchivo($_FILES['rut'],'Modulos/Proveedores/img','Imagen_no_disponible.svg.png');
                }else{ 
                    unset($_POST['rut']);
                }
            	$_POST['cliente_id'] = Auth::user()->cliente_id;
         		$Proveedores = $this->gstProveedores->registerProveedorProcess($_POST);
                $_SESSION[DATA][MSG] = MSG_REGISTER_PROVEE;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

            }
    	return view(RETURN_VIEW_PROVEE_CREAR,compact('Proveedores'));
        
    }
    
     /*
*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: funcion para listar Proveedores dentro del sistema
*/
   	public function listarProveedores(){
		
       $_SESSION[DATA][CLIENTE_TITLE] = 'Proveedores';
       $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menu Proveedores'=>'/Menu/19','Listar Proveedores'=>"#"];  
        $tableInv = new Table;
        $tableInv->setColum($tableInv,
            [
                ID=>'id',
                NOMBRE_PROVEE=>NAME_PROVEE,
                CEDULA_RESPONS=>CC_RESPONS,
                NAME_NIT=>NIT,
                NOMBRE_RESPONSE=>NAME_RESPONS,
                CAMERA_COMMERCE=>CAMERA_COMERCIO,
                NAME_RUT=>RUT,
                EST=>ESTADO_PROVEE
            ]
        );
        $opciones = 
            [
               VAL_ESTADO=> RUTA_CAMBIAR_EST,
               VAL_EDIT=> RUTA_EDITAR_PROVEE,
               VAL_DELETE_PROVEE=> RUTA_DELETE_PROVEE
            ];
        $data = $this->gstProveedores->getProveedorJoin(Auth::user()->cliente_id)->toArray();
        $tableInv->setItems($tableInv,$data);
        $tableInv = $tableInv->dps($tableInv);
    
        return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
	} 
/*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: funcion para cambiar estado de Proveedores dentro del sistema
*/
    public function cambiarEstadoProveedor()
    {

    $this->gstProveedores->actualizarEstado($_GET['id'],'estado_proveedor');
      $_SESSION[DATA][MSG] = CAMBIO_ESTADO_SUCCESS;
      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
      return redirect(RUTA_LST_PROVEE);

	}
/*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: funcion para editar Proveedores dentro del sistema
*/
	public function editarProveedor(){

        if(!isset($_GET['id'])) 
        {
            $_SESSION[DATA][MSG] = ERROR_NO_ENCONTRADO_PROVEEDOR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_LST_PROVEE);
        }
        if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = ERROR_PROVEE_NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_LST_PROVEE);
        } 
            $consul = $this->gstProveedores->getProveedor($_GET['id']);
        if ($consul == null)
        {
            $_SESSION[DATA][MSG] = ERROR_NO_DB;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_LST_PROVEE);
        }else{

                $_SESSION[DATA][CLIENTE_TITLE] = TN_TITLE_EDIT;
                $_SESSION[DATA][BREAD] = 
                [
                INICIO=>PATH_HOME,
                MENU_PROVEE=>"/Menu/19",
                LST_PROVEE=>RUTA_LST_PROVEE,
                EDIT_PROVEE=>"#"
                ]; 

                $proveedor = Proveedores::find($_GET['id']);

                return view(RETURN_VIEW_PROVEE_EDITAR,compact('proveedor'));
            }
    }  
/*
*Autor: Jasareth Obando
*Modulo: Proveedores
*Versiòn: 1.0
*Descripciòn: funcion editarProcess para editar Proveedores dentro del sistema
*/
    public function editarProveedorProcess(){
        if(!isset($_POST['id'])) 
        {
            $_SESSION[DATA][MSG] = ERROR_NO_ENCONTRADO_PROVEEDOR;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_LST_PROVEE);
        }
        if(!filter_var($_POST['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = ERROR_PROVEE_NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_LST_PROVEE);
        } 
            $consul = $this->gstProveedores->getProveedor($_POST['id']);
        if ($consul == null)
        {
            $_SESSION[DATA][MSG] = ERROR_NO_DB;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(RUTA_LST_PROVEE);
        }else{

        	unset($_POST['_token']);

                if($_FILES['camara_comercio']['name'] != ''){
                $_POST['camara_comercio'] = FuncionesGenerales::cargararchivo($_FILES['camara_comercio'],'Modulos/Proveedores/img/','Imagen_no_disponible.svg.png');
            }else{
                unset($_POST['camara_comercio']);    
            }
                if($_FILES['rut']['name'] != ''){
                $_POST['rut'] = FuncionesGenerales::cargararchivo($_FILES['rut'],'Modulos/Proveedores/img/','Imagen_no_disponible.svg.png');
            }else{
                unset($_POST['rut']);
            }
                $this->gstProveedores->getEditarProveedor($_POST['id'],$_POST);
                $_SESSION[DATA][MSG] = MSG_ACTUALIZADO;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
                return $this->listarProveedores();
            }
    } 

    public function eliminarProveedores()
    {
        $_SESSION[DATA][MSG] = MSG_DELETE_CORRECTAMENTE_PROVEEDOR;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        $this->gstProveedores->deleteProvee($_GET['id']);
        return redirect(RUTA_LST_PROVEE);
    }

}