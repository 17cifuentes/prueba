<?php

namespace App\Http\Modulos\Proveedores\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Proveedores;

/*
	*Autor: Jasareth obando 
	*Modulo: Proveedores
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para los Proveedores
	*/
class GstProveedores
{
	/*
	*Autor: Jasareth obando 
	*Modulo: Proveedores
	*Versiòn: 1.0
	*Descripciòn: funcion  de gestion para  el crear de los Proveedores
	*/
	public function getAllProveedores(){

		return $Proveedores = Proveedores::all();
	}
/*
	*Autor: Jasareth obando 
	*Modulo: Proveedores
	*Versiòn: 1.0
	*Descripciòn: funcion  de gestion para registrar los Proveedores
	*/

	public  function registerProveedorProcess($provee){

		Proveedores::insert($provee);
		$Proveedores = Proveedores::all();
	       
	}

	public static function getProveedor($id)
    {

     	return Proveedores::find($id);	
    }

    public function getProveedorJoin($id){

      return Proveedores::where('tn_sft_proveedores.cliente_id',$id)->get();
    }

     public function getEditarProveedor($data)
     {
     	try{
     		return Proveedores::where("id",$data["id"])->update($data); 
     	}catch(\Exception $v){
     		error_log("Error: ".$v->getMessage());

     		return false;
     	}
     	
     }
	public function deleteProvee($id)
    {
    	Proveedores::where('id',$id)->delete();
    }
    public function actualizarEstado($id,$columna)
    {
    	FuncionesGenerales::cambiarEstadoTable(new Modulos,$id,$columna);
    }


}