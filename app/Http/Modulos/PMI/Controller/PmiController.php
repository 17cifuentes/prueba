<?php

namespace App\Http\Modulos\PMI\Controller;
use App\Http\Controllers\Controller;
use App\Http\Modulos\PMI\Gst\GstPmi;
use App\Http\Gst\GstCliente;
use App\Http\Gst\GstUsuarios;
use Illuminate\Http\Request;
use App\Http\Gst\GstGeneral;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Modulos\PMI\Gst\GstTareasProyecto;
use App\Http\Clases\Table;
use Auth;


class PmiController extends Controller
{

	private $gstUsuarios;
	private $gstPmi;
	private $gstGeneral;
	private $gstTareasProyecto;
	private $funcionesGenerales;
	private $gstCliente;

	public function __construct(GstCliente $gstCliente,FuncionesGenerales $funcionesGenerales,GstUsuarios $gstUsuarios,GstPmi $gstPmi,GstGeneral $gstGeneral, GstTareasProyecto $gstTareasProyecto) 
	{
		$this->middleware(MIN_AUTH);
		session_start();
		$this->gstUsuarios = $gstUsuarios;
		$this->gstPmi = $gstPmi;
        $this->gstGeneral = $gstGeneral;
        $this->gstTareasProyecto = $gstTareasProyecto;
        $this->funcionesGenerales = $funcionesGenerales;
        $this->gstCliente = $gstCliente;

	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion retorna la vista para crear proyecto.
	*/
	public function crearProyecto(){
		
		$cliente=Auth::user()->cliente_id;

		$usuarios=$this->gstUsuarios->getUsers($cliente);

		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PMI=>'/Menu/37',CREAR_PROYECTO=>''];

		return view(PATH_PMI_CREAR_PROYECTO,[SFT_LIB=>[SFT_DATEPICKER]],compact('usuarios','cliente'));
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion que realiza y valida la creacion del proyecto.
	*/
	public function createProccess(Request $request){

		$this->gstGeneral->validateData($request);

		$response=$this->gstPmi->postRegisterPmi($_POST);
		
		if ($response) {

			$_SESSION[DATA][MSG] = MSG_CRE_PRO_SUC;

			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (FUN_LISTAR_PROYECTO);	
		}else{

			$_SESSION[DATA][MSG] = MSG_CRE_PRO_FAIL;

			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (FUN_CRE_PRO);
		}
		

	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion que lista los proyectos en BBDD.
	*/
	public function listarProyecto(){

		$cliente=Auth::user()->cliente_id;
		
		$data = $this->gstPmi->getAllPmiJoin($cliente);
		//dd($data);
		$tableInv = new Table;

		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PMI=>'/Menu/37',LISTAR_PROYECTO=>''];

		$tableInv->setColum($tableInv,
			[
				ID=>'id',
				'Proyecto'=>PMI_NAME,
				'Descripción'=>PMI_DESC,
				'Responsable'=>PMI_ENCARGADO,
				'Fecha inicial'=>PMI_INICIO,
				'Fecha límite'=>PMI_FIN

			]
		);

		$opciones = 
		[
			'Tareas'=> FUN_LIS_TAR_PRO,
			'Editar'=> FUN_EDI_PRO,
			'val-Eliminar'=> FUN_DEL_PRO,
			'Historial'=>FUN_HIS_PRO
		];

		
		
		$tableInv->setItems($tableInv,$data);
		
		$tableInv = $tableInv->dps($tableInv);

		return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion retorna la vista para editar el proyecto en BBDD.
	*/
	public function editarProyecto(){

		

		if (isset($_GET['id'])){

			$proyecto=$this->gstPmi->getDataProject($_GET['id']);

			$cliente=Auth::user()->cliente_id;

			$usuarios=$this->gstUsuarios->getUsers($cliente);
			

			$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PMI=>'/Menu/37',LISTAR_PROYECTO=>"listarProyecto",EDITAR_PROYECTO=>''];

			return view(PATH_PMI_EDITAR_PROYECTO,[SFT_LIB=>[SFT_DATEPICKER]],compact('proyecto','usuarios'));	
		}	
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion que valida y edita el proyectos en BBDD.
	*/
	public function editProccess(Request $request){

		$this->gstGeneral->validateData($request);

		$response = $this->gstPmi->updateDataProject('id',$_POST['id'],$_POST);

		if ($response) {

			$_SESSION[DATA][MSG] = MSG_EDICION_CORRECTA;

			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

			return redirect (FUN_LISTAR_PROYECTO);	

		}else{

			$_SESSION[DATA][MSG] = MSG_EDICION_ERROR;

			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

			return redirect (FUN_LISTAR_PROYECTO);
		}
		
	}

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Funcion elimina el  proyecto en BBDD.
	*/
	public function eliminarProyecto(){

		if (isset($_GET['id'])){
			
			$response = $this->gstPmi->deleteProjectPmi($_GET['id']);
			
			if ($response) {

				$_SESSION[DATA][MSG] = MSG_BORRADO_EXITOSO;

				$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

				return redirect (FUN_LISTAR_PROYECTO);	

			}else{

				$_SESSION[DATA][MSG] = MSG_BORRADO_ERROR;
				$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

				return redirect (FUN_LISTAR_PROYECTO);
			}	
		}
	}


	public function listTareasProyecto(){

		$tableInv = new Table;
		$_SESSION[DATA][CLIENTE_TITLE] = LISTAR_TAREAS_PROYECTO;
		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PMI=>'/Menu/37',LISTAR_PROYECTO=>'listarProyecto',LISTAR_TAREAS_PROYECTO =>''];
		$tableInv->setColum($tableInv,
			[
				ID=>'id',
				'Tarea'=>'nombre_tarea',
				'Prioridad'=>'nombre_estado',
				'Fecha'=>'fecha_tarea',
				'Responsable Tarea'=>PMI_ENCARGADO
			]
		);

		$opciones = 
		[
			"Editar Tarea" => 'PMI/editarTareaProyecto',
			"val- Eliminar Tarea" => 'PMI/eliminarTareaProyecto',
		];

		$data = $this->gstTareasProyecto->ConsultarTareas(Auth::user()->cliente_id,$_GET['id']);
		$tableInv->setItems($tableInv,$data);
		$tableInv = $tableInv->dps($tableInv);
		$proyecto=$this->gstPmi->getDataProject($_GET['id']);
		return view('Modulos.PMI.listTareasProyecto',['proyecto'=>$proyecto,SFT_LIB=>[SFT_DATATABLE],SFT_LIB_MOD=>[SFT_LIB_MOD_PMI],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
	}

	public function asignarTareasProyecto()
	{
		$proyecto=$this->gstPmi->getDataProject($_GET['id']);

		if($proyecto != "" or $proyecto != null)
		{
			$cliente=Auth::user()->cliente_id;
			$usuarios=$this->gstUsuarios->getUsers($cliente);
			$prioridad=$this->gstTareasProyecto->estadosRazon('PMI-Estados');

			$datos = [];
			$datos['proyecto'] = $proyecto;
			$datos['usuarios'] = $usuarios;
			$datos['prioridad'] = $prioridad;
			return json_encode($datos);
			
		}
		return null;
	}

	public function asignarTareasProyectoProcess()
	{
		$_GET['id_proyecto'] = $_GET['proyecto_id'];
		unset($_GET['proyecto_id']);
		unset($_GET['pmi_nombre']);
		unset($_GET['fec_inicio']);
		unset($_GET['fec_fin']);
		$_GET['cliente_id'] = Auth::user()->cliente_id;
		$respuesta1 = $this->gstTareasProyecto->validarDatosTarea($_GET);
		$respuesta2 = $this->gstTareasProyecto->validarCaracteres($_GET);
		if($respuesta1 == true && $respuesta2 == true )
		{
			$resultado = $this->gstTareasProyecto->insertarTarea($_GET);
			if($resultado == true)
			{
				return json_decode(true);
			}else{
				return json_decode(false);
			}

		}else{
			return json_decode(false);
		}
	}
	public function editarTareaProyecto()
	{
		if(!isset($_GET['id'])){
			return redirect('PMI/listarProyecto');
		}
		$tarea =$this->gstTareasProyecto->consultarDatosProyectoTarea($_GET['id']);
		$proyecto=$this->gstPmi->getDataProject($tarea[0]['id_proyecto']);
		$usuarios=$this->gstUsuarios->getUsers(Auth::user()->cliente_id);
		$prioridad=$this->gstTareasProyecto->estadosRazon('PMI-Estados');
		$_SESSION[DATA][CLIENTE_TITLE] = 'Editar Tarea Proyecto';
		$_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú PMI'=>'/Menu/37',LISTAR_PROYECTO=>'listarProyecto','Listar Tareas'=>'listTareasProyecto?id='.$tarea[0]['id_proyecto'],'Editar Tarea Proyecto'=>'#'];

		return view('Modulos.PMI.editarTareaProyecto',compact('tarea','proyecto','usuarios','prioridad'));

	}
	public function editarTareaProyectoProccess()
	{
		unset($_POST['_token']);
		$validadatos= $this->gstTareasProyecto->validarDatosEditarTareaProyecto($_POST);
		
		if($validadatos != false){
			$validar= $this->gstTareasProyecto->validarDatosVaciosEditarTareaProyecto($_POST);
			if($validar !=false )
			{
				$valida= $this->gstTareasProyecto->validarDatosCaracteresEditarTareaProyecto($_POST);
				if($valida != false){
					$respuestaServidor = $this->gstTareasProyecto->updateTareaProyecto($_POST['id_tarea'],$_POST);
					if($respuestaServidor){
						$_SESSION[DATA][MSG] = 'Se edito correctamente la tarea';
						$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
						return redirect('PMI/listTareasProyecto?id='.$_POST['proyecto_id']);
					}else{
						$_SESSION[DATA][MSG] = ERROR_PMI_PROCCESS;
						$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
						return redirect('PMI/listTareasProyecto?id='.$_POST['proyecto_id']);
					}
				}else{
					$_SESSION[DATA][MSG] = ERROR_PMI_PROCCESS;
					$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
					return redirect('PMI/listTareasProyecto?id='.$_POST['proyecto_id']);
				}
			}else{
				$_SESSION[DATA][MSG] = ERROR_PMI_PROCCESS;
				$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
				return redirect('PMI/listTareasProyecto?id='.$_POST['proyecto_id']);
			}
		}else{
			$_SESSION[DATA][MSG] = ERROR_PMI_PROCCESS;
			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
			return redirect('PMI/listTareasProyecto');
		}
	}
	public function eliminarTareaProyecto(){
		if(!isset($_GET['id'])){
			$_SESSION[DATA][MSG] = ERROR_PMI_PROCCESS;
			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
			return redirect('PMI/listTareasProyecto');
		}
		$tarea =$this->gstTareasProyecto->consultarDatosProyectoTarea($_GET['id']);
		$proyecto=$this->gstPmi->getDataProject($tarea[0]['id_proyecto']);
		$respuesta = $this->gstTareasProyecto->deleteTareaProyect($_GET['id']);
		if($respuesta){
			$_SESSION[DATA][MSG] = 'Se elimino la tarea correctamente';
			$_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
			return redirect('PMI/listTareasProyecto?id='.$proyecto['id']);
		}else{
			$_SESSION[DATA][MSG] = ERROR_PMI_PROCCESS;
			$_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
			return redirect('PMI/listTareasProyecto?id='.$proyecto['id']);
		}
		

	}

/**
     * Metodo para listar los proyectos que tiene asigandos ese usuario(responsable).
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
     */

  public function misProyectos(){

    $tableInv = new Table;
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,MENU_PMI=>'/Menu/37',MIS_PROYECTOS=>''];
    $tableInv->setColum($tableInv,
      [
        ID=>'id',
        'Proyecto'=>PMI_NAME,
        'Descripción'=>PMI_DESC,
        'Fecha inicial'=>PMI_INICIO,
        'Fecha límite'=>PMI_FIN,
        'Cliente'=>PMI_CLIENTE

      ]
    );
      $opciones = 
      [
        'Tareas'=> FUN_LST_MIS_TAREAS
      ];

    $cliente=Auth::user()->cliente_id;
    $data = $this->gstPmi->misTareas($cliente);
    $tableInv->setItems($tableInv,$data);

    $tableInv = $tableInv->dps($tableInv);

    return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
  }

  /**
     * Metodo para listar todas las tareas que tiene asigandas ese proyecto del usuario (responsable).
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
     */

  public function listTareasMisProyectos(){

    $tableInv = new Table;
    $_SESSION[DATA][CLIENTE_TITLE] = 
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú PMI'=>'/Menu/37',MIS_PROYECTOS=>'misProyectos','Listado mis tareas'=>''];
    $tableInv->setColum($tableInv,
      [
        ID=>'id',
        'Tarea'=>PMI_N_TAREA,
        'Prioridad'=>PMI_N_ESTADO,
        'Fecha'=>PMI_FECHA,
        'Responsable Tarea'=> PMI_ENCARGADO,
        'Finalizado'=>PMI_FINALIZAR,
        'Desasignado'=>PMI_DESASIGNAR
      ]
    );

    $opciones = 
    [
      "Finalizar" => FUN_LST_FINALIZAR,
      "Desasignar" => FUN_LST_DESASIGNAR
    ];

    $data = $this->gstTareasProyecto->ConsultarTareas(Auth::user()->cliente_id,$_GET['id']);
    
    $tableInv->setItems($tableInv,$data);
    $tableInv = $tableInv->dps($tableInv);
    $proyecto=$this->gstPmi->getDataProject($_GET['id']);
    return view('Modulos.PMI.listTareasMisProyectos',[SFT_LIB=>[SFT_DATATABLE],SFT_LIB_MOD=>[SFT_LIB_MOD_PMI],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
  }

  /**
     * Función para pintar formulario de desasignar tarea.
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
     */

  public function desasignarTareaMisProyectos()
  {
    if(!isset($_GET['id'])){
      return redirect(FUN_LST_MIS_PROYECTOS);
    }
    $tarea =$this->gstTareasProyecto->consultarDatosProyectoTarea($_GET['id']);
    $proyecto=$this->gstPmi->getDataProject($tarea[0]['id_proyecto']);
    $usuarios=$this->gstUsuarios->getUsers(Auth::user()->cliente_id);
    $prioridad=$this->gstTareasProyecto->estadosRazon('PMI-Estados');
    $_SESSION[DATA][CLIENTE_TITLE] = 'Desasignar tarea proyecto';
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú PMI'=>'/Menu/37',MIS_PROYECTOS=>'misProyectos','Listado mis tareas'=>'listTareasMisProyectos?id='.$tarea[0]['id_proyecto'],'Desasignar tarea proyecto'=>'#'];

    return view('Modulos.PMI.desasignarTareaMisProyectos',compact('tarea','proyecto','usuarios','prioridad'));
  }

  /**
     * Función process para desasignar una tarea.
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
     */

  public function desasignarTareaProyectoProccess(Request $request)
  {
    if(!isset($_GET['id'])){
      return redirect(FUN_LST_MIS_PROYECTOS);
    }
    $this->gstGeneral->validateData($request);
    $tarea =$this->gstTareasProyecto->consultarDatosProyectoTarea($_GET['id']);
    $proyecto=$this->gstPmi->getDataProject($tarea[0]['id_proyecto']);
    $cliente=Auth::user()->cliente_id;
    $usu=$this->gstUsuarios->getUsers($cliente)->toarray();
    
    $response=$this->gstPmi->desasignarTarea($_GET['id'],$_GET['desasignar']);
    if ($response) {

      $_SESSION[DATA][MSG] = MSG_TAREA_DESASIGNADA;

      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

      return redirect (FUN_LST_MIS_PROYECTOS);  
    }else{

      $_SESSION[DATA][MSG] = MSG_TAREA_NO_DESASIGNADA;

      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

      return redirect (FUN_LST_MIS_PROYECTOS);
    }
  }

  /**
     * Función para pintar formulario al finalizar una tarea.
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
     */

  public function finalizarTarea()
  {
    $tarea =$this->gstTareasProyecto->consultarDatosProyectoTarea($_GET['id']);
    $_SESSION[DATA][CLIENTE_TITLE] = 'Finalizar tarea';
    $_SESSION[DATA][BREAD] = [INICIO=>PATH_HOME,'Menú PMI'=>'/Menu/37',MIS_PROYECTOS=>'misProyectos','Listado mis tareas'=>'listTareasMisProyectos?id='.$tarea[0]['id_proyecto'],'Finalizar tarea'=>'#'];
    $proyecto=$this->gstPmi->getDataProject($tarea[0]['id_proyecto'])->toarray();
    $cliente=Auth::user()->cliente_id;
    $usu=$this->gstUsuarios->getUsers($cliente)->toarray();
  
    return view('Modulos.PMI.finalizarTarea',compact('usu','cliente'));
  }

  /**
     * Función process para registrar la finalización de una tarea.
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
     */
  
  public function finalizarTareaProccess(Request $request)
  {
    if(!isset($_GET['id'])){
      return redirect(FUN_LST_MIS_PROYECTOS);
    }
    $this->gstGeneral->validateData($request);
    $tarea =$this->gstTareasProyecto->consultarDatosProyectoTarea($_GET['id']);
    $proyecto=$this->gstPmi->getDataProject($tarea[0]['id_proyecto']);
    $cliente=Auth::user()->cliente_id;
    $usu=$this->gstUsuarios->getUsers($cliente)->toarray();
    
    $response=$this->gstPmi->finalizacionTarea($_GET['id'],$_GET['finalizar']);
    if ($response) {

      $_SESSION[DATA][MSG] = MSG_TAREA_FINALIZADA;

      $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

      return redirect (FUN_LST_MIS_PROYECTOS);  
    }else{

      $_SESSION[DATA][MSG] = MSG_TAREA_NO_FINALIZADA;

      $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;

      return redirect (FUN_LST_MIS_PROYECTOS);
    }
  }


	/**
     * Funcion para descargar el historial de un proyecto en PDF
     * @category PmiController
     * @package Modulo PMI
     * @version 1.0
     * @author Cristian Campo
     */
	public function historial(){

		$cliente = Auth::user()->cliente_id;

		$id = $_GET['id'];

		$project = $this->gstPmi->getDataProject($id,$cliente);

		$manager = $this->gstUsuarios->getUser($project[0]['responsable'])->toArray();

		$clienteDatos = $this->gstCliente->getCliente($cliente)->toArray();

		$tareas = $this->gstTareasProyecto->ConsultarTareas($cliente,$id)->toArray();

		$data = array('project','manager','tareas','clienteDatos');

		return $this->funcionesGenerales->getPDF(view(PATH_PMI_HISTORIAL_PROYECTO,compact($data)));
	}
}



