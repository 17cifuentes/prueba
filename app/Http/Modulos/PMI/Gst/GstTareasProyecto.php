<?php

namespace App\Http\Modulos\PMI\Gst;

use Illuminate\Http\Request;
use App\Tareas_Proyecto;
use App\Http\Gst\GstGeneral;

class GstTareasProyecto {

	public function ConsultarTareas($cliente_id,$proyecto)
	{
		return Tareas_Proyecto::
		select('tn_sft_mod_proyecto_tareas.id as id','nombre_tarea','nombre_estado','finalizar','desasignar','tn_sft_mod_proyecto_tareas.created_at as fecha_tarea','name')
		->where('tn_sft_mod_proyecto_tareas.cliente_id',$cliente_id)->where('id_proyecto',$proyecto)
		->join("tn_sft_estado","tn_sft_estado.id","tn_sft_mod_proyecto_tareas.prioridad")
		->join("users","users.id","tn_sft_mod_proyecto_tareas.responsable_tarea")
		->get();
	}

	public function estadosRazon($razon)
	{
		$gstGeneral = new GstGeneral;
		return $gstGeneral->getAllEstadosRazon($razon);
	}
	public function validarDatosTarea()
	{

		if($_GET['nombre_tarea'] == null or $_GET['nombre_tarea'] == "" || $_GET['prioridad'] == null or  $_GET['prioridad'] == "" || $_GET['tarea_descripcion'] == null or  $_GET['tarea_descripcion'] == "" || $_GET['responsable_tarea'] == null or  $_GET['responsable_tarea'] == "" || $_GET['id_proyecto'] == null or  $_GET['id_proyecto'] == "" || $_GET['cliente_id'] == null or  $_GET['cliente_id'] == "" )
		{
			return false;
		}else{
			return true;
		}

		
	}

	public function validarCaracteres($_post)
	{
		if(strlen($_post['nombre_tarea'])>250 or strlen($_post['tarea_descripcion'])>250){
			return false;
		}else{
			return true;
		}
	}

	public function insertarTarea($data)
	{
		try {
			return Tareas_Proyecto::insert($data);	
			
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
		
	}
	public function consultarDatosProyectoTarea($id_tarea)
	{
		return Tareas_Proyecto::where('id',$id_tarea)->get();
	}
	public function validarDatosEditarTareaProyecto($data)
	{
		if(isset($data['proyecto_id']) or isset($data['nombre_tarea']) or isset($data['prioridad']) or isset($data['tarea_descripcion']) or isset($data['responsable_tarea']) or isset($data['id_tarea']) )
		{

			return true;
		} else{
			return false;
		}
	}
	public function validarDatosVaciosEditarTareaProyecto($data)
	{
		if($data['proyecto_id'] == "" or $data['proyecto_id'] == null or $data['nombre_tarea'] == "" or $data['nombre_tarea'] == null or $data['prioridad'] == "" or $data['prioridad'] == null or $data['tarea_descripcion'] == "" or $data['tarea_descripcion'] == null or $data['responsable_tarea'] == "" or $data['responsable_tarea'] == null)
		{
			return false;
		}else{
			return true;
		}

	}
	public function validarDatosCaracteresEditarTareaProyecto($data)
	{
		if(strlen($data['nombre_tarea'])>250 or strlen($data['tarea_descripcion'])>250)
		{
			return false;
		}else{
			return true;
		}
	}
	public function updateTareaProyecto($id_tarea,$data)
	{
		unset($data['id_tarea']);
		unset($data['proyecto_id']);
		try {
			return Tareas_Proyecto::where('id',$id_tarea)->update($data);
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
	}
	public function deleteTareaProyect($id_tarea)
	{
		try {
			return Tareas_Proyecto::where('id',$id_tarea)->delete();
		} catch (\Exception $e) {
			error_log($e);
			return false;
		}
	}

}