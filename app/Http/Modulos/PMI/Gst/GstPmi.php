<?php

namespace App\Http\Modulos\PMI\Gst;


use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Pmi;
use App\Tareas_Proyecto;

class GstPmi {


    /*
    *Autor: Cristian Campo
    *Modulo: Pmi
    *Versiòn: 1.0
    *Descripciòn: Inserta los datos del proyecto en la BBDD.
    */
	public function postRegisterPmi($post){

		unset($post['_token']);

        try{

            return FuncionesDB::registrar(new Pmi,$post);

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    /*
    *Autor: Cristian Campo
    *Modulo: Pmi
    *Versiòn: 1.0
    *Descripciòn: Regresa todos los proyectos con sus datos a una vista.
    */
    public function getAllPmi($idCliente){

        try{

            return Pmi::where("cliente",$idCliente)->get();

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    /*
    *Autor: Cristian Campo
    *Modulo: Pmi
    *Versiòn: 1.0
    *Descripciòn: Regresa todos los datos de una relacion entre Pmi y Users.
    */
    public function getAllPmiJoin($idCliente){

        $data = $this->getAllPmi($idCliente);
        
        if ($data) {

            try{
                                    
                return FuncionesGenerales::listaOrderDataJoin(new Pmi,$data,[]);

            }catch(Exception $e){

                error_log("Error : ".$e->getMessage());

                return false;
            }

        }else{

            return redirect ('/PMI/listarProyecto');
        }
    }

    /*
    *Autor: Cristian Campo
    *Modulo: Pmi
    *Versiòn: 1.0
    *Descripciòn: Borra un registro de Pmi por medio del id.
    */
    public function deleteProjectPmi($id){

    	
        try{

            FuncionesDB::eliminar(TABLA_PMI,'id',$id);
            
            return true;

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    /*
    *Autor: Cristian Campo
    *Modulo: Pmi
    *Versiòn: 1.0
    *Descripciòn: Retorna el contenido de un registro en especifico.
    */
    public function getDataProject($id,$cliente=false){

        try{
            if ($cliente!=false) {
                return Pmi::where('id',$id)
                ->where('cliente',$cliente)
                ->get()
                ->toArray();    
            }else{
                
                 return Pmi::findOrFail($id);
            }

        }catch(Exception $e){

            error_log("Error : ".$e->getMessage());

            return false;
        }
    }

    /*
    *Autor: Cristian Campo
    *Modulo: Pmi
    *Versiòn: 1.0
    *Descripciòn: Actualiza los datos de un registro en especifíco.
    */
    public function updateDataProject($columna,$id,$data){
        
        
        try{

            return FuncionesDB::Editar(TABLA_PMI,$columna,$id,$data);
            
        }catch(\Exception $e) {
            error_log("Error : ".$e->getMessage());
            return false;
        }
    }
/**
     * Función para listar las tareas de el usuario(asignadas).
     * @category gstPmi
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
    */
    public function misTareas($cliente)
    {
        try{
            return Pmi::where('cliente',$cliente)->get();
            
        }catch(\Exception $v){

            error_log("Error: ".$v->getMessage());

            return false;
        }
    }
    /**
     * Función para insertar el finalizar de una tarea.
     * @category gstPmi
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
    */
    public function finalizacionTarea($id,$finalizar)
    {
        try{
            return Tareas_Proyecto::where('id',$id)->update(['finalizar'=>$finalizar]);
        }catch(\Exception $v){

            error_log("Error: ".$v->getMessage());

            return false;
        }
    }

    /**
     * Función para insertar el finalizar de una tarea.
     * @category gstPmi
     * @package Modulo PMI
     * @version 1.0
     * @author Jasareth Obando
    */

    public function desasignarTarea($id,$desasignar)
    {
        try{

            return Tareas_Proyecto::where('id',$id)->update(['desasignar'=>$desasignar]);

        }catch(\Exception $v){

            error_log("Error: ".$v->getMessage());

            return false;
        }

    }

}