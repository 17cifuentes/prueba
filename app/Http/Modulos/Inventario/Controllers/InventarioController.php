<?php
namespace App\Http\Modulos\Inventario\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Clases\Formulario;
use App\Http\Clases\Cards;
use App\Http\Clases\FuncionesGenerales;
use App\Http\Clases\FuncionesDB;
use URL;
use Auth;
use App\Http\Clases\Table;
use App\Inventario;
use App\Http\Modulos\Inventario\Gst\GstInventario;
use App\Http\Modulos\Productos\Gst\GstProductos;
use App\Http\Gst\GstUsuarios;
/*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: Controllador de Inventario dentro del sistema
*/
class InventarioController extends Controller
{
	private $gstInventario;
    private $gstProductos;
    private $gstUsuarios;
	function __construct(GstInventario $gstInventario,GstProductos $gstProductos, GstUsuarios $gstUsuarios){
        session_start();
        $this->middleware(MIN_AUTH);
		$this->gstInventario = $gstInventario;
        $this->gstProductos = $gstProductos;
        $this->gstUsuarios = $gstUsuarios;
	}
/*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: funcion para listar Inventario dentro del sistema
*/
	public function listarInventario(){
		//PermisosController::getPermisos();
		
       $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Inventario';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            MENU_INV=>"/Menu/25",
            LST_INV=>"/Inventario/listarInventario"
        ];  
        $tableInv = new Table;
        $tableInv->setColum($tableInv,
            [
                ID=>'id',
                NOMBRE_PROD=>TN_PRO_NOMBRE,
                CANT_DISPONIBLE=>TN_CANTIDAD,
                NIVEL_STOCK=>TN_NIVEL_STOCK
            ]
        );
        $opciones = 
            [
                VAL_ABASTECER=> PATH_CTRL_BASTECER,
                VAL_DAR_BAJA=> PATH_CTRL_BAJA,
                HISTORIAL=> PATH_CTRL_HISTORIAL

            ];
        $data = $this->gstInventario->getInventarioJoin(Auth::user()->cliente_id)->toArray();
        

        $tableInv->setItems($tableInv,$data);
       
        
        $tableInv = $tableInv->dps($tableInv);
    
         return view(PATH_LIST,[SFT_LIB=>[SFT_DATATABLE],TABLE=>$tableInv,'opc'=>json_encode($opciones)]);
	} 
/*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: funcion para crear Inventario dentro del sistema
*/

   public function crearInventario()
    {
        $_SESSION[DATA][CLIENTE_TITLE] = 'Registrar Producto';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            MENU_INV=>"/Menu/25",
            'Registrar Producto'=>PATH_CTRL_CREAR,
        ];
        
        $productos = $this->gstProductos->getProductoByAtributo('cliente_id', Auth::user()->cliente_id);
        return view('Modulos.Inventario.crearInventario',compact('productos'));
        
	}
 /*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: funcion para Registrar Inventario dentro del sistema
*/
	public function crearProcess()
    {
        if(strlen($_GET[TN_CANTIDAD]) > 11){
            $_SESSION[DATA][MSG] = ERROR_CANT_CARACTERES;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_CREAR);
        }else
        {

            $_GET['cliente_id'] = Auth::user()->cliente_id;

            $existencia = count($this->gstInventario->getInvForAtribbute('id_producto',$_GET['id_producto']));
            
            if($existencia > 0)
            {
                $_SESSION[DATA][MSG] = ERROR_YA_EXISTE;
                $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            }else{

                $this->gstInventario->getRegisterInvent($_GET);
                
                $_SESSION[DATA][MSG] = REGISTRO_CORRECTAMENTE;
                $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
            }
            return redirect(PATH_CTRL_CREAR);
        }
        
	}
/*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: funcion para Abastecer el Inventario dentro del sistema
*/
    public function abastecerCant()
    {
        if(!isset($_GET['id'])) 
        {
            $_SESSION[DATA][MSG] = ERROR_NO_ENCONTRADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = ERROR_NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        $consulta = $this->gstInventario->getInventarioId($_GET['id']);
        if ($consulta == null)
        {
            $_SESSION[DATA][MSG] = ERROR_NO_BD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        else
        {        
            extract($_GET);
            $_SESSION[DATA][CLIENTE_TITLE] = 'Abastecer';
            $_SESSION[DATA][BREAD] = 
            [
                INICIO=>PATH_HOME,
                MENU_INV=>"/Menu/25",
                LST_INV=>"/Inventario/listarInventario",
                'Abastecer inventario'=>"#"
            ];

            $datos = $this->gstInventario->getInventarioId($_GET['id']);

        return view('Modulos.Inventario.cantidad', compact('datos'));
        }
    }

    /*autor: jasareth obando 
    Modulo: Inventario
    Versión:1.0
    Descripción: modulo de abastecer process para aumentar la cantidad de un producto en el inventario dentro del sistema.
    */  

    public function abastecerCantProccess()
    {
        if(!isset($_GET['id'])) 
        {
            $_SESSION[DATA][MSG] = ERROR_NO_ENCONTRADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = ERROR_NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
         $consulta = $this->gstInventario->getInventarioId($_GET['id']);
        if ($consulta == null)
        {
            $_SESSION[DATA][MSG] = ERROR_NO_BD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        else
        {
            $dato = Inventario::find($_GET['id']);
            
            $total = $dato->cantidad + $_GET[TN_CANTIDAD];
            
            $movimiento = "se abastecio";
            $this->gstInventario->saveHistoricoInventario($_GET['id'],$dato[TN_CANTIDAD],$movimiento,$_GET[TN_CANTIDAD],$total);
           
            $this->gstInventario->abastecerInventario($_GET['id'],$total);
            $_SESSION[DATA][MSG] = ABASTECIDO_CORRECTAMENTE;
            $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;
        
            return redirect(PATH_CTRL_LST);
        }
    }
 /*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: funcion para dar de baja la cantidad del Inventario dentro del sistema
*/
    public function darDeBajaCant()
    {
        if(!isset($_GET['id'])) 
        {
            $_SESSION[DATA][MSG] = ERROR_NO_ENCONTRADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = ERROR_NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        $con = $this->gstInventario->getInventarioId($_GET['id']);
        if ($con == null)
        {
            $_SESSION[DATA][MSG] = ERROR_NO_BD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        else
        {        
        extract($_GET);
        $_SESSION[DATA][CLIENTE_TITLE] = 'Dar de baja';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            MENU_INV=>"/Menu/25",
            LST_INV=>"/Inventario/listarInventario",
            'Dar de baja Inventario'=>"/Inventario/darDeBajaCant"
        ];
        $dates = $this->gstInventario->getDarBajaId($_GET['id']);
        return view('Modulos.Inventario.darDeBaja', compact('dates'));
        }
    }
     //autor: jasareth obando 

    public function darDeBajaCantProcess(Request $request)
    {

        if(!isset($_GET['id'])) 
        {
            $_SESSION[DATA][MSG] = ERROR_NO_ENCONTRADO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        if(!filter_var($_GET['id'],FILTER_VALIDATE_INT))
        {
            $_SESSION[DATA][MSG] = ERROR_NO_NUMERICO;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        $con = $this->gstInventario->getInventarioId($_GET['id']);
        if ($con == null)
        {
            $_SESSION[DATA][MSG] = ERROR_NO_BD;
            $_SESSION[DATA][TYPE] = NOTIFICACION_ERROR;
            return redirect(PATH_CTRL_LST);
        }
        else
        {
        $date = Inventario::find($_GET['id']);
        $total = $date[TN_CANTIDAD] - $_GET[TN_CANTIDAD];
        $movimiento= "se dio de baja";

        $usu = $this->gstUsuarios->getUsers(Auth::user()->id);
        
        if($total <= $date[TN_NIVEL_STOCK])
        {
            $data[TN_TITULO] = TITLE_BAJO_STOCK;
            $data[TN_CUERPO] = CUERPO_MSG;
            $data[TN_PRO_NOMBRE] = $date->sftProductos->pro_nombre;
            $data[TN_NIVEL_STOCK] = $date[TN_NIVEL_STOCK];
            $data[TN_CANT_ANTERIOR] = $date[TN_CANTIDAD];
            $data[TN_CAT_FINAL] = $total;
            $data['server'] = $_SERVER['SERVER_NAME'];
            $data[TN_INV_CONSTANTE] = [PRODUCTO_NOMBRE,CUERPO_NIVEL_STOCK,CUERPO_CANTIDAD_ANTERIOR,CUERPO_CANTIDAD_ACTUAl];
            FuncionesGenerales::correo('core.Correos.basic2',$data,Auth::user()->email,'',$data[TN_TITULO],'');
            
        }
        $this->gstInventario->darDeBajaInventario($_GET['id'],$total);
        
        $_SESSION[DATA][MSG] = BAJA_CORRECTAMENTE;
        $_SESSION[DATA][TYPE] = NOTIFICACION_SUCCESS;

        $this->gstInventario->saveHistoricoInventario($_GET['id'],$date[TN_CANTIDAD],$movimiento,$_GET[TN_CANTIDAD],$total);
        return redirect(PATH_CTRL_LST);
        }
    }
 /*
*Autor: Jasareth Obando
*Modulo: Inventario
*Versiòn: 1.0
*Descripciòn: funcion para listar el historial dentro del sistema
*/
       public function listarHistorial(){
        $_SESSION[DATA][CLIENTE_TITLE] = 'Listar Historial';
        $_SESSION[DATA][BREAD] = 
        [
            INICIO=>PATH_HOME,
            MENU_INV=>"/Menu/25",
            LST_INV=>"/Inventario/listarInventario",
            'Lista Historial'=>"/Inventario/listarHistorial"
        ];  
        $tableInv = new Table;
        $tableInv->setColum($tableInv,
            [
                ID=>'id',
                NOMBRE_PROD=>TN_PRO_NOMBRE,
                CUERPO_CANTIDAD_ACTUAl=>CANT_ACTUAL,
                MOVIMIENTO=>MOVIMIENTO,
                VALOR=>'valor',
                CANTI_ACTUAL=>CANT_NUEVA,
                FECHA_INV=>FECHA_INV
            ]
        );
        
        $data = $this->gstInventario->listarHistorialJoin($_GET['id'])->toArray();
        $tableInv->setItems($tableInv,$data);
       
        
        $tableInv = $tableInv->dps($tableInv);
         return view(PATH_LIST,[TABLE=>$tableInv]);
       }


}
