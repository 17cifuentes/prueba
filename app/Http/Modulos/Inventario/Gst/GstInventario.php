<?php

namespace App\Http\Modulos\Inventario\Gst;

use Illuminate\Http\Request;
use App\Http\Clases\FuncionesDB;
use App\Http\Clases\FuncionesGenerales;
use App\Inventario;
use App\Historial;


	/*
	*Autor: Cristian Cifuentes
	*Modulo: Roles
	*Versiòn: 1.0
	*Descripciòn: Clase de gestion para los Roles
	*/
class GstInventario
{
 /*Autor:jasareth obando
*/
    public function getRegisterInvent(){
        Inventario::insert($_GET);
    }
    //autor: jasareth obando
    public function abastecerInventario($id,$cantidad)
    {
      try
      {
         return Inventario::where("id",$id)->update(['cantidad'=>$cantidad]);
      }
      catch(\Exception $abastecer){
        error_log("Error: ".$abastecer->getMessage());

        return false;
      }
   }
   //autor: jasareth obando
   public function getInventarioId($id)
   {
   		return Inventario::find($id);  
   }
    public function getInvForAtribbute($columna,$valor){
      return Inventario::where($columna,$valor)->get();  
   }
   //autor: jasareth obando

   public function darDeBajaInventario($id,$cantidad)
   {
    try
      {
         return Inventario::where("id",$id)->update(['cantidad'=>$cantidad]);
      }
      catch(\Exception $darbaja){
        error_log("Error: ".$darbaja->getMessage());

        return false;
      }
   }
   //autor: jasareth obando

   public function getDarBajaId($id){

   		return Inventario::find($id);  

    }
    //autor:jasareth obando

    public function getInventarioJoin($id){

      return Inventario::select('tn_sft_mod_inventario.id','pro_nombre','cantidad','nivel_stock')->join('tn_sft_productos_servicios','tn_sft_productos_servicios.id','id_producto')->where('tn_sft_mod_inventario.cliente_id',$id)->get();
    }

    //autor:jasareth obando

    public function saveHistoricoInventario($id_inv,$cantidad,$movimiento,$valor,$total){
      
      return Historial::insert(['id_inv'=>$id_inv,'cantidad_actual'=>$cantidad,'movimiento'=>$movimiento,'valor'=>$valor,'cantidad_nueva'=>$total]);
    }

    //autor:jasareth obando

    public  function listarHistorialJoin($id){

    return Inventario::select('sft_inventario_historico.id as id','pro_nombre','cantidad_actual','movimiento','valor','cantidad_nueva','sft_inventario_historico.created_at as fecha')->join('tn_sft_productos_servicios','tn_sft_productos_servicios.id','id_producto')->join('sft_inventario_historico','sft_inventario_historico.id_inv','tn_sft_mod_inventario.id')->where('sft_inventario_historico.id_inv',$id)->distinct()->get();

    } 
    public function getInventarioByAtributo($atributo, $valor)
    {
      return Inventario::where($atributo,$valor)->get();  
    }
}


