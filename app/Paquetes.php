<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    protected $table = 'tn_sft_promociones_paquetes';
     public $fields = ['id','nombre','id_opcion','id_operacion','id_tipo_promocion','valor_operacion','valor_promocion'];

     public function sftEstadoOpcion()
     {
     	return $this->hasOne('App\Estado','id','id_opcion');
     }
     public function sftEstadoOperacion()
     {
     	return $this->hasOne('App\Estado','id','id_operacion');
     }
     public function sftEstadoPromocion()
     {
     	return $this->hasOne('App\Estado','id','id_tipo_promocion');
     }
}

