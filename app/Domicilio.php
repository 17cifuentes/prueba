<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class domicilio extends Model
{
	protected $table = 'sft_domicilio';
	public $fields = ['id','domi_usuario_id','cliente_id','domi_estado','cancela_motivo','referencia','domi_forma_pago','domi_valor','transaccionState','tipo_transaccion'];
    public function sftEstado(){
        return $this->hasOne("App\EstadosDomicilio","id","domi_estado");
    }
    public function sftUsuario(){
    	return $this->hasOne("App\User","id","domi_usuario_id");
    }
    public function sftCliente(){
    	return $this->hasOne("App\Cliente","id","cliente_id");
    }
    
}
