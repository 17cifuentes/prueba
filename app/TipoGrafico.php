<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoGrafico extends Model
{
    protected $table = 'tn_sft_tipos_grafico';
}
