<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estructuras extends Model
{
    protected $table = 'tn_sft_estructuras';
}