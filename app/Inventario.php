<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = 'tn_sft_mod_inventario';

    public function sftProductos()
    {
        return $this->hasOne('App\Productos','id','id_producto');
    }
}
