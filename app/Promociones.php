<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Promociones extends Model
{
    protected $table = 'tn_sft_promociones';
    public $fields = ['id','id_producto','mensaje','inicio','fin'];
    
    public function sftProducto(){
    	return $this->hasOne('App\Productos','id','id_producto');
    }
}
