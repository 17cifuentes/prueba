<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CambioEstado extends Model
{
    protected $table = "tn_sft_cambiar_estado";
}
