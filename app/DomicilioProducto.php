<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomicilioProducto extends Model
{
    protected $table = 'sft_domicilios_producto';

    public $fields = ['id','id_domi','id_pro','remision_producto','cantidad_producto','precio_unitario','subtotal_producto','estado_producto','cancela_motivo'];
    
    public function sftDomicilio(){
    	return $this->hasOne("App\domicilio","id","id_domi");
    }
    public function sftProducto(){
    	return $this->hasOne("App\Productos","id","id_pro");
    }
    public function sftRemision(){
    	return $this->hasOne("App\RemisionesProductos","id","remision_producto");
    }
}