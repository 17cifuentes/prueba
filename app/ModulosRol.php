<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulosRol extends Model
{
    protected $table = 'tn_sft_detalle_modulo_rol';

    public function modulos(){
    	return $this->hasOne('App\Modulos','id','id_modulo');
    }
}

