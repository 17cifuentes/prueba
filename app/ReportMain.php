<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportMain extends Model
{
    protected $table = 'tn_sft_reportes_main';
    public $fields = [];
}
