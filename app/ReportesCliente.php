<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportesCliente extends Model
{
    protected $table = 'sft_tn_report_cliente';
    public $fields = [];
    public function sftReporte(){
    	return $this->hasOne("App\ReportMain","id","id_report");
    }
}
