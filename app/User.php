<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        NAME, TN_USU_CORRE, TN_USU_CORRE,TN_USU_IMG,'password'
    ];
    public $fields = ['id','name','email','cliente_id','rol_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        TN_USU_PASSWORD, TN_USU_REMENBER_TOKEN,
    ];

    public function rol(){
        return $this->hasOne('App\Roles','id','rol_id');
    }

    public function cliente(){
        return $this->hasOne('App\Cliente','id','cliente_id');
    }
    public function sftRol(){
        return $this->hasOne('App\Roles','id','rol_id');
    }

    public function sftCliente(){
        return $this->hasOne('App\Cliente','id','cliente_id');
    }
}
