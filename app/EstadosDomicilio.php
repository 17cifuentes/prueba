<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosDomicilio extends Model
{
    protected $table = 'sft_estados_domicilio';
    public $fields = ['id','nombre_estado'];

}
