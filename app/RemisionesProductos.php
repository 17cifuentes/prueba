<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemisionesProductos extends Model
{
    protected $table = 'sft_remisiones_productos';
    public $fields = ['id','id_domi','id_cliente_producto','estado_remision'];
    public function sftDomicilio(){
    	return $this->hasOne("App\domicilio","id","id_domi");
    }
    public function sftCliente(){
    	return $this->hasOne("App\Cliente","id","id_cliente_producto");
    }
}
