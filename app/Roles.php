<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
	protected $table = TN_ROLES;
	public $fields = ['id','rol_nombre','rol_descripcion','estado_roles	','cliente_id'];
	protected $fillable = [
        'id', 'rol_nombre', 'rol_descripcion','cliente_id', 'created_at','updated_at'
    ];
    
}
