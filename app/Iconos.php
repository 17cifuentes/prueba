<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iconos extends Model
{
    protected $table = 'sft_iconos';
    public $fields = ['id','nombre','formato'];
}