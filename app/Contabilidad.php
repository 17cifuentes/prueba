<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contabilidad extends Model
{
    protected $table = 'tn_sft_contabilidad_cuentas';
    protected $fields = ['id','nombre','descripcion','codigo','tipo_estado'];

    public function sftEstado()
    {
    	return $this->hasOne('App\Estado','id','tipo_estado');
    }
}