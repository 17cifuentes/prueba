<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'tn_sft_estado';
    public $fields = ['id','nombre_estado','modulo','razon','valor'];
}