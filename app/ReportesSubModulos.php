<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportesSubModulos extends Model
{
    protected $table = 'reportes_submodulo';
    public $fields = ['id','id_padre','id_hijo','nombre_padre','created_at','created_at'];
}
