<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    protected $table = 'tn_sft_cursos';

    public $fields = ['id','nombre_curso','descripcion_curso','encargado','cliente_id','img_curso','created_at','updated_at'];

    public function sftClienteCurso()
    {
    	return $this->hasOne("App\Cliente","id","cliente_id");
    }
    public function sftUsuariosCurso()
    {
    	return $this->hasOne("App\user","id","encargado");	
    }
    
}
