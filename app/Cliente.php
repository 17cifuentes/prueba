<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = TN_CLIENTE;
    public $fields = ['id','cliente_nombre','cliente_descripcion','direccion','telefono','correo','estado_cliente'];

    public function sftClienteRoles(){
    	return $this->hasMany("Roles","id_cliente","id");
    }
}
