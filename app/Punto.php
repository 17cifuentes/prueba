<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Punto extends Model
{
    protected $table = 'tn_sft_mod_pos_punto';

    public $fields = ['id','pun_nombre','pun_tipo','pun_posicion','pun_descripcion','estado','cliente_id'];

    protected $fillable = ['id','pun_nombre','pun_tipo','pun_posicion','pun_descripcion','estado','cliente_id'];

    public function sftEstado(){

    	return $this->hasOne('App\Estado','id','estado');
    }
}
