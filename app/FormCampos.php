<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormCampos extends Model
{
    protected $table = 'tn_sft_detalle_form_campos'; 

    public function campos(){
    	return $this->hasone('App\Campos','id','id_campo');
    }
}
