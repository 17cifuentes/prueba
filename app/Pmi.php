<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pmi extends Model
{
    //
    protected $table = 'tn_sft_mod_pmi_proyectos';

    protected $fillable = ['id',
    						'pmi_nombre',
    						'pmi_descripcion',
    						'responsable',
    						'fec_ini',
    						'fec_fic',
    						'cliente',];

	public $fields = ['id',
					  'pmi_nombre',
					  'pmi_descripcion',
					  'responsable',
					  'fec_ini',
					  'fec_fic',
					  'cliente',];

	/*
	*Autor: Cristian Campo
	*Modulo: Pmi
	*Versiòn: 1.0
	*Descripciòn: Relación entre tablas Pmi y User.
	*/
	public function sftUser(){

    	return $this->hasOne('App\User','id','responsable');
    }

    
}
