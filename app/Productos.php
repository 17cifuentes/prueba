<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = "tn_sft_productos_servicios";
    public $fields = ['id','pro_nombre','pro_descripcion'];

    public function categoria(){
    	
    	return $this->hasOne('Categorias','id','id');
    }
    public function caracteristicas()
    {
    	return $this->hasMany('App\CaracteristicasProducto','id_pro','id');
    }

    public function sftPromociones()
    {
        return $this->hasOne('App\Promociones','id_producto','id');
    }
}

