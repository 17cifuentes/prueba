<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosDomicilioCliente extends Model
{
    protected $table = 'sft_estados_domi_clientes';
    public $fields = ['id','nombre_estado'];

}
