<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialContacto extends Model
{
    protected $table = 'sft_mod_historial_contacto';
    public $fields=['id','id_contacto','nombre','apellido','telefono','direccion','correo','resumen','estado_contacto','cliente_id','tiempo_de_contacto','responsable','created_at'];
}
